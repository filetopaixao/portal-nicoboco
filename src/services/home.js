import { api, config } from '@services/api';

export const getInfoHome = () => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve({
        status: [
          { icon: 'saldo', title: 'SALDO EVERCOINS', description: '0' },
          {
            icon: 'liberacao',
            title: 'AGUARDANDO LIBERAÇÃO',
            description: '0',
          },
          { icon: 'liberado', title: 'SALDO LIBERADO', description: '0' },
          { icon: 'faturado', title: 'TOTAL FATURADO', description: '0' },
        ],

        tools: [
          { description: 'E-COMMERCE', variant: 'info', isComing: false },
          { description: 'LANDING PAGE', variant: '', isComing: true },
          {
            description: 'CARRINHO DINÂMICO',
            variant: 'danger',
            isComing: false,
          },
          {
            description: 'CUPOM DE DESCONTO',
            variant: 'warning',
            isComing: false,
          },
        ],

        spaces: [
          { icon: 'bank', description: 'BANK', variant: '', isComing: true },
          {
            icon: 'shopping',
            description: 'SHOPPING',
            variant: '',
            isComing: true,
          },
          {
            icon: 'marketing',
            description: 'MARKETING',
            variant: 'marketing',
            isComing: false,
          },
          {
            icon: 'university',
            description: 'UNIVERSITY',
            variant: 'university',
            isComing: false,
          },
        ],
      });
    }, 900);
  });
};

export async function getHome() {
  try {
    const response = await api.get('/api/portal/home', config);
    return response;
  } catch (e) {
    console.log('erro na busca das informações', e);
    return {};
  }
}
