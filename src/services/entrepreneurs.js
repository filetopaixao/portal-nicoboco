import { api } from '@services/api';

export async function getInfo() {
  try {
    const response = await api.get('/api/portal/associations/managers');
    return response;
  } catch (e) {
    console.log('erro na listagem', e);
    return {};
  }
}

export async function getById(id) {
  try {
    if (id) {
      const response = await api.get(`/api/portal/associations/managers${id}`);
      return response;
    }
    return null;
  } catch (e) {
    console.log('erro na busca pelo produto', e);
    return {};
  }
}
