import { api } from '@services/api';

export const getCars = async () => {
  try {
    const response = await api.get('/api/portal/checkouts');
    return response;
  } catch (e) {
    console.log('erro na listagem', e);
    return {};
  }
};

export const addCar = async (data) => {
  try {
    const response = await api.post('/api/portal/checkouts', data);
    return response;
  } catch (e) {
    console.log('erro no cadastro', e);
    return {};
  }
};

export const updateCar = async (id, data) => {
  try {
    const response = await api.put(`/api/portal/checkouts/${id}`, data);
    return response;
  } catch (e) {
    console.log('erro na atualização docarrinho dinâmico', e);
    return {};
  }
};

export const deleteCar = async (id) => {
  try {
    const response = await api.delete(`/api/portal/checkouts/${id}`);
    return response;
  } catch (e) {
    console.log('erro ao deletar carrinho dinâmico', e);
    return {};
  }
};

export const showCar = async (id) => {
  try {
    const response = await api.get(`/api/portal/checkouts/${id}`);
    return response;
  } catch (e) {
    console.log('erro ao buscar carrinho dinâmico', e);
    return {};
  }
};
