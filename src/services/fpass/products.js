import { api } from './api';

const getAll = async () => {
  try {
    const response = await api.get('/products');
    return response;
  } catch (error) {
    return error.response;
  }
};

export default getAll;
