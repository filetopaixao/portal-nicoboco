import { api, config } from '@services/api';

const getInfo = async () => {
  try {
    const response = await api.get('/api/portal/reports', config);
    return response;
  } catch (e) {
    console.log('erro na listagem', e);
    return {};
  }
};

export default getInfo;
