import { api } from '@services/api';

export const getList = async () => {
  try {
    const response = await api.get('/api/portal/coupons');
    return response;
  } catch (e) {
    console.log('erro na listagem', e);
    return {};
  }
};

export const addCoupon = async (data) => {
  try {
    const response = await api.post('/api/portal/coupons', data);
    return response;
  } catch (e) {
    console.log(e);
    return {};
  }
};

export const updateCoupon = async (id, data) => {
  try {
    const response = await api.put(`/api/portal/coupons/${id}`, data);
    return response;
  } catch (e) {
    console.log('erro na atualização do cupom', e);
    return {};
  }
};

export const deleteCoupon = async (id) => {
  try {
    const response = await api.delete(`/api/portal/coupons/${id}`);
    return response;
  } catch (e) {
    console.log('erro na atualização do cupom', e);
    return {};
  }
};
