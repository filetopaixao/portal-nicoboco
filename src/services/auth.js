import axios from 'axios';
import { api } from './api';

export async function authentication(credentials) {
  try {
    const response = await axios.post('http://3.131.4.158/api/auth/sign-in', credentials);
    localStorage.setItem('token', response.data.token);
    console.log(response.data);
  } catch (e) {
    console.log(e);
  }
}

export async function forgotPassword(email) {
  try {
    const response = await api.post('/api/auth/forgot-password', email);
    console.log(response.data);
  } catch (e) {
    console.log(e);
  }
}

export async function UpdatePassword(tokenAndPassword) {
  try {
    const response = await api.put('/api/auth/forgot-password', tokenAndPassword);
    console.log(response.data);
  } catch (e) {
    console.log(e);
  }
}

export function isAuthenticated() {
  return localStorage.getItem('token');
}

export function logout() {
  localStorage.clear();
  window.location.reload();
}
