import axios from 'axios';

// require('dotenv').config();
// require('dotenv/config');

export const api = axios.create({
  // baseURL: process.env.BASE_URL_API,
  baseURL: `http://3.131.4.158`,
  timeout: 0,
  headers: {
    'Content-Type': 'application/json',
    Authorization: localStorage.getItem('token') ? `bearer ${localStorage.getItem('token')}` : '',
  },
});

export const config = {
  headers: {
    // Authorization: localStorage.getItem('token') ? `bearer ${localStorage.getItem('token')}` : '',
  },
  // params: {
  //   Authorization: localStorage.getItem('token') ? `bearer ${localStorage.getItem('token')}` : '',
  // },
};

export default api;
