import { createGlobalStyle } from 'styled-components';

const GlobalStyles = createGlobalStyle`
  * {
    margin: 0;
    padding: 0;

    outline: 0;

    box-sizing: border-box;

    &::after,
    &::before {
      content: '';
      clear: both;
      display: none;
      visibility: hidden;
    }
  }

  html, body {
    height: 100%;
    width: 100%;
    overflow-x: hidden;
  }

  body {
    font-family: 'Open Sans', sans-serif !important;
    text-rendering: optimizeLegibility !important;
    -webkit-font-smoothing: antialiased !important;

    background-color: #f5f6fa;
  }

  img {
    display: block;
    height: 100%;
    width: 100%;
  }

  .full-width {
    width: 100;
  }

  .mt20 {
    margin-top: 20px;
  }

  .text-upp {
    text-transform: uppercase;
  }
`;

export default GlobalStyles;
