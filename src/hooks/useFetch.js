import useSWR from 'swr';
import axios from 'axios';

const api = axios.create({
  baseURL: 'http://localhost:3333',
});

export default function useFetch(url) {
  const { data, error } = useSWR(url, async () => {
    const response = await api.get(url);
    return response.data;
  });

  return { data, error };
}
