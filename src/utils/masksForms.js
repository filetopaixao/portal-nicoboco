export const cpfMask = (value) => {
  return value
    .replace(/\D/g, '') // substitui qualquer caracter que nao seja numero por nada
    .replace(/(\d{3})(\d)/, '$1.$2') // captura 2 grupos de numero o primeiro de 3 e o segundo de 1, apos capturar o primeiro grupo ele adiciona um ponto antes do segundo grupo de numero
    .replace(/(\d{3})(\d)/, '$1.$2')
    .replace(/(\d{3})(\d{1,2})/, '$1-$2')
    .replace(/(-\d{2})\d+?$/, '$1') // captura 2 numeros seguidos de um traço e não deixa ser digitado mais nada
    .substring(0, 19);
};

export const formatReal = (v) => {
  const real = v
    .replace(/\D/g, '') // permite digitar apenas numero
    .replace(/(\d{1})(\d{17})$/, '$1.$2')
    .replace(/(\d{1})(\d{14})$/, '$1.$2') // coloca ponto antes dos ultimos digitos
    .replace(/(\d{1})(\d{11})$/, '$1.$2') // coloca ponto antes dos ultimos 13 digitos
    .replace(/(\d{1})(\d{8})$/, '$1.$2') // coloca ponto antes dos ultimos 10 digitos
    .replace(/(\d{1})(\d{5})$/, '$1.$2') // coloca ponto antes dos ultimos 7 digitos
    .replace(/(\d{1})(\d{1,2})$/, '$1,$2'); // coloca virgula antes dos ultimos 4 digitos

  return `R$ ${real}`;
};

export const removeMask = (number) => {
  return number.replace(/\D/g, '');
};

export const numberCredit = (value) => {
  return value
    .replace(/\D/g, '') // Permite apenas dígitos
    .replace(/(\d{4})/g, '$1.') // Coloca um ponto a cada 4 caracteres
    .replace(/\.$/, '') // Remove o ponto se estiver sobrando
    .substring(0, 19); // Limita o tamanho
};

export const numberCVV = (value) => {
  return value
    .replace(/\D/g, '') // Permite apenas dígitos
    .substring(0, 4); // Limita o tamanho
};

export const emailMask = (email) => {
  const espress = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/gi;
  // const espress2 = /^[a-z0-9.]+@[a-z0-9]+\.[a-z]+(\.[a-z]+)?$/i;
  const reponse = email.replace(espress);
  console.log(reponse);
  return email;
};

export const dateMask = (date) => {
  return date
    .replace(/\D/g, '')
    .replace(/^(\d{2})(\d)/g, '$1/$2')
    .replace(/(\d)(\d{4})$/, '$1/$2');
};

export const cepMask = (number) => {
  return number.replace(/\D/g, '').replace(/(\d)(\d{3})$/, '$1-$2');
};

export const phoneMask = (number) => {
  return number
    .replace(/\D/g, '') // Remove tudo o que não é dígito
    .replace(/^(\d{2})(\d)/g, '($1) $2') // Coloca parênteses em volta dos dois primeiros dígitos
    .replace(/(\d)(\d{4})$/, '$1-$2') // Coloca hífen entre o quarto e o quinto dígitos
    .substring(0, 15); // Limita o tamanho
};
