import Avatar from '@assets/images/profile-1.png';

export const getMyManager = {
  image: Avatar,
  name: 'Bruno Alves',
  work: 'gerente fpass',
  email: 'bruno.gerente@fpass.com.br',
  telephony: '(63) 99661-0000',
  phone: '(63) 98160-0000',
};

export default getMyManager;
