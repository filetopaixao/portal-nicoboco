export const getListMyClients = {
  myClients: [
    {
      id: 1,
      name: 'Garcia Lopes',
      email: 'garcia.lopes@gmail.com',
      phone: '(63) 9 9999-0000',
      city: 'São Paulo/SP',
    },
    {
      id: 2,
      name: 'Claudia Gonçalves',
      email: 'garcia.lopes@gmail.com',
      phone: '(63) 9 9999-0000',
      city: 'São Paulo/SP',
      permitted: true,
    },
    {
      id: 3,
      name: 'José Maria Teixeira',
      email: 'garcia.lopes@gmail.com',
      phone: '(63) 9 9999-0000',
      city: 'São Paulo/SP',
      permitted: true,
    },
    {
      id: 4,
      name: 'Carlos de Araújo',
      email: 'garcia.lopes@gmail.com',
      phone: '(63) 9 9999-0000',
      city: 'São Paulo/SP',
      permitted: true,
    },
    {
      id: 5,
      name: 'Gisele Almeida Leite',
      email: 'garcia.lopes@gmail.com',
      phone: '(63) 9 9999-0000',
      city: 'São Paulo/SP',
      permitted: true,
    },
    {
      id: 6,
      name: 'Ana Maria Sousa',
      email: 'garcia.lopes@gmail.com',
      phone: '(63) 9 9999-0000',
      city: 'São Paulo/SP',
      permitted: true,
    },
    {
      id: 7,
      name: 'Gisele Almeida Leite',
      email: 'garcia.lopes@gmail.com',
      phone: '(63) 9 9999-0000',
      city: 'São Paulo/SP',
      permitted: true,
    },
    {
      id: 8,
      name: 'Ana Maria Sousa',
      email: 'garcia.lopes@gmail.com',
      phone: '(63) 9 9999-0000',
      city: 'São Paulo/SP',
      permitted: true,
    },
  ],
  pages: [
    { number: 1, selected: true },
    { number: 2, selected: false },
    { number: 3, selected: false },
  ],
};

export default getListMyClients;
