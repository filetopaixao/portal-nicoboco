import { takeEvery, put, spawn } from 'redux-saga/effects';

import { Types as DucksTypes } from '@redux/ducks/myClients';

import { getListMyClients } from '@utils/mock/myClients';

function* getMyClients() {
  const response = yield getListMyClients;
  yield put({ type: 'POST_MY_CLIENTS', payload: { ...response } });
}

function* watchGetMyClients() {
  yield takeEvery(DucksTypes.GET_MY_CLIENTS, getMyClients);
}

export default function* myClients() {
  yield spawn(watchGetMyClients);
}
