import { takeEvery, put, spawn } from 'redux-saga/effects';

import { Types as DucksTypes } from '@redux/ducks/withdraw';

import { getListWithdraw } from '@utils/mock/withdraw';

function* getWithdraw() {
  const response = yield getListWithdraw;
  yield put({ type: 'POST_WITHDRAW', payload: { ...response } });
}

function* watchGetWithdraw() {
  yield takeEvery(DucksTypes.GET_WITHDRAW, getWithdraw);
}

export default function* withdraw() {
  yield spawn(watchGetWithdraw);
}
