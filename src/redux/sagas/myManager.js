import { takeEvery, put, spawn } from 'redux-saga/effects';

import { Types as DucksTypes } from '@redux/ducks/myManager';

import { getMyManager } from '@utils/mock/myManager';

function* getManager() {
  const response = yield getMyManager;
  yield put({ type: 'POST_MY_MANAGER', payload: { ...response } });
}

function* watchGetManager() {
  yield takeEvery(DucksTypes.GET_MY_MANAGER, getManager);
}

export default function* withdraw() {
  yield spawn(watchGetManager);
}
