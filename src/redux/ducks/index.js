import { combineReducers } from 'redux';
import menu from './menu';
import home from './home';
import poupup from './poupup';
import withdraw from './withdraw';
import myManager from './myManager';
import products from './products';
import coupons from './coupon';
import dinamycCar from './dinamycCar';
import theme from './theme';
import login from './login';

export default combineReducers({
  menu,
  home,
  poupup,
  withdraw,
  myManager,
  products,
  coupons,
  dinamycCar,
  login,
  theme,
});
