export const Types = {
  GET_COUPONS: 'GET_COUPONS',
  ADDED_COUPON: 'ADDED_COUPON',
  EDIT_COUPON_EXISTS: 'EDIT_COUPON_EXISTS',
  GET_COD_COUPON: 'GET_COD_COUPON',
  SET_COD_COUPON: 'SET_COD_COUPON',
  RESET_COD_COUPON: 'RESET_COD_COUPON',
  FIND_COUPON: 'FIND_COUPON',
  REMOVE_COUPON_SELECTED: 'REMOVE_COUPON_SELECTED',
  SUBMIT_SEND_FORM: 'SUBMIT_SEND_FORM',
  GET_SEND_FORM: 'GET_SEND_FORM',
  GET_COUPON: 'GET_COUPON',
  RESET_COUPON: 'RESET_COUPON',
  LOAD_COUPON_ADDED: 'LOAD_COUPON_ADDED',
  DELETE_COUPON: 'DELETE_COUPON',
  EDIT_COUPON: 'EDIT_COUPON',
};

export const Actions = {
  getCoupons: () => ({
    type: Types.GET_COUPONS,
    payload: {},
  }),
  addCoupon: (coupon) => ({
    type: Types.ADDED_COUPON,
    coupon,
    payload: {},
  }),
  editCoupon: (coupon) => ({
    type: Types.EDIT_COUPON_EXISTS,
    coupon,
    payload: {},
  }),
  getCodCoupon: () => ({
    type: Types.GET_COD_COUPON,
    payload: {},
  }),
  setCodCoupon: (codCoupon) => ({
    type: Types.SET_COD_COUPON,
    codCoupon,
    payload: {},
  }),
  resetCodCoupon: () => ({
    type: Types.RESET_COD_COUPON,
    payload: {},
  }),
  findCoupon: (coupon) => ({
    type: Types.FIND_COUPON,
    coupon,
    payload: {},
  }),
  removeCouponSelected: (id) => ({
    type: Types.REMOVE_COUPON_SELECTED,
    id,
    payload: {},
  }),
  handleSubmitSendForm: (valuesSend) => ({
    type: Types.SUBMIT_SEND_FORM,
    valuesSend,
    payload: {},
  }),
  getSendForm: () => ({
    type: Types.GET_SEND_FORM,
    payload: {},
  }),
  loadCouponAdded: (coupon) => ({
    type: Types.LOAD_COUPON_ADDED,
    payload: {},
    coupon,
  }),
  getCoupon: () => ({
    type: Types.GET_COUPON,
    payload: {},
  }),
  resetCoupon: () => ({
    type: Types.RESET_COUPON,
    payload: {},
  }),
  deletedCoupon: (coupon) => ({
    type: Types.DELETE_COUPON,
    coupon,
    payload: {},
  }),
  editedCoupon: (coupon) => ({
    type: Types.EDIT_COUPON,
    coupon,
    payload: {},
  }),
};

export const Selectors = {};

const INITIAL_STATE = {
  couponsAdded: [],
  couponAdded: {},
  couponDeleted: {},
  couponEdited: {},
  couponsExist: [],
  setCodCoupon: '',
};

export default function coupons(state = INITIAL_STATE, action) {
  switch (action.type) {
    case Types.DELETE_COUPON:
      return {
        ...state,
        couponDeleted: action.coupon,
      };
    case Types.EDIT_COUPON:
      console.log('dentro do redux');
      return {
        ...state,
        couponEdited: action.coupon,
      };
    case Types.LOAD_COUPON_ADDED:
      return {
        ...state,
        couponAdded: action.coupon,
      };
    case Types.GET_COUPON:
      return state;
    case Types.RESET_COUPON:
      return {
        ...state,
        couponAdded: {},
        couponDeleted: {},
        couponEdited: {},
      };
    case Types.GET_COUPONS:
      return state;
    case Types.ADDED_COUPON:
      return {
        ...state,
        setCodCoupon: '',
        couponsAdded: [...state.couponsAdded, action.coupon],
      };
    case Types.EDIT_COUPON_EXISTS:
      return {
        ...state,
        setCodCoupon: '',
        couponsAdded: state.couponsAdded.map((coupon) => {
          if (action.id === coupon.id) {
            return {
              ...coupon,
              name: action.codigoCouponEdit,
              codCoupon: action.codigoCouponEdit,
              dateIni: action.dateInitialEdit,
              dateFim: action.dateFinalEdit,
              qtdPurchase: action.qtdPurchaseEdit,
              percent: action.discountValueEdit,
              discount: `Desconto ${action.discountValueEdit}%`,
              limit: action.qtdPurchaseEdit,
            };
          }
          return coupon;
        }),
      };
    case Types.GET_COD_COUPON:
      return {
        ...state,
      };
    case Types.SET_COD_COUPON:
      return {
        ...state,
        setCodCoupon: action.codCoupon,
      };
    case Types.RESET_COD_COUPON:
      return {
        ...state,
        setCodCoupon: '',
      };
    case Types.FIND_COUPON:
      return {
        ...state,
        couponsExist: state.couponsAdded.map((coupon) => action.coupon.id === coupon.id && coupon),
      };
    case Types.SUBMIT_SEND_FORM:
      return {
        ...state,
        valuesSendForm: action.valuesSend,
      };
    case Types.REMOVE_PRODUCT:
      return {
        ...state,
        couponsAdded: state.couponsAdded.filter((coupon) => coupon.id !== action.id),
      };
    default:
      return state;
  }
}
