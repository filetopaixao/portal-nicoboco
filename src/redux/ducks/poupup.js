export const Types = {
  VISIBLE_SUCCESS: 'VISIBLE_SUCCESS',
  NOT_VISIBLE_SUCCESS: 'NOT_VISIBLE_SUCCESS',
  VISIBLE_EXTEND_PERIOD: 'VISIBLE_EXTEND_PERIOD',
  NOT_VISIBLE_EXTEND_PERIOD: 'NOT_VISIBLE_EXTEND_PERIOD',
};

export const Actions = {
  visibleSuccess: (info) => ({
    type: Types.VISIBLE_SUCCESS,
    info,
    payload: {},
  }),
  notVisibleSuccess: () => ({
    type: Types.NOT_VISIBLE_SUCCESS,
  }),
  visibleExtendPeriod: (info) => ({
    type: Types.VISIBLE_EXTEND_PERIOD,
    info,
    payload: {},
  }),
  notVisibleExtendPeriod: () => ({
    type: Types.NOT_VISIBLE_EXTEND_PERIOD,
  }),
};

const INITIAL_STATE = {
  success: {
    title: '',
    description: '',
    visible: false,
  },
  extendPeriod: {
    image: '',
    title: '',
    description: '',
    descriptionMobile: '',
    visible: false,
  },
};

export default function poupup(state = INITIAL_STATE, action) {
  switch (action.type) {
    case Types.VISIBLE_SUCCESS:
      return {
        ...state,
        success: action.info,
      };
    case Types.NOT_VISIBLE_SUCCESS:
      return {
        ...state,
        success: {
          title: '',
          description: '',
          visible: false,
        },
      };
    case Types.VISIBLE_EXTEND_PERIOD:
      return {
        ...state,
        extendPeriod: action.info,
      };
    case Types.NOT_VISIBLE_EXTEND_PERIOD:
      return {
        ...state,
        extendPeriod: {
          image: '',
          title: '',
          description: '',
          descriptionMobile: '',
          visible: false,
        },
      };
    default:
      return state;
  }
}
