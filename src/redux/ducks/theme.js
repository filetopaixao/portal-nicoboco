export const Types = {
  GET_THEME: 'GET_THEME',
  POST_THEME: 'POST_THEME',
};

export const Actions = {
  getTheme: () => ({
    type: Types.GET_THEME,
    payload: {},
  }),
  postTheme: (themeName, themeColor, primaryColor, secondaryColor, tertiaryColor, logo, capa) => ({
    type: Types.POST_THEME,
    themeName,
    themeColor,
    primaryColor,
    secondaryColor,
    tertiaryColor,
    logo,
    capa,
    payload: {},
  }),
};

export const Selectors = {};

const INITIAL_STATE = {
  themeName: 'Light',
  themeColor: '#F5F6FA',
  primaryColor: '#000000',
  secondaryColor: '#41B2FD',
  tertiaryColor: '',
  logo: '',
  capa: '',
};

export default function theme(state = INITIAL_STATE, action) {
  switch (action.type) {
    case Types.GET_THEME:
      return state;
    case Types.POST_THEME:
      return {
        ...state,
        themeName: action.themeName,
        themeColor: action.themeColor,
        primaryColor: action.primaryColor,
        secondaryColor: action.secondaryColor,
        tertiaryColor: action.tertiaryColor,
        logo: action.logo,
        capa: action.capa,
      };
    default:
      return state;
  }
}
