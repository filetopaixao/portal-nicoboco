export const Types = {
  GET_MY_MANAGER: 'GET_MY_MANAGER',
  POST_MY_MANAGER: 'POST_MY_MANAGER',
};

export const Actions = {
  getMyManager: () => ({
    type: Types.GET_MY_MANAGER,
    payload: {},
  }),
};

export const Selectors = {};

const INITIAL_STATE = {
  manager: {},
};

export default function myManager(state = INITIAL_STATE, action) {
  switch (action.type) {
    case Types.GET_MY_MANAGER:
      return state;

    case Types.POST_MY_MANAGER:
      return {
        ...state,
        manager: action.payload,
      };

    default:
      return state;
  }
}
