import React, { useState, useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';
import html2canvas from 'html2canvas';

import Modal from '@components/molecules/Modals';
import CollapseOut from '@components/atoms/Collapse/CollapseOutWithdraw';
import PersonalData from '@pages/MyAccount/PersonalData';
import Contact from '@pages/MyAccount/Contact';
import Address from '@pages/MyAccount/Address';
import BankData from '@pages/MyAccount/BankData';
import ModalChangePass from '@components/molecules/Modals/ChangePass';
import ChangePassForm from '@components/molecules/Forms/MyAccount/ChangePassword';

import { getUserSocials, getUser } from '@services/user';

import Logo from '@assets/images/logo_ever_white.png';

import * as S from './styled';

const ModalComponentProduct = ({ isModalVisible, setIsModalVisible, opContent }) => {
  const [activeBtn1, setActiveBtn1] = useState(true);
  const [activeBtn2, setActiveBtn2] = useState(false);
  const [activeBtn3, setActiveBtn3] = useState(false);
  const [activeBtn4, setActiveBtn4] = useState(false);

  const handleContent1 = useCallback(() => {
    setActiveBtn1(true);
    setActiveBtn2(false);
    setActiveBtn3(false);
    setActiveBtn4(false);
  }, []);
  const handleContent2 = useCallback(() => {
    setActiveBtn1(false);
    setActiveBtn2(true);
    setActiveBtn3(false);
    setActiveBtn4(false);
  }, []);
  const handleContent3 = useCallback(() => {
    setActiveBtn1(false);
    setActiveBtn2(false);
    setActiveBtn3(true);
    setActiveBtn4(false);
  }, []);
  const handleContent4 = useCallback(() => {
    setActiveBtn1(false);
    setActiveBtn2(false);
    setActiveBtn3(false);
    setActiveBtn4(true);
  }, []);

  const loadContent = useCallback(() => {
    switch (opContent) {
      case 'content1':
        return handleContent1();
      case 'content2':
        return handleContent2();
      case 'content3':
        return handleContent3();
      case 'content4':
        return handleContent4();
      default:
        return handleContent1();
    }
  }, [opContent, handleContent1, handleContent2, handleContent3, handleContent4]);

  useEffect(() => {
    loadContent();
  }, [loadContent]);

  const clickButton = (e) => {
    if (e.target.id === 'btn1') {
      if (activeBtn1) {
        setActiveBtn1(false);
      } else {
        handleContent1();
      }
    } else if (e.target.id === 'btn2') {
      if (activeBtn2) {
        setActiveBtn2(false);
      } else {
        handleContent2();
      }
    } else if (e.target.id === 'btn3') {
      if (activeBtn3) {
        setActiveBtn3(false);
      } else {
        handleContent3();
      }
    } else if (e.target.id === 'btn4') {
      if (activeBtn4) {
        setActiveBtn4(false);
      } else {
        handleContent4();
      }
    }
  };

  const [isModalVisibleBC, setIsModalVisibleBC] = useState(false);
  const [isModalVisibleCP, setIsModalVisibleCP] = useState(false);
  const [userData, setUserData] = useState({});
  const [userSocials, setUserSocials] = useState({});

  useEffect(() => {
    getUser()
      .then((response) => {
        const userDataResponse = response.data;
        setUserData({
          id: userDataResponse.id,
          type_id: userDataResponse.type_id,
          birthdate: userDataResponse.birthdate,
          cpf_cnpj: userDataResponse.cpf_cnpj,
          email: userDataResponse.email,
          full_name: `${userDataResponse.first_name} ${userDataResponse.last_name}`,
          first_name: userDataResponse.first_name,
          last_name: userDataResponse.last_name,
          gender: userDataResponse.gender,
          photo: userDataResponse.image.url,
          legal_person: userDataResponse.legal_person,
          phone: userDataResponse.phone,
          office: 'Empreendedora EVER',
        });
      })
      .catch(() => {
        setUserData({
          id: Math.random() * 10,
          type_id: '2',
          birthdate: '2000-10-03',
          cpf_cnpj: '000.000.000-00',
          email: 'mail@mail.com',
          full_name: `First-name Last-name`,
          first_name: 'First-name',
          last_name: 'Last-Name',
          gender: 'gender',
          photo: Logo,
          legal_person: 'Person',
          phone: '(00) 0 0000-0000',
          office: 'Empreendedora EVER',
        });
      });
  }, []);

  useEffect(() => {
    getUserSocials()
      .then((response) => {
        const userSocialsResponse = response.data;

        if (userSocialsResponse.store) {
          setUserSocials(userSocialsResponse);
        } else {
          setUserSocials({
            id: userSocialsResponse.id,
            whatsapp: '(63) 9 9966-0001',
            instagram: userSocialsResponse.instagram,
            facebook: userSocialsResponse.facebook,
            youtube: userSocialsResponse.youtube,
            store: 'https://loja.everbynature.com.br?me=1',
          });
        }
      })
      .catch(() => {
        setUserData({
          id: Math.random(),
          whatsapp: '(00) 0 0000-0000',
          instagram: 'user_insta',
          facebook: 'user_facebook',
          youtube: 'user_youtube',
          store: 'https://loja.everbynature.com.br?me=1',
        });
      });
  }, []);

  const handlePrint = useCallback(() => {
    const div = document.querySelector('#virtualCard');
    html2canvas(div).then((canvas) => {
      const img = canvas.toDataURL('image/jpeg', 1.0).replace('image/jpeg', 'image/octet-stream');
      const link = document.createElement('a');
      const d = new Date();
      const date = d.toLocaleDateString();
      const hours = `${d.getHours()}:${d.getMinutes()}`;
      link.download = `screenshot-${date} ${hours}.jpeg`;
      link.href = img;
      link.click();
    });
  }, []);

  return (
    <S.ModalContainer>
      <Modal title="" isVisible={isModalVisible} onClose={() => setIsModalVisible(!isModalVisible)}>
        <S.ModelTitle>Minha Conta</S.ModelTitle>
        <S.Container>
          <S.CardContainer>
            <div>
              <S.Avatar photo={userData.photo}>
                <S.LbUploadPhoto htmlFor="input-file" />
                <S.UploadPhoto type="file" id="input-file" />
              </S.Avatar>
              <S.PersonName>
                <S.H3>{userData.full_name}</S.H3>
              </S.PersonName>
              <S.Office>
                <p>{userData.office}</p>
              </S.Office>
              <S.ButtonAccount
                className="password"
                variant="secondary"
                onClick={() => setIsModalVisibleCP(!isModalVisibleCP)}
              >
                Alterar Senha
              </S.ButtonAccount>
            </div>
            <S.ButtonAccount
              className="change-card"
              variant="dark"
              onClick={() => setIsModalVisibleBC(!isModalVisibleBC)}
            >
              Gerar Cartão Visual
            </S.ButtonAccount>

            <ModalChangePass
              variant="md"
              id="business_card"
              title="Alterar senha"
              isVisible={isModalVisibleCP}
              onClose={() => setIsModalVisibleCP(!isModalVisibleCP)}
            >
              <S.ContainerChangePass>
                <ChangePassForm closeModal={() => setIsModalVisibleCP(!isModalVisibleCP)} />
              </S.ContainerChangePass>
            </ModalChangePass>

            <Modal
              variant="sm"
              id="change_pass"
              title="Cartão Virtual"
              isVisible={isModalVisibleBC}
              onClose={() => setIsModalVisibleBC(!isModalVisibleBC)}
            >
              <S.ContainerModal>
                <S.P>Cartão gerado automaticamente:</S.P>
              </S.ContainerModal>
              <S.ContainerBusinessCard>
                <S.BusinessCard id="virtualCard">
                  <S.ContainerModal>
                    <S.LogoStyle src={Logo} />
                  </S.ContainerModal>
                  <S.ContainerModal>
                    <S.ContentModal>
                      <S.ContentBC>
                        <S.AvatarBC photo={userData.photo} />
                      </S.ContentBC>
                      <S.ContentBC>
                        <S.H5>{userData.full_name}</S.H5>
                      </S.ContentBC>
                      <S.ContentBC>
                        <S.P>{userData.office}</S.P>
                      </S.ContentBC>
                    </S.ContentModal>
                  </S.ContainerModal>
                  <S.ContainerModal>
                    <S.ContentModal className="userSocials">
                      <S.ContactBC>
                        <S.Whatsapp>{userSocials.whatsapp}</S.Whatsapp>
                      </S.ContactBC>
                      <S.ContactBC>
                        <S.Instagram>{userSocials.instagram}</S.Instagram>
                      </S.ContactBC>
                      <S.ContactBC>
                        <S.Facebook>{userSocials.facebook}</S.Facebook>
                      </S.ContactBC>
                      <S.ContactBC>
                        <S.Youtube>{userSocials.youtube}</S.Youtube>
                      </S.ContactBC>
                      <S.Link>
                        <S.P>{userSocials.store}</S.P>
                      </S.Link>
                    </S.ContentModal>
                  </S.ContainerModal>
                </S.BusinessCard>
              </S.ContainerBusinessCard>
              <S.ContainerCenter>
                <S.ButtonAccount variant="primary" onClick={() => handlePrint()}>
                  baixar cartão
                </S.ButtonAccount>
              </S.ContainerCenter>
            </Modal>
          </S.CardContainer>

          <div>
            <S.GridCollapse>
              <CollapseOut
                backgroundColor="collapsesWithdraw"
                active={activeBtn1}
                title="Dados Pessoais"
                onClick={clickButton}
                id="btn1"
              >
                <PersonalData usarCard={false} />
              </CollapseOut>
              <CollapseOut
                backgroundColor="collapsesWithdraw"
                active={activeBtn2}
                title="Contatos"
                onClick={clickButton}
                id="btn2"
              >
                <Contact />
              </CollapseOut>
              <CollapseOut
                backgroundColor="collapsesWithdraw"
                active={activeBtn3}
                title="Endereço"
                onClick={clickButton}
                id="btn3"
              >
                <Address />
              </CollapseOut>
              <CollapseOut
                backgroundColor="collapsesWithdraw"
                active={activeBtn4}
                title="Dados Bancários"
                onClick={clickButton}
                id="btn4"
              >
                <BankData />
              </CollapseOut>
            </S.GridCollapse>
            <S.GridButtons qtd={4}>
              <S.Column>
                <S.Button className="btnGrid" onClick={clickButton} id="btn1" active={activeBtn1}>
                  Dados Pessoais
                </S.Button>
              </S.Column>
              <S.Column>
                <S.Button className="btnGrid" onClick={clickButton} id="btn2" active={activeBtn2}>
                  Contatos
                </S.Button>
              </S.Column>
              <S.Column>
                <S.Button className="btnGrid" onClick={clickButton} id="btn3" active={activeBtn3}>
                  Endereço
                </S.Button>
              </S.Column>
              <S.Column>
                <S.Button className="btnGrid" onClick={clickButton} id="btn4" active={activeBtn4}>
                  Dados Bancários
                </S.Button>
              </S.Column>
            </S.GridButtons>
            {activeBtn1 ? (
              <S.Content>
                <PersonalData usarCard={false} />
              </S.Content>
            ) : null}

            {activeBtn2 ? (
              <S.Content>
                <Contact />
              </S.Content>
            ) : null}

            {activeBtn3 ? (
              <S.Content>
                <Address />
              </S.Content>
            ) : null}

            {activeBtn4 ? (
              <S.Content>
                <BankData />
              </S.Content>
            ) : null}
          </div>
        </S.Container>
      </Modal>
    </S.ModalContainer>
  );
};

ModalComponentProduct.propTypes = {
  isModalVisible: PropTypes.bool,
  setIsModalVisible: PropTypes.func,
  opContent: PropTypes.string,
};
ModalComponentProduct.defaultProps = {
  isModalVisible: false,
  setIsModalVisible: () => {},
  opContent: 'content1',
};

export default ModalComponentProduct;
