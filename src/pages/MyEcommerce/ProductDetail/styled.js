import styled from 'styled-components';

export const ModalContainer = styled.div`
  @media (max-width: 600px) {
    overflow-y: scroll;
  }
`;

export const ModalContent = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  column-gap: 20px;
  padding: 20px;

  @media (max-width: 1000px) {
    display: block;
  }

  button {
    font-size: 17px;
    letter-spacing: 0px;
    text-transform: uppercase;

    width: 100%;
    height: 53px;

    padding: 0;
    margin-top: 20px;

    box-shadow: 0px 3px 6px #00000029;
    border: none;
    border-radius: 15px;
    opacity: 1;
    cursor: pointer;

    color: #ffffff;
    background: ${(props) => props.secondaryColor};

    &:focus {
      outline: thin dotted;
      outline: 0px auto -webkit-focus-ring-color;
      outline-offset: 0px;
    }

    @media (max-width: 800px) {
      font-size: 14px;
    }
  }
`;

export const ModalContainerProductImage = styled.div`
  div#items {
    display: flex;
    overflow-x: auto;

    scroll-snap-type: x mandatory;
    -webkit-overflow-scrolling: touch;
    scroll-behavior: smooth;

    padding: 10px;

    ::-webkit-scrollbar {
      -webkit-appearance: none;
    }
    ::-webkit-scrollbar:vertical {
      width: 14px;
    }
    ::-webkit-scrollbar:horizontal {
      height: 14px;
    }
    ::-webkit-scrollbar-thumb {
      border-radius: 10px;

      background-color: ${(props) => (props.theme === 'Dark' ? '#47505E' : '#CECECE')};
    }
    ::-webkit-scrollbar-track {
      border-radius: 10px;
    }
  }

  div.item {
    flex: none;
    width: 100%;

    scroll-snap-align: start;

    img {
      object-fit: cover;
    }
  }

  img {
    width: 280px;
    height: 300px;

    margin: 0 auto;

    border-radius: 10px;

    @media (max-width: 600px) {
      width: 195px;
      height: 200px;
      margin: 0 auto;
    }
  }

  @media (max-width: 700px) {
    margin-right: 0px;
  }

  @media (min-width: 1000px) {
    div#items-wrapper {
      width: 375px;

      margin-bottom: 30px;
    }
  }
`;

export const ModalContainerProductInfo = styled.div`
  width: 100%;

  ul {
    list-style: none;
  }

  > div:first-child {
    margin-top: 10px;
    padding: 10px;

    @media (max-width: 700px) {
      margin-top: 40px;
    }
  }

  > div:last-child {
    padding: 10px;
    @media (max-width: 700px) {
      margin-bottom: 40px;
    }
  }

  > div {
    padding: 0;

    > div {
      grid-template-columns: calc(100% - 10px) 30px;

      h3 {
        font-size: 12px;
      }
    }

    > div:first-child {
      border-radius: 15px 15px 0 0;

      div:last-child {
        width: 40%;
      }
    }

    > div.active:first-child {
      background: #ccedff;
      color: #01a3ff;
      padding: 10px;
    }
  }
`;

export const ModalTitleProduct = styled.p`
  font-size: 22px;
  font-weight: bold;
  letter-spacing: 0px;

  margin-bottom: 20px;

  opacity: 1;

  color: ${(props) => (props.theme === 'Dark' ? '#FFFFFF' : '#82868D')};

  @media (max-width: 700px) {
    font-size: 14px;
  }
`;

export const ModalNav = styled.ul`
  &.nav {
    display: flex;

    li:first-child {
      margin-right: 5px;
    }
  }
`;

export const ModalNavItem = styled.li`
  text-align: center;

  width: 100%;
  height: 35px;

  cursor: pointer;
  text-decoration: none;

  border-radius: 15px 15px 0px 0px;
  opacity: 1;

  a {
    font-size: 16px;
    letter-spacing: 0px;
    font-weight: bold;

    display: block;
    vertical-align: sub;

    padding: 5px;

    text-decoration: none;
    opacity: 1;

    color: #5e5e5e;
  }

  &.active {
    /*background: #ccedff 0% 0% no-repeat padding-box;*/
    background: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};

    a {
      color: ${(props) => (props.theme === 'Dark' ? '#202731' : '#FFFFFF')};
    }
  }

  &.default {
    background: ${(props) => (props.theme === 'Dark' ? '#29303A' : '#FBFBFB')};
    a {
      color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
    }
  }

  @media (min-width: 1800px) {
    width: 100%;
  }
`;

export const ModalContainerProductInfoContent = styled.div`
  height: 415px;

  padding: 20px;

  opacity: 1;
  overflow-y: scroll;
  box-shadow: 0px 3px 6px #00000029;

  color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};

  background: ${(props) => (props.theme === 'Dark' ? '#29303A' : '#FBFBFB')};

  @media (max-width: 1400px) {
    max-width: 565px;
    max-height: 415px;
  }

  @media (min-width: 1800px) {
    width: 100%;
  }

  @media (max-width: 700px) {
    height: 170px;
  }

  ::-webkit-scrollbar {
    -webkit-appearance: none;
  }
  ::-webkit-scrollbar:vertical {
    width: 14px;
  }
  ::-webkit-scrollbar:horizontal {
    height: 14px;
  }
  ::-webkit-scrollbar-thumb {
    border-radius: 10px;

    background-color: ${(props) => (props.theme === 'Dark' ? '#47505E' : '#CECECE')};
  }
  ::-webkit-scrollbar-track {
    border-radius: 10px;
  }
`;

export const ModalContainerProductInfoContentDescription = styled.div`
  section {
    margin-bottom: 30px;
  }

  &.visible {
    display: block;
  }

  &.invisible {
    display: none;
  }
`;

export const ModalContainerProductInfoContentUtils = styled.div`
  ul {
    padding-left: 20px;

    list-style: unset;
  }

  section {
    margin-bottom: 30px;
  }

  &.visible {
    display: block;
  }

  &.invisible {
    display: none;
  }
`;
