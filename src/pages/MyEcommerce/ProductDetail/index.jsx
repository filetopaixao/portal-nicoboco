import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';

import Modal from '@components/molecules/Modals/ModalComponent';

import * as S from './styled';

const ModalComponentProduct = ({
  isModalVisible,
  setIsModalVisible,
  product,
  description,
  feature,
  allerg,
  benef,
  charac,
  contraind,
  indic,
  ingred,
  usageSug,
}) => {
  const themeStore = useSelector((state) => state.theme);
  const [navItem, setNavItem] = useState('utils');

  return (
    <S.ModalContainer>
      <Modal
        title=""
        variant="lg"
        alignCenter
        isVisible={isModalVisible}
        onClose={() => setIsModalVisible(!isModalVisible)}
        MaxHeight="700px"
        ContentHeight="auto"
      >
        <S.ModalContent secondaryColor={themeStore.secondaryColor}>
          <S.ModalContainerProductImage>
            <div id="items-wrapper">
              <div id="items">
                {product.images ? (
                  product.images.map((item) => (
                    <div key={Math.random()} className="item">
                      <img src={item} alt={product.name} />
                    </div>
                  ))
                ) : (
                  <div className="item">
                    <img src="https://mercipro.org/wp-content/uploads/2018/09/404.png" alt={product.name} />
                  </div>
                )}
              </div>
            </div>
            <button type="button">Baixar Benefícios do Produto</button>
            <button type="button">Baixar Recomendações de Uso</button>
          </S.ModalContainerProductImage>
          <S.ModalContainerProductInfo>
            <S.ModalTitleProduct className="titleProduct" theme={themeStore.themeName}>
              {product.name}
            </S.ModalTitleProduct>
            <S.ModalNav className="nav">
              {description && (
                <S.ModalNavItem className={navItem === 'utils' ? 'active' : 'default'} theme={themeStore.themeName}>
                  <a href="#!" onClick={() => setNavItem('utils')}>
                    Descrição
                  </a>
                </S.ModalNavItem>
              )}

              {feature && (
                <S.ModalNavItem
                  className={navItem === 'description' ? 'active' : 'default'}
                  theme={themeStore.themeName}
                >
                  <a href="#!" onClick={() => setNavItem('description')}>
                    Características
                  </a>
                </S.ModalNavItem>
              )}
            </S.ModalNav>
            <S.ModalContainerProductInfoContent theme={themeStore.themeName}>
              {description && (
                <S.ModalContainerProductInfoContentUtils className={navItem === 'utils' ? 'visible' : 'invisible'}>
                  <section>
                    <h4>{product.name}</h4>

                    {indic && (
                      <ul>
                        {indic.map((item) => (
                          <li key={item.id}>{item.name}</li>
                        ))}
                      </ul>
                    )}
                  </section>
                  <section>
                    <h4>{product.description}</h4>

                    {benef && (
                      <ul>
                        {benef.map((item) => (
                          <li key={item.id}>{item.name}</li>
                        ))}
                      </ul>
                    )}
                  </section>
                </S.ModalContainerProductInfoContentUtils>
              )}

              {feature && (
                <S.ModalContainerProductInfoContentDescription
                  className={navItem === 'description' ? 'visible' : 'invisible'}
                >
                  <section>
                    <h4>Sugestões de Uso</h4>
                    {usageSug.map((item) => (
                      <p key={item.id}>{item.name}</p>
                    ))}
                  </section>
                  <section>
                    <h4>Info</h4>
                    {charac.map((item) => (
                      <p key={item.id}>{item.name}</p>
                    ))}
                  </section>
                  <section>
                    <h4>Alergias</h4>
                    {allerg.map((item) => (
                      <p key={item.id}>{item.name}</p>
                    ))}
                  </section>
                  <section>
                    <h4>Contra Indicações</h4>
                    {contraind.map((item) => (
                      <p key={item.id}>{item.name}</p>
                    ))}
                  </section>
                  <section>
                    <h4>Ingredientes</h4>
                    {ingred.map((item) => (
                      <p key={item.id}>{item.name}</p>
                    ))}
                  </section>
                </S.ModalContainerProductInfoContentDescription>
              )}
            </S.ModalContainerProductInfoContent>
          </S.ModalContainerProductInfo>
        </S.ModalContent>
      </Modal>
    </S.ModalContainer>
  );
};

ModalComponentProduct.propTypes = {
  product: PropTypes.oneOfType([PropTypes.object]),
  description: PropTypes.oneOfType([PropTypes.func, PropTypes.object, PropTypes.string]),
  feature: PropTypes.oneOfType([PropTypes.func, PropTypes.object, PropTypes.string]),
  isModalVisible: PropTypes.bool,
  setIsModalVisible: PropTypes.func,
  allerg: PropTypes.oneOfType([PropTypes.array]),
  benef: PropTypes.oneOfType([PropTypes.array]),
  charac: PropTypes.oneOfType([PropTypes.array]),
  contraind: PropTypes.oneOfType([PropTypes.array]),
  indic: PropTypes.oneOfType([PropTypes.array]),
  ingred: PropTypes.oneOfType([PropTypes.array]),
  usageSug: PropTypes.oneOfType([PropTypes.array]),
};
ModalComponentProduct.defaultProps = {
  product: {},
  description: '',
  feature: '',
  isModalVisible: false,
  setIsModalVisible: () => {},
  allerg: [],
  benef: [],
  charac: [],
  contraind: [],
  indic: [],
  ingred: [],
  usageSug: [],
};

export default ModalComponentProduct;
