import React from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';

import Copy from '@assets/images/copy.svg';
import Share from '@assets/images/share.svg';

import * as S from './styled';

const Products = ({ data, openModal, selectedProduct }) => {
  const themeStore = useSelector((state) => state.theme);
  const handleSelectProduto = async (product) => {
    await selectedProduct(product);
    openModal(true);
  };

  return (
    <S.ContentProductList>
      {data.map((productItem) => (
        <S.Product className="product" key={productItem.id}>
          <S.ProductContainer className="product__container" theme={themeStore.themeName}>
            <S.ProductTitle theme={themeStore.themeName}>{productItem.name}</S.ProductTitle>

            <S.ProductImage className="product__image">
              <img src={productItem.images[0]} alt={productItem.name} />
            </S.ProductImage>

            <S.ProductValues className="product__value">
              {!productItem.price && <S.TextValue> Preço de venda: </S.TextValue>}
              {productItem.price && <S.TextValue> Preço: </S.TextValue>}
              <S.TextValue size="sm">
                {new Intl.NumberFormat('pt-BR', {
                  style: 'currency',
                  currency: 'BRL',
                }).format(productItem.price)}
              </S.TextValue>
            </S.ProductValues>

            {!productItem.purchase && (
              <S.ProductValues className="product__commision">
                <S.TextValue> Comissão: </S.TextValue>
                <S.TextValue>
                  R$
                  {` ${productItem.commission}`}
                </S.TextValue>
              </S.ProductValues>
            )}
          </S.ProductContainer>

          <S.ProducInfo className="product__info">
            <div>
              <S.ProductLink
                onClick={() => handleSelectProduto(productItem)}
                type="button"
                href="#"
                alt="conheça melhor"
                primaryColor={themeStore.primaryColor}
              >
                Conheça melhor
              </S.ProductLink>
            </div>
            <S.ButtonGroupCopyShare>
              <S.ProductTextClick size="xs" variant="bold" onClick={() => console.log(productItem.url)}>
                <img src={Copy} alt="copy to clipboard" />
              </S.ProductTextClick>

              <S.ProductTextClick size="xs" variant="bold" onClick={() => console.log('share product')}>
                <img src={Share} alt="share product" />
              </S.ProductTextClick>
            </S.ButtonGroupCopyShare>
          </S.ProducInfo>
        </S.Product>
      ))}
    </S.ContentProductList>
  );
};

Products.propTypes = {
  data: PropTypes.oneOfType([PropTypes.array]),
  openModal: PropTypes.func,
  selectedProduct: PropTypes.func,
};
Products.defaultProps = {
  data: [],
  openModal: () => {},
  selectedProduct: () => {},
};

export default Products;
