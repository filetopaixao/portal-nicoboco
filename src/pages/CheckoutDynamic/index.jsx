import React, { useEffect, useState } from 'react';
import { useLocation } from 'react-router-dom';

import { getCars, showCar } from '../../services/dinamycCar';

import * as S from './styled';

const CheckoutDynamic = () => {
  const [products, setProducts] = useState([]);
  const { pathname } = useLocation();
  const routeArray = pathname.split('/');
  const linkCarName = routeArray[routeArray.length - 1];
  const [buttonCard, setButtonCard] = useState(true);
  const [buttonBillet, setButtonBillet] = useState(false);

  const [activeLoading, setActiveLoading] = useState(true);

  const [subTotal, setSubTotal] = useState(0);
  const [desconto, setDesconto] = useState(0);
  const [total, setTotal] = useState(0);

  useEffect(() => {
    Promise.all([
      getCars().then((response) => {
        const { data } = response.data;
        // console.log(data);
        const filteredItems = data.filter(
          (item) => item.url && item.url.toLowerCase().includes(linkCarName.toLowerCase())
        );

        showCar(filteredItems[0].id).then((responseShow) => {
          setProducts(responseShow.data.products);
          console.log('asdasd', responseShow.data);

          const { subT, desc } = responseShow.data.products.reduce(
            (accumulator, product) => {
              accumulator.subT += parseFloat(product.price);
              accumulator.desc += (parseFloat(product.price) * responseShow.data.discount_applied) / 100;
              return accumulator;
            },
            {
              subT: 0,
              desc: 0,
            }
          );

          if (subT === 0) {
            setSubTotal(0);
            setDesconto(0);
            setTotal(0);
          }
          setSubTotal(subT);
          setDesconto(desc);
          setTotal(subT - desc);

          setActiveLoading(false);
        });
      }),
    ]).then(() => {});
  }, [linkCarName]);

  const handleSelecteOp = (op) => {
    switch (op) {
      case 'card':
        setButtonCard(true);
        setButtonBillet(false);
        break;
      case 'billet':
        setButtonCard(false);
        setButtonBillet(true);
        break;
      default:
        setButtonCard(true);
        break;
    }
  };

  if (activeLoading) {
    return <S.ImageLoadding />;
  }
  // http://www.buscacep.correios.com.br/sistemas/buscacep/"
  return (
    <S.Container>
      <S.Header>
        <S.Logo>
          <S.ImageLogoEver />
        </S.Logo>
        <S.Login>
          <a href="https://portal.everbynature.com.br/">
            <S.IconUser />
            <span>Acessar minha conta</span>
          </a>
        </S.Login>
      </S.Header>

      <S.Banner />

      <S.Separator>
        <div className="discount">
          <S.IconPercentImg />
          <h4>20%asdas DESCONTO</h4>
        </div>

        <div className="discount">
          <S.IconCardImg />
          <h4>ATÉ 5X SEM JUROS</h4>
        </div>
      </S.Separator>

      <S.Content>
        <S.ListProducts>
          <S.TableContainer>
            <S.Theader>
              <S.Th>Produto</S.Th>
              <S.Th>Descrição</S.Th>
              <S.Th>Preço Unit</S.Th>
              <S.Th>Quantidade</S.Th>
              <S.Th>SubTotal</S.Th>
            </S.Theader>

            <S.Tbody>
              {products.map((item) => (
                <S.TableLine>
                  <S.TableCell>
                    <img src={item.image.url} alt="Produto" />
                  </S.TableCell>
                  <S.TableCell>
                    <S.Cell>{item.name}</S.Cell>
                  </S.TableCell>
                  <S.TableCell>
                    <S.Cell>{item.price}</S.Cell>
                  </S.TableCell>
                  <S.TableCell>
                    <S.Cell>2</S.Cell>
                  </S.TableCell>
                  <S.TableCell>
                    <S.Cell>{item.price * 2}</S.Cell>
                  </S.TableCell>
                </S.TableLine>
              ))}
            </S.Tbody>
          </S.TableContainer>

          <div>
            <S.ContainerResults>
              <S.CepContainer>
                <div>
                  <p>Calcular o frete:</p>
                </div>
                <div>
                  <input type="text" name="cep" id="cep" />
                </div>
                <div>
                  <button type="button">Calcular</button>
                </div>
                <div>
                  <a
                    href="https://buscacepinter.correios.com.br/app/endereco/index.php"
                    rel="noopener noreferrer"
                    target="_blank"
                  >
                    Não sei o CEP
                  </a>
                </div>
              </S.CepContainer>
              <S.Results>
                <S.LineResult>
                  <p>SubTotal:</p>
                  <p>R$ {subTotal.toFixed(2)}</p>
                </S.LineResult>

                <S.LineResult>
                  <p>Frete:</p>
                  <p>R$ 100,00</p>
                </S.LineResult>

                <S.LineResult>
                  <p>Desconto:</p>
                  <p>R$ {desconto.toFixed(2)}</p>
                </S.LineResult>
              </S.Results>
            </S.ContainerResults>
            <S.SumContainer>
              <p>Total:</p>
              <p>{total.toFixed(2)}</p>
            </S.SumContainer>
          </div>
        </S.ListProducts>

        <S.ContainerForms>
          <S.FormsData>
            <S.FormDataPersonal>
              <S.HeaderForm>
                <h2>Identificação</h2>
              </S.HeaderForm>
              <S.ContentForm>
                <div>
                  <S.InputLabel>Label</S.InputLabel>
                  <input type="text" />
                </div>

                <S.ButtonGroup>
                  <div>
                    <S.InputLabel>Label</S.InputLabel>
                    <input type="text" />
                  </div>
                  <div>
                    <S.InputLabel>Label</S.InputLabel>
                    <input type="text" />
                  </div>
                </S.ButtonGroup>

                <S.ButtonGroup>
                  <div>
                    <S.InputLabel>Label</S.InputLabel>
                    <input type="text" />
                  </div>
                  <div>
                    <S.InputLabel>Label</S.InputLabel>
                    <input type="text" />
                  </div>
                </S.ButtonGroup>
              </S.ContentForm>
            </S.FormDataPersonal>
            <S.FormDataAddress>
              <S.HeaderForm>
                <h2>Endereço de Entrega</h2>
              </S.HeaderForm>

              <S.ContentForm>
                <div>
                  <S.InputLabel>Label</S.InputLabel>
                  <input type="text" />
                </div>

                <S.ButtonGroup className="input-number-address">
                  <div className="input-number-address">
                    <S.InputLabel>Label</S.InputLabel>
                    <input type="text" />
                  </div>
                  <div>
                    <S.InputLabel>Label</S.InputLabel>
                    <input type="text" />
                  </div>
                </S.ButtonGroup>

                <div>
                  <S.InputLabel>Label</S.InputLabel>
                  <input type="text" />
                </div>

                <S.ButtonGroup>
                  <div>
                    <S.InputLabel>Label</S.InputLabel>
                    <input type="text" />
                  </div>
                  <div>
                    <S.InputLabel>Label</S.InputLabel>
                    <input type="text" />
                  </div>
                </S.ButtonGroup>
              </S.ContentForm>
            </S.FormDataAddress>
          </S.FormsData>

          <S.FormCardBillet>
            <S.HeaderForm>
              <h2>Cartao de credito</h2>
            </S.HeaderForm>

            <S.PagamentosOps>
              <S.ButtonCard
                className={buttonCard ? 'active' : ''}
                onClick={() => handleSelecteOp('card')}
                type="button"
              >
                <span>Cartão de Crédito</span>
              </S.ButtonCard>
              <S.ButtonBillet
                className={buttonBillet ? 'active' : ''}
                onClick={() => handleSelecteOp('billet')}
                type="button"
              >
                <span>Boleto</span>
              </S.ButtonBillet>
            </S.PagamentosOps>

            {buttonCard && (
              <S.ContentForm>
                <div>
                  <S.InputLabel>Label</S.InputLabel>
                  <input type="text" />
                </div>

                <div>
                  <S.InputLabel>Label</S.InputLabel>
                  <input type="text" />
                </div>

                <S.ButtonGroupCard className="input-numbers-card">
                  <div>
                    <S.InputLabel>Label</S.InputLabel>
                    <input type="text" />
                  </div>
                  <div>
                    <S.InputLabel>Label</S.InputLabel>
                    <input type="text" />
                  </div>
                  <div>
                    <S.InputLabel>Label</S.InputLabel>
                    <input type="text" />
                  </div>
                </S.ButtonGroupCard>

                <div>
                  <S.InputLabel>Label</S.InputLabel>
                  <input type="text" />
                </div>

                <hr />

                <button type="button">Fechar Pedido</button>
              </S.ContentForm>
            )}

            {buttonBillet && (
              <S.ContentFormBillet>
                <h4>Atente-se aos detalhes:</h4>
                <article>
                  <ul>
                    <li>O boleto pode ser pago em qualquer agência bancária</li>
                    <li>O banco nos enviará automaticamente a confirmação do pagamento em até 3 dias úteis</li>
                    <li>
                      Se o boleto não for pago até a data de vencimento (Nos casos de vencimento em final de semana ou
                      feriado, no próximo dia útil), seu pedido será cancelado automaticamente{' '}
                    </li>
                    <li>
                      Depois do pagamento , verifique seu e-mail para receber mais informações sobre seu pedido,
                      verifique também a caixa de spam.
                    </li>
                  </ul>
                </article>

                <hr />

                <button type="button">Gerar Boleto</button>
              </S.ContentFormBillet>
            )}

            <S.ImageSecurity />
          </S.FormCardBillet>
        </S.ContainerForms>
      </S.Content>

      <S.Footer>
        <S.RodapeCheckout />
        {/* <S.ContentInfoFooter>
          <p>
            EVER by Nature ® é uma marca registrada de EVER E-COMMERCE, IMPORTAÇÃO E EXPORTAÇÃO EIRELI |
            CNPJ30.816.903/0001-06
            <span> | </span>
            <a
              href="https://everbynature.com.br/politica/politica_de_devolucao_e_troca/"
              rel="noopener noreferrer"
              target="_blank"
            >
              Política de Devolução e Troca
            </a>
            <span> | </span>
            <a
              href="https://everbynature.com.br/politica/politica_de_privacidade/"
              rel="noopener noreferrer"
              target="_blank"
            >
              Política de Privacidade
            </a>
            <span> | </span>
            <a href="https://everbynature.com.br/politica/termos_de_uso/" rel="noopener noreferrer" target="_blank">
              Termos de uso
            </a>
            <span> | </span>
            Os preços anunciados neste site via e-mail ou qualquer outra ferramenta de marketing podem ser alterados sem
            prévio aviso.
          </p>
        </S.ContentInfoFooter> */}
      </S.Footer>
    </S.Container>
  );
};

export default CheckoutDynamic;
