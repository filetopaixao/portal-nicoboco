import styled from 'styled-components';

import PageLoadding from '@assets/images/page_loadding.gif';
import NotFound from '@assets/images/erro-404.svg';
import LogoEver from '../../assets/images/logo-fpass-checkout.png';
import Icon from '../../assets/images/user_icon.png';
import IconPercent from '../../assets/images/icon_percent.png';
import IconCard from '../../assets/images/icon_card.png';
import IconFreight from '../../assets/images/icon_freight.png';
import Security from '../../assets/images/security.png';

import BannerCheckoutFpassDesktop from '../../assets/images/banner-checkout-fpass-desktop.jpg';
import BannerCheckoutFpassMobile from '../../assets/images/banner-checkout-fpass-mobile.jpg';
import RodapeFpass from '../../assets/images/rodape-checkout-fpass.png';

export const Header = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  height: 65px;
  background: #111a21;
`;

export const Logo = styled.div`
  text-align: right;
  width: 250px;
  justify-content: right;
`;

export const ImageLogoEver = styled.div`
  content: url(${LogoEver});
  width: 100px;
  height: auto;
  margin-left: auto;
`;

export const RodapeCheckout = styled.div`
  content: url(${RodapeFpass});
  width: 100%;
  height: auto;
  margin-left: auto;
`;

export const Banner = styled.div`
  width: 100%;
  height: auto;

  background-size: contain;
  background-repeat: no-repeat;

  @media (min-width: 600px) {
    content: url(${BannerCheckoutFpassDesktop});
  }
  @media (max-width: 600px) {
    content: url(${BannerCheckoutFpassMobile});
  }
`;

export const IconPercentImg = styled.div`
  content: url(${IconPercent});
  width: 43px;
  height: auto;
`;

export const IconCardImg = styled.div`
  content: url(${IconCard});
  width: 43px;
  height: auto;
`;

export const IconFreightImg = styled.div`
  content: url(${IconFreight});
  width: 43px;
  height: auto;
  margin: 0 auto;
`;

export const Login = styled.div`
  width: 450px;

  a {
    display: flex;
    align-items: center;
    text-decoration: none;
    cursor: pointer;
  }

  span,
  a {
    color: #fff;
  }
`;

export const Separator = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  height: 45px;
  align-items: center;

  background-color: #ce3cea;

  > div.discount {
    justify-content: center;
    display: flex;
    h4 {
      color: #fff;
      font-size: 27px;
      font-weight: bold;
    }
  }
`;

export const IconUser = styled.div`
  content: url(${Icon});
  width: 34px;
  height: auto;
  margin-right: 5px;
`;

export const Container = styled.div``;

export const Footer = styled.div`
  display: flex;
  height: 112px;
  padding: 0 140px;
  background-color: #111a21;
`;

export const ListProducts = styled.section`
  min-height: 500px;
  margin-bottom: 30px;
  padding: 10px;

  border: 2px solid #cbcbcc;
  border-radius: 5px;
`;

export const ContainerForms = styled.section`
  display: grid;
  grid-template-columns: 1fr 1fr;
  column-gap: 110px;
  min-height: 657px;

  h2 {
    color: rgb(5, 56, 87);
    margin: 10px;
  }
`;

export const HeaderForm = styled.div`
  background-color: rgb(237, 236, 241);
  padding: 3px;
`;

export const FormDataPersonal = styled.div`
  margin-bottom: 15px;
  border: 2px solid #cbcbcc;
  border-radius: 5px;
`;

export const FormDataAddress = styled.div`
  border: 2px solid #cbcbcc;
  border-radius: 5px;
`;

export const FormsData = styled.div``;

export const FormCardBillet = styled.div`
  min-height: 657px;
  border: 2px solid #cbcbcc;
  border-radius: 5px;

  hr {
    margin: 20px;
  }
`;

export const InputLabel = styled.p`
  font-size: 14px;
  color: rgb(5, 56, 87);
`;

export const ContentForm = styled.div`
  padding: 10px;

  input {
    width: 100%;
    height: 38px;
    border-radius: 5px;
    border: 1px solid rgb(210, 210, 210);
    margin-bottom: 10px;
  }

  button {
    color: #fff;
    font-weight: 900;
    height: 55px;
    width: 100%;

    padding: 6px 16px;
    font-size: 0.875rem;
    min-width: 64px;

    box-sizing: border-box;
    transition: background-color 250ms cubic-bezier(0.4, 0, 0.2, 1) 0ms,
      box-shadow 250ms cubic-bezier(0.4, 0, 0.2, 1) 0ms, border 250ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
    font-family: 'Roboto', 'Helvetica', 'Arial', sans-serif;
    font-weight: 500;
    line-height: 1.75;

    border: none;
    border-radius: 4px;
    letter-spacing: 0.02857em;
    text-transform: uppercase;

    background-color: rgb(1, 167, 67);
  }
`;

export const ContentFormBillet = styled.div`
  min-height: 393px;

  padding: 10px;

  ul {
    padding: 0 40px;
  }

  color: rgb(116, 114, 123);

  h4 {
    padding: 0 40px;
    margin-bottom: 15px;
  }

  button {
    color: #fff;
    font-weight: 900;
    height: 55px;
    width: 100%;

    padding: 6px 16px;
    font-size: 0.875rem;
    min-width: 64px;

    box-sizing: border-box;
    transition: background-color 250ms cubic-bezier(0.4, 0, 0.2, 1) 0ms,
      box-shadow 250ms cubic-bezier(0.4, 0, 0.2, 1) 0ms, border 250ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
    font-family: 'Roboto', 'Helvetica', 'Arial', sans-serif;
    font-weight: 500;
    line-height: 1.75;

    border: none;
    border-radius: 4px;
    letter-spacing: 0.02857em;
    text-transform: uppercase;

    background-color: rgb(1, 167, 67);
  }
`;

export const ButtonGroup = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  column-gap: 15px;
  align-items: center;
  width: 100%;

  &.input-number-address {
    display: grid;
    grid-template-columns: 0.3fr 1fr;
    column-gap: 15px;
  }
`;

export const ContentInfoFooter = styled.div`
  padding: 0 50px;
  p {
    text-align: center;
    color: #fff;
  }

  a {
    text-decoration: none;
    color: #fff;
  }
`;

export const ButtonGroupCard = styled(ButtonGroup)`
  &.input-numbers-card {
    display: grid;
    grid-template-columns: 0.3fr 0.3fr 1fr;
    column-gap: 15px;
  }
`;

export const TableContainer = styled.div`
  border-bottom: 2px solid #5e5e5e;
`;

export const Theader = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr 1fr 1fr;
  align-items: center;
  height: 70px;
  border-bottom: 2px solid #5e5e5e;
`;

export const Th = styled.p`
  font-size: 16px;
  text-align: center;
  color: #5e5e5e;
`;

export const TableCell = styled.div``;

export const Cell = styled.p`
  font-size: 16px;
  text-align: center;
  color: #5e5e5e;
`;

export const Content = styled.div`
  max-width: 1300px;
  padding: 32px;
  margin: 0 auto;
`;

export const Tbody = styled.div``;

export const TableLine = styled.div`
  text-align: center;
  display: grid;
  grid-template-columns: 1fr 1fr 1fr 1fr 1fr;
  align-items: center;

  margin: 20px 0;
  width: 100%;

  img {
    height: 90px;
    width: 90px;
    margin: 0 auto;
  }

  p {
    color: #5e5e5e;
  }
`;

export const ContainerBackEndInfo = styled.div`
  padding: 20px;
`;

export const InfoBackEnd = styled.h2`
  color: #5e5e5e;
  font: normal normal normal 14px Poppins;
`;

export const ImageNotFound = styled.div`
  content: url(${NotFound});
  width: 18rem;
  height: auto;

  margin-top: 30px;
  margin: 0 auto;

  background-size: contain;
  background-repeat: no-repeat;

  @media (min-width: 1200px) {
    width: 33rem;
    height: auto;
  }
`;

export const ImageLoadding = styled.div`
  content: url(${PageLoadding});
  width: 20rem;
  height: auto;

  margin-top: 30px;
  margin: 0 auto;

  background-size: contain;
  background-repeat: no-repeat;

  @media (max-width: 600px) {
    width: 16rem;
    height: auto;
  }
`;

export const CepContainer = styled.div`
  display: flex;
  align-items: center;
  width: 650px;

  p {
    color: #5e5e5e;
  }

  button {
    height: 40px;
    width: 70px;
    color: rgb(114, 113, 121);
    border: none;
    border-radius: 10px;
    cursor: pointer;
    background-image: linear-gradient(rgb(235, 238, 243), rgb(212, 217, 223));
  }

  input {
    height: 39px;
    padding: 0 20px;

    border-radius: 15px;
    border: 1px solid #bcbcbc;
  }

  a {
    color: rgb(16, 65, 95);
    text-decoration: none;
  }

  > div:nth-child(n) {
    margin-right: 7px;
  }
`;

export const ContainerResults = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  height: 130px;

  border-bottom: 2px solid #5e5e5e;
`;

export const SumContainer = styled.div`
  display: flex;
  width: 100%;
  justify-content: right;
  height: 40px;
  display: flex;
  align-items: center;

  p {
    color: #5e5e5e;
  }

  p:first-child {
    margin: 0 10px;
  }
`;

export const Results = styled.div`
  display: block;
  width: 300px;
  justify-content: right;
  text-align: right;

  p {
    color: #5e5e5e;
    text-align: right;
  }
`;

export const LineResult = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 5px;
  text-align: right;
  justify-content: right;

  p:first-child {
    margin: 0 10px;
  }
`;

export const ButtonCard = styled.button`
  &.active {
    color: rgb(115, 113, 124);
    background-image: linear-gradient(rgb(212, 217, 223), rgb(115, 113, 124));
  }
`;

export const ButtonBillet = styled.button`
  &.active {
    color: rgb(115, 113, 124);
    background-image: linear-gradient(rgb(212, 217, 223), rgb(115, 113, 124));
  }
`;

export const PagamentosOps = styled.div`
  display: flex;
  justify-content: space-around;
  margin: 30px 10px 15px;

  button {
    font-size: 12px;

    display: flex;
    align-items: center;
    justify-content: center;

    width: 200px;
    height: 60px;

    transition: all 0.3s ease 0s;
    border: medium none;
    border-radius: 5px;
    box-shadow: rgb(0, 0, 0) 0px 0px 5px -2px;
    outline: currentcolor none medium;
    cursor: pointer;

    background-color: #e0e0e0;
    color: rgb(115, 113, 124);
  }

  span {
    font-size: 12px;

    color: rgb(115, 113, 124);
  }
`;

export const ImageSecurity = styled.div`
  content: url(${Security});
  width: 250px;
  height: auto;
  margin: 0 auto;

  background-size: contain;
  background-repeat: no-repeat;
`;
