import React, { useCallback, useState } from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';

import Toast from '@components/atoms/Toast';
import Modal from '@components/molecules/Modals/ModalComponent';
import PanelPixel from '@components/molecules/Modals/PanelPixel';
import ConfirmationDelete from '@components/molecules/Modals/ModalConf';
import ConfirmationSuccess from '@components/molecules/Modals/ModalConfSuccess';

import { deleteCar } from '../../../services/dinamycCar';

import * as S from './styled';

const EditCar = ({ dinamycCar, isModalVisible, setIsModalVisible }) => {
  const themeStore = useSelector((state) => state.theme);
  const [isToastVisible, setIsToastVisible] = useState(false);
  const [urlDisp] = useState(true);
  const [isModalPanelPixelVisible, setIsModalPanelPixelVisible] = useState(false);
  const [isModalDelConf, setIsModalDelConf] = useState(false);
  const [isModalConfSuccess, setIsModalConfSuccess] = useState(false);

  const handleActiveModalPanelPixel = () => {
    setIsModalPanelPixelVisible(!isModalPanelPixelVisible);
  };

  const handleActiveDelConf = (item) => {
    console.log(item);
    setIsModalDelConf(!isModalDelConf);
  };

  const handleDelConf = async () => {
    await deleteCar(dinamycCar.id).then((response) => {
      console.log(response);
    });
    console.log(dinamycCar);
    setIsModalVisible(!isModalVisible);
    setIsModalDelConf(!isModalDelConf);
    window.location.reload();
  };

  const handleActiveConfSuccess = () => {
    setIsModalVisible(!isModalVisible);
    setIsModalConfSuccess(!isModalConfSuccess);
    window.location.href = '/playlist';
  };

  const handleActiveToast = useCallback(() => {
    setIsToastVisible(true);
    setTimeout(() => {
      setIsToastVisible(false);
    }, 3000);
  }, [setIsToastVisible]);

  const handleCopy = useCallback(
    (id) => {
      const btnCopy = document.getElementById(id);
      console.log(btnCopy.value);
      btnCopy.select();
      document.execCommand('copy');

      handleActiveToast();
    },
    [handleActiveToast]
  );

  return (
    <>
      <Modal
        variant="mdMin"
        alignCenter
        title=""
        isVisible={isModalVisible}
        onClose={() => setIsModalVisible(!isModalVisible)}
        MaxHeight="95vh"
        MaxWidth="600px"
        ContentHeight="auto"
      >
        <S.ModalContent dinamycCar={dinamycCar}>
          <S.ModalHeader theme={themeStore.themeName}>
            <h3>Editar Playlist</h3>
          </S.ModalHeader>

          <section>
            <S.InputContainer>
              <S.InputLabel theme={themeStore.themeName}>Qual o nome da sua playlist?</S.InputLabel>
              <S.Input type="text" theme={themeStore.themeName} />
            </S.InputContainer>

            <S.InputContainer>
              <S.InputLabel theme={themeStore.themeName}>Nome da URL</S.InputLabel>
              <S.Input type="text" theme={themeStore.themeName} />
            </S.InputContainer>

            {urlDisp ? (
              <S.UrlDip>
                <span>✓ URL disponível</span>
              </S.UrlDip>
            ) : null}
          </section>
          <section>
            <S.UrlContainer theme={themeStore.themeName}>
              <p>http://blablablablablabla.com.br</p>
              <S.Button onClick={() => handleCopy(`${dinamycCar.id.toString()}linkThanks`)}>
                <S.InputCopy
                  type="text"
                  id={`${dinamycCar.id}linkThanksEditCar`}
                  onChange={() => {}}
                  value={`Esse texto copiado do - ${dinamycCar.name} - linkThanksEditCar`}
                />
                <div />
              </S.Button>
            </S.UrlContainer>
            <S.UrlContainer>
              <p>http://blablablablablabla.com.br</p>
              <S.Button onClick={() => handleCopy(`${dinamycCar.id.toString()}linkCart`)}>
                <S.InputCopy
                  type="text"
                  id={`${dinamycCar.id}linkCartEditCar`}
                  onChange={() => {}}
                  value={`Esse texto copiado do - ${dinamycCar.name} - linkCartEditCar`}
                />
                <div />
              </S.Button>
            </S.UrlContainer>
          </section>

          <S.ButtonContainerPanelPixel secondaryColor={themeStore.secondaryColor}>
            <button onClick={() => handleActiveModalPanelPixel()} type="button">
              Painel Pixel
            </button>
          </S.ButtonContainerPanelPixel>

          <S.ContentInfo theme={themeStore.themeName}>
            <S.ContentInfoTitle theme={themeStore.themeName}>Produtos:</S.ContentInfoTitle>
            <div>
              <S.ContainerProductsList>
                <S.UlCategorie>
                  <S.LiCategorie>
                    <p>Produtos Nacionais</p>
                    <S.UlProduct>
                      <S.LiProduct>
                        <div>
                          <p>Garlic Oil</p>
                        </div>

                        <div>
                          <p>2</p>
                        </div>

                        <div>
                          <p>R$ 118,00</p>
                        </div>
                      </S.LiProduct>
                      <S.LiProduct>
                        <div>
                          <p>Fish Oil</p>
                        </div>

                        <div>
                          <p>1</p>
                        </div>

                        <div>
                          <p>R$ 79,00</p>
                        </div>
                      </S.LiProduct>
                    </S.UlProduct>
                  </S.LiCategorie>

                  <S.LiCategorie>
                    <p>Infoprodutos</p>
                    <S.UlProduct>
                      <S.LiProduct>
                        <div>
                          <p>Curso Imunidade</p>
                        </div>

                        <div>
                          <p />
                        </div>

                        <div>
                          <p>R$ 823,00</p>
                        </div>
                      </S.LiProduct>
                    </S.UlProduct>
                  </S.LiCategorie>
                </S.UlCategorie>
              </S.ContainerProductsList>
              <S.Separation theme={themeStore.themeName} />
              <S.ContainerResult>
                <div>
                  <S.ResultLine>
                    <div>
                      <p>Subtotal</p>
                    </div>
                    <div>
                      <p>R$ 1.500,00</p>
                    </div>
                  </S.ResultLine>

                  <S.ResultLine>
                    <div>
                      <p>Desconto</p>
                    </div>
                    <div>
                      <p>R$ 150,00</p>
                    </div>
                  </S.ResultLine>

                  <S.ResultLine>
                    <div>
                      <p>Valor Final</p>
                    </div>
                    <div>
                      <p>R$ 850,00</p>
                    </div>
                  </S.ResultLine>
                </div>
              </S.ContainerResult>
            </div>
          </S.ContentInfo>

          <S.ContainerButtonDelete>
            <button onClick={() => handleActiveDelConf(dinamycCar)} type="button">
              Excluir Playlist
            </button>
          </S.ContainerButtonDelete>

          <S.ContainerButtonSave primaryColor={themeStore.primaryColor}>
            <button onClick={() => handleActiveConfSuccess()} type="button">
              Salvar Alterações
            </button>
          </S.ContainerButtonSave>
        </S.ModalContent>
      </Modal>
      <Toast
        variant="success"
        icon="success"
        showIcon
        isVisible={isToastVisible}
        onClose={() => setIsToastVisible(!isToastVisible)}
      >
        Link copiado com sucesso!!
      </Toast>
      <PanelPixel isModalVisible={isModalPanelPixelVisible} setIsModalVisible={setIsModalPanelPixelVisible} />
      <ConfirmationDelete
        exeFunctionAction={handleDelConf}
        isModalDelConf={isModalDelConf}
        setIsModalDelConf={setIsModalDelConf}
        msg1="Tem certeza que deseja excluir esse Carrinho Dinâmico?"
        msg2="Ao excluir o carrinho não estará mais disponível para utilização!"
      />
      <ConfirmationSuccess
        isModalConfSuccess={isModalConfSuccess}
        setIsModalConfSuccess={setIsModalConfSuccess}
        msg1="Alterações realizadas com sucesso!"
        msg2="As configurações do carrinho foram alteradas"
      />
    </>
  );
};

EditCar.propTypes = {
  dinamycCar: PropTypes.oneOfType([PropTypes.string, PropTypes.node, PropTypes.object]),
  isModalVisible: PropTypes.bool,
  setIsModalVisible: PropTypes.func,
};
EditCar.defaultProps = {
  dinamycCar: {},
  isModalVisible: false,
  setIsModalVisible: () => {},
};

export default EditCar;
