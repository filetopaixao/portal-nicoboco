import styled from 'styled-components';
import { shade } from 'polished';

import IconCopy from '@assets/images/copy.svg';

export const ModalContent = styled.div`
  height: 100%;

  padding: 0px 20px 10px;

  z-index: 2;
  color: #5e5e5e;

  @media (max-width: 500px) {
    padding: 0px 10px 5px;
  }
`;

export const ModalHeader = styled.header`
  h3 {
    font-size: 14px;
    font-weight: 600;
    text-align: center;

    width: 100%;

    color: ${(props) => (props.theme === 'Dark' ? '#fff' : '#484646')};

    @media (min-width: 768px) {
      font-size: 18px;
    }
  }
`;

export const ModalBody = styled.div``;

export const Button = styled.button`
  font-size: 18px;
  text-transform: uppercase;
  text-align: center;

  display: flex;
  align-items: center;
  margin: 0 auto;
  min-height: 26px;

  cursor: pointer;
  border: none;
  border-radius: 0;
  box-shadow: none;
  background: none;
  position: relative;
  outline: 0;

  color: #fff;

  @media (min-width: 1500px) {
    min-height: 38px;
  }
`;

export const InputCopy = styled.input`
  visibility: hidden;
  display: none;
`;

export const InputContainer = styled.div`
  display: block;

  margin-bottom: 3px;
`;

export const InputLabel = styled.span`
  font-size: 14px;
  letter-spacing: 0px;
  color: ${(props) => (props.theme === 'Dark' ? '#fff' : '#484646')};
`;

export const Input = styled.input`
  font-size: 12px;

  height: 25px;
  width: 100%;

  padding: 0 20px;

  border: none;
  border-radius: 15px;
  border: ${(props) => (props.theme === 'Dark' ? '1px solid #2F3743' : '1px solid #EAEAEA')};

  background: ${(props) => (props.theme === 'Dark' ? '#29303A' : '#FBFBFB')};

  color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
  ::placeholder {
    color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
  }
  @media (min-width: 1500px) {
    height: 40px;
  }
`;

export const UrlDip = styled.div`
  span {
    font-size: 10px;
    line-height: 1px;
    letter-spacing: 0px;

    margin: 10px 0;

    color: #2fdf46;
  }
`;

export const UrlContainer = styled.div`
  display: flex;
  align-items: center;

  p {
    font-size: 12px;

    color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
  }

  button {
    width: 17px;

    margin: 0;
    margin-left: 10px;
  }

  div {
    -webkit-mask-image: url(${IconCopy});
    mask-image: url(${IconCopy});
    mask-repeat: no-repeat;
    mask-size: 17px;
    background-color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
    width: 17px;
    height: 17px;
  }
`;

export const ButtonContainerPanelPixel = styled.div`
  button {
    font-size: 17px;
    text-transform: uppercase;
    //font-weight: bold;

    height: 53px;
    width: 100%;

    border: none;
    border-radius: 15px;

    color: #fff;
    background: ${(props) => props.secondaryColor};

    &:hover {
      background: ${(props) => `${shade(0.2, props.secondaryColor)}`};
    }

    /*@media (min-width: 1500px) {
      height: 40px;
    }*/
  }
`;

export const ContentInfo = styled.section`
  > div {
    border-radius: 10px;

    border: ${(props) => (props.theme === 'Dark' ? '1px solid #2F3743' : '1px solid #EAEAEA')};

    background: ${(props) => (props.theme === 'Dark' ? '#29303A' : '#FBFBFB')};

    color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
  }
`;

export const ContentInfoTitle = styled.h4`
  font-size: 14px;
  font-weight: bold;

  margin: 5px 0;
  margin-bottom: 0;

  color: ${(props) => (props.theme === 'Dark' ? '#fff' : '#484646')};
`;

export const UlCategorie = styled.ul`
  padding: 2px 10px 2px 40px;

  @media (min-width: 1500px) {
    padding: 10px 10px 10px 40px;
  }
`;

export const LiCategorie = styled.li`
  > p {
    font-weight: bold;
  }
`;

export const UlProduct = styled.ul`
  list-style: none;
`;

export const LiProduct = styled.li`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;

  margin: 5px 0;

  p {
    font-size: 12px;
  }

  > div:nth-child(2),
  > div:nth-child(3) {
    text-align: right;
  }
`;

export const ContainerProductsList = styled.div``;

export const Separation = styled.hr`
  height: 3px;
  width: 90%;

  margin: 0 auto;

  border-radius: 15px;
  border: none;

  background: #7070704d;
`;

export const ContainerResult = styled.div`
  display: flex;
  justify-content: flex-end;

  padding: 3px 10px;

  @media (min-width: 1500px) {
    padding: 20px 10px;
  }

  > div {
    width: 200px;

    p {
      text-transform: uppercase;
    }
  }
`;

export const ResultLine = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  align-items: center;
  column-gap: 15px;

  p {
    font-size: 12px;
  }

  > div:nth-child(2) {
    text-align: right;
  }
`;

export const ContainerButtonDelete = styled.div`
  width: 100%;

  margin-top: 10px;

  button {
    text-transform: uppercase;
    font-size: 17px;
    //font-weight: bold;

    width: 100%;
    height: 53px;
    padding: 0 20px;

    border: none;
    border-radius: 15px;

    color: #fff;
    background: #ff0000 0% 0% no-repeat padding-box;
    box-shadow: 0px 3px 6px #00000029;

    &:hover {
      background: ${shade(0.2, '#ff0000')};
    }

    @media (min-width: 1500px) {
      height: 53px;
    }
  }
`;

export const ContainerButtonSave = styled.div`
  width: 100%;

  margin-top: 10px;

  button {
    text-transform: uppercase;
    font-size: 17px;
    //font-weight: bold;

    width: 100%;
    height: 53px;
    padding: 0 20px;

    border: none;
    border-radius: 15px;

    color: #fff;
    box-shadow: 0px 3px 6px #00000029;
    background: ${(props) => props.primaryColor};

    &:hover {
      background: ${(props) => `${shade(0.2, props.primaryColor)}`};
    }

    @media (min-width: 1500px) {
      height: 53px;
    }
  }
`;
