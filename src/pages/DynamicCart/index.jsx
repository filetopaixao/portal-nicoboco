/* eslint-disable react/button-has-type */
import React, { useState, useEffect, useCallback } from 'react';
import { useHistory } from 'react-router';
import { useDispatch, useSelector } from 'react-redux';

import CartResume from '@components/atoms/Checkout/CheckoutResume/';

import Toast from '@components/atoms/Toast';
import FilterComponent from '@components/molecules/Filter';
import Pagination from '@components/molecules/Pagination';
import ButtonToggleSwitch from '@components/atoms/ButtonToggleSwitch';

import Arrow from '@assets/images/arrowCheckout.svg';

import { Actions as DinamicCarActions } from '@redux/ducks/dinamycCar';
import { getCars } from '../../services/dinamycCar';
// import getAll from '../../services/fpass/products';

import ModalEditCar from './EditCar';

import * as S from './styled';

const Cupom = () => {
  const themeStore = useSelector((state) => state.theme);
  const history = useHistory();
  const [nameDinamicCar, setNameDinamicCar] = useState('');
  const [listCarDinamyc, setListCarDinamyc] = useState([]);
  const [dinamycCarSelected, setDinamycCarSelected] = useState({});

  const [isToastVisible, setIsToastVisible] = useState(false);

  const [filterText, setFilterText] = useState('');
  const [currentPage, setCurrentPage] = useState(1);
  const [perPage] = useState(10);

  const [isModalVisible, setIsModalVisible] = useState(false);

  const [verify, setVerify] = useState('pause');
  const dispatch = useDispatch();

  const [activeLoading, setActiveLoading] = useState(true);

  // useEffect(() => {
  //   getAll()
  //     .then((response) => {
  //       setFpassProduct(response.data);
  //     })
  //     .catch((getAllError) => {
  //       console.log(getAllError);
  //     });
  // }, []);

  // useEffect(() => {
  //   //apagar depois
  //   var car
  //   let valorTotal

  //   let fpassProduc = await getAll();
  //   console.log(fpassProduct)
  //   fpassProduc.data.forEach( async productFpass => {
  //     car = await showCar(item.id);
  //     await car.data.products.map(async(product) => {
  //       if(product.id === 10 && productFpass.id === '5fb3053b90229e4b7b6cabbd'){
  //         console.log(product.id, productFpass.price)
  //         valorTotal = formateValueBr(productFpass.price - (productFpass.price * parseInt(item.discount, 10).toFixed(0) / 100));
  //       }
  //       else if(product.id === 9 && productFpass.id === '6025766338c9d7348ed59a5b'){
  //         console.log(product.id, productFpass.price)
  //         valorTotal = formateValueBr(productFpass.price - (productFpass.price * parseInt(item.discount, 10).toFixed(0) / 100));
  //       }
  //       if(product.id === 11 && productFpass.id === '5fbd8ba3730f5c59db77d753'){
  //         console.log(product.id, productFpass.price)
  //         valorTotal = formateValueBr(productFpass.price - (productFpass.price * parseInt(item.discount, 10).toFixed(0) / 100));
  //       }
  //     })
  //   })
  // //apagar depois
  // console.log('valor total', valorTotal)
  // setCartsArrayOficial(valorTotal)

  // }, []);

  const handleActiveModal = (item) => {
    console.log(item);
    setDinamycCarSelected(item);
    setIsModalVisible(!isModalVisible);
  };

  const handleActiveToast = useCallback(() => {
    setIsToastVisible(true);
    setTimeout(() => {
      setIsToastVisible(false);
    }, 3000);
  }, [setIsToastVisible]);

  const handleWindowCopy = (value) => {
    navigator.clipboard.writeText(value);
    handleActiveToast();
  };

  const handleCopy = useCallback(
    (id) => {
      // const btnCopy = document.getElementById(id);
      // console.log(btnCopy.value);
      // btnCopy.select();
      // document.execCommand('copy');

      const copyText = document.querySelector(id);
      copyText.select();
      document.execCommand('copy');

      handleActiveToast();
    },
    [handleActiveToast]
  );

  const handleActive = (e, item) => {
    console.log(listCarDinamyc);
    e.preventDefault();
    const newDatas = listCarDinamyc.map((data) => {
      return data.id === item.id ? { ...data, active: !data.active } : data;
    });
    setListCarDinamyc(newDatas);
  };

  const handleSetExpansed = (e, item) => {
    console.log(listCarDinamyc);
    e.preventDefault();
    const newDatas = listCarDinamyc.map((data) => {
      return data.id === item.id
        ? { ...data, activeExpansed: !data.activeExpansed }
        : { ...data, activeExpansed: false };
    });
    setListCarDinamyc(newDatas);
  };

  const loadCars = useCallback(() => {
    getCars().then((response) => {
      const { data } = response.data;
      console.log(data);

      const res = data.map((item) => {
        const arrayUrl = item.url.split('/');
        console.log(arrayUrl[arrayUrl.length - 1]);

        return {
          id: item.id,
          name: item.name,
          discount: item.discount_applied,
          products: item.products,
          value: item.total,
          active: item.is_active,
          activeExpansed: false,
          linkCart: arrayUrl[arrayUrl.length - 1],
          url: item.url,
          linkThanks: `${item.name} - linkThanks`,
          status: 'toogle',
          action: 'action',
        };
      });

      setListCarDinamyc(res);
      setActiveLoading(false);

      // let arrayCars = []
      // let car

      // res.map(async (item) => {
      //   car = await showCar(item.id);
      //   arrayCars.push(car)
      //   setCartsArrayOficial(arrayCars)
      // })
    });
  }, []);

  useEffect(() => {
    loadCars();
  }, [loadCars]);

  const filteredItems = listCarDinamyc.filter(
    (item) => item.name && item.name.toLowerCase().includes(filterText.toLowerCase())
  );

  const indexOfLast = currentPage * perPage;
  const indexOfFirst = indexOfLast - perPage;
  const currentListCarDinamyc = filteredItems.slice(indexOfFirst, indexOfLast);

  const paginate = useCallback((pageNumber) => {
    setCurrentPage(pageNumber);
  }, []);

  const handleSelectCod = (value) => {
    const newValue = value.replace(/\s/g, '');
    setNameDinamicCar(newValue);

    let filtered = listCarDinamyc.filter(
      (item) => item.name && item.name.toLowerCase().includes(nameDinamicCar.toLowerCase())
    );
    if (filtered.length > 0) {
      setVerify('existente');
    } else {
      setVerify('liberado');
      filtered = [];
    }
  };

  // const loadNameDinamyCar = () => {
  //   if (verify === 'liberado') {
  //     dispatch(DinamicCarActions.setNameDinamicCar(nameDinamicCar));
  //     history.push('/playlist/create');
  //     setVerify('pause');

  //     return true;
  //   }
  //   return false;
  //   // setNameDinamicCar('');
  // };

  // const verifyNameDinamycCar = () => {
  //   let filtered = listCarDinamyc.filter(
  //     (item) => item.name && item.name.toLowerCase().includes(nameDinamicCar.toLowerCase())
  //   );
  //   if (filtered.length > 0) {
  //     setVerify('existente');
  //   } else {
  //     setVerify('liberado');
  //     filtered = [];
  //   }
  // };

  const handleVerify = () => {
    const filtered = listCarDinamyc.filter(
      (item) => item.name && item.name.toLowerCase().includes(nameDinamicCar.toLowerCase())
    );

    if (filtered.length > 0) {
      setVerify('existente');
    } else {
      setVerify('liberado');
    }

    if (verify === 'liberado') {
      dispatch(DinamicCarActions.setNameDinamicCar(nameDinamicCar));
      history.push('/playlist/create');
      setVerify('pause');
    }
    setNameDinamicCar('');

    // Promise.all([verifyNameDinamycCar()]).then(() => {
    //   loadNameDinamyCar();
    // });
  };

  if (activeLoading) {
    return <S.ImageLoadding />;
  }

  return (
    <>
      <S.ContainerCart>
        <S.CheckoutContainer theme={themeStore.themeName} className="createCheckout create" variant="normal">
          <S.CheckoutTitle theme={themeStore.themeName}>Criar uma nova Playlist</S.CheckoutTitle>

          <S.CheckoutContent>
            <S.CheckoutLabel theme={themeStore.themeName}>Qual o nome da sua playlist?</S.CheckoutLabel>
            <S.CheckoutInput
              id="cupomName"
              name="name"
              value={nameDinamicCar}
              onChange={(e) => handleSelectCod(e.target.value)}
              placeholder="Digite o nome"
              type="text"
              theme={themeStore.themeName}
            />
          </S.CheckoutContent>
          {verify === 'existente' ? <S.Incorrect>X Nome indisponível</S.Incorrect> : null}
          {verify === 'liberado' ? <S.Available>✓ Nome disponível</S.Available> : null}

          <S.CheckoutButtonGroup className="center">
            <S.CheckoutButton primaryColor={themeStore.primaryColor} variant="primary" onClick={() => handleVerify()}>
              <S.CheckoutText size="sm" variant="medium">
                criar nova
              </S.CheckoutText>
            </S.CheckoutButton>
          </S.CheckoutButtonGroup>
        </S.CheckoutContainer>

        <CartResume className="resume" variant="resume" image="ecommerce" />
      </S.ContainerCart>

      <FilterComponent onFilter={(e) => setFilterText(e.target.value)} filterText={filterText} />

      <S.ContainerDataTable theme={themeStore.themeName}>
        <S.TableHeader columnsGrid={6} theme={themeStore.themeName}>
          <S.TextCell theme={themeStore.themeName} className="text-upp">
            Nome da url
          </S.TextCell>
          <S.TextCell theme={themeStore.themeName} className="text-upp off-1">
            Desconto
          </S.TextCell>
          <S.TextCell theme={themeStore.themeName} className="text-upp">
            Valor Final
          </S.TextCell>
          <S.TextCell theme={themeStore.themeName} className="text-upp off-2">
            Link (url) da playlist
          </S.TextCell>
          <S.TextCell theme={themeStore.themeName} className="text-upp off-3">
            Nome da urlLink (url) obrigado
          </S.TextCell>
          <S.TextCell theme={themeStore.themeName} className="text-upp">
            Status
          </S.TextCell>
          <S.TextCell theme={themeStore.themeName} className="text-upp" />
          <S.TextCell theme={themeStore.themeName} className="text-upp btn-expansed" />
        </S.TableHeader>
        <S.TableBody>
          <>
            {currentListCarDinamyc.length <= 0 ? (
              <S.ContainerBackEndInfo>
                <S.InfoBackEnd theme={themeStore.themeName}>Não há playlists cadastradas</S.InfoBackEnd>
                {/* <S.ImageNotFound /> */}
              </S.ContainerBackEndInfo>
            ) : (
              <>
                {currentListCarDinamyc.map((item) => {
                  return (
                    <>
                      <S.TableLine theme={themeStore.themeName} key={`${item.id.toString()}idUser`}>
                        <S.TableCell theme={themeStore.themeName} className="text-upp">
                          {item.name}
                        </S.TableCell>
                        <S.TableCell theme={themeStore.themeName} className="text-upp off-1">
                          {`${parseInt(item.discount, 10).toFixed(0)}%`}
                        </S.TableCell>
                        <S.TableCell theme={themeStore.themeName} className="text-upp">
                          {item.value}
                        </S.TableCell>
                        <S.TableCell theme={themeStore.themeName} className="text-upp off-2">
                          <S.ButtonCopy onClick={() => handleCopy(`#${item.linkCart}`)}>
                            <S.InputCopy value={item.url} id={item.linkCart} type="text" onChange={() => {}} />
                            <div />
                          </S.ButtonCopy>
                        </S.TableCell>
                        <S.TableCell theme={themeStore.themeName} className="text-upp off-3">
                          <S.ButtonCopy
                            onClick={() => handleWindowCopy(`Esse texto copiado do - ${item.name} - linkThanks`)}
                          >
                            <S.InputCopy
                              type="text"
                              id={`${item.name}linkThanks`}
                              onChange={() => {}}
                              value={`Esse texto copiado do - ${item.name} - linkThanks`}
                            />
                            <div />
                          </S.ButtonCopy>
                        </S.TableCell>
                        <S.TableCell theme={themeStore.themeName} className="text-upp">
                          <a href="#!" onClick={(e) => handleActive(e, item)} className="toggle-switch">
                            <ButtonToggleSwitch checked={item.active} onChange={() => {}} />
                          </a>
                        </S.TableCell>
                        <S.TableCell theme={themeStore.themeName} className="text-upp">
                          <S.Button onClick={() => handleActiveModal(item)}>
                            <div className="button-edit" />
                          </S.Button>
                        </S.TableCell>
                        <S.TableCell theme={themeStore.themeName} className="text-upp off-button-set btn-expansed">
                          <a href="#!" onClick={(e) => handleSetExpansed(e, item)}>
                            <S.ButtonExpansed src={Arrow} active={item.activeExpansed} />
                          </a>
                        </S.TableCell>
                      </S.TableLine>
                      {item.activeExpansed ? (
                        <S.TableLineExpansed theme={themeStore.themeName} className="activeExpansed">
                          <S.TableCell theme={themeStore.themeName} className="text-upp">
                            <S.ExpansedContent>
                              <h4>DESCONTO</h4>
                              <span>{item.discount} %</span>
                            </S.ExpansedContent>
                          </S.TableCell>
                          <S.TableCell theme={themeStore.themeName} className="text-upp">
                            <S.ExpansedContent>
                              <h4>Link (URL) Carrinho</h4>
                              <S.ButtonCopy onClick={() => handleWindowCopy(item.url)}>
                                <S.InputCopy
                                  value={item.url}
                                  id={`${item.linkCart}linkCartExpansed`}
                                  type="text"
                                  onChange={() => {}}
                                />
                                <div />
                              </S.ButtonCopy>
                            </S.ExpansedContent>
                          </S.TableCell>
                          <S.TableCell theme={themeStore.themeName} className="text-upp">
                            <S.ExpansedContent>
                              <h4>Link (URL) Obrigado</h4>
                              <S.ButtonCopy
                                onClick={() => handleWindowCopy(`Esse texto copiado do - ${item.name} - linkThanks`)}
                              >
                                <S.InputCopy
                                  type="text"
                                  id={`${item.name}linkThanksExpansed`}
                                  onChange={() => {}}
                                  value={`Esse texto copiado do - ${item.name} - linkThanks`}
                                />
                                <div />
                                {/* <img id={item.website} src={IconCopy} alt={item.name} data-copy={item.website} /> */}
                              </S.ButtonCopy>
                            </S.ExpansedContent>
                          </S.TableCell>
                        </S.TableLineExpansed>
                      ) : null}
                    </>
                  );
                })}
              </>
            )}
          </>
        </S.TableBody>
      </S.ContainerDataTable>

      <S.ContainerPagination className="pagination">
        <Pagination currentPage={currentPage} perPage={perPage} total={listCarDinamyc.length} paginate={paginate} />
      </S.ContainerPagination>
      <Toast
        variant="success"
        icon="success"
        showIcon
        isVisible={isToastVisible}
        onClose={() => setIsToastVisible(!isToastVisible)}
      >
        Link copiado com sucesso!!
      </Toast>
      <ModalEditCar
        dinamycCar={dinamycCarSelected}
        isModalVisible={isModalVisible}
        setIsModalVisible={setIsModalVisible}
      />
    </>
  );
};

export default Cupom;
