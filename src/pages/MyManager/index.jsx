import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import { Actions as MyManagerActions } from '@redux/ducks/myManager';

import Profile from '@components/atoms/Cards/profile';

import * as S from './styled';

const Withdraw = () => {
  const dispatch = useDispatch();
  const myManager = useSelector((state) => state.myManager);
  // const poupup = useSelector((state) => state.poupup);

  useEffect(() => {
    dispatch(MyManagerActions.getMyManager());
  }, [dispatch]);
  return (
    <>
      <S.Container>
        <Profile
          image={myManager.manager.image}
          name={myManager.manager.name}
          work={myManager.manager.work}
          email={myManager.manager.email}
          telephony={myManager.manager.telephony}
          phone={myManager.manager.phone}
        />
      </S.Container>
      {/* {poupup.success.visible ? <Success title={poupup.success.title} description={poupup.success.description} /> : ''}
      {poupup.extendPeriod.visible ? (
        <ExtendPeriod
          image={poupup.extendPeriod.image}
          title={poupup.extendPeriod.title}
          description={poupup.extendPeriod.description}
          descriptionMobile={poupup.extendPeriod.descriptionMobile}
        />
      ) : (
        ''
      )} */}
    </>
  );
};

export default Withdraw;
