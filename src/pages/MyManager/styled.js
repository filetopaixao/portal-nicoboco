import styled from 'styled-components';

export const Container = styled.div`
  a {
    width: 100%;

    margin-bottom: 3px;
  }

  button {
    text-align: left;

    margin-bottom: 10px;

    width: 100%;
    height: 53px;
  }
`;

export default Container;
