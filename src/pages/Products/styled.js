import styled, { css } from 'styled-components';
import { shade } from 'polished';

import PageLoadding from '@assets/images/page_loadding.gif';
import ArrowTop from '@assets/images/arrowTop.svg';
import IconCopy from '@assets/images/copy.svg';

import { Container, Input } from '@components/atoms';

const containerResultOps = {
  selected: () => css`
    opacity: 1;
  `,
  not_selected: () => css`
    opacity: 0.5;

    button {
      opacity: 1;

      border: 1px solid #bdbdbd;

      background: #fff;
      color: #bdbdbd;
    }
  `,
};

const collapsesCssOp = {
  opMax600: () => css`
    background: transparent;

    @media (max-width: 600px) {
      padding: inherit;
    }
  `,
};

export const ContainerPage = styled(Container)`
  select {
    border: ${(props) => (props.theme === 'Dark' ? '1px solid #2F3743' : '1px solid #EAEAEA')};

    background: ${(props) => (props.theme === 'Dark' ? '#29303A' : '#FBFBFB')};

    color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#CECECE')};

    ::placeholder {
      color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
    }

    cursor: pointer;
  }

  .purchase {
    &__status {
      &__commision,
      &__discount {
        height: auto;
      }

      &__commision {
        margin-bottom: 10px;
      }
    }
  }

  .text_title {
    text-align: center;

    margin: 0 auto;

    color: ${(props) => (props.theme === 'Dark' ? '#fff' : '#484646')};
  }
`;

export const ContainerStatus = styled.div`
  display: block;

  width: 100%;

  div.purchase {
    margin: 0 auto;
    margin-bottom: 20px;

    @media (min-width: 1300px) {
      margin: inherit;

      &:first-child {
        margin-right: 15px;
        // background: transparent linear-gradient(261deg, #28bc8f 0%, #6aff6f 100%) 0% 0% no-repeat padding-box;
      }
      &:last-child {
        margin-left: 15px;
        // background: transparent linear-gradient(262deg, #e351d0 0%, #9355d9 100%) 0% 0% no-repeat padding-box;
      }
    }
  }

  @media (min-width: 1300px) {
    display: flex;
    justify-content: space-between;
  }
`;

export const ContainerCollapses = styled.div``;

export const Content = styled.div`
  width: 100%;

  margin-top: 20px;

  > div:first-child {
    padding: 15px 15px;
    margin-bottom: 20px;

    ${(props) => {
      if (props.order) return collapsesCssOp.opMax600;
      if (props.theme === 'Dark') return 'background: #202731; padding: 30px 30px 10px;';
      return 'background: #fff; padding: 30px 30px 10px;';
    }}
    @media (max-width: 800px) {
      padding: 15px 15px;
    }
  }

  > div:last-child {
    padding: 10px 15px;

    border: ${(props) => (props.theme === 'Dark' ? '1px solid #2F3743' : '1px solid #EAEAEA')};

    background: ${(props) => (props.theme === 'Dark' ? '#202731' : '#FFFFFF')};

    color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
  }

  > div {
    padding: 5px;

    border-radius: 15px;
  }

  .full_width {
    width: 100%;
  }

  @media (min-width: 1250px) {
    display: grid;
    /* grid-template-columns: 1fr 594px; */
    grid-template-columns: 1fr 40%;
    grid-column-gap: 30px;
  }
`;

export const ContainerPurchaseListProducts = styled(Container)`
  > p {
    margin-top: 20px;

    color: ${(props) => (props.theme === 'Dark' ? '#fff' : '#484646')};
  }

  select {
    margin-bottom: 10px;
  }

  div#filter_container {
    width: 100%;

    margin: 10px 0px;
    padding: 0;

    border: none;
    border-radius: 10px;
    opacity: 1;

    background: inherit;

    ::after {
      margin-left: 20px;
    }

    input {
      padding-left: 20px;

      opacity: 1;

      border: ${(props) => (props.theme === 'Dark' ? '1px solid #2F3743' : '1px solid #EAEAEA')};

      background: ${(props) => (props.theme === 'Dark' ? '#29303A' : '#FBFBFB')};

      color: ${(props) => (props.theme === 'Dark' ? '#47505E' : '#CECECE')};

      ::placeholder {
        color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
      }
    }

    select {
      padding-left: 20px;

      opacity: 1;

      border: ${(props) => (props.theme === 'Dark' ? '1px solid #2F3743' : '1px solid #EAEAEA')};

      background: ${(props) => (props.theme === 'Dark' ? '#29303A' : '#FBFBFB')};

      color: ${(props) => (props.theme === 'Dark' ? '#47505E' : '#CECECE')};

      ::placeholder {
        color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
      }
    }
  }
`;

export const ContainerListProducts = styled(Container)`
  margin-top: 10px;

  > div {
    max-height: 470px;
    min-height: 400px;
    max-width: 1100px;

    padding: 10px;

    overflow-y: scroll;

    border-radius: initial;

    border: ${(props) => (props.theme === 'Dark' ? '1px solid #2F3743' : '1px solid #EAEAEA')};

    background: ${(props) => (props.theme === 'Dark' ? '#29303A' : '#FBFBFB')};

    color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};

    @media (max-width: 1500px) and (min-width: 600px) {
      display: grid;
      grid-template-columns: 1fr 1fr 1fr;
      width: 500px;
      margin: 0 auto;
    }

    @media (min-width: 1500px) {
      display: grid;
      grid-template-columns: 1fr 1fr 1fr 1fr;

      margin: 0 auto;
    }

    ::-webkit-scrollbar {
      -webkit-appearance: none;
    }
    ::-webkit-scrollbar:vertical {
      width: 14px;
    }
    ::-webkit-scrollbar:horizontal {
      height: 14px;
    }
    ::-webkit-scrollbar-thumb {
      border-radius: 3px;

      background-color: ${(props) => (props.theme === 'Dark' ? '#47505E' : '#CECECE')};
    }
    ::-webkit-scrollbar-track {
      border-radius: 10px;
    }
  }
`;

export const ContainerPurchaseList = styled(Container)``;

export const ContainerPurchaseListSelected = styled(Container)`
  display: block;

  max-height: auto;
  @media (min-width: 800px) {
    max-height: 620px;
  }

  div:first-child {
    display: flex;
    justify-content: flex-end;
  }
`;

export const LiTable = styled.li`
  &.off-title {
    @media (max-width: 800px) {
      display: none;
    }
  }

  &.align-center {
    align-items: center;
    display: flex;
    justify-content: center;
  }

  button {
    width: 25px;
    height: 25px;
    font-weight: bold;

    border-radius: 5px;
    border: 0;

    background: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#CECECE')};
    color: ${(props) => (props.theme === 'Dark' ? '#29303A' : '#82868D')};
  }
`;

export const ContainerTable = styled.div`
  border: ${(props) => (props.theme === 'Dark' ? '1px solid #2F3743' : '1px solid #EAEAEA')};

  background: ${(props) => (props.theme === 'Dark' ? '#29303A' : '#FBFBFB')};

  color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};

  margin-top: 25px;

  li {
    color: ${(props) => (props.theme === 'Dark' ? '#fff' : '#484646')};

    font-size: 14px;
    font-weight: bold;
  }

  .table_header_purchase {
    li {
      text-transform: uppercase;
    }
  }

  > div:first-child {
    ul {
      display: flex;
      justify-content: space-between;

      padding: 10px 15px 0 10px;
      margin-bottom: 20px;

      width: 100%;

      list-style: none;
    }
  }

  > div.table_body_purchase {
    min-height: 300px;
    height: 100%;
    max-height: 380px;

    overflow-y: scroll;

    ::-webkit-scrollbar {
      -webkit-appearance: none;
    }
    ::-webkit-scrollbar:vertical {
      width: 14px;
    }
    ::-webkit-scrollbar:horizontal {
      height: 14px;
    }
    ::-webkit-scrollbar-thumb {
      border-radius: 3px;

      background-color: ${(props) => (props.theme === 'Dark' ? '#47505E' : '#CECECE')};
    }
    ::-webkit-scrollbar-track {
      border-radius: 10px;
    }

    ul {
      display: flex;
      justify-content: space-between;

      width: 100%;

      padding: 0 0px 0 10px;
      margin: 0 5px 30px 0;

      list-style: none;

      li.btn-bigger-one {
        display: flex;
        justify-content: space-around;

        width: 60px;
        height: 40px;

        border-radius: 9px;
        border: ${(props) => (props.theme === 'Dark' ? '1px solid #47505E' : '1px solid #EAEAEA')};

        background: ${(props) => (props.theme === 'Dark' ? '#29303A' : '#FBFBFB')};

        color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
      }

      li {
        max-width: 60px;
        width: 100%;
        height: 40px;

        > div.btn-group {
          display: block;

          button {
            display: block;

            border: none;

            background: transparent;

            cursor: pointer;
          }

          button:first-child {
            &:after {
              content: '';
              clear: both;
              display: block;
              visibility: visible;
              align-self: center;

              height: 10px;
              width: 10px;

              position: relative;
              right: 0px;
              bottom: 0;
              opacity: 1;
              z-index: 1;

              background: url(${ArrowTop}) no-repeat;
              background-size: contain;
            }
          }

          button:last-child {
            &:after {
              content: '';
              clear: both;
              display: block;
              visibility: visible;
              align-self: center;

              height: 10px;
              width: 10px;

              position: relative;
              right: 0px;
              bottom: 0;
              opacity: 1;
              z-index: 1;

              background: url(${ArrowTop}) no-repeat;
              background-size: contain;
              transform: rotate(180deg);
            }
          }

          button:hover {
            background: ${shade(-0.2, '#F2F2F8')};
          }
        }

        img {
          display: flex;
          justify-content: center;
          align-items: center;

          width: 40px;
          height: auto;

          margin: 0 auto;
        }
      }
    }

    @media (max-width: 1400px) {
      font-size: 12px;
    }
  }
`;

export const ContainerResult = styled.div`
  text-align: -webkit-right;
  display: flex;
  justify-content: flex-end;

  width: 100%;

  margin-top: 15px;
  padding-right: 15px;

  ${(props) => containerResultOps[props.productsSelected]}

  section {
    max-width: 240px;
    width: 100%;

    @media (max-width: 800px) {
      height: 200px;
    }

    ul {
      max-width: 275px;

      margin: 0 auto;
      margin-bottom: 20px;

      li {
        display: flex;
        justify-content: space-between;

        p,
        h3 {
          text-transform: uppercase;
          font-weight: bold;
          color: ${(props) => (props.theme === 'Dark' ? '#fff' : '#484646')};
        }
      }
    }

    button {
      text-align: center;

      max-width: 286px;
      width: 100%;

      margin: 0 auto;

      background: #2fdf46;
    }

    @media (min-width: 1400px) {
      max-width: 286px;
    }
  }
`;

export const ContainerPurchaseFinalized = styled.div`
  text-align: center;

  width: 100%;
  min-height: 550px;

  padding: 50px;
  margin-top: 20px;

  border-radius: 10px;
  border: ${(props) => (props.theme === 'Dark' ? '1px solid #2F3743' : '1px solid #EAEAEA')};

  background: ${(props) => (props.theme === 'Dark' ? '#202731' : '#FFFFFF')};

  button:hover {
    opacity: 1;
  }

  button {
    text-align: center;
    font: normal normal medium 18px/27px Poppins;
    letter-spacing: 0px;
    text-transform: uppercase;

    border: none;
    border-radius: 15px;

    cursor: pointer;

    color: #fff;
  }

  .btn_pdf {
    height: 40px;
    width: 100%;

    @media (min-width: 600px) {
      max-width: 360px;
    }

    background: #0b58e0 0% 0% no-repeat padding-box;
    box-shadow: 0px 3px 6px #00000029;

    border-radius: 15px;

    opacity: 1;

    transition: all 0.2s;
  }

  .btn_pdf:hover {
    background: ${shade(0.2, '#0b58e0')};
  }

  .btn_relatorios {
    height: 40px;
    width: 100%;

    @media (min-width: 600px) {
      max-width: 360px;
    }

    opacity: 0.5;

    background: #e1dede 0% 0% no-repeat padding-box;
  }

  .button_group {
    @media (min-width: 992px) {
      button:first-child {
        margin-right: 20px;
      }

      button:last-child {
        margin-left: 20px;
      }
    }
  }

  header {
    h2 {
      margin-bottom: 20px;
      color: ${(props) => (props.theme === 'Dark' ? '#fff' : '#484646')};
    }

    p {
      color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
      font-weight: bold;
    }
  }

  section {
    img {
      width: 200px;
      height: auto;

      margin: 40px auto;
    }

    p {
      font-weight: bold;
      color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
    }

    .mt100 {
      margin-top: 100px;
      button {
        background: ${(props) => props.primaryColor};
      }
    }

    .mt30 {
      margin-top: 30px;
    }
  }
`;

export const SCInput = styled(Input)`
  text-decoration: none;
  font-size: 14px;
  letter-spacing: 6px;

  width: 100%;
  height: 30px;

  padding: 0;
  padding-left: 5px;

  border: none;
  cursor: pointer;

  background: transparent;
  opacity: 1;
  color: #5e5e5e;

  &:focus {
    outline: thin dotted;
    outline: 0px auto -webkit-focus-ring-color;
    outline-offset: 0px;
  }
`;

export const CopyButton = styled.button`
  width: 30px;
  height: 30px;

  border: none;
  outline: none;

  background: url(${IconCopy}) no-repeat;
  cursor: pointer;
`;

export const ContentInput = styled.div`
  display: flex;
  align-items: center;

  width: 100%;
  height: 40px;
  padding: 5px 10px;

  border-radius: 10px;
  border: 1px solid #cdcdcd;

  background: #f1eeee 0% 0% no-repeat padding-box;

  transition: border-color 0.3s;

  > div {
    width: 100%;
  }

  @media (max-width: 992px) {
    width: 100%;
  }
`;

export const ImageLoadding = styled.div`
  content: url(${PageLoadding});
  width: 20rem;
  height: auto;

  margin-top: 30px;
  margin: 0 auto;

  background-size: contain;
  background-repeat: no-repeat;

  @media (max-width: 600px) {
    width: 16rem;
    height: auto;
  }
`;
