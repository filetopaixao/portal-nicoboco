import styled from 'styled-components';
import { Container as Ct } from '@components/atoms/Container/styled';

export const Container = styled(Ct)`
  display: flex;
  justify-content: center;
  align-items: center;

  width: 100%;
  height: 100vh;
  padding: 20px;

  background: ${(props) => (props.theme === 'Dark' ? '#111A21' : '#F5F6FA')};
  background-size: cover;
  background-attachment: fixed;
`;

export const ModalContent = styled.div`
  display: inline-table;

  color: ${(props) => (props.theme === 'Dark' ? '#fff' : '#484646')};
`;

export const ModalText = styled.div`
  text-align: justify;

  height: calc(100vh - 21rem);
  margin-bottom: 30px;
  padding: 10px;

  overflow: auto;

  color: ${(props) => (props.theme === 'Dark' ? '#fff' : '#484646')};

  @media (min-width: 600px) {
    height: calc(100vh - 25rem);
  }

  ::-webkit-scrollbar {
    -webkit-appearance: none;
  }
  ::-webkit-scrollbar:vertical {
    width: 12px;
  }
  ::-webkit-scrollbar:horizontal {
    height: 12px;
  }
  ::-webkit-scrollbar-thumb {
    border-radius: 10px;
    border: 2px solid #ffffff;

    background-color: rgba(0, 0, 0, 0.5);
  }
  ::-webkit-scrollbar-track {
    border-radius: 10px;

    background-color: #ffffff;
  }
`;

export const ModalButton = styled.div`
  display: flex;
  justify-content: center;

  width: 100%;
  margin-top: 20px;
`;
