import React from 'react';
import { useSelector } from 'react-redux';
import ContainerForm from '@components/atoms/Container/login';
import ForgotPassForm from '@components/molecules/Forms/ForgotPass';
import * as S from './styled';

const ForgotPass = () => {
  const themeStore = useSelector((state) => state.theme);
  return (
    <S.Container theme={themeStore.themeName}>
      <ContainerForm
        title="Problemas no Login?"
        subtitle="Para redefinir sua senha, digite o endereço de e-mail que você usa para fazer login na EVER."
        backTo="/login"
      >
        <ForgotPassForm redirectTo="/confirmation" />
      </ContainerForm>
    </S.Container>
  );
};

export default ForgotPass;
