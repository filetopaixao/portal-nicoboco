import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';

import Modal from '@components/molecules/Modals/ModalComponent';
import { Checkbox, Button } from '@components/atoms';

import { authentication } from '@services/auth';

import * as S from './styled';

const TermEUA = () => {
  const themeStore = useSelector((state) => state.theme);
  const [isModalVisible, setIsModalVisible] = useState(true);
  const [checked, setChecked] = useState(false);
  const [variant, setVariant] = useState('disabled');
  const history = useHistory();

  const handleInputChange = () => {
    setChecked(!checked);
    if (!checked) return setVariant('primary');
    return setVariant('disabled');
  };

  const redirectToLogin = () => history.push('/login');

  const handleCloser = () => {
    localStorage.removeItem('data_user_email');
    localStorage.removeItem('data_user_password');
    setIsModalVisible(!isModalVisible);
    redirectToLogin();
  };

  const handleConfirmation = () => {
    const email = localStorage.getItem('data_user_email');
    const password = localStorage.getItem('data_user_password');
    authentication({ email, password }).then(() => {
      localStorage.removeItem('data_user_email');
      localStorage.removeItem('data_user_password');
      history.push('/');
    });
  };

  return (
    <S.Container>
      <Modal
        alignCenter
        MaxHeight="80vh"
        isVisible={isModalVisible}
        title="Term of Agreement - United States"
        onClose={() => handleCloser()}
      >
        <S.ModalContent>
          <S.ModalText theme={themeStore.themeName}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras posuere, purus vitae laoreet sodales, metus
            nunc sodales turpis, id molestie nisl nulla sed purus. Etiam feugiat sem nec urna bibendum rutrum. Sed magna
            massa, egestas non maximus nec, ultricies a risus. Suspendisse ut velit vitae turpis fermentum pharetra a at
            elit. Vivamus sollicitudin nunc leo, a gravida diam congue posuere. Donec posuere, eros ac semper malesuada,
            orci magna vestibulum turpis, in dictum dui tortor vel nunc. Phasellus et lectus nisi. Nam mattis blandit
            felis ut mollis. Duis sed fringilla nunc. Aenean quis metus id lorem rhoncus finibus sagittis ut augue.
            Vivamus feugiat et enim non placerat. Pellentesque scelerisque vestibulum neque at volutpat. Sed ut dapibus
            nunc, ut placerat turpis. Vestibulum diam turpis, placerat ut vulputate vel, fringilla nec mi. Aliquam
            tristique turpis sed velit blandit eleifend. Ut nulla velit, hendrerit volutpat ullamcorper nec, consequat
            ut tortor. Aliquam efficitur porta nulla. Phasellus pretium ante at elit tempus commodo. Nulla convallis
            lacus non est vulputate, vitae pharetra felis ullamcorper. Suspendisse sed augue facilisis, tristique tellus
            ut, luctus neque. Sed malesuada, tellus non commodo hendrerit, felis sapien facilisis libero, eget tincidunt
            magna nulla ut orci. Mauris fringilla tellus diam, sed eleifend justo fermentum at. Curabitur pellentesque
            turpis lacus, non vulputate elit venenatis sed. Pellentesque sit amet eleifend massa, ac facilisis tellus.
            Ut eu massa sit amet ipsum vestibulum tristique. Nulla dignissim purus sit amet diam mattis pretium id in
            sapien. Aenean luctus lacinia varius. Quisque sit amet risus urna. Pellentesque mollis felis tincidunt
            bibendum congue. Fusce vitae vulputate augue. Duis imperdiet est ut ex porta, a venenatis lorem euismod.
            Nulla viverra ex ex. Aenean ut laoreet purus. Donec tristique nulla egestas diam sollicitudin eleifend.
            Mauris leo est, dapibus mattis odio in, dapibus congue mi. Fusce pellentesque ligula at est interdum, at
            lacinia neque lobortis. Vestibulum scelerisque magna magna, quis rutrum urna fermentum vel. Curabitur
            consectetur molestie nisi eget scelerisque. Curabitur tincidunt elit non quam molestie aliquet. Praesent
            accumsan quis diam ac pharetra. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere
            cubilia curae; In ac maximus tellus. Nulla consectetur magna tellus. Praesent sollicitudin gravida urna eu
            finibus. Proin vestibulum risus vel turpis porta sodales. Duis volutpat orci nunc, a varius lacus interdum
            quis. Phasellus ac luctus lectus. Fusce consectetur orci in ligula imperdiet, id venenatis nisl iaculis.
            Nullam congue, elit vel sodales mattis, odio nibh tincidunt mi, vitae ornare felis ipsum id ex. Nullam a
            urna a velit ultrices faucibus. Nam vehicula blandit volutpat. Suspendisse mattis laoreet dolor in
            scelerisque. Nullam non nisl quis odio congue gravida. Suspendisse dolor nibh, tristique a sollicitudin eu,
            tristique mollis tortor. Phasellus lacinia a nisl vitae porta. Ut ultricies eros eget erat imperdiet
            sollicitudin vitae in tortor. Ut orci augue, fermentum sagittis imperdiet sed, porttitor sed justo. Lorem
            ipsum dolor sit amet, consectetur adipiscing elit. Cras posuere, purus vitae laoreet sodales, metus nunc
            sodales turpis, id molestie nisl nulla sed purus. Etiam feugiat sem nec urna bibendum rutrum. Sed magna
            massa, egestas non maximus nec, ultricies a risus. Suspendisse ut velit vitae turpis fermentum pharetra a at
            elit. Vivamus sollicitudin nunc leo, a gravida diam congue posuere. Donec posuere, eros ac semper malesuada,
            orci magna vestibulum turpis, in dictum dui tortor vel nunc. Phasellus et lectus nisi. Nam mattis blandit
            felis ut mollis. Duis sed fringilla nunc. Aenean quis metus id lorem rhoncus finibus sagittis ut augue.
            Vivamus feugiat et enim non placerat. Pellentesque scelerisque vestibulum neque at volutpat. Sed ut dapibus
            nunc, ut placerat turpis. Vestibulum diam turpis, placerat ut vulputate vel, fringilla nec mi. Aliquam
            tristique turpis sed velit blandit eleifend. Ut nulla velit, hendrerit volutpat ullamcorper nec, consequat
            ut tortor. Aliquam efficitur porta nulla. Phasellus pretium ante at elit tempus commodo. Nulla convallis
            lacus non est vulputate, vitae pharetra felis ullamcorper. Suspendisse sed augue facilisis, tristique tellus
            ut, luctus neque. Sed malesuada, tellus non commodo hendrerit, felis sapien facilisis libero, eget tincidunt
            magna nulla ut orci. Mauris fringilla tellus diam, sed eleifend justo fermentum at. Curabitur pellentesque
            turpis lacus, non vulputate elit venenatis sed. Pellentesque sit amet eleifend massa, ac facilisis tellus.
            Ut eu massa sit amet ipsum vestibulum tristique. Nulla dignissim purus sit amet diam mattis pretium id in
            sapien. Aenean luctus lacinia varius. Quisque sit amet risus urna. Pellentesque mollis felis tincidunt
            bibendum congue. Fusce vitae vulputate augue. Duis imperdiet est ut ex porta, a venenatis lorem euismod.
            Nulla viverra ex ex. Aenean ut laoreet purus. Donec tristique nulla egestas diam sollicitudin eleifend.
            Mauris leo est, dapibus mattis odio in, dapibus congue mi. Fusce pellentesque ligula at est interdum, at
            lacinia neque lobortis. Vestibulum scelerisque magna magna, quis rutrum urna fermentum vel. Curabitur
            consectetur molestie nisi eget scelerisque. Curabitur tincidunt elit non quam molestie aliquet. Praesent
            accumsan quis diam ac pharetra. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere
            cubilia curae; In ac maximus tellus. Nulla consectetur magna tellus. Praesent sollicitudin gravida urna eu
            finibus. Proin vestibulum risus vel turpis porta sodales. Duis volutpat orci nunc, a varius lacus interdum
            quis. Phasellus ac luctus lectus. Fusce consectetur orci in ligula imperdiet, id venenatis nisl iaculis.
            Nullam congue, elit vel sodales mattis, odio nibh tincidunt mi, vitae ornare felis ipsum id ex. Nullam a
            urna a velit ultrices faucibus. Nam vehicula blandit volutpat. Suspendisse mattis laoreet dolor in
            scelerisque. Nullam non nisl quis odio congue gravida. Suspendisse dolor nibh, tristique a sollicitudin eu,
            tristique mollis tortor. Phasellus lacinia a nisl vitae porta. Ut ultricies eros eget erat imperdiet
            sollicitudin vitae in tortor. Ut orci augue, fermentum sagittis imperdiet sed, porttitor sed justo.
          </S.ModalText>
          <div>
            <Checkbox onChange={handleInputChange} text="I accept the Term of Agreement" />
          </div>
          <S.ModalButton>
            <Button variant={variant} onClick={() => handleConfirmation()}>
              continue
            </Button>
          </S.ModalButton>
        </S.ModalContent>
      </Modal>
    </S.Container>
  );
};

export default TermEUA;
