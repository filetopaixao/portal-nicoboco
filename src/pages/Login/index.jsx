import React from 'react';
import { useSelector } from 'react-redux';
import ContainerForm from '@components/atoms/Container/login';
import LoginForm from '@components/molecules/Forms/Login';
import * as S from './styled';

const LoginPage = () => {
  const themeStore = useSelector((state) => state.theme);
  return (
    <S.Container theme={themeStore.themeName}>
      <ContainerForm title="Olá," subtitle="seu escritório digital te aguarda!">
        <LoginForm redirectTo="/termBR" />
      </ContainerForm>
    </S.Container>
  );
};

export default LoginPage;
