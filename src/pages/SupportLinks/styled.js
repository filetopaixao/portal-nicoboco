import styled, { css } from 'styled-components';
import { shade } from 'polished';

import PageLoadding from '../../assets/images/page_loadding.gif';
import NotFound from '../../assets/images/erro-404.svg';
import Visao from '../../assets/images/visao.svg';
import Missao from '../../assets/images/missao.svg';
import Valores from '../../assets/images/valores.svg';
import Historia from '../../assets/images/historia.svg';
import Catalogo from '../../assets/images/catalogo.svg';
import Ingredientes from '../../assets/images/ingredientes.svg';
import Qualidade from '../../assets/images/qualidade.svg';
import Producao from '../../assets/images/producao.svg';
import EmpreendedorVantagens from '../../assets/images/empreendedor_vantagens.svg';
import ParceirosVantagens from '../../assets/images/parceiros_vantagens.svg';
import EmpreendedorOquee from '../../assets/images/empreendedor_oquee.svg';
import ParceirosOquee from '../../assets/images/parceiros_oquee.svg';
import EmpreendedorGanhos from '../../assets/images/empreendedor_ganhos.svg';
import ParceirosGanhos from '../../assets/images/parceiros_ganhos.svg';
import EmpreendedorPlanoDeCrescimento from '../../assets/images/empreendedor_plano_de_crescimento.svg';
import ParceirosPlanoDeCrescimento from '../../assets/images/parceiros_plano_de_crescimento.svg';

const images = {
  visao: () => css`
    content: url(${Visao});
  `,
  missao: () => css`
    content: url(${Missao});
  `,
  valores: () => css`
    content: url(${Valores});
  `,
  historia: () => css`
    content: url(${Historia});
  `,
  catalogo: () => css`
    content: url(${Catalogo});
  `,
  ingredientes: () => css`
    content: url(${Ingredientes});
  `,
  qualidade: () => css`
    content: url(${Qualidade});
  `,
  producao: () => css`
    content: url(${Producao});
  `,
  empreendedorVantagens: () => css`
    content: url(${EmpreendedorVantagens});
  `,
  parceirosVantagens: () => css`
    content: url(${ParceirosVantagens});
  `,
  empreendedorOquee: () => css`
    content: url(${EmpreendedorOquee});
  `,
  parceirosOquee: () => css`
    content: url(${ParceirosOquee});
  `,
  empreendedorGanhos: () => css`
    content: url(${EmpreendedorGanhos});
  `,
  parceirosGanhos: () => css`
    content: url(${ParceirosGanhos});
  `,
  empreendedorPlanoDeCrescimento: () => css`
    content: url(${EmpreendedorPlanoDeCrescimento});
  `,
  parceirosPlanoDeCrescimento: () => css`
    content: url(${ParceirosPlanoDeCrescimento});
  `,
};

export const Image = styled.div`
  ${(props) => images[props.image]}
  width: 50px;
  height: 50px;

  margin-top: 30px;
  margin: 0 auto;

  background-size: contain;
  background-repeat: no-repeat;
`;

export const TitleContainerLink = styled.p`
  text-align: left;
  letter-spacing: 0px;
  color: ${(props) => (props.theme === 'Dark' ? '#fff' : '#484646')};
  text-transform: uppercase;
`;

export const ContainerLinks = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr 1fr;
  column-gap: 20px;

  padding: 30px 50px;
  margin-bottom: 20px;

  border-radius: 15px;
  border: ${(props) => (props.theme === 'Dark' ? '1px solid #2F3743' : '1px solid #EAEAEA')};

  background: ${(props) => (props.theme === 'Dark' ? '#202731' : '#FFFFFF')};

  color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
`;

export const CardLink = styled.a`
  text-decoration: none;
  cursor: pointer;
`;

export const ContentLink = styled.div`
  text-align: center;

  //min-height: 200px;
  min-width: 200px;
  padding: 20px 27px 20px 27px;

  border-radius: 15px;
  background: ${(props) => (props.primaryColor ? props.primaryColor : props.secondaryColor)};

  /*
  &:hover {
    background: ${shade(0.1, '#01a3ff')};
  }
  */
`;

export const TextCard = styled.p`
  text-align: center;
  letter-spacing: 0px;
  color: #ffffff;
  text-transform: uppercase;
  font-size: 17px;
  font-weight: bold;
  margin-top: 10px;
`;

export const ContainerBackEndInfo = styled.div`
  padding: 20px;
`;

export const InfoBackEnd = styled.h2`
  color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
`;

export const ImageNotFound = styled.div`
  content: url(${NotFound});
  width: 18rem;
  height: auto;

  margin-top: 30px;
  margin: 0 auto;

  background-size: contain;
  background-repeat: no-repeat;

  @media (min-width: 1200px) {
    width: 16rem;
    height: auto;
  }
`;

export const ImageLoadding = styled.div`
  content: url(${PageLoadding});
  width: 20rem;
  height: auto;

  margin-top: 30px;
  margin: 0 auto;

  background-size: contain;
  background-repeat: no-repeat;

  @media (max-width: 600px) {
    width: 16rem;
    height: auto;
  }
`;
