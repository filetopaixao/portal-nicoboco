import React, { useState, useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import Container from '@components/atoms/Container';
import Label from '@components/atoms/Label';
import Input from '@components/atoms/Input';
import Select from '@components/atoms/Select';
import Button from '@components/atoms/Button';

import { Actions as ThemeActions } from '@redux/ducks/theme';

import * as S from './styled';

const Admin = () => {
  const dispatch = useDispatch();
  const themeStore = useSelector((state) => state.theme);

  const [themeName, setThemeName] = useState('');
  const [themeColor, setThemeColor] = useState('');
  const [primaryColor, setPrimaryColor] = useState('');
  const [secondaryColor, setSecondaryColor] = useState('');
  const [tertiaryColor, setTertiaryColor] = useState('');

  const [logo, setLogo] = useState('');
  const [capa, setCapa] = useState('');

  const options = [
    {
      label: 'Dark',
      value: 'Dark',
    },
    {
      label: 'Light',
      value: 'Light',
    },
  ];

  useEffect(() => {
    dispatch(ThemeActions.getTheme());
    setThemeName(themeStore.themeName || '');
    setThemeColor(themeStore.themeColor || '');
    setPrimaryColor(themeStore.primaryColor || '');
    setSecondaryColor(themeStore.secondaryColor || '');
    setTertiaryColor(themeStore.tertiaryColor || '');
    setLogo(themeStore.logo || '');
    setCapa(themeStore.capa || '');
  }, [themeStore, dispatch]);

  const onChangeTheme = (e) => {
    if (e.target.value === 'Dark') {
      setThemeName('Dark');
      setThemeColor('#111A21');
      // setPrimaryColor('#CE3CEA');
      // setSecondaryColor('#7241FD');
    } else {
      setThemeName('Light');
      setThemeColor('#F5F6FA');
      // setPrimaryColor('#CE3CEA');
      // setSecondaryColor('#7241FD');
    }
  };

  const handleTheme = useCallback(async () => {
    await dispatch(
      ThemeActions.postTheme(themeName, themeColor, primaryColor, secondaryColor, tertiaryColor, logo, capa)
    );
  }, [dispatch, themeName, themeColor, primaryColor, secondaryColor, tertiaryColor, logo, capa]);

  const getBase64 = (file) => {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.onload = () => resolve(reader.result);
      reader.onerror = (error) => reject(error);
      reader.readAsDataURL(file);
    });
  };

  const handleUpload = async (e, i) => {
    const file = e.target.files[0];
    await getBase64(file).then((base64) => {
      if (i === 'logo') {
        localStorage.logo = base64;
      } else {
        localStorage.capa = base64;
      }
      // console.debug('file stored', base64);
    });
    if (i === 'logo') {
      setLogo(localStorage.getItem('logo'));
    } else {
      setCapa(localStorage.getItem('capa'));
    }
  };

  return (
    <Container>
      <Label htmlFor="theme">Tema:</Label>
      <Select
        name="theme"
        id="inputTheme"
        options={options}
        onChange={(e) => onChangeTheme(e)}
        defaultValue={themeName}
      />
      <Label htmlFor="primary">Cor primária:</Label>
      <Input
        name="primary"
        variant="formData"
        type="text"
        id="primaryColor"
        onChange={(e) => setPrimaryColor(e.target.value)}
        value={primaryColor}
      />
      <Label htmlFor="secondary">Cor secundária:</Label>
      <Input
        name="secondary"
        variant="formData"
        type="text"
        id="secondaryColor"
        onChange={(e) => setSecondaryColor(e.target.value)}
        value={secondaryColor}
      />
      <Label htmlFor="secondary">Cor Terciária:</Label>
      <Input
        name="tertiary"
        variant="formData"
        type="text"
        id="tertiaryColor"
        onChange={(e) => setTertiaryColor(e.target.value)}
        value={tertiaryColor}
      />
      <S.Avatar photo={logo}>
        <S.LbUploadPhoto htmlFor="input-file" />
        <S.UploadPhoto type="file" id="input-file" onChange={(e) => handleUpload(e, 'logo')} />
      </S.Avatar>
      <S.Avatar photo={capa}>
        <S.LbUploadPhoto htmlFor="input-file2" />
        <S.UploadPhoto type="file" id="input-file2" onChange={(e) => handleUpload(e, 'capa')} />
      </S.Avatar>
      <Button onClick={() => handleTheme()}>Salvar</Button>
    </Container>
  );
};

export default Admin;
