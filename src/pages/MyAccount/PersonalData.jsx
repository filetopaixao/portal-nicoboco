import React, { useState, useCallback, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { useLocation } from 'react-router-dom'; // ,useParams
import html2canvas from 'html2canvas';
import PropTypes from 'prop-types';

import PersonalDataForm from '@components/molecules/Forms/MyAccount/PersonalData/';
import Modal from '@components/molecules/Modals';
import ModalChangePass from '@components/molecules/Modals/ModalChangePass';
import ChangePassForm from '@components/molecules/Forms/MyAccount/ChangePassword';

import { getUserSocials, getUser } from '@services/user';

import Logo from '@assets/images/logoFpass.png';

import * as S from './styled';

const PersonalData = ({ usarCard }) => {
  const themeStore = useSelector((state) => state.theme);
  const handlePrint = useCallback(() => {
    const div = document.querySelector('#virtualCard');
    html2canvas(div).then((canvas) => {
      const img = canvas.toDataURL('image/jpeg', 1.0).replace('image/jpeg', 'image/octet-stream');
      const link = document.createElement('a');
      const d = new Date();
      const date = d.toLocaleDateString();
      const hours = `${d.getHours()}:${d.getMinutes()}`;
      link.download = `screenshot-${date} ${hours}.jpeg`;
      link.href = img;
      link.click();
    });
  }, []);

  const [userSocials, setUserSocials] = useState({});
  const [userData, setUserData] = useState({});

  useEffect(() => {
    getUser()
      .then((response) => {
        const userDataResponse = response.data;

        setUserData({
          id: userDataResponse.id,
          type_id: userDataResponse.type_id,
          birthdate: userDataResponse.birthdate,
          cpf_cnpj: userDataResponse.cpf_cnpj,
          email: userDataResponse.email,
          full_name: `${userDataResponse.first_name} ${userDataResponse.last_name}`,
          first_name: userDataResponse.first_name,
          last_name: userDataResponse.last_name,
          gender: userDataResponse.gender,
          photo: userDataResponse.image.url,
          legal_person: userDataResponse.legal_person,
          phone: userDataResponse.phone,
          office: 'AGENTE FPASS',
        });
      })
      .catch(() => {
        setUserData({});
      });
  }, []);

  useEffect(() => {
    getUserSocials()
      .then((response) => {
        const userSocialsResponse = response.data;

        if (userSocialsResponse.store) {
          setUserSocials(userSocialsResponse);
        } else {
          setUserSocials({
            id: userSocialsResponse.id,
            whatsapp: '(63) 9 9966-0001',
            instagram: userSocialsResponse.instagram,
            facebook: userSocialsResponse.facebook,
            youtube: userSocialsResponse.youtube,
            store: 'https://loja.everbynature.com.br?me=1',
          });
        }
      })
      .catch(() => {
        setUserData({});
      });
  }, []);

  const [isModalVisibleBC, setIsModalVisibleBC] = useState(false);
  const [isModalVisibleCP, setIsModalVisibleCP] = useState(false);

  const { pathname } = useLocation();
  const routeArray = pathname.split('/');
  const activeChangePass = routeArray[routeArray.length - 2];

  useEffect(() => {
    if (activeChangePass === 'activeChangePass') {
      setIsModalVisibleCP(true);
    }
  }, [activeChangePass]);

  return (
    <S.ContainerAccount usarCard={usarCard}>
      {usarCard ? (
        <S.ContainerProfile theme={themeStore.themeName}>
          <S.Avatar photo={userData.photo}>
            <S.LbUploadPhoto htmlFor="input-file" />
            <S.UploadPhoto type="file" id="input-file" />
          </S.Avatar>
          <S.PersonName>
            <S.H3 theme={themeStore.themeName}>{userData.full_name}</S.H3>
          </S.PersonName>
          <S.Office theme={themeStore.themeName}>
            <p>{userData.office}</p>
          </S.Office>
          <S.ButtonAccount
            secondaryColor={themeStore.secondaryColor}
            variant="secondary"
            onClick={() => setIsModalVisibleCP(!isModalVisibleCP)}
          >
            Alterar Senha
          </S.ButtonAccount>
          <S.ButtonAccount
            secondaryColor={themeStore.secondaryColor}
            variant="dark"
            onClick={() => setIsModalVisibleBC(!isModalVisibleBC)}
          >
            Gerar Cartão Visual
          </S.ButtonAccount>
        </S.ContainerProfile>
      ) : null}

      <PersonalDataForm />
      <Modal
        variant="sm"
        title="Cartão Virtual"
        isVisible={isModalVisibleBC}
        onClose={() => setIsModalVisibleBC(!isModalVisibleBC)}
      >
        <S.ContainerModal>
          <S.P theme={themeStore.themeName}>Cartão gerado automaticamente:</S.P>
        </S.ContainerModal>
        <S.ContainerBusinessCard>
          <S.BusinessCard id="virtualCard" theme={themeStore.themeName}>
            <S.ContainerModal>
              <S.LogoStyle src={themeStore.logo ? themeStore.logo : Logo} />
            </S.ContainerModal>
            <S.ContainerModal>
              <S.ContentModal>
                <S.ContentBC>
                  <S.AvatarBC photo={userData.photo} />
                </S.ContentBC>
                <S.ContentBC>
                  <S.H5 theme={themeStore.themeName}>{userData.full_name}</S.H5>
                </S.ContentBC>
                <S.ContentBC>
                  <S.P theme={themeStore.themeName}>{userData.office}</S.P>
                </S.ContentBC>
              </S.ContentModal>
            </S.ContainerModal>
            <S.ContainerModal>
              <S.ContentModal className="userSocials">
                <S.ContactBC theme={themeStore.themeName}>
                  <div className="whatsapp-icon" />
                  <S.Whatsapp theme={themeStore.themeName}>{userSocials.whatsapp}</S.Whatsapp>
                </S.ContactBC>
                <S.ContactBC theme={themeStore.themeName}>
                  <div className="instagram-icon" />
                  <S.Instagram theme={themeStore.themeName}>{userSocials.instagram}</S.Instagram>
                </S.ContactBC>
                <S.ContactBC theme={themeStore.themeName}>
                  <div className="facebook-icon" />
                  <S.Facebook theme={themeStore.themeName}>{userSocials.facebook}</S.Facebook>
                </S.ContactBC>
                <S.ContactBC theme={themeStore.themeName}>
                  <div className="youtube-icon" />
                  <S.Youtube theme={themeStore.themeName}>{userSocials.youtube}</S.Youtube>
                </S.ContactBC>
                <S.Link>
                  <S.P theme={themeStore.themeName}>{userSocials.store}</S.P>
                </S.Link>
              </S.ContentModal>
            </S.ContainerModal>
          </S.BusinessCard>
        </S.ContainerBusinessCard>
        <S.ContainerCenter>
          <S.ButtonAccount primaryColor={themeStore.primaryColor} variant="primary" onClick={() => handlePrint()}>
            baixar cartão
          </S.ButtonAccount>
        </S.ContainerCenter>
      </Modal>
      <ModalChangePass
        variant="md"
        title="Alterar senha"
        isVisible={isModalVisibleCP}
        onClose={() => setIsModalVisibleCP(!isModalVisibleCP)}
      >
        <S.ContainerChangePass>
          <ChangePassForm closeModal={() => setIsModalVisibleCP(!isModalVisibleCP)} />
        </S.ContainerChangePass>
      </ModalChangePass>
    </S.ContainerAccount>
  );
};

PersonalData.propTypes = {
  usarCard: PropTypes.bool,
};

PersonalData.defaultProps = {
  usarCard: true,
};

export default PersonalData;
