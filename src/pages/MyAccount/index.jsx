import React from 'react';

import Tab from '@components/molecules/Tabs';

import PersonalData from './PersonalData';
import Contact from './Contact';
import Address from './Address';
import BankData from './BankData';
import * as S from './styled';

const MyAccount = () => {
  const PersonalDataJSX = <PersonalData usarCard />;
  const ContactJSX = <Contact />;
  const AddressJSX = <Address />;
  const BankDataJSX = <BankData />;
  return (
    <S.ContainerAccount>
      <Tab
        content1={PersonalDataJSX}
        buttonTxt1="dados pessoais"
        content2={ContactJSX}
        buttonTxt2="contatos"
        content3={AddressJSX}
        buttonTxt3="endereço"
        content4={BankDataJSX}
        buttonTxt4="dados bancários"
      />
    </S.ContainerAccount>
  );
};

export default MyAccount;
