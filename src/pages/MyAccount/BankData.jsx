import React, { useState } from 'react';
import { useSelector } from 'react-redux';

import BankData from '@components/molecules/Forms/MyAccount/BankData';
import Modal from '@components/molecules/Modals/';

import * as S from './styled';

const PersonalData = () => {
  const themeStore = useSelector((state) => state.theme);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [banks, setBanks] = useState([
    {
      id: 1,
      name: 'Carla Sousa Caetano',
      bank: 'Banco Bradesco S.A.',
      status: 'approved',
    },
  ]);

  const handleRemove = (e) => {
    setBanks([]);
    if (e.target.id === 'confirm') setIsModalVisible(!isModalVisible);
  };

  const handleClick = () => false;

  return (
    <S.ContainerAccount>
      {banks.length > 0 ? (
        banks.map((bank) => (
          <S.CardAddress theme={themeStore.themeName} key={bank.id}>
            <div className="div-content-card">
              <p>{bank.name}</p>
              <p>{bank.bank}</p>
              <S.Status status={bank.status}>
                {bank.status === 'approved' ? 'conta aprovada' : null}
                {bank.status === 'invalid' ? 'conta inválida' : null}
                {bank.status === 'pending' ? 'verificação pendente' : null}
              </S.Status>
            </div>
            <S.RemoveAddress theme={themeStore.themeName} onClick={handleRemove} id={bank.id}>
              <S.RemoveIcon theme={themeStore.themeName} />
            </S.RemoveAddress>
          </S.CardAddress>
        ))
      ) : (
        <>
          <S.Alert primaryColor={themeStore.primaryColor}>
            <S.AlertIcon primaryColor={themeStore.primaryColor} />
            Não é permitido cadastrar uma conta salário. Confira todas as informações antes de cadastrar a conta. Não
            nos responsabilizamos casos sejam informados dados incorretos.
          </S.Alert>
          <S.ContainerButton>
            <S.ButtonAccount
              secondaryColor={themeStore.secondaryColor}
              size="100%"
              variant="secondary"
              type="submit"
              onClick={handleClick}
            >
              adicionar comprovante de conta
            </S.ButtonAccount>
          </S.ContainerButton>
          <BankData />
        </>
      )}
      <Modal title={null} isVisible={isModalVisible} onClose={() => setIsModalVisible(!isModalVisible)} variant="md">
        <S.ContainerModal>
          <S.ConfirmIcon />
        </S.ContainerModal>
        <S.ContainerModal>
          <S.H3>Tem certeza que deseja trocar de conta?</S.H3>
        </S.ContainerModal>
        <S.ContainerModal>
          <S.P>Ao fazer isso a conta atual será excluída do sistema, permanecendo apenas a nova conta.</S.P>
        </S.ContainerModal>
        <S.ContainerBtns>
          <S.ContainerBtn>
            <S.ButtonAccount variant="secondary" type="submit" onClick={handleRemove} id="confirm">
              sim
            </S.ButtonAccount>
          </S.ContainerBtn>
          <S.ContainerBtn>
            <S.ButtonAccount variant="secondary" type="submit" onClick={() => setIsModalVisible(!isModalVisible)}>
              não
            </S.ButtonAccount>
          </S.ContainerBtn>
        </S.ContainerBtns>
      </Modal>
    </S.ContainerAccount>
  );
};

export default PersonalData;
