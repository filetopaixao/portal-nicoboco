import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';

import AddressForm from '@components/molecules/Forms/MyAccount/Address';
import Modal from '@components/molecules/Modals/';

import { getUserAddress, deleteUserAddress } from '@services/user';

import * as S from './styled';

const Address = () => {
  const themeStore = useSelector((state) => state.theme);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [addresses, setAddresses] = useState([]);

  useEffect(() => {
    getUserAddress()
      .then((response) => {
        const { data } = response.data;

        const serialized = data.map((item) => {
          return {
            id: item.id,
            cep: item.cep,
            city: item.city,
            complement: item.complement,
            country: item.country,
            neighborhood: item.neighborhood,
            number: item.number,
            state: item.state,
            street: item.street,
            address: `${item.street} - ${item.neighborhood}, Número ${item.number}`,
          };
        });
        setAddresses(serialized);
        console.log(data);
      })
      .catch(() => {
        setAddresses([]);
      });
  }, []);

  const handleRemove = async (e) => {
    await deleteUserAddress(e);
    setAddresses(addresses.filter((address) => address.id !== e));
  };

  return (
    <S.ContainerAccount>
      {addresses.map((address) => (
        <S.CardAddress key={address.id} theme={themeStore.themeName}>
          <div className="div-content-card">
            <p>{`${address.city}-${address.state} (${address.cep})`}</p>
            <p>{address.address}</p>
            <p>{address.number ? `Número: ${address.number}` : ''}</p>
            <p>{address.complement}</p>
          </div>
          <S.RemoveAddress theme={themeStore.themeName} onClick={() => handleRemove(address.id)} id={address.id}>
            <S.RemoveIcon theme={themeStore.themeName} />
          </S.RemoveAddress>
        </S.CardAddress>
      ))}
      <S.ContainerButton>
        <S.ButtonAccount
          secondaryColor={themeStore.secondaryColor}
          variant="secondary"
          type="submit"
          onClick={() => setIsModalVisible(!isModalVisible)}
        >
          Adicionar Endereço
        </S.ButtonAccount>
      </S.ContainerButton>
      <Modal
        title="Adicionar endereço"
        variant="md"
        isVisible={isModalVisible}
        onClose={() => setIsModalVisible(!isModalVisible)}
      >
        <AddressForm
          addressesArray={addresses}
          setAddresses={setAddresses}
          funcSubmit={() => setIsModalVisible(!isModalVisible)}
        />
      </Modal>
    </S.ContainerAccount>
  );
};

export default Address;
