import React, { useState, useEffect, useCallback } from 'react';
import { useHistory } from 'react-router';
import { useDispatch, useSelector } from 'react-redux';

import CheckoutResume from '@components/atoms/Checkout/CheckoutResume/';

import Toast from '@components/atoms/Toast';
import FilterComponent from '@components/molecules/Filter';
import Pagination from '@components/molecules/Pagination';
import ButtonToggleSwitch from '@components/atoms/ButtonToggleSwitch';

import { Actions as CouponsActions } from '@redux/ducks/coupon';

import Arrow from '@assets/images/arrowCheckout.svg';

import { getList } from '../../services/coupon';
import { formateDateTime } from '../../utils/formatDate';

import Modal from './EditCupom';

import * as S from './styled';

const Cupom = () => {
  const themeStore = useSelector((state) => state.theme);
  const history = useHistory();
  const [nameCupom, setNameCupom] = useState('');
  const [coupons, setCoupons] = useState([]);

  const [isToastVisible, setIsToastVisible] = useState(false);

  const [filterText, setFilterText] = useState('');
  const [currentPage, setCurrentPage] = useState(1);
  const [perPage] = useState(5);

  const [isModalVisible, setIsModalVisible] = useState(false);
  const [couponSelected, setCouponSelected] = useState({});
  const [activeLoading, setActiveLoading] = useState(true);

  const dispatch = useDispatch();
  const couponsStore = useSelector((state) => state.coupons);

  const [verify, setVerify] = useState('pause');

  useEffect(() => {
    const load = async () => {
      const { data: response } = await getList();
      const res = response.data.map((item) => {
        return {
          id: item.id,
          code: item.code,
          discount: item.discount,
          valid_from: formateDateTime(new Date(item.valid_from)),
          valid_until: formateDateTime(new Date(item.valid_until)),
          active: false,
          activeExpansed: false,
          quantity: item.quantity,
          conversion: 'ever',
          status: 'toogle',
          action: 'action',
        };
      });
      setCoupons(res);

      console.log(res);
      setActiveLoading(false);
    };

    load();
    setNameCupom('');
    setVerify('');
  }, []);

  useEffect(() => {
    dispatch(CouponsActions.getCoupon());
    setCoupons([...coupons, couponsStore.couponAdded]);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dispatch, couponsStore.couponAdded, setCoupons]);

  useEffect(() => {
    dispatch(CouponsActions.getCoupon());
    const newCoupons = coupons.filter((item) => item.id !== couponsStore.couponDeleted.id);
    setCoupons(newCoupons);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [couponsStore.couponDeleted, dispatch, setCoupons]);

  useEffect(() => {
    dispatch(CouponsActions.getCoupon());
    if (couponsStore.couponEdited) {
      console.log('foi atualizado');
    }
    // const newCoupons = coupons.map((item) => {
    //   if (couponsStore.couponEdited.id === item.id) {
    //     return {
    //       ...item,
    //       discount: item.discount,
    //       valid_from: item.valid_from,
    //       valid_until: item.valid_until,
    //       quantity: item.quantity,
    //     };
    //   }
    //   return item;
    // });
    // setCoupons(newCoupons);
    // // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [couponsStore.couponEdited, dispatch]);

  const handleActiveModalEdit = (item) => {
    let dataIni = item.valid_from.split(' ')[0];
    dataIni = dataIni.split('/');
    let dataFim = item.valid_until.split(' ')[0];
    dataFim = dataFim.split('/');

    const coupon = {
      id: item.id,
      code: item.code,
      discount: parseFloat(item.discount),
      valid_from: `${dataIni[2]}-${dataIni[1]}-${dataIni[0]}`,
      valid_until: `${dataFim[2]}-${dataFim[1]}-${dataFim[0]}`,
      active: false,
      activeExpansed: false,
      quantity: item.quantity,
      conversion: 'ever',
      status: 'toogle',
      action: 'action',
    };
    setCouponSelected(coupon);
    setIsModalVisible(!isModalVisible);
  };

  const handleActiveToast = useCallback(() => {
    setIsToastVisible(true);
    setTimeout(() => {
      setIsToastVisible(false);
    }, 3000);
  }, [setIsToastVisible]);

  const handleActive = (e, item) => {
    e.preventDefault();
    const newDatas = coupons.map((data) => {
      return data.id === item.id ? { ...data, active: !data.active } : data;
    });
    setCoupons(newDatas);
  };

  const handleSetExpansed = (e, item) => {
    e.preventDefault();
    const newDatas = coupons.map((data) => {
      return data.id === item.id
        ? { ...data, activeExpansed: !data.activeExpansed }
        : { ...data, activeExpansed: false };
    });
    setCoupons(newDatas);
  };

  const filteredItems = coupons.filter(
    (item) => item.code && item.code.toLowerCase().includes(filterText.toLowerCase())
  );

  const indexOfLast = currentPage * perPage;
  const indexOfFirst = indexOfLast - perPage;
  const currentCoupons = filteredItems.slice(indexOfFirst, indexOfLast);

  const paginate = useCallback((pageNumber) => {
    setCurrentPage(pageNumber);
  }, []);

  const handleWindowCopy = (value) => {
    navigator.clipboard.writeText(value);
    handleActiveToast();
  };

  const handleSelectCod = (value) => {
    const newValue = value.replace(/\s/g, '');
    setNameCupom(newValue);

    let filtered = coupons.filter((item) => item.code && item.code.toLowerCase().includes(newValue.toLowerCase()));
    if (filtered.length > 0) {
      setVerify('existente');
    } else {
      setVerify('liberado');
      filtered = [];
    }
  };

  const handleVerify = () => {
    const filtered = coupons.filter((item) => item.code && item.code.toLowerCase().includes(nameCupom.toLowerCase()));
    if (filtered.length > 0) {
      setVerify('existente');
    } else {
      setVerify('liberado');
    }

    if (verify === 'liberado') {
      dispatch(CouponsActions.setCodCoupon(nameCupom));
      history.push('/cupom/create');
      setVerify('pause');
    }
    setNameCupom('');
  };

  if (activeLoading) {
    return <S.ImageLoadding />;
  }

  return (
    <>
      <S.ContainerCupom>
        <S.CheckoutContainer className="createCheckout create" variant="normal" theme={themeStore.themeName}>
          <S.CheckoutTitle theme={themeStore.themeName}>Crie um novo cupom de Desconto</S.CheckoutTitle>

          <S.CheckoutContent>
            <S.CheckoutLabel theme={themeStore.themeName}>Qual o código do cupom?</S.CheckoutLabel>
            <S.CheckoutInput
              id="cupomName"
              name="name"
              value={nameCupom}
              onChange={(e) => handleSelectCod(e.target.value)}
              placeholder="Digite o código"
              type="text"
              theme={themeStore.themeName}
            />
          </S.CheckoutContent>
          {verify === 'existente' ? <S.Incorrect>X Código indisponível</S.Incorrect> : null}
          {verify === 'liberado' ? <S.Available>✓ Código disponível</S.Available> : null}

          <S.CheckoutButtonGroup className="center">
            <S.CheckoutButton primaryColor={themeStore.primaryColor} onClick={() => handleVerify()}>
              <S.CheckoutText size="sm" variant="medium">
                criar novo
              </S.CheckoutText>
            </S.CheckoutButton>
          </S.CheckoutButtonGroup>
        </S.CheckoutContainer>
        <CheckoutResume className="resume" variant="cupom" image="coupon" />
      </S.ContainerCupom>

      <FilterComponent onFilter={(e) => setFilterText(e.target.value)} filterText={filterText} />

      <S.ContainerDataTable theme={themeStore.themeName}>
        <S.TableHeader columnsGrid={6} theme={themeStore.themeName}>
          <S.TextCell className="text-upp" theme={themeStore.themeName}>
            cupom
          </S.TextCell>
          <S.TextCell className="text-upp" theme={themeStore.themeName}>
            desconto
          </S.TextCell>
          <S.TextCell className="text-upp off-1" theme={themeStore.themeName}>
            validade
          </S.TextCell>
          <S.TextCell className="text-upp off-2" theme={themeStore.themeName}>
            cupons
            <br />
            disponíveis
          </S.TextCell>
          <S.TextCell className="text-upp off-3" theme={themeStore.themeName}>
            cupons
            <br />
            utilizados
          </S.TextCell>
          <S.TextCell className="text-upp" theme={themeStore.themeName}>
            Status
          </S.TextCell>
          <S.TextCell className="text-upp" theme={themeStore.themeName} />
          <S.TextCell className="text-upp btn-expansed" theme={themeStore.themeName} />
        </S.TableHeader>
        <S.TableBody>
          <>
            {currentCoupons.length <= 0 ? (
              <S.ContainerBackEndInfo>
                <S.InfoBackEnd theme={themeStore.themeName}>Não há cupons cadastrados</S.InfoBackEnd>
                {/* <S.ImageNotFound /> */}
              </S.ContainerBackEndInfo>
            ) : (
              <>
                {currentCoupons.map((item) => (
                  <>
                    <S.TableLine key={item.id.toString()} theme={themeStore.themeName}>
                      <S.TableCell className="text-upp" theme={themeStore.themeName}>
                        <S.ButtonGroupCoupon>
                          <p>{item.code}</p>
                          <S.Button onClick={() => handleWindowCopy(item.code)}>
                            <S.InputCopy
                              type="text"
                              id={`${item.id}linkCupom`}
                              onChange={() => {}}
                              value={`Esse texto copiado do - ${item.code} - LinkCupom`}
                            />
                            <div />
                          </S.Button>
                        </S.ButtonGroupCoupon>
                      </S.TableCell>
                      <S.TableCell theme={themeStore.themeName} className="text-up">
                        {`${parseInt(item.discount, 10).toFixed(0)}%`}
                      </S.TableCell>
                      <S.TableCell theme={themeStore.themeName} className="text-upp off-1">
                        {item.valid_until}
                      </S.TableCell>
                      <S.TableCell theme={themeStore.themeName} className="text-upp off-2">
                        {item.quantity}
                      </S.TableCell>
                      <S.TableCell theme={themeStore.themeName} className="text-upp off-3">
                        {item.conversion}
                      </S.TableCell>
                      <S.TableCell theme={themeStore.themeName} className="text-upp">
                        <a href="#!" onClick={(e) => handleActive(e, item)} className="toggle-switch">
                          <ButtonToggleSwitch checked={item.active} onChange={() => {}} />
                        </a>
                      </S.TableCell>
                      <S.TableCell theme={themeStore.themeName} className="text-upp">
                        <S.ButtonEdit className="btn-edit" onClick={() => handleActiveModalEdit(item)}>
                          <div />
                        </S.ButtonEdit>
                      </S.TableCell>
                      <S.TableCell theme={themeStore.themeName} className="text-upp off-button-set">
                        <a href="#!" onClick={(e) => handleSetExpansed(e, item)}>
                          <S.ButtonExpansed src={Arrow} active={item.activeExpansed} />
                        </a>
                      </S.TableCell>
                    </S.TableLine>
                    {item.activeExpansed ? (
                      <S.TableLineExpansed className="activeExpansed" theme={themeStore.themeName}>
                        <S.TableCell theme={themeStore.themeName} className="text-upp">
                          <div>
                            <S.TitleValueExpan>
                              <p>Validade</p>
                            </S.TitleValueExpan>
                            <div>
                              <p>{item.valid_until}</p>
                            </div>
                          </div>
                        </S.TableCell>
                        <S.TableCell theme={themeStore.themeName} className="text-upp">
                          <div>
                            <S.TitleValueExpan>
                              <p>
                                cupons
                                <br />
                                disponíveis
                              </p>
                            </S.TitleValueExpan>
                            <div>
                              <p>{item.quantity}</p>
                            </div>
                          </div>
                        </S.TableCell>
                        <S.TableCell theme={themeStore.themeName} className="text-upp">
                          <div>
                            <S.TitleValueExpan>
                              <p>
                                cupons
                                <br />
                                utilizados
                              </p>
                            </S.TitleValueExpan>
                            <div>
                              <p>{item.conversion}</p>
                            </div>
                          </div>
                        </S.TableCell>
                      </S.TableLineExpansed>
                    ) : null}
                  </>
                ))}
              </>
            )}
          </>
        </S.TableBody>
      </S.ContainerDataTable>

      <S.ContainerPagination className="pagination">
        <Pagination currentPage={currentPage} perPage={perPage} total={coupons.length} paginate={paginate} />
      </S.ContainerPagination>
      <Modal coupon={couponSelected} isModalVisible={isModalVisible} setIsModalVisible={setIsModalVisible} />
      <Toast
        variant="success"
        icon="success"
        showIcon
        isVisible={isToastVisible}
        onClose={() => setIsToastVisible(!isToastVisible)}
      >
        Link copiado com sucesso!!
      </Toast>
    </>
  );
};

export default Cupom;
