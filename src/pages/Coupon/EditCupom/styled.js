import styled from 'styled-components';
import { shade } from 'polished';

export const ModalContent = styled.div`
  height: 100%;

  padding: 20px 20px 10px;

  z-index: 2;
  color: #5e5e5e;

  @media (max-width: 500px) {
    padding: 10px 10px 5px;
  }

  img {
    display: block;
    height: 100%;
    width: 20px !important;

    @media (min-width: 1400px) {
      height: 100%;
      width: 25px !important;
    }
  }
`;

export const ModalHeader = styled.header`
  color: ${(props) => (props.theme === 'Dark' ? '#fff' : '#484646')};

  h3 {
    font-size: 14px;
    font-weight: 600;
    text-align: center;

    width: 100%;

    @media (min-width: 768px) {
      font-size: 18px;
    }
  }
`;

export const ModalBody = styled.div``;

export const ContainerForm = styled.form`
  @media (min-width: 800px) {
    > div:nth-child(n) {
      margin-bottom: 20px;
    }
  }
`;

export const InputTitle = styled.h4`
  font-weight: lighter;
  color: ${(props) => (props.theme === 'Dark' ? '#fff' : '#484646')};
`;

export const InputCodCoupon = styled.input`
  height: 40px;
  width: 100%;

  padding: 3px 20px;

  border-radius: 15px;
  border: ${(props) => (props.theme === 'Dark' ? '1px solid #47505E' : '1px solid #CECECE')};

  background: ${(props) => (props.theme === 'Dark' ? '#29303A' : '#FBFBFB')};
  color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};

  ::placeholder {
    color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
  }
`;

export const InputDateIni = styled.input`
  height: 40px;
  width: 100%;

  border-radius: 15px;
  border: ${(props) => (props.theme === 'Dark' ? '1px solid #47505E' : '1px solid #CECECE')};

  background: ${(props) => (props.theme === 'Dark' ? '#29303A' : '#FBFBFB')};
  color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};

  ::placeholder {
    color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
  }

  ::-webkit-calendar-picker-indicator {
    filter: invert(0.5);
  }
`;

export const InputDateFim = styled.input`
  height: 40px;
  width: 100%;

  border-radius: 15px;
  border: ${(props) => (props.theme === 'Dark' ? '1px solid #47505E' : '1px solid #CECECE')};

  background: ${(props) => (props.theme === 'Dark' ? '#29303A' : '#FBFBFB')};
  color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};

  ::placeholder {
    color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
  }

  ::-webkit-calendar-picker-indicator {
    filter: invert(0.5);
  }
`;

export const ButtonGroup = styled.div`
  button {
    text-transform: uppercase;
    font-size: 17px;
    //font-weight: bold;

    width: 100%;
    height: 55px;
    margin-top: 15px;

    border: none;
    border-radius: 15px;
  }
`;

export const ButtonGroupDate = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  column-gap: 15px;
  row-gap: 15px;
  align-items: center;

  input {
    padding: 3px 20px;
    letter-spacing: 0px;
    color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
  }

  @media (min-width: 800px) {
    grid-template-columns: 1fr 1fr;
    column-gap: 15px;
  }
`;

export const ResultContainer = styled.div`
  display: flex;
  align-items: center;

  h4 {
    margin-right: 15px;
  }

  p {
    color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
  }
`;

export const SelectContainer = styled.div`
  select {
    height: 40px;
    width: 100%;

    padding: 3px 20px;

    border-radius: 15px;
    border: ${(props) => (props.theme === 'Dark' ? '1px solid #47505E' : '1px solid #CECECE')};

    background: ${(props) => (props.theme === 'Dark' ? '#29303A' : '#FBFBFB')};
    color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
  }
`;

export const ButtonSave = styled.button`
  color: #fff;
  height: 53px;
  background: ${(props) => props.primaryColor};
  box-shadow: 0px 3px 6px #00000029;

  /*
  &:hover {
    background: ${shade(0.2, '#01a3ff')};
  }
  */
`;

export const ButtonDelete = styled.button`
  font-size: 17px;
  color: #fff;
  height: 53px;
  background: #ff0000 0% 0% no-repeat padding-box;
  box-shadow: 0px 3px 6px #00000029;

  &:hover {
    background: ${shade(0.2, '#ff0000')};
  }
`;
