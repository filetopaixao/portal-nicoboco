import React, { useState, useEffect, useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import 'isomorphic-fetch';

import { Text, Button } from '@components/atoms';
import SelectCategories from '@components/atoms/Select/selectCategories';
import { Products } from '@components/molecules';
import CardPurchase from '@components/atoms/Cards/purchase';
import ModalVideo from '@components/molecules/Modals/modalComponentVideo';
import Filter from '@components/molecules/Filter';
import PurchaseCollapses from '@components/organisms/Purchase/';
import Loading from '@components/atoms/Loading';

import { getCategoryProducts, getProductByCategory, getCategories } from '@services/products';
import { formateValueBr } from '@utils/formatValueBr';
import dataMock from '@utils/mock/purchase';
import { Actions as ProductsActions } from '@redux/ducks/products';

import Video from '@assets/images/video.svg';
import FinalizedPurchase from '@assets/images/aquirir_produtos.svg';

import * as S from './styled';

const Purchase = () => {
  const { data } = useSWR('/api/portal/my-ecommerce', getCategories);
  console.log('swr', data);

  // useEffect(() => {
  //   setOptions(data);
  // }, [data]);

  const [products, setProducts] = useState([]);
  const [productsSelected, setProductsSelected] = useState([]);
  const [confirmOrder, setConfirmOrder] = useState(false);

  const dispatch = useDispatch();
  const productsStore = useSelector((state) => state.products);

  const [subTotal, setSubTotal] = useState(0);
  const [desconto, setDesconto] = useState(0);
  const [total, setTotal] = useState(0);
  const [filterText, setFilterText] = useState('');

  const [isModalVideoVisible, setIsModalVideoVisible] = useState(false);
  const linkVideo = 'https://www.youtube.com/embed/LiiYMEWKVnY';

  const [options, setOptions] = useState([]);

  useEffect(() => {
    getCategoryProducts().then((response) => {
      try {
        const opsCategory = response.data.data.map((item) => {
          return {
            id: item.id,
            label: item.name,
            value: item.id,
          };
        });
        setOptions(opsCategory);
      } catch (error) {
        console.log(error);
      }
    });
  }, []);

  const handleSelectedCategory = useCallback(
    (e) => {
      getProductByCategory(e.target.value)
        .then((response) => {
          console.log(productsStore);
          const serializedProducts = response.data.data.map((item) => {
            const nameProduct = item.name;
            const arrayName = nameProduct.split(' ');
            dispatch(ProductsActions.findProduct(item));
            return {
              id: item.id,
              title: arrayName[0],
              description: item.description,
              srcImg: item.image.url,
              valueProduct: item.price,
              valueCommision: item.commission,
              urlProduct: item.url,
              purchase: true,
              product: false,
              isSelected: false,
              qtd: 0,
            };
          });
          setProducts(serializedProducts);
        })
        .catch(() => {
          const serializedProducts = dataMock.map((item) => {
            return {
              id: item.id,
              title: item.title,
              description: item.nameImg,
              srcImg: item.srcImg,
              valueProduct: item.valueProduct,
              valueCommision: item.valueCommision,
              urlProduct: item.urlProduct,
              purchase: true,
              product: false,
              isSelected: false,
              qtd: 0,
            };
          });
          setProducts(serializedProducts);
        })
        .catch((err) => {
          if (err) {
            console.log(err);
          }
        });
    },
    [dispatch, productsStore]
  );

  const handleOpenModalVideo = useCallback(() => {
    setIsModalVideoVisible(!isModalVideoVisible);
  }, [isModalVideoVisible]);

  const handleSelectProduct = useCallback(
    (prodSelect) => {
      const newProducts = products.map((product) => {
        let newProduct = {};
        if (product.id === prodSelect.id) {
          if (productsSelected.find((productSelected) => productSelected.id === prodSelect.id)) {
            console.log('existe');
            newProduct = { ...product, isSelected: !product.isSelected, qtd: product.qtd + 1 };
            dispatch(ProductsActions.selectProductExists(newProduct));
            return newProduct;
          }
          newProduct = { ...product, isSelected: !product.isSelected, qtd: 1 };
          dispatch(ProductsActions.selectProduct(newProduct));
          return newProduct;
        }
        return product;
      });
      setProducts(newProducts);
    },
    [dispatch, products, productsSelected]
  );

  const handleReduceQtdSelectProduct = useCallback(
    (prodSelect) => {
      dispatch(ProductsActions.reduceSelectProduct(prodSelect));
    },
    [dispatch]
  );

  const handleIncreaseQtdSelectProduct = useCallback(
    (prodSelect) => {
      dispatch(ProductsActions.selectProductExists(prodSelect));
    },
    [dispatch]
  );

  useEffect(() => {
    dispatch(ProductsActions.getProductsSelected());
    setProductsSelected(productsStore.productHandleSelected || []);
  }, [productsStore, dispatch, productsSelected]);

  const handleRemoveProduct = useCallback(
    (index) => {
      dispatch(ProductsActions.removeProductselect(index));
      const newArray = productsSelected.filter((product) => product.id !== index);
      setProductsSelected(newArray);

      const newProducts = products.map((product) => {
        if (product.id === index) {
          return { ...product, isSelected: !product.isSelected };
        }
        return product;
      });
      setProducts(newProducts);
    },
    [dispatch, products, productsSelected]
  );

  useEffect(() => {
    const { subT, desc } = productsSelected.reduce(
      (accumulator, product) => {
        if (product.qtd > 1) {
          accumulator.subT += parseFloat(product.valueProduct) * product.qtd;
        } else {
          accumulator.subT += parseFloat(product.valueProduct);
        }
        accumulator.desc += (parseFloat(product.valueProduct) * 40) / 100;
        return accumulator;
      },
      {
        subT: 0,
        desc: 0,
      }
    );

    if (subT === 0) {
      setSubTotal(0);
      setDesconto(0);
      setTotal(0);
    }
    setSubTotal(subT);
    setDesconto(desc);
    setTotal(subT - desc);
  }, [productsSelected]);

  const handleConfirmOrder = () => {
    if (productsSelected.length >= 1) {
      setConfirmOrder(!confirmOrder);
    }
  };

  const [finalized, setFinalized] = useState(false);
  const [handleValueSelectPaymentOne, setHandleValueSelectPaymentOne] = useState('');
  const [handleValueSelectPaymentTow, setHandleValueSelectPaymentTow] = useState('');
  const [valuesSendForm, setValuesSendForm] = useState({});
  const [addressesSelected, setAddressesSelected] = useState({});

  useEffect(() => {
    dispatch(ProductsActions.getSendForm());
    setAddressesSelected(productsStore.addressesSelected);
    setValuesSendForm(productsStore.valuesSendForm);
    setFinalized(productsStore.purchaseFinalized);

    setHandleValueSelectPaymentOne(productsStore.valuesSendForm.valueSelectPaymentOne);
    setHandleValueSelectPaymentTow(productsStore.valuesSendForm.valueSelectPaymentTow);
  }, [
    productsStore,
    dispatch,
    addressesSelected,
    valuesSendForm,
    finalized,
    handleValueSelectPaymentOne,
    handleValueSelectPaymentTow,
  ]);

  const handleCopy = () => {
    const btnCopy = document.getElementById('copy');
    btnCopy.select();
    document.execCommand('copy');
  };

  const codeBar = '00190500954014481606906809350314337370000000100';

  if (!data) {
    return <p>Carregando</p>;
  }

  return (
    <>
      {options.length <= 0 ? (
        <Loading />
      ) : (
        <>
          <S.ContainerPage className="purchase">
            <S.ContainerStatus className="purchase__status">
              <CardPurchase
                className="purchase__status__commision"
                icon="commision"
                title="Saldo Liberado"
                variant="commision"
                value="R$ 15.200"
              />
              <CardPurchase
                className="purchase__status__discount"
                icon="discount"
                title="Disconto para compra"
                variant="discount"
                discount="40%"
              />
            </S.ContainerStatus>

            {finalized ? (
              <S.ContainerPurchaseFinalized>
                <header>
                  <h2>Obrigado</h2>
                  <p>Seu pedido foi realizado com Sucesso</p>
                </header>
                <section>
                  <img
                    className="image-purchase-finalized"
                    src={FinalizedPurchase}
                    alt="Compra realizada com sucesso!!"
                  />
                </section>
                {handleValueSelectPaymentOne === 'boleto' || handleValueSelectPaymentTow === 'boleto' ? (
                  <section>
                    <p>
                      Aguardamos a confirmação do pagamento. Lembrando que boletos bancarios são compensados em até 2
                      dias úteis após o pagamento. Você pode acompanhar o andamento do pedido em Relatórios de Compras.
                    </p>

                    <div className="mt30">
                      <p>código de barras</p>
                      <S.ContentInput onClick={() => handleCopy()}>
                        <S.SCInput
                          id="copy"
                          name="copy"
                          type="text"
                          placeholder="copiar"
                          value={codeBar}
                          onChange={() => false}
                        />
                        <S.CopyButton type="button" onClick={() => handleCopy()} />
                      </S.ContentInput>
                    </div>

                    <div className="mt30 button_group">
                      <button className="btn_pdf" type="button">
                        Gerar boleto em PDF
                      </button>
                      <button className="btn_relatorios" type="button">
                        Conferir Relatórios de Compras
                      </button>
                    </div>
                  </section>
                ) : (
                  <section>
                    <p>Você pode acompanhar o adamento do seu pedido em Relatórios de Compras.</p>
                    <div className="mt100">
                      <button className="btn_relatorios" type="button">
                        Conferir Relatórios de Compras
                      </button>
                    </div>
                  </section>
                )}
              </S.ContainerPurchaseFinalized>
            ) : (
              <S.Content order={confirmOrder}>
                {confirmOrder ? (
                  <PurchaseCollapses />
                ) : (
                  <S.ContainerPurchaseList className="purchase__list">
                    <Text size="sm" variant="bold" className="text_title">
                      Selecione os produtos que deseja comprar
                    </Text>

                    <S.ContainerPurchaseListProducts className="purchase__list__products">
                      <Text size="sm" variant="normal">
                        Categorias
                      </Text>

                      <SelectCategories
                        name="categories"
                        id="categories"
                        options={options}
                        defaultValue="Escolha a categoria do seu produto"
                        onChange={(e) => handleSelectedCategory(e)}
                      />

                      <Filter
                        placeholder="Já sabe qual produto comprar? Pesquise o nome dele aqui"
                        onFilter={(e) => setFilterText(e.target.value)}
                        filterText={filterText}
                      />
                      <S.ContainerListProducts>
                        <Products
                          handleSelected={handleSelectProduct}
                          data={products}
                          page="purchase"
                          className="scrollbar"
                        />
                      </S.ContainerListProducts>
                    </S.ContainerPurchaseListProducts>
                  </S.ContainerPurchaseList>
                )}

                <S.ContainerPurchaseListSelected>
                  <div>
                    <a href="#!" onClick={() => handleOpenModalVideo()}>
                      <img src={Video} alt="Assitir" />
                    </a>
                  </div>

                  <Text size="sm" variant="bold" className="text_title">
                    Seu Carrinho de Compras
                  </Text>

                  <S.ContainerTable>
                    <div className="table_header_purchase">
                      <ul>
                        <li>Produto</li>
                        <li>Descrição</li>
                        <li>Qt</li>
                        <li>Preço Un.</li>
                        {confirmOrder === false ? <li>Excluir</li> : null}
                      </ul>
                    </div>

                    <div className="table_body_purchase full_width">
                      {productsSelected.map((product) => (
                        <ul key={product.id}>
                          <li>
                            <img src={product.srcImg} alt={product.nameImg} />
                          </li>
                          <li>{product.title}</li>
                          {confirmOrder === false && product.qtd >= 0 ? (
                            <li className="btn-bigger-one">
                              <p>{product.qtd}</p>
                              <div className="btn-group">
                                <button onClick={() => handleIncreaseQtdSelectProduct(product)} type="button">
                                  {' '}
                                </button>
                                <button onClick={() => handleReduceQtdSelectProduct(product)} type="button">
                                  {' '}
                                </button>
                              </div>
                            </li>
                          ) : (
                            <li>{product.qtd}</li>
                          )}

                          <li>{product.valueProduct}</li>

                          {confirmOrder === false && product.qtd >= 0 ? (
                            <li>
                              <button onClick={() => handleRemoveProduct(product.id)} type="button">
                                X
                              </button>
                            </li>
                          ) : null}
                        </ul>
                      ))}
                    </div>
                  </S.ContainerTable>

                  <S.ContainerResult productsSelected={productsSelected.length >= 1 ? 'selected' : 'not_selected'}>
                    <section>
                      <ul>
                        <li>
                          <p>Subtotal:</p>
                          <p>{formateValueBr(subTotal)}</p>
                        </li>
                        <li>
                          <p>Desconto:</p>
                          <p>{formateValueBr(desconto)}</p>
                        </li>
                        <li>
                          <h3>TOTAL:</h3>
                          <h3>{formateValueBr(total)}</h3>
                        </li>
                      </ul>
                      {confirmOrder ? null : (
                        <Button onClick={() => handleConfirmOrder()} variant="success" type="button">
                          Confirmar Pedido
                        </Button>
                      )}
                    </section>
                  </S.ContainerResult>
                </S.ContainerPurchaseListSelected>
              </S.Content>
            )}
          </S.ContainerPage>
          <ModalVideo
            isModalVideoVisible={isModalVideoVisible}
            setIsModalVideoVisible={setIsModalVideoVisible}
            linkVideo={linkVideo}
          />
        </>
      )}
    </>
  );
};

Purchase.getInitialProps = async () => {
  const response = getCategoryProducts();
  const opsCategory = response.data.data.map((item) => {
    return {
      id: item.id,
      label: item.name,
      value: item.id,
    };
  });

  console.log(opsCategory[0].nome);

  return { categories: opsCategory };
};

export default Purchase;
