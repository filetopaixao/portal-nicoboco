import React, { useState, useEffect, useCallback } from 'react';

import ConviteParceiros from './InvitationPartners';
import MeusParceiros from './MyPartners';
import SolicitacoesParceiros from './SolicitationPartners';

import * as S from './styled';

const MyPartners = () => {
  const [activeBtn1, setActiveBtn1] = useState(true);
  const [activeBtn2, setActiveBtn2] = useState(false);
  const [activeBtn3, setActiveBtn3] = useState(false);

  const opContent = 'content1';

  const handleContent1 = useCallback(() => {
    setActiveBtn1(true);
    setActiveBtn2(false);
    setActiveBtn3(false);
  }, []);
  const handleContent2 = useCallback(() => {
    setActiveBtn1(false);
    setActiveBtn2(true);
    setActiveBtn3(false);
  }, []);
  const handleContent3 = useCallback(() => {
    setActiveBtn1(false);
    setActiveBtn2(false);
    setActiveBtn3(true);
  }, []);

  const loadContent = useCallback(() => {
    switch (opContent) {
      case 'content1':
        return handleContent1();
      case 'content2':
        return handleContent2();
      case 'content3':
        return handleContent3();
      default:
        return handleContent1();
    }
  }, [handleContent1, handleContent2, handleContent3]);

  useEffect(() => {
    loadContent();
  }, [loadContent]);

  const clickButton = (e) => {
    if (e.target.id === 'btn1') {
      if (activeBtn1) {
        setActiveBtn1(false);
      } else {
        handleContent1();
      }
    } else if (e.target.id === 'btn2') {
      if (activeBtn2) {
        setActiveBtn2(false);
      } else {
        handleContent2();
      }
    } else if (e.target.id === 'btn3') {
      if (activeBtn3) {
        setActiveBtn3(false);
      } else {
        handleContent3();
      }
    }
  };
  return (
    <div>
      <S.GridButtons qtd={3}>
        <S.Column>
          <S.Button className="btnGrid" onClick={clickButton} id="btn1" active={activeBtn1}>
            Convite Parceiros
          </S.Button>
        </S.Column>
        <S.Column>
          <S.Button className="btnGrid" onClick={clickButton} id="btn2" active={activeBtn2}>
            Meus Parceiros
          </S.Button>
        </S.Column>
        <S.Column>
          <S.Button className="btnGrid" onClick={clickButton} id="btn3" active={activeBtn3}>
            Solicitações de Parceiros
          </S.Button>
        </S.Column>
      </S.GridButtons>

      {activeBtn1 ? (
        <S.Content>
          <ConviteParceiros />
        </S.Content>
      ) : null}

      {activeBtn2 ? (
        <S.Content>
          <MeusParceiros />
        </S.Content>
      ) : null}

      {activeBtn3 ? (
        <S.Content>
          <SolicitacoesParceiros />
        </S.Content>
      ) : null}
    </div>
  );
};
export default MyPartners;
