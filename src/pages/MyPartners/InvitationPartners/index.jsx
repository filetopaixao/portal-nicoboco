import React, { useState } from 'react';

import Modal from '@components/molecules/Modals';
import InputShareCopy from '@components/molecules/InputShareCopy';
import ToggleSwitch from '@components/atoms/Button/toggleSwitch';

import * as S from './styled';

const ConviteParceiros = () => {
  const [isVisible, setIsVisible] = useState(false);
  const [toggle, setToggle] = useState(false);
  return (
    <>
      <S.Container>
        <S.ContainerProfessional>
          <S.H3>profissional da saúde</S.H3>
          <S.ContentContainer>
            <S.ContainerCards>
              <S.Card onClick={() => setIsVisible(!isVisible)}>
                <S.SingleCard color="darkBlue">
                  <S.CardIcon icon="medico" />
                  <S.CardTitle>médico</S.CardTitle>
                  <S.CommissionText>Comissão 35%</S.CommissionText>
                </S.SingleCard>
              </S.Card>
              <S.Card onClick={() => setIsVisible(!isVisible)}>
                <S.SingleCard color="darkBlue">
                  <S.CardIcon icon="nutricionista" />
                  <S.CardTitle>nutricionista</S.CardTitle>
                  <S.CommissionText>Comissão 35%</S.CommissionText>
                </S.SingleCard>
              </S.Card>
              <S.Card onClick={() => setIsVisible(!isVisible)}>
                <S.SingleCard color="darkBlue">
                  <S.CardIcon icon="terapeuta" />
                  <S.CardTitle>terapeuta</S.CardTitle>
                  <S.CommissionText>Comissão 35%</S.CommissionText>
                </S.SingleCard>
              </S.Card>
              <S.Card onClick={() => setIsVisible(!isVisible)}>
                <S.SingleCard color="darkBlue">
                  <S.CardIcon icon="no" />
                  <S.CardTitle>outros</S.CardTitle>
                  <S.CommissionText>Comissão 35%</S.CommissionText>
                </S.SingleCard>
              </S.Card>
            </S.ContainerCards>
          </S.ContentContainer>
        </S.ContainerProfessional>
        <S.ContainerProfessional>
          <S.H3>profissional fitness</S.H3>
          <S.ContentContainer>
            <S.ContainerCards>
              <S.Card onClick={() => setIsVisible(!isVisible)}>
                <S.SingleCard color="green">
                  <S.CardIcon icon="personalTrainer" />
                  <S.CardTitle>personal trainer</S.CardTitle>
                  <S.CommissionText>Comissão 35%</S.CommissionText>
                </S.SingleCard>
              </S.Card>
              <S.Card onClick={() => setIsVisible(!isVisible)}>
                <S.SingleCard color="green">
                  <S.CardIcon icon="educadorFisico" />
                  <S.CardTitle>educador físico</S.CardTitle>
                  <S.CommissionText>Comissão 35%</S.CommissionText>
                </S.SingleCard>
              </S.Card>
              <S.Card onClick={() => setIsVisible(!isVisible)}>
                <S.SingleCard color="green">
                  <S.CardIcon icon="academia" />
                  <S.CardTitle>academia</S.CardTitle>
                  <S.CommissionText>Comissão 35%</S.CommissionText>
                </S.SingleCard>
              </S.Card>
              <S.Card onClick={() => setIsVisible(!isVisible)}>
                <S.SingleCard color="green">
                  <S.CardIcon icon="no" />
                  <S.CardTitle>outros</S.CardTitle>
                  <S.CommissionText>Comissão 35%</S.CommissionText>
                </S.SingleCard>
              </S.Card>
            </S.ContainerCards>
          </S.ContentContainer>
        </S.ContainerProfessional>
        <S.ContainerProfessional>
          <S.H3>profissional da beleza</S.H3>
          <S.ContentContainer>
            <S.ContainerCards>
              <S.Card onClick={() => setIsVisible(!isVisible)}>
                <S.SingleCard color="pink">
                  <S.CardIcon icon="salaoBeleza" />
                  <S.CardTitle>salão de beleza</S.CardTitle>
                  <S.CommissionText>Comissão 35%</S.CommissionText>
                </S.SingleCard>
              </S.Card>
              <S.Card onClick={() => setIsVisible(!isVisible)}>
                <S.SingleCard color="pink">
                  <S.CardIcon icon="massagista" />
                  <S.CardTitle>massagista</S.CardTitle>
                  <S.CommissionText>Comissão 35%</S.CommissionText>
                </S.SingleCard>
              </S.Card>
              <S.Card onClick={() => setIsVisible(!isVisible)}>
                <S.SingleCard color="pink">
                  <S.CardIcon icon="esteticista" />
                  <S.CardTitle>esteticista</S.CardTitle>
                  <S.CommissionText>Comissão 35%</S.CommissionText>
                </S.SingleCard>
              </S.Card>
              <S.Card onClick={() => setIsVisible(!isVisible)}>
                <S.SingleCard color="pink">
                  <S.CardIcon icon="no" />
                  <S.CardTitle>outros</S.CardTitle>
                  <S.CommissionText>Comissão 35%</S.CommissionText>
                </S.SingleCard>
              </S.Card>
            </S.ContainerCards>
          </S.ContentContainer>
        </S.ContainerProfessional>
        <S.ContainerProfessional>
          <S.H3>revendedores online</S.H3>
          <S.ContentContainer>
            <S.ContainerCards>
              <S.Card onClick={() => setIsVisible(!isVisible)}>
                <S.SingleCard color="blue">
                  <S.CardIcon icon="influenciador" />
                  <S.CardTitle>influenciador</S.CardTitle>
                  <S.CommissionText>Comissão 35%</S.CommissionText>
                </S.SingleCard>
              </S.Card>
              <S.Card onClick={() => setIsVisible(!isVisible)}>
                <S.SingleCard color="blue">
                  <S.CardIcon icon="afiliado" />
                  <S.CardTitle>afiliado</S.CardTitle>
                  <S.CommissionText>Comissão 35%</S.CommissionText>
                </S.SingleCard>
              </S.Card>
              <S.Card onClick={() => setIsVisible(!isVisible)}>
                <S.SingleCard color="blue">
                  <S.CardIcon icon="dropshipping" />
                  <S.CardTitle>dropshipping</S.CardTitle>
                  <S.CommissionText>Comissão 35%</S.CommissionText>
                </S.SingleCard>
              </S.Card>
              <S.Card onClick={() => setIsVisible(!isVisible)}>
                <S.SingleCard color="blue">
                  <S.CardIcon icon="no" />
                  <S.CardTitle>outros</S.CardTitle>
                  <S.CommissionText>Comissão 35%</S.CommissionText>
                </S.SingleCard>
              </S.Card>
            </S.ContainerCards>
          </S.ContentContainer>
        </S.ContainerProfessional>
        <S.ContainerProfessional>
          <S.H3>revendedores offline</S.H3>
          <S.ContentContainer>
            <S.ContainerCards>
              <S.Card onClick={() => setIsVisible(!isVisible)}>
                <S.SingleCard color="orange">
                  <S.CardIcon icon="revendedoresCosm" />
                  <S.CardTitle>revendedores de cosméticos</S.CardTitle>
                  <S.CommissionText>Comissão 35%</S.CommissionText>
                </S.SingleCard>
              </S.Card>
              <S.Card onClick={() => setIsVisible(!isVisible)}>
                <S.SingleCard color="orange">
                  <S.CardIcon icon="revendedoresProdNat" />
                  <S.CardTitle>revendedores de cosméticos</S.CardTitle>
                  <S.CommissionText>Comissão 35%</S.CommissionText>
                </S.SingleCard>
              </S.Card>
              <S.Card onClick={() => setIsVisible(!isVisible)}>
                <S.SingleCard color="orange">
                  <S.CardIcon icon="lojaFisica" />
                  <S.CardTitle>revendedores de cosméticos</S.CardTitle>
                  <S.CommissionText>Comissão 35%</S.CommissionText>
                </S.SingleCard>
              </S.Card>
              <S.Card onClick={() => setIsVisible(!isVisible)}>
                <S.SingleCard color="orange">
                  <S.CardIcon icon="no" />
                  <S.CardTitle>revendedores de cosméticos</S.CardTitle>
                  <S.CommissionText>Comissão 35%</S.CommissionText>
                </S.SingleCard>
              </S.Card>
            </S.ContainerCards>
          </S.ContentContainer>
        </S.ContainerProfessional>

        <Modal title="Links Parceiros" variant="md" isVisible={isVisible} onClose={() => setIsVisible(!isVisible)}>
          <S.InfoCtn flex>
            <S.InfoCtn noMargin marginRight>
              <S.H4>link liberado para entrada sem aprovação</S.H4>
            </S.InfoCtn>
            <S.InfoCtn noMargin>
              <ToggleSwitch value={toggle} onChange={() => setToggle(!toggle)} />
            </S.InfoCtn>
          </S.InfoCtn>
          <S.InfoCtn>
            <S.SimpleText>Link para página de convite:</S.SimpleText>
            <InputShareCopy value="https://parceiro.everbynature.com.br/medico" id="iput-convite" />
          </S.InfoCtn>
          <S.InfoCtn>
            <S.SimpleText>Link direto para cadastro:</S.SimpleText>
            <InputShareCopy value="https://parceiro.everbynature.com.br/medico/cadastro" id="input-direto" />
          </S.InfoCtn>
        </Modal>
      </S.Container>
    </>
  );
};

export default ConviteParceiros;
