import styled, { css } from 'styled-components';
import { shade } from 'polished';

import Txt from '@components/atoms/Text';
import Lnk from '@components/atoms/Link';
import Spc from '@components/molecules/SpaceLink';
import Lb from '@components/atoms/Label';
import Inp from '@components/atoms/Input';

import Txtarea from '@components/atoms/Textarea';
import Btn from '@components/atoms/Button';

import PageLoadding from '@assets/images/page_loadding.gif';
import NotFound from '@assets/images/not-found.png';

const status = {
  active: () => css`
    display: inherit;
    max-width: inherit;
    -webkit-line-clamp: 3;
    -webkit-box-orient: vertical;
    overflow: inherit;
    text-overflow: inherit;
    line-height: 1.625;
    white-space: inherit;
    /*color: ${shade(0.2, '#5e5e5e')};*/
  `,
  default: () => css`
    cursor: pointer;
    display: -webkit-box;
    max-width: 90%;
    -webkit-line-clamp: 3;
    -webkit-box-orient: horizontal;
    overflow: hidden;
    text-overflow: ellipsis;
    line-height: 1.625;
    white-space: nowrap;
  `,
};

export const Container = styled.div`
  display: inline-flex;
  flex-direction: column;
  flex-wrap: wrap;
  -webkit-box-align: center;
  align-items: center;
  align-content: center;
  width: 100%;
  min-height: 95vh;
  border-radius: 15px;
  border: ${(props) => (props.theme === 'Dark' ? '1px solid #2F3743' : '1px solid #EAEAEA')};

  background: ${(props) => (props.theme === 'Dark' ? '#202731' : '#FFFFFF')};

  color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};

  &.space-justify-center-container {
    justify-content: center;
  }

  @media (min-width: 600px) {
    padding: 25px;
  }

  @media (max-width: 375px) {
    justify-content: start;

    margin-top: 80px;
    margin-bottom: 29px;
    margin-left: 23px;

    width: 330px;

    &.space-justify-center-container {
      justify-content: flex-start;
    }
  }

  @media (max-width: 320px) {
    justify-content: start;

    margin-top: 80px;
    margin-bottom: 29px;
    margin-left: 15px;

    width: 290px;

    &.space-justify-center-container {
      justify-content: flex-start;
    }
  }
`;

export const Div = styled.div`
  width: 90%;
  min-height: 700px;

  &.contact_form {
    max-width: 815px;
  }
`;

export const Textarea = styled(Txtarea)`
  max-width: 100%;
  min-width: 100%;
  width: 100%;
  max-height: 204px;
  min-height: 204px;
  height: 204px;
  border: ${(props) => (props.theme === 'Dark' ? '1px solid #47505E' : '1px solid #EAEAEA')};

  background: ${(props) => (props.theme === 'Dark' ? '#29303A' : '#FBFBFB')};

  color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
  ::placeholder {
    color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
  }
`;

export const DivConteudo = styled.div`
  margin-top: 35px;
  width: 100%;

  li {
    list-style: none;
  }

  a {
    text-decoration: none;
  }

  button {
    background: ${(props) => props.primaryColor};
  }
`;

export const TextTitulo = styled(Txt)`
  color: ${(props) => (props.theme === 'Dark' ? '#fff' : '#484646')};
  text-align: center;

  @media (max-width: 425px) {
    font-size: 20px;

    margin-top: 50px;
  }
`;

export const TextSubtitulo = styled(Txt)`
  text-align: center;

  margin-top: 7px;

  color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
`;

export const Titulo = styled(Txt)`
  text-align: center;

  margin-top: 20px;

  color: ${(props) => (props.theme === 'Dark' ? '#fff' : '#484646')};
`;

export const TituloDetalheForm = styled(Txt)`
  text-align: center;

  margin-top: 20px;

  color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
`;

export const Space = styled(Spc)`
  background-color: '#fff';
`;

export const Label = styled(Lb)`
  font-size: 16px;

  margin-top: 15px;

  color: ${(props) => (props.theme === 'Dark' ? '#fff' : '#484646')};

  flex: auto;

  transition: all 0.1s;
  /* transition-timing-function: cubic-bezier(0.61, -0.24, 0.32, 1.49); */

  /*
  &.question_label:hover {
    color: ${shade(-0.2, '#5e5e5e')};
  }
  */

  &.question_label {
    cursor: pointer;
    ${(props) => status[props.active]}
  }
`;

export const LabelDetailForm = styled(Lb)`
  font-size: 16px;
  font-weight: bold;

  margin-top: 15px;

  color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};

  flex: auto;

  transition: all 0.1s;
  /* transition-timing-function: cubic-bezier(0.61, -0.24, 0.32, 1.49); */

  /*
  &.question_label:hover {
    color: ${shade(-0.2, '#5e5e5e')};
  }
  */

  &.question_label {
    cursor: pointer;
    ${(props) => status[props.active]}
  }
`;

export const Input = styled(Inp)`
  height: 40px;
  border-radius: 15px;
  border: ${(props) => (props.theme === 'Dark' ? '1px solid #47505E' : '1px solid #EAEAEA')};

  background: ${(props) => (props.theme === 'Dark' ? '#29303A' : '#FBFBFB')};

  color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};

  ::placeholder {
    color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
  }
`;

export const Text = styled(TextTitulo)`
  margin-top: 20px;

  @media (max-width: 425px) {
    font-size: 16px;

    margin-top: 20px;
  }
`;

export const TextRodape = styled(Txt)`
  color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
  text-align: center;

  margin-top: 20px;

  @media (max-width: 425px) {
    font-size: 20px;

    margin-top: 50px;
  }
`;

export const ButtonCloser = styled.div`
  display: flex;
  justify-content: flex-end;

  margin-top: 0;

  width: 100%;
`;

export const CloseButton = styled(Lnk)`
  text-align: right;

  padding: 20px;

  letter-spacing: 0px;
  opacity: 1;

  color: #cbcaca;
`;

export const QuestionContainer = styled.div`
  padding: 30px 30px 71px 30px;

  border-radius: 15px;

  background: ${(props) => (props.theme === 'Dark' ? '#29303A' : '#FBFBFB')};

  color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};

  .video_content {
    iframe {
      display: block;

      max-width: 577px;
      width: 100%;
      max-height: 324px;
      height: -webkit-fill-available;

      margin: 0 auto;

      border: 0.5px solid #cbcace;
      border-radius: 8px;
    }
  }

  .answer_content {
    margin-bottom: 60px;

    p {
      color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
    }
  }
`;

export const QuestionLink = styled.a`
  display: flex;
  justify-content: space-between;
  align-items: center;

  margin-bottom: 20px;

  label {
    flex: inherit;
  }
`;

export const CardSpaceIcon = styled.div`
  height: 13px;
  width: 13px;

  p.indicative_arrow {
    font-size: 10px;
    color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
    font-weight: bold;
  }
`;

export const GetInTouch = styled.div`
  .link_default {
    text-decoration: none;
  }
  button {
    font-size: 17px;
    background: ${(props) => props.primaryColor};
  }
`;

export const InputContainer = styled.div``;

export const Button = styled(Btn)`
  font-size: 17px;
`;

export const ul = styled.ul`
  list-style-type: none;
  font-weight: bold;

  width: 100%;
`;

export const li = styled.li`
  text-align: center;

  margin: 5px;
  padding: 15px;

  width: 100%;

  border-radius: 15px;

  background-color: #cbcac0;
  color: black;
`;

export const LabelLink = styled(Label)``;

export const ContainerBackEndInfo = styled.div`
  padding: 20px;
`;

export const InfoBackEnd = styled.h2`
  color: ${(props) => (props.theme === 'Dark' ? '#fff' : '#484646')};
`;

export const ImageNotFound = styled.div`
  content: url(${NotFound});
  width: 18rem;
  height: auto;

  margin-top: 30px;
  margin: 0 auto;

  background-size: contain;
  background-repeat: no-repeat;

  @media (min-width: 1200px) {
    width: 16rem;
    height: auto;
  }
`;

export const ImageLoadding = styled.div`
  content: url(${PageLoadding});
  width: 20rem;
  height: auto;

  margin-top: 30px;
  margin: 0 auto;

  background-size: contain;
  background-repeat: no-repeat;

  @media (max-width: 600px) {
    width: 16rem;
    height: auto;
  }
`;
