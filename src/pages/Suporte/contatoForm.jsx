import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { useFormik } from 'formik';

import Button from '@components/atoms/Button';
import Select from '@components/atoms/Select';
import Inputfile from '@components/molecules/InputFile';

import { uploadFile, sendFormContact } from '@services/suporte_faq';

import Validator from './validator-suport-contact';

import * as S from './styled';

function FormSuporteContato() {
  const [arrayImage, setArrayImage] = useState([]);
  const { handleChange, handleBlur, values, errors, setFieldValue } = useFormik({
    initialValues: {
      sector: '',
      email: '',
      subject: '',
      description: '',
      file: '',
    },
    onSubmit: () => {},
    validationSchema: Validator,
  });

  const handleChangeImage = async (e) => {
    const formData = new FormData();
    const image = e.target.files[0];

    setFieldValue('file', e.target.value);

    formData.append('image', image, image.name);

    uploadFile(formData).then((response) => {
      console.log(response.data);
      const array = [];
      array.push(response.data.url);
      setArrayImage(...arrayImage, array);
    });
  };

  const handleSendEmail = async (e) => {
    e.preventDefault();
    const formContact = {
      sector: values.sector,
      email: values.email,
      subject: values.subject,
      description: values.description,
      attachments: arrayImage,
    };

    console.log(formContact);

    sendFormContact(formContact).then((response) => {
      console.log(response);
    });
  };

  const themeStore = useSelector((state) => state.theme);

  const [setorOps] = useState([
    {
      label: 'Consultoria de vendas',
      value: 'Consultoria de vendas',
    },
    {
      label: 'Recursos Humanos',
      value: 'Recursos Humanos',
    },
    {
      label: 'Controle de estoque',
      value: 'Controle de estoque',
    },
  ]);

  return (
    <S.Container theme={themeStore.themeName}>
      <S.ButtonCloser>
        <Link to="/suporte">
          <S.CloseButton>FecharX</S.CloseButton>
        </Link>
      </S.ButtonCloser>
      <S.Div className="contact_form">
        <S.Titulo size="lg" variant="bold" theme={themeStore.themeName}>
          Entrar em contato com o suporte
        </S.Titulo>
        <S.TextSubtitulo theme={themeStore.themeName} size="sm">
          Estamos aqui para ajudar. Conte-nos mais.
        </S.TextSubtitulo>
        <S.DivConteudo primaryColor={themeStore.primaryColor}>
          <form onSubmit={(e) => handleSendEmail(e)}>
            <S.InputContainer>
              <S.Label theme={themeStore.themeName}>Com qual setor deseja falar?</S.Label>
              <Select
                name="sector"
                id="sector"
                options={setorOps}
                onChange={(e) => {
                  setFieldValue('sector', e.target.value);
                }}
                defaultValue={values.sector}
              />
            </S.InputContainer>

            <S.InputContainer>
              <S.Label theme={themeStore.themeName}>E-mail:</S.Label>
              <S.Input
                id="email"
                type="text"
                name="email"
                placeholder="Seu E-mail"
                value={values.email}
                onChange={handleChange}
                onBlur={handleBlur}
                errorMessage={errors.email}
                theme={themeStore.themeName}
              />
            </S.InputContainer>

            <S.InputContainer>
              <S.Label theme={themeStore.themeName}>Assunto:</S.Label>
              <S.Input
                id="subject"
                type="text"
                name="subject"
                placeholder="Qual o Assunto"
                value={values.subject}
                onChange={handleChange}
                onBlur={handleBlur}
                errorMessage={errors.subject}
                theme={themeStore.themeName}
              />
            </S.InputContainer>

            <S.InputContainer>
              <S.Label theme={themeStore.themeName}>Descrição:</S.Label>
              <S.Textarea
                id="description"
                name="description"
                placeholder="Por favor os detalhes da sua solicitação. Um membro de nossa equipe irá respondé-lo(a) assim que possível."
                value={values.description}
                onChange={handleChange}
                theme={themeStore.themeName}
              />
            </S.InputContainer>

            <S.InputContainer>
              <S.Label theme={themeStore.themeName}>Anexo:</S.Label>
              <Inputfile
                name="file"
                type="file"
                placeholder="Adicione um arquivo"
                value={values.file}
                onChange={(e) => handleChangeImage(e)}
                onBlur={handleBlur}
                errorMessage={errors.subject}
                accept="image/*"
              />
            </S.InputContainer>

            <Button.Group className="center">
              <S.Button type="submit" variant="secondary">
                Enviar
              </S.Button>
            </Button.Group>
          </form>
        </S.DivConteudo>
      </S.Div>
    </S.Container>
  );
}
export default FormSuporteContato;
