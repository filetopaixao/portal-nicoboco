import * as Yup from 'yup';

const Validator = Yup.object({
  sector: Yup.string().max(18),
  email: Yup.string().email(),
  subject: Yup.string(),
  description: Yup.string(),
  file: Yup.string(),
});

export default Validator;
