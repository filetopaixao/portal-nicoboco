import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';

import Status from '@components/molecules/Status/userStatusPanel';
import Welcome from '@components/molecules/Welcome';
import Tools from '@components/molecules/Tools';
import Space from '@components/molecules/Space';
import PopUp from '@components/molecules/PopUp';

import { getHome } from '@services/home';
import { getUser } from '@services/user';
import { spacesUser, toolsUser } from '@config/userSpaceConfig';

import * as S from './styled';

const Home = () => {
  const themeStore = useSelector((state) => state.theme);
  const [userFinancesInfo, setUserFinancesInfo] = useState({});
  const [userPanelPhrase, setUserPanelPhrase] = useState('');
  const [userTools, setUserTools] = useState([]);
  const [userSpaceUtilized, setUserSpaceUtilized] = useState([]);
  const [userType, setUserType] = useState('admin');
  const [userName, setUserName] = useState('admin');
  const [isModalVisible, setIsModalVisible] = useState(false);

  const [load, setLoad] = useState([]);

  const firstSession = localStorage.getItem('first_session');

  useEffect(() => {
    getHome()
      .then((response) => {
        if (response.data) {
          const { coins, commissionNotReleased, commissionReleased, totalEarnings, phrase } = response.data;
          const info = {
            coins,
            commissionNotReleased,
            commissionReleased,
            totalEarnings,
          };
          setUserFinancesInfo(info);
          setUserPanelPhrase(phrase);

          if (!firstSession) {
            setTimeout(() => {
              setLoad(info);
            }, 6000);
          }
          setLoad(info);
        }
      })
      .catch((err) => {
        console.log(err);
        setUserFinancesInfo({
          coins: 0,
          commissionNotReleased: 0,
          commissionReleased: 0,
          totalEarnings: 0,
        });
        setUserPanelPhrase('');
      });
  }, [firstSession]);

  useEffect(() => {
    getUser().then((response) => {
      if (response.data) {
        const { first_name: name, type_id: typeUser, gender } = response.data;
        if (typeUser === 2) {
          setUserSpaceUtilized(spacesUser.entrepreneurs);
          setUserTools(toolsUser.entrepreneurs);
          if (gender === 'male') {
            setUserType('Empreendedor');
          } else {
            setUserType('Empreendedora');
          }
        }
        if (typeUser === 3) {
          if (gender === 'female') {
            setUserType('Parceiro)');
          } else {
            setUserType('Parceira');
          }
        }
        setUserName(name);
      }
    });

    setUserSpaceUtilized(spacesUser.entrepreneurs);
    setUserTools(toolsUser.entrepreneurs);
  }, []);

  useEffect(() => {
    if (!firstSession) {
      setIsModalVisible(true);
      localStorage.setItem('first_session', true);
    }
  }, [firstSession]);

  return (
    <>
      {load.length <= 0 ? (
        <S.ImageLoadding />
      ) : (
        <S.Container theme={themeStore.themeName}>
          <S.Status>
            <Status data={userFinancesInfo} />
          </S.Status>
          <Welcome person={userType} name={userName} msg={userPanelPhrase} author=" " />
          <Tools title="FERRAMENTAS DE VENDAS ONLINE" data={userTools} />
          <Space title="ESPAÇO FPASS" data={userSpaceUtilized} />
        </S.Container>
      )}

      <PopUp isModalVisible={isModalVisible} setIsModalVisible={() => setIsModalVisible(false)} />
    </>
  );
};

export default Home;
