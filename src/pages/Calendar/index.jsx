import React, { useState, useEffect } from 'react';
import ReactDOMServer from 'react-dom/server';

import getInfo from '@services/calendar';
import FullCalendar from '@fullcalendar/react';
import dayGridPlugin from '@fullcalendar/daygrid';

import Events from './events';
import Welcome from './calendar';

import * as S from './styled';

const Calendars = () => {
  const [events, setEvents] = useState([]);
  // const [month] = useState(6);
  const width = window.innerWidth;

  useEffect(() => {
    getInfo().then((response) => {
      setEvents(response.data);
      console.log('EVENTS', response.data);
    });

    // const allEvents = [
    //   {
    //     id: 1,
    //     color: 'pink',
    //     title: 'Titulo',
    //     desc: 'Este é o conteúdo deste item',
    //     date: '2020-05-03',
    //   },
    //   {
    //     id: 2,
    //     color: 'green',
    //     title: 'Titulo',
    //     desc: 'Este é o conteúdo deste item',
    //     date: '2020-06-14',
    //   },
    //   {
    //     id: 3,
    //     color: 'yellow',
    //     title: 'Titulo',
    //     desc: 'Este é o conteúdo deste item',
    //     date: '2020-06-05',
    //   },
    //   {
    //     id: 4,
    //     color: 'blue',
    //     title: 'Titulo',
    //     desc: 'Este é o conteúdo deste item',
    //     date: '2020-07-21',
    //   },
    //   {
    //     id: 5,
    //     color: 'yellow',
    //     title: 'Titulo',
    //     desc: 'Este é o conteúdo deste item',
    //     date: '2020-08-08',
    //   },
    //   {
    //     id: 6,
    //     color: 'blue',
    //     title: 'Titulo',
    //     desc: 'Este é o conteúdo deste item',
    //     date: '2020-09-28',
    //   },
    // ];
  }, []);

  return (
    <S.Container>
      <Welcome />
      <S.Row>
        {width > 700 ? <S.HalfContainer id="eventsList" /> : false}
        <S.CalendarGrid>
          <FullCalendar
            plugins={[dayGridPlugin]}
            initialView="dayGridMonth"
            headerToolbar={{
              start: 'prev',
              center: 'title',
              end: 'next',
            }}
            view
            titleFormat={{ month: 'long', year: 'numeric' }}
            locale="pt-br"
            buttonText={{
              prev: '<',
              next: '>',
            }}
            events={async (info, successCallback, failureCallback) => {
              try {
                const currentDate = info.end;
                currentDate.setMonth(currentDate.getMonth() - 1);

                const currentMonthStr = currentDate
                  .toLocaleString([], {
                    weekday: 'short',
                    year: 'numeric',
                    month: 'long',
                    day: 'numeric',
                  })
                  .split(' ')[3];

                const currentYearStr = currentDate
                  .toLocaleString([], {
                    weekday: 'short',
                    year: 'numeric',
                    month: 'long',
                    day: 'numeric',
                  })
                  .split(' ')[5];

                const titleCalendar = await (
                  <S.TitleCalendar>
                    <h2>{currentMonthStr}</h2>
                    <p>{currentYearStr}</p>
                  </S.TitleCalendar>
                );

                document.getElementsByClassName(
                  'fc-toolbar-title'
                )[0].innerHTML = await ReactDOMServer.renderToStaticMarkup(titleCalendar);

                const currentMonth = await (parseInt(info.endStr.split('-')[1], 10) - 1);
                const currentYear = await parseInt(info.endStr.split('-')[0], 10);
                let isEmpty = true;
                let eventsList = events.map((event) => {
                  // console.log('PARSE', parseInt(event.date.split('-')[0], 10));
                  // console.log('CURRENT', currentMonth);

                  if (
                    parseInt(event.date.split('-')[1], 10) === currentMonth &&
                    parseInt(event.date.split('-')[0], 10) === currentYear
                  ) {
                    isEmpty = false;
                    return (
                      <Events
                        key={event.id}
                        color="blue"
                        desc={event.description}
                        date={event.date}
                        title={event.name}
                      />
                    );
                  }

                  return null;
                });

                if (isEmpty) {
                  eventsList = (
                    <S.EmptyEvents>
                      <p>Não há evento para esta data</p>
                    </S.EmptyEvents>
                  );
                }

                document.getElementById('eventsList').innerHTML = ReactDOMServer.renderToStaticMarkup(eventsList);
                let backgroundColor = '';
                let borderColor = '';

                successCallback(
                  events.map((event) => {
                    switch (event.color) {
                      case 'pink':
                        backgroundColor = '#F14479';
                        borderColor = '#F14479';
                        break;
                      case 'green':
                        backgroundColor = '#2FDF46';
                        borderColor = '#2FDF46';
                        break;
                      case 'yellow':
                        backgroundColor = '#F6B546';
                        borderColor = '#F6B546';
                        break;
                      default:
                        backgroundColor = '#01A3FF';
                        borderColor = '#01A3FF';
                        break;
                    }

                    // console.log('DATEEEE', event.date.getDate() + 1);
                    const newDate = new Date(event.date);
                    console.log('NEW DATE', newDate);

                    return {
                      id: event.id,
                      date: newDate.setDate(newDate.getDate() + 1),
                      backgroundColor,
                      borderColor,
                    };
                  })
                );
              } catch (err) {
                failureCallback(err);
              }
            }}
          />
        </S.CalendarGrid>
        {width <= 700 ? <S.HalfContainer id="eventsList" /> : false}
      </S.Row>
    </S.Container>
  );
};

export default Calendars;
