import React from 'react';
import PropTypes from 'prop-types';

import Text from '@components/atoms/Text';
import * as S from './styled';

const WelcomeCalendar = ({ msg }) => (
  <S.ContainerStatus image="calendar" className="container1">
    <div className="calendar__container">
      <Text size="md" variant="light" className="welcome__description">
        {msg}
      </Text>
    </div>
  </S.ContainerStatus>
);

WelcomeCalendar.propTypes = {
  msg: PropTypes.string,
};
WelcomeCalendar.defaultProps = {
  msg: 'Acompanhe os eventos da EVER e aproveite os conteúdos exclusivos para Empreendedores.',
};

export default WelcomeCalendar;
