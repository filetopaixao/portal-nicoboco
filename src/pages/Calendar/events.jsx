import React from 'react';
import Text from '@components/atoms/Text';
import { PropTypes } from 'prop-types';
import { Item, Dates, Content } from './styled';

function Itens({ color, desc, title, date }) {
  let Wrapper;
  let DateStyle;
  const upperTitle = title.toUpperCase();
  switch (color) {
    case 'pink':
      Wrapper = Item.pink;
      DateStyle = Dates.pink;
      break;
    case 'green':
      Wrapper = Item.green;
      DateStyle = Dates.green;
      break;
    case 'yellow':
      Wrapper = Item.yellow;
      DateStyle = Dates.yellow;
      break;
    default:
      Wrapper = Item.blue;
      DateStyle = Dates.blue;
      break;
  }
  let dt = date.split('-');
  dt = dt.map((elem) => parseInt(elem, 10));
  const data = new Date(Date.UTC(dt[0], dt[1] - 1, dt[2] + 1, 0, 0, 0));

  const formatedDate = Intl.DateTimeFormat('pt-BR', { month: 'short', day: 'numeric' }).format(data);
  return (
    <Wrapper>
      <DateStyle size="md" variant="bold" className="item-event">
        {formatedDate}
      </DateStyle>
      <Content>
        <Text size="md" variant="bold" className="item-event item-title">
          {upperTitle}
        </Text>
        <Text size="sm" variant="light" className="item-event">
          {desc}
        </Text>
      </Content>
    </Wrapper>
  );
}

Itens.propTypes = {
  color: PropTypes.string.isRequired,
  desc: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  date: PropTypes.string.isRequired,
};
export default Itens;
