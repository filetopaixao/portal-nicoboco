import DesignSystem from 'design-system-utils';

import FontsConfig from './fonts';
import ColorConfig from './colors';
import GridConfigs from './grid';
import RadiusConfigs from './radius';

const fonts = FontsConfig;
const colors = ColorConfig;
const grid = GridConfigs;
const radius = RadiusConfigs;

const designTokens = {
  colors: {
    pallete: colors,
  },
  grid: {
    breakpoints: grid.breakpoints,
    containers: grid.container,
  },
  types: {
    baseFont: fonts.baseFontSize,
    sizes: fonts.sizes,
  },
  radius,
};

export default new DesignSystem(designTokens);
