import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';

import Toast from '@components/atoms/Toast';

import WahtsApp from '@assets/images/whatsapp.svg';
import Facebook from '@assets/images/facebook.svg';
import Email from '@assets/images/email.svg';

import * as S from './styled';

const ButtonGroupShareCopy = ({ urlWhatsApp, urlFacebook, urlEmail }) => {
  const themeStore = useSelector((state) => state.theme);
  const [isToShareVisible, setIsToShareVisible] = useState(false);
  const [isToastVisible, setIsToastVisible] = useState(false);

  const handleToShare = () => {
    setIsToShareVisible(!isToShareVisible);

    setTimeout(() => {
      setIsToShareVisible(false);
    }, 50000);
  };

  const handleOutSideClick = (e) => {
    if (e.target.id === 'toast') setIsToShareVisible(false);
  };

  const handleActiveToast = () => {
    setIsToastVisible(true);

    setTimeout(() => {
      setIsToastVisible(false);
    }, 3000);
  };

  const handleCopy = () => {
    const btnCopy = document.getElementById('copy');
    btnCopy.select();
    document.execCommand('copy');

    handleActiveToast();
  };

  return (
    <>
      <S.Container id="toast" onClick={handleOutSideClick}>
        <S.ContainerToast>
          <S.Button
            width="100px"
            alignText="left"
            height="33px"
            padding="8px 15px"
            onClick={handleCopy}
            type="button"
            icon="copy"
            showIcon
            secondaryColor={themeStore.secondaryColor}
          >
            Copiar
          </S.Button>
        </S.ContainerToast>
        <S.ContainerToShare>
          <S.Button
            width="135px"
            alignText="left"
            height="33px"
            padding="8px 15px"
            onClick={() => handleToShare()}
            type="button"
            icon="share"
            showIcon
            secondaryColor={themeStore.secondaryColor}
          >
            Compatilhar
          </S.Button>
          {isToShareVisible ? (
            <S.ContentToShare>
              <li>
                <a href={urlWhatsApp} rel="noopener noreferrer" target="_blank">
                  <img src={WahtsApp} alt="WhatsApp" />
                  <p>WhatsApp</p>
                </a>
              </li>
              <li>
                <a href={urlFacebook} rel="noopener noreferrer" target="_blank">
                  <img src={Facebook} alt="Facebook" />
                  <p>Facebook</p>
                </a>
              </li>
              <li>
                <a href={urlEmail} rel="noopener noreferrer" target="_blank">
                  <img src={Email} alt="E-mail" />
                  <p>E-mail</p>
                </a>
              </li>
            </S.ContentToShare>
          ) : null}
        </S.ContainerToShare>
      </S.Container>
      <Toast
        variant="success"
        icon="success"
        showIcon
        isVisible={isToastVisible}
        onClose={() => setIsToastVisible(!isToastVisible)}
      >
        Link copiado com sucesso!!
      </Toast>
    </>
  );
};

ButtonGroupShareCopy.propTypes = {
  urlWhatsApp: PropTypes.oneOfType([PropTypes.string]),
  urlFacebook: PropTypes.oneOfType([PropTypes.string]),
  urlEmail: PropTypes.oneOfType([PropTypes.string]),
};
ButtonGroupShareCopy.defaultProps = {
  urlWhatsApp: '!#',
  urlFacebook: '!#',
  urlEmail: '!#',
};

export default ButtonGroupShareCopy;
