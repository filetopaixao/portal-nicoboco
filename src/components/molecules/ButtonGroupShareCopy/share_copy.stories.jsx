import React from 'react';
import { withKnobs } from '@storybook/addon-knobs';

import { ContainerStorybook } from '@assets/styles/components';
import ButtonGroupShareCopy from './index';

export default {
  title: 'molecules/ButtonGroupShareCopy',
  component: ButtonGroupShareCopy,
  decorators: [withKnobs],
};

export const element = () => (
  <ContainerStorybook>
    <ButtonGroupShareCopy />
  </ContainerStorybook>
);

element.story = {
  name: 'Default',
};
