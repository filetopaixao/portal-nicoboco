import styled, { css } from 'styled-components';
import { shade } from 'polished';

import IconCopy from '@assets/images/copyWhite.svg';
import IconShare from '@assets/images/shareWhite.svg';

const icons = {
  copy: () => css`
    background: url(${IconCopy}) no-repeat;
  `,
  share: () => css`
    background: url(${IconShare}) no-repeat;
  `,
};

const iconsPosition = {
  arrowRight: () => css`
    right: -1px;
    top: 10px;
    width: 20px;
  `,
  share: () => css`
    top: 6px;
  `,
  center: () => css`
    position: inherit;
    right: inherit;
    top: inherit;
  `,
};
const showIcon = () => css`
  &::after {
    content: '';
    clear: both;
    display: block;
    visibility: visible;

    position: absolute;
    right: 8px;
    top: 8px;

    @media (min-width: 1500px) {
      height: 25px;
      width: 25px;
    }
    height: 17px;
    width: 17px;
  }
`;

export const Container = styled.div`
  padding: 0;
  display: flex;

  button {
    @media (min-width: 1050px) {
      font-size: 12px;

      ::after {
        height: 18px;
      }
    }

    transition: background-color 0.2s;
    z-index: 2;

    @media (max-width: 1050px) {
      display: block;
      margin-top: 10px;
      font-size: 12px;
    }
  }

  button:focus {
    outline: thin dotted;
    outline: 0px auto -webkit-focus-ring-color;
    outline-offset: 0px;
  }

  button:hover {
    background: ${shade(0.2, '#1045d4')};
  }

  button:first-child {
    margin-bottom: 10px;

    @media (min-width: 414px) {
      margin-bottom: 0;
      margin-right: 10px;
    }

    @media (min-width: 1024px) {
      margin-left: 10px;
    }
  }

  @media (min-width: 1024px) {
    flex-direction: row;
  }
`;

export const ContainerToShare = styled.div`
  //display: block;

  justify-content: center;
  display: inline-flex;
`;

export const ContainerToast = styled.div``;

export const ContentToShare = styled.ul`
  position: absolute;

  width: 150px;

  margin-top: 33px;
  margin-left: 20px;
  padding: 10px;

  list-style: none;
  border-radius: 10px;
  z-index: 2;

  background: #fff;

  @media (max-width: 800px) {
    margin-top: 45px;
  }

  li {
    display: flex;

    img {
      width: 20px;
      height: 20px;

      margin-right: 15px;
    }

    p {
      font-size: 13px;

      color: #5e5e5e;
    }

    &:nth-child(-n + 2) {
      margin-bottom: 12px;
    }

    a {
      text-decoration: none;

      display: flex;
      justify-content: flex-end;

      cursor: pointer;
    }
  }
`;

export const Button = styled.button`
  font-size: 18px;
  text-transform: uppercase;
  text-align: ${(props) => props.alignText};

  position: relative;
  cursor: pointer;
  border: none;
  border-radius: 15px;

  box-shadow: 0 3px 6px rgba(0, 0, 0, 0.16);
  outline: 0;

  color: #fff;

  height: ${(props) => (props.height ? props.height : '33px')};
  width: ${(props) => props.width};
  margin: ${(props) => props.margin};
  padding: ${(props) => props.padding};

  ${(props) => (props.fullSize ? 'width: 100%;' : '')};

  background: ${(props) => `${props.secondaryColor} !important`};

  ${(props) => (props.showIcon ? showIcon : '')};

  &::after {
    ${(props) => icons[props.icon]};
    ${(props) => iconsPosition[props.iconPosition]};
    background-size: contain;
  }
  /* @media (min-width: 1500px) {
    min-height: 38px;
  } */
`;
