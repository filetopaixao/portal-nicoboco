import styled, { css } from 'styled-components';

import CloudFooter from '@assets/images/cloud_footer.png';
import Clouds from '@assets/images/clouds.png';
import capaFpass from '@assets/images/capaFpass.png';

const flyer = {
  active: () => css`
    bottom: 170%;
  `,
};

export const Container = styled.div`
  div#modal {
    @media (min-width: 1100px) {
      align-items: center;
    }

    > div {
      display: flex;
      align-items: center;

      width: 90%;

      box-shadow: 0px 3px 26px #00000040;

      color: #fff;
      background: ${(props) =>
        props.tertiaryColor
          ? `transparent linear-gradient(180deg, ${props.tertiaryColor} 0%, ${props.primaryColor} 46%, ${props.secondaryColor} 100%) 0% 0% no-repeat padding-box;`
          : `transparent linear-gradient(180deg, ${props.primaryColor} 0%, ${props.secondaryColor} 100%) 0% 0% no-repeat padding-box;`};
      /*
      &::after {
        display: flex;

        min-height: 110px;

        margin: -31px;

        content: '';
        clear: both;
        visibility: visible;
        position: relative;

        //background: url(${CloudFooter}) no-repeat;
        background-size: cover;
      }
      

      @media (min-width: 1100px) {
        display: grid;

        width: 55%;

        margin-top: inherit;
        padding: 0;
      }
      */
    }
  }
`;

export const Content = styled.div`
  display: grid;
  grid-template-rows: 1fr 1fr;
  grid-template-areas:
    'contentInfo'
    'contentImage';

    .container-background-cloud{
      display: flex;
      flex-direction: column;
      align-items: center;
      justify-content: center;
    }
  /*
  .container-background-cloud {
    &::after {
      content: '';
      display: block;
      position: relative;
      min-height: 110px;
      width: 90%;
      margin: 0 auto;
      clear: both;
      visibility: visible;
      background: url(${Clouds}) no-repeat;
      background-size: contain;
      margin-top: -116px;
    }
    */
  }

  @media (min-width: 1100px) {
    grid-template-rows: inherit;
    grid-template-columns: 1fr 1fr;
    grid-template-areas: 'contentImage contentInfo';
  }
`;

export const ContentInfo = styled.section`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;

  font-size: 17px;
  text-align: left;

  //margin-top: 50px;

  grid-area: contentInfo;

  button {
    display: block;

    height: 50px;
    width: 270px;

    background: ${(props) => props.primaryColor};

    @media (max-width: 500px) {
      margin: 50px auto 0 auto;
    }

    @media (min-width: 500px) {
      width: 250px;

      margin: 50px auto;
    }
  }

  header {
    margin-top: 20px;
    margin-bottom: 40px;
  }

  header h3 {
    font-weight: 600;
    font-size: 24px;
    padding: 0px;
    text-align: center;

    @media (min-width: 1100px) {
      padding-right: 20px;
      text-align: left;
    }
  }

  p {
    margin-bottom: 20px;
    padding: 0;
    text-align: center;

    @media (min-width: 1100px) {
      padding-right: 20px;
      text-align: left;
    }
  }
`;

export const ContainerImage = styled.div`
  grid-area: contentImage;

  height: 280px;
  width: 200px;

  margin: 0 auto;

  /* position: relative;
  bottom: -33%;
  z-index: 1; */

  ${(props) => props.active && flyer.active}

  transition: bottom 1s;
  transition-timing-function: cubic-bezier(0.26, 0.44, 1, 0.46);
`;

export const Image = styled.div`
  height: 280px;
  width: 280px;
  ${(props) =>
    props.image.includes('data:image')
      ? `background: url(${props.image}) no-repeat;`
      : `background: url(${capaFpass}) no-repeat;`};
  background-size: contain;
`;
