/* eslint-disable react-hooks/rules-of-hooks */
import React, { useState } from 'react';
import Button from '@components/atoms/Button/styled';
import { withKnobs } from '@storybook/addon-knobs';

import { ContainerStorybook } from '@assets/styles/components';
import PopUp from './index';

export default {
  title: 'molecules/PopUp',
  component: PopUp,
  decorators: [withKnobs],
};

export const element = () => {
  const [isModalVisible, setIsModalVisible] = useState(false);

  return (
    <ContainerStorybook>
      <PopUp isVisible={isModalVisible} onClose={() => setIsModalVisible(!isModalVisible)} />
      <Button variant="primary" onClick={() => setIsModalVisible(!isModalVisible)}>
        show modal
      </Button>
    </ContainerStorybook>
  );
};

element.story = {
  name: 'Default',
};
