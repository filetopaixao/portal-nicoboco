import React from 'react';
import PropTypes from 'prop-types';

import Text from '@components/atoms/Text';

import Video from '@assets/images/video.svg';

import * as S from './styled';

const BannerEcommerce = ({ link, image, msg, imageMobile, openModalVideo, simple }) => (
  <S.BannerContainerSimple image={image} imageMobile={imageMobile}>
    <S.BannerContentSimple simple={simple}>
      <Text size="md" variant="light" className="welcome__description">
        {msg}
      </Text>
      <div>
        <S.BannerLinkText>{link}</S.BannerLinkText>
      </div>
    </S.BannerContentSimple>
    <S.BannerContentVideoSimple>
      <a href="#!" onClick={() => openModalVideo(true)}>
        <img src={Video} alt="Assitir" />
      </a>
    </S.BannerContentVideoSimple>
  </S.BannerContainerSimple>
);

BannerEcommerce.propTypes = {
  link: PropTypes.string,
  image: PropTypes.string,
  msg: PropTypes.string,
  imageMobile: PropTypes.bool,
  openModalVideo: PropTypes.func,
  simple: PropTypes.bool,
};
BannerEcommerce.defaultProps = {
  link: '',
  image: '',
  msg: '',
  imageMobile: true,
  openModalVideo: () => {},
  simple: true,
};

export default BannerEcommerce;
