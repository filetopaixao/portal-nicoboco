import styled, { css } from 'styled-components';

const variants = {
  sales: () => css`
    background: transparent linear-gradient(270deg, #fc599a 0%, #ffc400 100%);
  `,
  commision: () => css`
    background: transparent linear-gradient(270deg, #1289e3 0%, #3ce9e9 100%);
  `,
  lootReleased: () => css`
    background: transparent linear-gradient(270deg, #28bc8f 0%, #6aff6f 100%);
  `,
  totalBilled: () => css`
    background: transparent linear-gradient(90deg, #e351d0 0%, #9355d9 100%);
  `,
  bank: () => css`
    background: linear-gradient(261deg, #000 0%, #fff 100%);
  `,
  shopping: () => css`
    background: linear-gradient(261deg, #f14479 0%, #ffaf28 100%);
  `,
  marketing: () => css`
    background: linear-gradient(261deg, #28bc8f 0%, #6aff6f 100%);
  `,
  university: () => css`
    background: linear-gradient(261deg, #1289e3 0%, #3ce9e9 100%);
  `,
  discount: () => css`
    background: linear-gradient(261deg, #9355d9 0%, #e351d0 100%);
  `,
};

const bgVariantNotification = {
  success: () => css`
    background: #2fdf46;
  `,
  info: () => css`
    background: #2fdfd3;
  `,
  infoDark: () => css`
    background: #1590e3;
  `,
  warning: () => css`
    background: #f6b546;
  `,
  danger: () => css`
    background: #f14479;
  `,
  ever: () => css`
    background: #ff7f00;
  `,
  partner: () => css`
    background: #9655d9;
  `,
};

export const ContainerTool = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: flex-start;
  flex-wrap: wrap;

  margin-top: 25px;
  width: 100%;
`;

export const Cards = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  column-gap: 20px;
  row-gap: 10px;

  width: 100%;
  padding: 10px;

  border: ${(props) => (props.theme === 'Dark' ? '1px solid #2F3743' : '1px solid #EAEAEA')};
  border-radius: 15px;
  background: ${(props) => (props.theme === 'Dark' ? '#202731' : '#FFFFFF')};

  @media (min-width: 800px) {
    display: grid;
    grid-template-columns: 1fr 1fr 1fr 1fr;
    column-gap: 20px;
    row-gap: 10px;

    padding: 15px;
  }

  a {
    text-decoration: none;
  }
`;

export const Title = styled.h3`
  font-weight: 600;

  display: block;
  margin-bottom: 10px;
  width: 100%;

  @media (max-width: 1336px) {
    font-size: 14px;
  }

  @media (min-width: 1337px) {
    font-size: 18px;
  }
`;

export const CardSpace = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-wrap: wrap;
  padding: 5px;
  position: relative;
  height: 100%;
  width: 100%;
  border-radius: 15px;
  margin: 5px;

  @media (min-width: 800px) {
    padding: 15px;
  }

  /*${(props) => (props.variant ? variants[props.variant] : 'background: #E1DEDE;')};*/
`;

export const CardSale = styled(CardSpace)`
  cursor: pointer;

  margin: 0 auto;
  max-width: 320px;

  background: ${(props) => (props.theme ? props.theme : '#E1DEDE')};
  /*${(props) => bgVariantNotification[props.variant]}*/
`;

export const CardSaleTitle = styled.p`
  font-size: 14px;
  font-weight: bold;
  text-transform: uppercase;

  ${(props) => (props.isComing ? 'opacity: 0.5;' : null)}

  color: #fff;

  @media (max-width: 800px) {
    font-size: 10px;
    font-weight: lighter;
  }

  @media (min-width: 1500px) {
    font-size: 17px;
  }
`;
