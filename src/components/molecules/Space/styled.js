import styled from 'styled-components';

export const ContainerSpace = styled.div`
  /* display: flex;
  justify-content: flex-start;
  align-items: flex-start;
  flex-wrap: wrap; */

  margin-top: 25px;
  width: 100%;
`;

export const Cards = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  column-gap: 10px;
  row-gap: 10px;

  width: 100%;
  padding: 10px;

  border-radius: 15px;
  border: ${(props) => (props.theme === 'Dark' ? '1px solid #2F3743' : '1px solid #EAEAEA')};

  background: ${(props) => (props.theme === 'Dark' ? '#202731' : '#fff')};

  @media (min-width: 800px) {
    display: grid;
    grid-template-columns: 1fr 1fr 1fr 1fr;
    column-gap: 20px;

    padding: 15px;
  }

  /* @media (max-width: 414px) {
    justify-content: center;
  }

  @media (min-width: 414px) {
    justify-content: space-between;
    padding: 20px;
  }

  @media (min-width: 1366px) {
    padding: 35px 80px;
  } */

  a {
    text-decoration: none;
  }
`;

export const Title = styled.h3`
  font-weight: 600;

  display: block;
  margin-bottom: 10px;
  width: 100%;

  @media (max-width: 1337px) {
    font-size: 14px;
  }

  @media (min-width: 1337px) {
    font-size: 18px;
  }
`;
