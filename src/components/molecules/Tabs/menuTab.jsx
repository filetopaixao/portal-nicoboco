import React, { useState } from 'react';
import PropTypes from 'prop-types';

import Banner from '@components/molecules/Welcome/banner';

import * as S from './styled';

const MenuTab = ({ tabs }) => {
  const [activeBtn, setActiveBtn] = useState();
  const [bannerTxt, setBannerTxt] = useState(tabs[0].bannerTxt ? tabs[0].bannerTxt : '');
  const [content, setContent] = useState(tabs[0].content);

  const clickButton = (key) => {
    setContent(tabs[key].content);
    setBannerTxt(tabs[key].bannerTxt);
    setActiveBtn(key);
  };

  return (
    <S.Container>
      <S.GridButtons qtd={tabs.length} menu>
        {tabs.map((tab, key) => {
          return (
            <S.Column>
              <S.Button
                onClick={() => clickButton(key)}
                id={`btn${key}`}
                active={activeBtn ? key === activeBtn : key === 0}
              >
                {tab.button}
              </S.Button>
            </S.Column>
          );
        })}
      </S.GridButtons>
      <S.Welcome>
        <Banner image="partners" copyShare={false} title={bannerTxt} />
      </S.Welcome>
      <S.ContentCointainer menu>
        <S.Content>{content}</S.Content>
      </S.ContentCointainer>
    </S.Container>
  );
};

MenuTab.propTypes = {
  tabs: PropTypes.arrayOf(PropTypes.object),
};

MenuTab.defaultProps = {
  tabs: [
    {
      content: 'Content 1',
      button: 'Button 1',
    },
    {
      content: 'Content 2',
      button: 'Button 2',
    },
  ],
};

export default MenuTab;
