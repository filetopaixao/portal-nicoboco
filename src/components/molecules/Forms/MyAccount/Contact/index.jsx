import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { useFormik } from 'formik';

import { getUserSocials, addUserSocials } from '@services/user';

import Schema from './Schema';

import * as S from '../styled';

const Contact = () => {
  const themeStore = useSelector((state) => state.theme);
  const { handleChange, setFieldValue, handleSubmit, errors, values } = useFormik({
    initialValues: {
      email: '',
      phone: '',
      whatsapp: '',
      facebook: '',
      instagram: '',
      youtube: '',
    },
    onSubmit: async () => {
      await addUserSocials(values);
    },
    validationSchema: Schema,
    validateOnChange: false,
    validateOnBlur: false,
  });

  useEffect(() => {
    getUserSocials()
      .then((response) => {
        const { data } = response;
        setFieldValue('email', data.email);
        setFieldValue('phone', data.phone);
        setFieldValue('whatsapp', data.whatsapp);
        setFieldValue('facebook', data.facebook);
        setFieldValue('instagram', data.instagram);
        setFieldValue('youtube', data.youtube);
      })
      .catch(() => {
        setFieldValue('email', 'mail@mail.com');
        setFieldValue('phone', '(00) 0 0000-0000');
        setFieldValue('whatsapp', '(00) 0 0000-0000');
        setFieldValue('facebook', 'user_facebook');
        setFieldValue('instagram', 'user_insta');
        setFieldValue('youtube', 'user_youtube');
      });
  }, [setFieldValue]);

  return (
    <S.FormAccount onSubmit={handleSubmit}>
      <S.FormGroup>
        <S.LabelAccount theme={themeStore.themeName} htmlFor="inputEmail">
          E-mail:
        </S.LabelAccount>
        <S.InputAccount
          name="email"
          variant="formData"
          type="text"
          id="inputEmail"
          onChange={handleChange}
          value={values.email}
          theme={themeStore.themeName}
        />
        {errors.email ? <S.ErrorMessage>{errors.email}</S.ErrorMessage> : null}
      </S.FormGroup>
      <S.Row>
        <S.InputLeft>
          <S.LabelAccount theme={themeStore.themeName} htmlFor="inputPhone">
            Telefone:
          </S.LabelAccount>
          <S.InputAccount
            name="phone"
            variant="formData"
            type="text"
            id="inputPhone"
            onChange={handleChange}
            value={values.phone}
            theme={themeStore.themeName}
          />
          {errors.phone ? <S.ErrorMessage>{errors.phone}</S.ErrorMessage> : null}
        </S.InputLeft>
        <S.InputRight>
          <S.LabelAccount theme={themeStore.themeName} htmlFor="inputWhatsapp">
            WhatsApp:
          </S.LabelAccount>
          <S.InputAccount
            name="whatsapp"
            variant="formData"
            type="text"
            id="inputWhatsapp"
            onChange={handleChange}
            value={values.whatsapp}
            theme={themeStore.themeName}
          />
          {errors.whatsapp ? <S.ErrorMessage>{errors.whatsapp}</S.ErrorMessage> : null}
        </S.InputRight>
      </S.Row>
      <S.FormGroup>
        <S.LabelAccount theme={themeStore.themeName} htmlFor="inputFacebook">
          Facebook:
        </S.LabelAccount>
        <S.InputAccount
          name="facebook"
          variant="formData"
          type="text"
          id="inputFacebook"
          onChange={handleChange}
          value={values.facebook}
          theme={themeStore.themeName}
        />
        {errors.facebook ? <S.ErrorMessage>{errors.facebook}</S.ErrorMessage> : null}
      </S.FormGroup>
      <S.FormGroup>
        <S.LabelAccount theme={themeStore.themeName} htmlFor="inputInstagram">
          Instagram:
        </S.LabelAccount>
        <S.InputAccount
          name="instagram"
          variant="formData"
          type="text"
          id="inputInstagram"
          onChange={handleChange}
          value={values.instagram}
          theme={themeStore.themeName}
        />
        {errors.instagram ? <S.ErrorMessage>{errors.instagram}</S.ErrorMessage> : null}
      </S.FormGroup>
      <S.FormGroup>
        <S.LabelAccount theme={themeStore.themeName} htmlFor="inputYoutube">
          YouTube:
        </S.LabelAccount>
        <S.InputAccount
          name="youtube"
          variant="formData"
          type="text"
          id="inputYoutube"
          onChange={handleChange}
          value={values.youtube}
          theme={themeStore.themeName}
        />

        {errors.youtube ? <S.ErrorMessage>{errors.youtube}</S.ErrorMessage> : null}
      </S.FormGroup>

      <S.FormGroup>
        <S.ButtonAccount primaryColor={themeStore.primaryColor} variant="primary" type="submit">
          Salvar Alterações
        </S.ButtonAccount>
      </S.FormGroup>
    </S.FormAccount>
  );
};

export default Contact;
