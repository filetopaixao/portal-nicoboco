import React from 'react';
import { useSelector } from 'react-redux';
import { useFormik } from 'formik';

import Select from '@components/atoms/Select';

import Schema from './Schema';

import * as S from '../styled';

const BankData = () => {
  const themeStore = useSelector((state) => state.theme);
  const { handleChange, handleSubmit, values, errors, setFieldValue } = useFormik({
    initialValues: {
      fullname: '',
      cpfCnpj: '',
      bank: '',
      type: '',
      agency: '',
      accountNumber: '',
    },
    onSubmit: (inputValues) => {
      console.log(inputValues);
    },
    validationSchema: Schema,
    validateOnChange: false,
    validateOnBlur: false,
  });

  const banks = [
    {
      label: 'Caixa Econômica Federal',
      value: 'Caixa Econômica Federal',
    },
    {
      label: 'Banco Inter',
      value: 'Banco Inter',
    },
  ];

  const types = [
    {
      label: 'Conta Corrente',
      value: 'Conta Corrente',
    },
    {
      label: 'Conta Poupança',
      value: 'Conta Poupança',
    },
  ];

  return (
    <S.FormAccount onSubmit={handleSubmit}>
      <S.FormGroup>
        <S.LabelAccount theme={themeStore.themeName} htmlFor="inputName">
          Nome do titular da conta:
        </S.LabelAccount>
        <S.InputAccount
          name="fullname"
          variant="formData"
          type="text"
          id="inputName"
          onChange={handleChange}
          value={values.fullname}
          placeholder="Digite o nome completo"
          theme={themeStore.themeName}
        />

        {errors.fullname ? <S.ErrorMessage>{errors.fullname}</S.ErrorMessage> : null}
      </S.FormGroup>
      <S.FormGroup>
        <S.LabelAccount theme={themeStore.themeName} htmlFor="inputCpfCnpj">
          CPF/CNPJ do Favorecido:
        </S.LabelAccount>
        <S.InputAccount
          name="cpfCnpj"
          variant="formData"
          type="text"
          id="inputCpfCnpj"
          onChange={handleChange}
          value={values.cpfCnpj}
          placeholder="Digite o CPF ou CNPJ"
          theme={themeStore.themeName}
        />

        {errors.cpfCnpj ? <S.ErrorMessage>{errors.cpfCnpj}</S.ErrorMessage> : null}
      </S.FormGroup>
      <S.Row>
        <S.InputLeft theme={themeStore.themeName}>
          <S.LabelAccount theme={themeStore.themeName} htmlFor="selectBank">
            Banco:
          </S.LabelAccount>
          <Select
            name="bank"
            id="selectBank"
            options={banks}
            onChange={(e) => setFieldValue('bank', e.target.value)}
            defaultValue={values.bank}
            theme={themeStore.themeName}
          />
          {errors.bank ? <S.ErrorMessage>{errors.bank}</S.ErrorMessage> : null}
        </S.InputLeft>
        <S.InputRight theme={themeStore.themeName}>
          <S.LabelAccount theme={themeStore.themeName} htmlFor="selectType">
            Tipo de conta:
          </S.LabelAccount>
          <Select
            name="type"
            id="selectType"
            options={types}
            onChange={(e) => setFieldValue('type', e.target.value)}
            defaultValue={values.type}
          />
          {errors.type ? <S.ErrorMessage>{errors.type}</S.ErrorMessage> : null}
        </S.InputRight>
      </S.Row>
      <S.FormGroup>
        <S.LabelAccount theme={themeStore.themeName} htmlFor="inputAgency">
          Agência:
        </S.LabelAccount>
        <S.InputAccount
          name="agency"
          variant="formData"
          type="text"
          id="inputAgency"
          onChange={handleChange}
          value={values.agency}
          placeholder="0000"
          theme={themeStore.themeName}
        />

        {errors.agency ? <S.ErrorMessage>{errors.agency}</S.ErrorMessage> : null}
      </S.FormGroup>
      <S.FormGroup>
        <S.LabelAccount theme={themeStore.themeName} htmlFor="inputAccount">
          Número da conta:
        </S.LabelAccount>
        <S.InputAccount
          name="account"
          variant="formData"
          type="text"
          id="inputAccount"
          onChange={handleChange}
          value={values.account}
          placeholder="00000000-0"
          theme={themeStore.themeName}
        />

        {errors.account ? <S.ErrorMessage>{errors.account}</S.ErrorMessage> : null}
      </S.FormGroup>
      <S.FormGroup>
        <S.ButtonAccount primaryColor={themeStore.primaryColor} size="100%" variant="primary" type="submit">
          incluir conta
        </S.ButtonAccount>
      </S.FormGroup>
    </S.FormAccount>
  );
};

export default BankData;
