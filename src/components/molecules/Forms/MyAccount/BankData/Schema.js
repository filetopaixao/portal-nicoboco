import * as Yup from 'yup';

import { validCpf } from '@utils/validators';

const Schema = Yup.object().shape({
  fullname: Yup.string()
    .required('Name é obrigatório.')
    .test({
      name: 'validateName',
      test(value) {
        const onlyText = /^[a-zA-Z ]*$/.test(value);

        if (!onlyText) {
          return this.createError({
            message: 'não pode conter números',
            path: 'fullname',
          });
        }
        return true;
      },
    }),
  profilename: Yup.string()
    .required('Profile name é obrigatório.')
    .test({
      name: 'validateProfileName',
      test(value) {
        const onlyText = /^[a-zA-Z ]*$/.test(value);

        if (!onlyText) {
          return this.createError({
            message: 'não pode conter números',
            path: 'profilename',
          });
        }
        return true;
      },
    }),
  gender: Yup.string().required('campo obrigatório'),
  birthday: Yup.string().max(10).required('campo obrigatório'),
  person: Yup.string().required('campo obrigatório'),
  cpf: Yup.string()
    .max(14)
    .required('campo obrigatório')
    .test('testCpf', 'campo obrigatório', (value) => validCpf(value)),
});

export default Schema;
