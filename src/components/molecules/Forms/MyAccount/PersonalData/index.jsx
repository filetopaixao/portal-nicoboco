import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { useFormik } from 'formik';
// import { format, parseISO } from 'date-fns';
// import pt from 'date-fns/locale/pt';

import Select from '@components/atoms/Select';

import { getUser, updateUser } from '@services/user';

import Logo from '@assets/images/logo_ever_white.png';

import Schema from './Schema';

import * as S from '../styled';

const PersonalData = () => {
  const themeStore = useSelector((state) => state.theme);
  const [userData, setUserData] = useState({});
  const { handleChange, handleSubmit, values, errors, setFieldValue } = useFormik({
    initialValues: {
      first_name: '',
      last_name: '',
      gender: '',
      birthdate: '',
      cpf: '',
      // person: 'fisica',
    },
    onSubmit: async (inputValues) => {
      await updateUser({
        ...userData,
        first_name: inputValues.first_name,
        last_name: inputValues.last_name,
        gender: inputValues.gender,
        birthdate: inputValues.birthdate,
        cpf: inputValues.cpf,
      });
    },
    validationSchema: Schema,
    validateOnChange: false,
    validateOnBlur: false,
  });

  useEffect(() => {
    // console.log(new Date(userData.birthdate));
    // console.log(format(new Date(date.getFullYear(), date.getMonth(), date.getDate()), 'yyyy-MM-dd'));

    getUser()
      .then((response) => {
        const userDataResponse = response.data;
        setFieldValue('first_name', userDataResponse.first_name);
        setFieldValue('last_name', userDataResponse.last_name);
        setFieldValue('gender', userDataResponse.gender);
        setFieldValue('birthdate', '1997-11-07');
        setFieldValue('cpf', userDataResponse.cpf_cnpj);
        setUserData(userDataResponse);
      })
      .catch(() => {
        setUserData({
          id: 1,
          type_id: '2',
          birthdate: '2000-10-03',
          cpf_cnpj: '000.000.000-00',
          email: 'mail@mail.com',
          full_name: `First-name Last-name`,
          first_name: 'First-name',
          last_name: 'Last-Name',
          gender: 'gender',
          photo: Logo,
          legal_person: 'Person',
          phone: '(00) 0 0000-0000',
          office: 'Empreendedora EVER',
        });
      });
  }, [setFieldValue]);

  const options = [
    {
      label: 'Feminino',
      value: 'Feminino',
    },
    {
      label: 'Masculino',
      value: 'Masculino',
    },
  ];

  return (
    <S.FormAccount onSubmit={handleSubmit}>
      <S.FormGroup>
        <S.LabelAccount theme={themeStore.themeName} htmlFor="first_name">
          Nome completo:
        </S.LabelAccount>
        <S.InputAccount
          variant="formData"
          name="first_name"
          type="text"
          id="first_name"
          onChange={handleChange}
          value={values.first_name}
          theme={themeStore.themeName}
        />

        {errors.fullname ? <S.ErrorMessage>{errors.fullname}</S.ErrorMessage> : null}
      </S.FormGroup>
      <S.FormGroup>
        <S.LabelAccount theme={themeStore.themeName} htmlFor="last_name">
          Nome exibido na Plataforma EVER:
        </S.LabelAccount>
        <S.InputAccount
          name="last_name"
          variant="formData"
          type="text"
          id="last_name"
          onChange={handleChange}
          value={values.last_name}
          theme={themeStore.themeName}
        />

        {errors.profilename ? <S.ErrorMessage>{errors.profilename}</S.ErrorMessage> : null}
      </S.FormGroup>
      <S.Row>
        <S.InputLeft theme={themeStore.themeName}>
          <S.LabelAccount theme={themeStore.themeName} htmlFor="selectGender">
            Gênero:
          </S.LabelAccount>
          <Select
            name="gender"
            id="inputGender"
            options={options}
            onChange={(e) => {
              setFieldValue('gender', e.target.value);
            }}
            defaultValue={values.gender}
          />
          {errors.gender ? <S.ErrorMessage>{errors.gender}</S.ErrorMessage> : null}
        </S.InputLeft>
        <S.InputRight>
          <S.LabelAccount theme={themeStore.themeName} htmlFor="inputBirthdate">
            Data de nascimento:
          </S.LabelAccount>
          <S.InputAccount
            name="birthdate"
            type="date"
            id="inputBirthdate"
            onChange={handleChange}
            value={values.birthdate}
            theme={themeStore.themeName}
          />
          {errors.birthdate ? <S.ErrorMessage>{errors.birthdate}</S.ErrorMessage> : null}
        </S.InputRight>
      </S.Row>
      {/* <S.FormGroup>
        <S.LabelAccount>Pessoa:</S.LabelAccount>
        <S.RadioGroup>
          <S.InputAccount
            name="person"
            type="radio"
            id="inputPersonFisica"
            onChange={handleChange}
            value="fisica"
            checked={values.person === 'fisica'}
          />
          <S.LabelAccount htmlFor="inputPersonFisica">Física</S.LabelAccount>
          <S.InputAccount
            name="person"
            type="radio"
            id="inputPersonJuridica"
            onChange={handleChange}
            value="juridica"
            checked={values.person === 'juridica'}
          />
          <S.LabelAccount htmlFor="inputPersonJuridica">Jurídica</S.LabelAccount>
        </S.RadioGroup>
        {errors.person ? <S.ErrorMessage>{errors.person}</S.ErrorMessage> : null}
      </S.FormGroup> */}
      <S.FormGroup>
        <S.LabelAccount theme={themeStore.themeName} htmlFor="inputCPF">
          CPF:
        </S.LabelAccount>
        <S.InputAccount
          name="cpf"
          variant="formData"
          type="text"
          id="inputCPF"
          onChange={handleChange}
          value={values.cpf}
          theme={themeStore.themeName}
        />
        {errors.cpf ? <S.ErrorMessage>{errors.cpf}</S.ErrorMessage> : null}
      </S.FormGroup>
      <S.Alert secondaryColor={themeStore.secondaryColor}>
        <S.AlertIcon secondaryColor={themeStore.secondaryColor} />
        Após verificação de sua conta na EVER, você não poderá mais alterar o CPF, mas ainda poderá migrar a conta para
        um CNPJ.
      </S.Alert>
      <S.FormGroup>
        <S.ButtonAccount primaryColor={themeStore.primaryColor} variant="primary" type="submit">
          Salvar Alterações
        </S.ButtonAccount>
      </S.FormGroup>
    </S.FormAccount>
  );
};

export default PersonalData;
