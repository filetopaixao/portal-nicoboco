import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import { Link, useHistory } from 'react-router-dom';
import { useFormik } from 'formik';

import { Checkbox, Button } from '@components/atoms';
import { authentication } from '@services/auth';

import SchemaLogin from './SchemaLogin';

import * as S from './styled';

const LoginForm = ({ redirectTo }) => {
  const themeStore = useSelector((state) => state.theme);
  const history = useHistory();
  const [verifyEmail, setVerifyEmail] = useState(false);
  const [verifyPassword, setVerifyPassword] = useState(false);

  const formik = useFormik({
    initialValues: {
      email: '',
      password: '',
    },
    onSubmit: (values) => {
      const { email, password } = values;

      if (email === '' && password === '') {
        setVerifyEmail(true);
        setVerifyPassword(true);
      }

      if (email === '') {
        setVerifyEmail(true);
        return;
      }
      setVerifyEmail(false);

      if (password === '') {
        setVerifyPassword(true);
        return;
      }
      setVerifyPassword(false);

      if (verifyEmail === false && email !== '' && verifyPassword === false && password !== '') {
        const firstSession = localStorage.getItem('first_session');

        if (firstSession) {
          authentication({ email, password }).then(() => {
            localStorage.removeItem('data_user_email');
            localStorage.removeItem('data_user_password');
            history.push('/');
            // window.location.reload();
          });
        } else {
          history.push(redirectTo);

          localStorage.setItem('data_user_email', email);
          localStorage.setItem('data_user_password', password);
        }
      }
    },
    validationSchema: SchemaLogin,
  });

  return (
    <S.Form onSubmit={formik.handleSubmit} secondaryColor={themeStore.secondaryColor}>
      <S.InputComponent
        name="email"
        type="email"
        variant="login"
        placeholder="e-mail"
        onChange={formik.handleChange}
        value={formik.values.email}
        theme={themeStore.themeName}
        primaryColor={themeStore.primaryColor}
      />
      {verifyEmail ? <div className="ErrorMessage">Preencha o campo de email.</div> : null}
      <S.InputComponent
        name="password"
        type="password"
        variant="login"
        placeholder="senha"
        onChange={formik.handleChange}
        value={formik.values.password}
        theme={themeStore.themeName}
        primaryColor={themeStore.primaryColor}
      />
      {verifyPassword ? <div className="ErrorMessage">Preencha o campo de senha.</div> : null}

      <div className="remember">
        <Checkbox onChange={formik.handleChange} text="Lembrar senha" />
      </div>
      <div className="forgot">
        <Link to="/forgot">Esqueceu sua senha?</Link>
      </div>

      <Button height="40px" icon="arrowRight" variant="primary" showIcon typeButton="submit">
        Entrar
      </Button>
    </S.Form>
  );
};

LoginForm.propTypes = {
  redirectTo: PropTypes.string,
};

LoginForm.defaultProps = {
  redirectTo: '',
};

export default LoginForm;
