import * as Yup from 'yup';

const SchemaLogin = Yup.object().shape({
  email: Yup.string(),
  password: Yup.string().min(6, 'A senha deve ter pelo menos 6 caracteres.'),
});

export default SchemaLogin;
