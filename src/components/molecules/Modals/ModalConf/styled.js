import styled from 'styled-components';
import { shade } from 'polished';

export const ModalContent = styled.div`
  height: 100%;

  padding: 20px 20px 10px;

  z-index: 2;
  color: #5e5e5e;

  @media (max-width: 500px) {
    padding: 10px 10px 5px;
  }
`;

export const ModalHeader = styled.header`
  h3 {
    font-size: 14px;
    font-weight: 600;
    text-align: center;

    color: ${(props) => (props.theme === 'Dark' ? '#FFFFFF' : '#484646')};

    width: 100%;

    @media (min-width: 768px) {
      font-size: 18px;
    }
  }
`;

export const ModalBody = styled.div`
  height: auto;
  margin-top: 10px;

  @media (min-width: 1400px) {
    margin: 23px 0px 40px 0px;
  }

  @media (max-width: 992px) {
    margin: 10px 0px 10px 0px;
  }
`;

export const ButtonGroup = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  column-gap: 30px;
  align-items: center;

  button:first-child {
    margin-left: auto;
  }

  button:last-child {
    margin-right: auto;
  }
`;

export const Button = styled.button`
  font-size: 14px;
  font-weight: bold;
  text-transform: uppercase;

  height: 40px;
  width: 135px;

  border-radius: 15px;
  border: none;

  cursor: pointer;
  color: #fff;
  background: ${(props) => props.primaryColor};
  border-radius: 15px;

  /*
  &:hover {
    background: ${shade(0.2, '#01A3FF')};
  }
  */

  @media (max-width: 600px) {
    font-size: 11px;
    max-height: 35px;
  }
`;

export const ImageContainer = styled.div`
  -webkit-mask-image: url(${(props) => props.ImageDelConf});
  mask-image: url(${(props) => props.ImageDelConf});
  mask-repeat: no-repeat;
  mask-size: 256px;

  width: 256px;
  height: 256px;

  margin: 0 auto;

  margin-bottom: 23px;

  background-size: contain;
  background-repeat: no-repeat;
  background: ${(props) => props.primaryColor};

  @media (min-width: 1200px) {
    width: 16rem;
    height: 16rem;
  }
`;

export const ContainerInfo = styled.section`
  width: 90%;
  margin: 0 auto;

  color: ${(props) => (props.theme === 'Dark' ? '#FFFFFF' : '#484646')};

  h3 {
    font-weight: bold;
    text-align: center;
  }

  p {
    text-align: center;
  }
`;
