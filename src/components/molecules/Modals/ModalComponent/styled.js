import styled, { css } from 'styled-components';

const variants = {
  lg: () => css`
    width: 85%;
  `,
  md: () => css`
    width: 50%;
  `,
  mdMin: () => css`
    width: 40%;
  `,
  sm: () => css`
    width: 30%;
  `,
};

const paddingsModalContent = {
  changePass: () => css`
    padding: 0;
    @media (min-width: 992px) {
      display: flex;
      flex-direction: row;
      height: 450px;
    }
  `,
  video: () => css`
    padding: 30px 30px;

    @media (max-width: 1024px) {
      padding: 20px 25px !important;
    }
  `,
  default: () => css`
    padding: 15px 15px;
  `,
};

export const Modal = styled.div`
  display: flex;
  justify-content: center;
  ${(props) => props.alignCenter && 'align-items: center;'}

  width: 100%;
  height: 100vh;

  position: fixed;
  left: 0;
  top: 0;
  overflow: auto;
  z-index: 10;

  background: #ffffff;
  background-color: rgba(0, 0, 0, 0.6);
  box-shadow: 0 3px 26px rgba(0, 0, 0, 0.3);
`;

export const ModalContent = styled.div`
  ${(props) => variants[props.variant]};

  height: ${(props) => props.ContentHeight};
  max-height: ${(props) => props.MaxHeight};
  max-width: ${(props) => props.MaxWidth};
  min-height: ${(props) => props.MinHeight};
  ${(props) => paddingsModalContent[props.classType]};
  margin: ${(props) => props.MarginContent};

  // margin: 30px 0 30px 0;

  position: relative;
  border: ${(props) => (props.theme === 'Dark' ? '1px solid #2F3743' : '1px solid #EAEAEA')};
  border-radius: 20px;
  z-index: 2;
  overflow: hidden;

  background: ${(props) => (props.theme === 'Dark' ? '#202731' : '#FFFFFF')};
  color: #5e5e5e;

  span {
    margin-left: 10px;
  }

  @media (max-width: 600px) {
    ${(props) => (props.MaxHeight ? props.MaxHeight : 'max-height: 95vh;')}
  }

  @media (max-width: 700px) {
    width: 90%;
  }

  @media (max-width: 1024px) {
    padding: 10px 10px;
  }
`;

export const ButtonCloserModal = styled.div`
  display: flex;
  justify-content: flex-end;

  &.closerSidebar {
    display: none;
    @media (max-width: 600px) {
      display: flex;
      padding-right: 10px;
    }
  }

  &.closerContent {
    display: flex;
    padding-right: 10px;
    @media (max-width: 600px) {
      display: none;
    }
  }
`;

export const Button = styled.button`
  font-size: 14px;
  letter-spacing: 0px;
  text-transform: none;

  position: fixed;
  padding: 0;

  opacity: 1;
  box-shadow: none;
  border: none;
  cursor: pointer;
  position: absolute;

  color: #cbcaca;
  background: transparent;

  @media (min-width: 800px) {
    position: inherit;
  }

  &:focus {
    outline: thin dotted;
    outline: 0px auto -webkit-focus-ring-color;
    outline-offset: 0px;
  }
`;

export const ModalHeader = styled.header`
  h3 {
    font-size: 14px;
    font-weight: 600;
    text-align: center;

    color: ${(props) => (props.theme === 'Dark' ? '#fff' : '#484646')};

    width: 100%;

    @media (min-width: 768px) {
      font-size: 18px;
    }
  }
`;

export const ModalBody = styled.div`
  ${(props) => (props.center ? 'text-align: center;' : '')}

  height: auto;

  /* @media (min-width: 1400px) {
    margin: 23px 0px 40px 0px;
  } */

  ${(props) => (props.type === 'changePass' ? 'width: 100%;' : '')}

  @media (max-width: 992px) {
    margin: 10px 0px 10px 0px;
  }
`;
