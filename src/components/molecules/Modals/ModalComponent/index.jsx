import React from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';

import * as S from './styled';

const Modal = ({
  children,
  title,
  onClose,
  id,
  isVisible,
  variant,
  alignCenter,
  btnCloser,
  classType,
  ContentHeight,
  MaxWidth,
  MaxHeight,
  MinHeight,
  MarginContent,
  PaddingContent,
}) => {
  const themeStore = useSelector((state) => state.theme);
  const handleOutSideClick = (e) => {
    if (e.target.id === id) onClose();
  };

  return (
    <>
      {isVisible ? (
        <S.Modal id={id} onClick={handleOutSideClick} alignCenter={alignCenter}>
          <S.ModalContent
            variant={variant}
            MarginContent={MarginContent}
            PaddingContent={PaddingContent}
            ContentHeight={ContentHeight}
            MaxWidth={MaxWidth}
            MaxHeight={MaxHeight}
            MinHeight={MinHeight}
            classType={classType}
            theme={themeStore.themeName}
          >
            {btnCloser ? (
              <S.ButtonCloserModal>
                <S.Button id="btnModelCloser" className="btnModelCloser" type="button" onClick={() => onClose()}>
                  Fechar X
                </S.Button>
              </S.ButtonCloserModal>
            ) : null}
            {title ? (
              <S.ModalHeader theme={themeStore.themeName}>
                <h3>{title}</h3>
              </S.ModalHeader>
            ) : null}
            <S.ModalBody>{children}</S.ModalBody>
          </S.ModalContent>
        </S.Modal>
      ) : null}
    </>
  );
};

Modal.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.object),
    PropTypes.object,
    PropTypes.element,
    PropTypes.number,
    PropTypes.func,
    PropTypes.string,
  ]),
  title: PropTypes.string,
  onClose: PropTypes.func,
  id: PropTypes.string,
  isVisible: PropTypes.bool,
  variant: PropTypes.string,
  alignCenter: PropTypes.bool,
  btnCloser: PropTypes.bool,
  classType: PropTypes.string,
  ContentHeight: PropTypes.string,
  MaxWidth: PropTypes.string,
  MaxHeight: PropTypes.string,
  MinHeight: PropTypes.string,
  MarginContent: PropTypes.string,
  PaddingContent: PropTypes.string,
};

Modal.defaultProps = {
  children: () => {},
  title: 'title',
  onClose: () => false,
  id: 'modal',
  isVisible: false,
  variant: 'lg',
  alignCenter: false,
  btnCloser: true,
  classType: 'default',
  ContentHeight: '90vh',
  MaxWidth: '800px',
  MaxHeight: '900px',
  MinHeight: '0',
  MarginContent: '0',
  PaddingContent: '',
};

export default Modal;
