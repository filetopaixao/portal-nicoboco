import React from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';

import Button from '@components/atoms/Button';

import capaFpass from '@assets/images/capaFpass.png';

import * as S from './styled';

const ModalChangePass = ({ children, title, onClose, id, isVisible, variant }) => {
  const themeStore = useSelector((state) => state.theme);
  const handleOutSideClick = (e) => {
    if (e.target.id === id) onClose();
  };

  return (
    <>
      {isVisible ? (
        <S.Modal id={id} onClick={handleOutSideClick} alignCenter>
          <S.ModalContent theme={themeStore.themeName} variant={variant} type="changePass">
            <S.Sidebar theme={themeStore.themeName} image={themeStore.capa ? themeStore.capa : capaFpass}>
              <S.ButtonCloserModal className="closerSidebar">
                <Button className="btnMdCloser " type="button" onClick={() => onClose()}>
                  Fechar X
                </Button>
              </S.ButtonCloserModal>
            </S.Sidebar>
            <S.ModalBody type="changePass">
              <S.ButtonCloserModal className="closerContent">
                <Button className="btnMdCloser" type="button" onClick={() => onClose()}>
                  Fechar X
                </Button>
              </S.ButtonCloserModal>
              {title ? (
                <S.ModalHeader theme={themeStore.themeName}>
                  <h3>{title}</h3>
                </S.ModalHeader>
              ) : null}
              {children}
            </S.ModalBody>
          </S.ModalContent>
        </S.Modal>
      ) : null}
    </>
  );
};

ModalChangePass.propTypes = {
  children: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  title: PropTypes.string,
  onClose: PropTypes.func,
  id: PropTypes.string,
  isVisible: PropTypes.bool,
  variant: PropTypes.string,
};

ModalChangePass.defaultProps = {
  children: 'content',
  title: 'title',
  onClose: () => false,
  id: 'modal',
  isVisible: true,
  variant: 'lg',
};

export default ModalChangePass;
