import styled, { css } from 'styled-components';

// import ChangePassImg from '@assets/images/change_password.svg';

const variants = {
  lg: () => css`
    width: 85%;
  `,
  md: () => css`
    width: 50%;
  `,
  sm: () => css`
    width: 30%;
  `,
};

export const Modal = styled.div`
  display: flex;
  justify-content: center;
  ${(props) => props.alignCenter && 'align-items: center;'}

  width: 100%;
  height: 100vh;

  position: fixed;
  left: 0;
  top: 0;
  overflow: auto;
  z-index: 999;

  box-shadow: 0 3px 26px rgba(0, 0, 0, 0.3);
  background: #ffffff;
  background-color: rgba(0, 0, 0, 0.6);
`;

export const ModalContent = styled.div`
  display: inline-table;

  ${(props) => variants[props.variant]}
  height: auto;

  margin: 20px 0 20px 0;
  ${(props) => (props.type === 'changePass' ? 'padding: 0;' : 'padding: 15px 30px;')}

  border-radius: 20px;
  z-index: 2;
  background: ${(props) => (props.theme === 'Dark' ? '#202731' : '#FFFFFF')};
  color: #5e5e5e;

  overflow: hidden;

  span {
    margin-left: 10px;
  }

  @media (max-width: 500px) {
    width: 90%;
    padding: 15px 15px;
  }

  @media (min-width: 992px) {
    ${(props) =>
      props.type === 'changePass' ? 'display: flex; flex-direction: row; height: 450px; padding: 0;' : null}
  }
`;

export const Sidebar = styled.div`
  width: 100%;
  height: 200px;
  background: ${(props) =>
    `url(${props.image}) center center no-repeat ${props.theme === 'Dark' ? '#29303A' : '#FBFBFB'}`};
  background-size: 150px;
  @media (min-width: 992px) {
    width: 300px;
    height: auto;
  }
`;

export const ButtonCloserModal = styled.div`
  display: flex;
  justify-content: flex-end;

  &.closerSidebar {
    display: none;
    @media (max-width: 600px) {
      display: flex;
      padding-right: 10px;
    }
  }

  &.closerContent {
    display: flex;
    padding-right: 10px;
    @media (max-width: 600px) {
      display: none;
    }
  }

  button {
    font-size: 14px;

    padding: 0;

    letter-spacing: 0px;
    text-transform: none;

    opacity: 1;
    box-shadow: none;

    color: #cbcaca;
    background: transparent;

    &:focus {
      outline: thin dotted;
      outline: 0px auto -webkit-focus-ring-color;
      outline-offset: 0px;
    }
  }
`;

export const ModalBody = styled.div`
  ${(props) => (props.center ? 'text-align: center;' : '')}

  height: auto;
  margin-top: 10px;

  @media (min-width: 1400px) {
    margin: 23px 0px 40px 0px;
  }

  ${(props) => (props.type === 'changePass' ? 'width: 100%;' : '')}

  @media (max-width: 992px) {
    margin: 10px 0px 10px 0px;
  }
`;

export const ModalHeader = styled.header`
  h3 {
    font-size: 14px;
    font-weight: 600;
    text-align: center;

    color: ${(props) => (props.theme === 'Dark' ? '#fff' : '#484646')};

    width: 100%;

    @media (min-width: 768px) {
      font-size: 18px;
    }
  }
`;
