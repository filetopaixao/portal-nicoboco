import styled from 'styled-components';
import { shade } from 'polished';

export const ModalContent = styled.div`
  height: 100%;

  padding: 20px 20px 10px;

  z-index: 2;
  color: #5e5e5e;

  @media (max-width: 500px) {
    padding: 10px 10px 5px;
  }
`;

export const ModalHeader = styled.header`
  h3 {
    font-size: 14px;
    font-weight: 600;
    text-align: center;

    width: 100%;

    color: ${(props) => (props.theme === 'Dark' ? '#fff' : '#484646')};

    @media (min-width: 768px) {
      font-size: 18px;
    }
  }
`;

export const ModalBody = styled.div`
  height: auto;
  margin-top: 10px;

  /* @media (min-width: 1400px) {
    margin: 23px 0px 40px 0px;
  } */

  @media (max-width: 992px) {
    margin: 10px 0px 10px 0px;
  }

  @media (max-width: 600px) {
    margin: 0px;
  }
`;

export const PanelPixelForm = styled.form`
  width: 100%;

  label {
    font-size: 13px;
    margin-bottom: 5px;
  }
`;

export const ContainerInput = styled.div`
  margin-top: 3px;
  margin-bottom: 10px;
`;

export const ContainerSelect = styled.div`
  margin-bottom: 10px;

  @media (max-width: 600px) {
    width: 85%;

    margin: 0 auto;
  }

  select {
    cursor: pointer;
    color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};

    @media (max-width: 600px) {
      height: 35px;
    }
  }
`;

export const InputGroup = styled.div`
  display: grid;
  grid-template-columns: 2fr auto;
  column-gap: 20px;
  align-items: center;

  @media (max-width: 992px) {
    grid-template-columns: 1fr;
    row-gap: 20px;
  }

  @media (max-width: 600px) {
    grid-template-columns: 1fr;
    row-gap: 10px;

    button {
      width: 100%;
    }
  }
`;

export const InputPixel = styled.input`
  height: 40px;
  width: 100%;

  padding: 0px 10px;

  cursor: pointer;

  border-radius: 15px;
  border: ${(props) => (props.theme === 'Dark' ? '1px solid #47505E' : '1px solid #CECECE')};

  background: ${(props) => (props.theme === 'Dark' ? '#29303A' : '#FBFBFB')};

  color: ${(props) => (props.theme === 'Dark' ? '#47505E' : '#CECECE')};

  @media (max-width: 600px) {
    height: 35px;
  }

  ::placeholder {
    color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
  }
`;

export const ButtonSubmit = styled.button`
  font-size: 14px;
  font-weight: bold;
  text-transform: uppercase;

  @media (max-width: 600px) {
    font-size: 11px;
    max-height: 35px;
  }

  height: 53px;
  width: 150px;

  border-radius: 15px;
  border: none;

  cursor: pointer;
  color: #fff;
  background: ${(props) => props.secondaryColor};
  border-radius: 15px;

  &:hover {
    background: ${(props) => `${shade(0.2, props.secondaryColor)}`};
  }
`;

export const ButtonSave = styled.button`
  font-size: 14px;
  font-weight: bold;
  text-transform: uppercase;

  height: 53px;
  width: 100%;

  @media (min-width: 600px) and (max-width: 1300px) {
    font-size: 12px;
    height: 35px;
  }

  @media (max-width: 600px) {
    font-size: 11px;
    max-height: 35px;
  }

  border-radius: 15px;
  border: none;

  color: #fff;
  background: ${(props) => props.primaryColor};
  box-shadow: 0px 3px 6px #00000029;

  /*&:hover {
    background: ${shade(0.2, '#ff7f00')};
  }*/
`;

export const ButtonDelete = styled.button`
  width: 25px;
  height: 25px;

  border-radius: 5px;
  border: 1px solid #5e5e5e;

  color: #5e5e5e;
  background: #bdbdbd 0% 0% no-repeat padding-box;
  &:hover {
    background: ${shade(0.2, '#bdbdbd')};
  }
`;

export const ContainerPagination = styled.div`
  width: 100%;

  > div {
    margin-top: 10px;
    margin-bottom: 10px;
    margin-right: 0px;

    @media (max-width: 600px) {
      margin-top: 5px;
      margin-bottom: 5px;
    }
  }

  @media (max-width: 600px) {
    span,
    p {
      font-size: 12px;
    }
  }

  input {
    @media (max-width: 600px) {
      height: 25px;
    }
  }

  button {
    @media (max-width: 600px) {
      height: 30px;
      min-height: 30px;
    }
  }
`;

export const ContainerTable = styled.div`
  // height: calc(50vh - 7rem);
  height: auto;

  > p {
    font-size: 16px;
    font-weight: bold;
    color: #5e5e5e;
  }
`;

export const TableHeader = styled.div`
  text-align: center;

  display: grid;
  grid-template-columns: 1fr 1fr 1fr 0.5fr;

  font-weight: bold;

  @media (max-width: 800px) {
    display: grid;
    grid-template-columns: 1fr 1fr 0.5fr 0.5fr;
  }
  padding: 14px 0;
  margin-top: 15px;
  @media (max-width: 600px) {
    margin-top: 0px;
    padding: 5px 0;
  }

  border-bottom: ${(props) => (props.theme === 'Dark' ? '1px solid #2F3743' : '1px solid #EAEAEA')};
`;

export const TextCell = styled.p`
  font-size: 12px;
  color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};

  @media (min-width: 600px) {
    &.btn-expansed {
      display: none;
    }
  }

  @media (max-width: 800px) {
    &.off-1 {
      display: none;
    }
  }
`;

export const TableBody = styled.div`
  height: calc(50vh - 12rem);
  overflow-y: scroll;

  @media (max-width: 1300px) {
    max-height: 160px;
  }

  @media (max-width: 600px) {
    height: calc(50vh - 11rem);
  }

  img {
    display: block;
    height: 100%;
    width: 20px !important;

    @media (min-width: 1400px) {
      height: 100%;
      width: 25px !important;
    }
  }
  ::-webkit-scrollbar {
    -webkit-appearance: none;
  }
  ::-webkit-scrollbar:vertical {
    width: 14px;
  }
  ::-webkit-scrollbar:horizontal {
    height: 14px;
  }
  ::-webkit-scrollbar-thumb {
    border-radius: 3px;

    background-color: ${(props) => (props.theme === 'Dark' ? '#47505E' : '#CECECE')};
  }
  ::-webkit-scrollbar-track {
    border-radius: 10px;
  }
`;

export const TableLine = styled.div`
  text-align: center;

  display: grid;
  grid-template-columns: 1fr 1fr 1fr 0.5fr;
  align-items: center;

  @media (max-width: 800px) {
    display: grid;
    grid-template-columns: 1fr 1fr 0.5fr 0.5fr;
  }

  padding: 10px 0px;

  border-bottom: ${(props) => (props.theme === 'Dark' ? '1px solid #2F3743' : '1px solid #EAEAEA')};
`;

export const TableCell = styled.div`
  font-size: 12px;
  color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};

  @media (min-width: 600px) {
    &.btn-expansed {
      display: none;
    }
  }

  @media (max-width: 800px) {
    &.off-1 {
      display: none;
    }
  }
`;

export const Button = styled.button`
  font-size: 18px;
  text-transform: uppercase;
  text-align: center;

  display: flex;
  align-items: center;
  margin: 0 auto;
  min-height: 26px;

  cursor: pointer;
  border: none;
  border-radius: 0;
  box-shadow: none;
  background: none;
  position: relative;
  outline: 0;

  color: #fff;
`;

export const ButtonExpansed = styled.img`
  grid-area: 'close';
  width: 20px;
  height: 100%;
  cursor: pointer;
  transform: rotate(180deg);
  transition: transform 300ms linear;

  ${(props) => (props.active ? 'transform: rotate(0);' : '')};
`;

export const TableLineExpansed = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  justify-content: center;
  align-items: center;
  row-gap: 20px;

  padding: 5px 0px;

  transition: all cubic-bezier(0.175, 0.885, 0.32, 1.275);

  border-bottom: ${(props) => (props.theme === 'Dark' ? '1px solid #2F3743' : '1px solid #EAEAEA')};
  background: aliceblue;

  div,
  p {
    text-align: center;
  }

  @media (min-width: 600px) {
    display: none;
  }

  &.activeExpansed {
    height: auto;
  }

  &.defaultExpansed {
    height: 0px;
  }
`;
