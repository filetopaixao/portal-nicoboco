import React, { useState, useCallback } from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';

import { Label, Select } from '@components/atoms';

import Pagination from '@components/molecules/Pagination';

import Arrow from '@assets/images/arrowCheckout.svg';

import Modal from '../ModalComponent';

import * as S from './styled';

const opOrigin = [
  {
    label: 'FACEBOOK PIXEL',
    value: 'FACEBOOKPIXEL',
  },
  {
    label: 'GOOGLE ANALYTCS',
    value: 'GOOGLEANALYTCS',
  },
  {
    label: `TWITTER'S TRACKING`,
    value: 'TWITTERSTRACKING',
  },
];

const opLocation = [
  {
    label: 'PÁGINA CARRINHO',
    value: 'PAGINACARRINHO',
  },
  {
    label: 'PÁGINA OBRIGADO',
    value: 'PAGINAOBRIGADO',
  },
];

const PanelPixel = ({ isModalVisible, setIsModalVisible }) => {
  const themeStore = useSelector((state) => state.theme);
  const [dataJson, setDataJson] = useState([
    {
      id: 1,
      name: 'Facebook Pixel',
      origin: 'Entrada',
      pixel: '089849840',
      flow: '12257asdg268',
      activeExpansed: false,
    },
    {
      id: 2,
      name: 'Google Pixel',
      origin: 'Saida',
      pixel: '8379837',
      flow: '12257asdg268',
      activeExpansed: false,
    },
    {
      id: 3,
      name: 'Twitter',
      origin: 'Saída',
      pixel: '8373u830',
      flow: '12257asdg268',
      activeExpansed: false,
    },
    {
      id: 4,
      name: 'Twitter',
      origin: 'Entrada',
      pixel: '8873879',
      flow: '12257asdg268',
      activeExpansed: false,
    },
    {
      id: 5,
      name: 'Twitter',
      origin: 'Saída',
      pixel: '2626753',
      flow: '12257asdg268',
      activeExpansed: false,
    },
    {
      id: 6,
      name: 'Twitter',
      origin: 'Entrada',
      pixel: '30840808',
      flow: '12257asdg268',
      activeExpansed: false,
    },
    {
      id: 7,
      name: 'Twitter',
      origin: 'Saída',
      pixel: '10083098',
      flow: '12257asdg268',
      activeExpansed: false,
    },
  ]);

  const [currentPage, setCurrentPage] = useState(1);
  const [perPage] = useState(4);

  const indexOfLast = currentPage * perPage;
  const indexOfFirst = indexOfLast - perPage;
  const currentDataJson = dataJson.slice(indexOfFirst, indexOfLast);

  const paginate = useCallback((pageNumber) => {
    setCurrentPage(pageNumber);
  }, []);

  const handleSelectLocation = (index) => {
    console.log(index);
  };

  const handleSelectOrigin = (index) => {
    console.log(index);
  };

  const handleSetExpansed = (item) => {
    const newDatas = dataJson.map((data) => {
      return data.id === item.id ? { ...data, activeExpansed: !data.activeExpansed } : data;
    });
    setDataJson(newDatas);
  };

  return (
    <Modal
      variant="md"
      alignCenter
      title=""
      isVisible={isModalVisible}
      onClose={() => setIsModalVisible(!isModalVisible)}
      MaxHeight="95vh"
      ContentHeight="auto"
    >
      <S.ModalContent>
        <S.ModalHeader theme={themeStore.themeName}>
          <h3>Painel Pixel</h3>
        </S.ModalHeader>

        <S.ModalBody>
          <S.PanelPixelForm>
            <S.ContainerSelect theme={themeStore.themeName}>
              <Label htmlFor="local">Local:</Label>
              <Select options={opLocation} onChange={(e) => handleSelectLocation(e.target.value)} />
            </S.ContainerSelect>

            <S.ContainerSelect theme={themeStore.themeName}>
              <Label htmlFor="origin">Origem:</Label>
              <Select options={opOrigin} onChange={(e) => handleSelectOrigin(e.target.value)} />
            </S.ContainerSelect>

            <S.ContainerInput>
              <Label htmlFor="nameOrigin">ID Pixel / Analytics:</Label>
              <S.InputGroup>
                <S.InputPixel
                  theme={themeStore.themeName}
                  type="text"
                  placeholder="Cole aqui seu ID pixel"
                  id="nameOrigin"
                  name="nameOrigin"
                />
                <S.ButtonSubmit secondaryColor={themeStore.secondaryColor} type="submit" className="btn-submit">
                  ADICIONAR
                </S.ButtonSubmit>
              </S.InputGroup>
            </S.ContainerInput>
          </S.PanelPixelForm>

          <S.ContainerTable>
            <S.TableHeader theme={themeStore.themeName}>
              <S.TextCell className="text-upp" theme={themeStore.themeName}>
                Origem
              </S.TextCell>
              <S.TextCell className="text-upp off-1" theme={themeStore.themeName}>
                Pixel/Analytics
              </S.TextCell>
              <S.TextCell className="text-upp" theme={themeStore.themeName}>
                Fluxo
              </S.TextCell>
              <S.TextCell className="text-upp" theme={themeStore.themeName} />
              <S.TextCell className="text-upp btn-expansed" theme={themeStore.themeName} />
            </S.TableHeader>
            <S.TableBody theme={themeStore.themeName}>
              {currentDataJson.map((item) => (
                <>
                  <S.TableLine key={Math.random().toString()} theme={themeStore.themeName}>
                    <S.TableCell className="text-upp" theme={themeStore.themeName}>
                      {item.origin}
                    </S.TableCell>
                    <S.TableCell className="text-upp off-1" theme={themeStore.themeName}>
                      {item.pixel}
                    </S.TableCell>
                    <S.TableCell className="text-upp" theme={themeStore.themeName}>
                      {item.flow}
                    </S.TableCell>
                    <S.TableCell className="text-upp" theme={themeStore.themeName}>
                      <S.ButtonDelete type="button" theme={themeStore.themeName}>
                        X
                      </S.ButtonDelete>
                    </S.TableCell>
                    <S.TableCell className="text-upp btn-expansed" theme={themeStore.themeName}>
                      <S.Button onClick={() => handleSetExpansed(item)}>
                        <S.ButtonExpansed src={Arrow} active={item.activeExpansed} />
                      </S.Button>
                    </S.TableCell>
                  </S.TableLine>
                  {item.activeExpansed ? (
                    <S.TableLineExpansed className="activeExpansed" theme={themeStore.themeName}>
                      <p>ID PIXEL</p>
                      <S.TableCell className="text-upp">{item.pixel}</S.TableCell>
                    </S.TableLineExpansed>
                  ) : null}
                </>
              ))}
            </S.TableBody>
          </S.ContainerTable>

          <S.ContainerPagination className="pagination">
            <Pagination currentPage={currentPage} perPage={perPage} total={dataJson.length} paginate={paginate} />
          </S.ContainerPagination>

          <S.ButtonSave type="button" className="btn-save" primaryColor={themeStore.primaryColor}>
            SALVAR
          </S.ButtonSave>
        </S.ModalBody>
      </S.ModalContent>
    </Modal>
  );
};

PanelPixel.propTypes = {
  isModalVisible: PropTypes.bool,
  setIsModalVisible: PropTypes.func,
};
PanelPixel.defaultProps = {
  isModalVisible: false,
  setIsModalVisible: () => {},
};

export default PanelPixel;
