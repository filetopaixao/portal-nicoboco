import React, { useState, useCallback } from 'react';
import { useSelector } from 'react-redux';

import PropTypes from 'prop-types';

import Toast from '@components/atoms/Toast';
import ModalComponet from '../ModalComponent';

import WahtsApp from '../../../../assets/images/whatsapp.svg';
import Facebook from '../../../../assets/images/facebook.svg';
import Email from '../../../../assets/images/email.svg';

import * as S from './styled';

const ModalShareCopy = ({ linkName, linkSuport, isModalVisible, setIsModalVisible }) => {
  const themeStore = useSelector((state) => state.theme);
  const [shareCopy, setShareCopy] = useState(false);
  const [isToastVisible, setIsToastVisible] = useState(false);

  const [urlWhatsApp] = useState('url share');
  const [urlFacebook] = useState('url share');
  const [urlEmail] = useState('url share');

  const handleShare = (e) => {
    e.preventDefault();
    setShareCopy(!shareCopy);
  };

  const handleActiveToast = useCallback(() => {
    setIsToastVisible(true);
    setTimeout(() => {
      setIsToastVisible(false);
    }, 3000);
  }, [setIsToastVisible]);

  const copy = (e) => {
    e.preventDefault();
    const copyText = document.querySelector('#input');
    copyText.select();
    document.execCommand('copy');
    handleActiveToast();
  };

  const closerModal = () => {
    setShareCopy(false);
    setIsToastVisible(false);
    setIsModalVisible(!isModalVisible);
  };

  return (
    <>
      <ModalComponet
        variant="mdMin"
        alignCenter
        title=""
        isVisible={isModalVisible}
        onClose={() => closerModal()}
        MaxHeight="95vh"
        MaxWidth="600px"
        ContentHeight="350px"
      >
        <S.TitleLink theme={themeStore.themeName}>{linkName}</S.TitleLink>
        <S.Container>
          <S.InputLabel theme={themeStore.themeName}>Link da Página:</S.InputLabel>

          <S.InputContainer theme={themeStore.themeName}>
            <S.Input
              theme={themeStore.themeName}
              type="text"
              id="input"
              name="link"
              value={linkSuport}
              onChange={() => null}
            />
            <S.ContainerButton>
              <S.ButtonGroup>
                <a href="!#" id="copy" onClick={(e) => copy(e)}>
                  <S.IconCopy theme={themeStore.themeName} />
                </a>
                <a href="!#" onClick={(e) => handleShare(e)}>
                  <S.IconShare theme={themeStore.themeName} />
                </a>
              </S.ButtonGroup>
              <S.ContainerShareCopy shareCopy={shareCopy}>
                <S.ContentToShare>
                  <li>
                    <a href={urlWhatsApp} rel="noopener noreferrer" target="_blank">
                      <img src={WahtsApp} alt="WhatsApp" />
                      <p>WhatsApp</p>
                    </a>
                  </li>
                  <li>
                    <a href={urlFacebook} rel="noopener noreferrer" target="_blank">
                      <img src={Facebook} alt="Facebook" />
                      <p>Facebook</p>
                    </a>
                  </li>
                  <li>
                    <a href={urlEmail} rel="noopener noreferrer" target="_blank">
                      <img src={Email} alt="E-mail" />
                      <p>E-mail</p>
                    </a>
                  </li>
                </S.ContentToShare>
              </S.ContainerShareCopy>
            </S.ContainerButton>
          </S.InputContainer>
        </S.Container>
      </ModalComponet>
      <Toast
        variant="success"
        icon="success"
        showIcon
        isVisible={isToastVisible}
        onClose={() => setIsToastVisible(!isToastVisible)}
      >
        Link copiado com sucesso!!
      </Toast>
    </>
  );
};

ModalShareCopy.propTypes = {
  linkName: PropTypes.string,
  linkSuport: PropTypes.string,
  isModalVisible: PropTypes.bool,
  setIsModalVisible: PropTypes.func,
};
ModalShareCopy.defaultProps = {
  linkName: '',
  linkSuport: '',
  isModalVisible: false,
  setIsModalVisible: () => {},
};

export default ModalShareCopy;
