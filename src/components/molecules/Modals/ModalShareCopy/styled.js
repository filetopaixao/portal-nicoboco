import styled from 'styled-components';

import Share from '../../../../assets/images/share.svg';
import Copy from '../../../../assets/images/copy.svg';

export const Container = styled.div`
  margin-top: 5rem;
  padding: 0 10px;
`;

export const InputContainer = styled.div`
  padding: 0 20px;
  display: flex;
  align-items: center;
  height: 40px;

  border-radius: 12px;
  border: ${(props) => (props.theme === 'Dark' ? '1px solid #2F3743' : '1px solid #EAEAEA')};

  background: ${(props) => (props.theme === 'Dark' ? '#29303A' : '#FBFBFB')};

  color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
  // opacity: 0.5;
`;

export const ContainerButton = styled.div`
  position: relative;
`;

export const ButtonGroup = styled.div`
  display: flex;
  column-gap: 10px;
  align-items: center;
`;

export const ContainerShareCopy = styled.div`
  position: absolute;

  ${(props) => (props.shareCopy ? 'display: block' : 'display: none')}
`;

export const ContentToShare = styled.ul`
  width: 150px;
  margin-top: 11px;
  padding: 10px;
  list-style: none;
  border-radius: 10px;
  background: #e8e8e8;
  margin-left: -72px;
  @media (max-width: 800px) {
    margin-top: 45px;
  }

  li {
    display: flex;

    img {
      width: 20px;
      height: 20px;

      margin-right: 15px;
    }

    p {
      font-size: 13px;

      color: #5e5e5e;
    }

    &:nth-child(-n + 2) {
      margin-bottom: 12px;
    }

    a {
      text-decoration: none;

      display: flex;
      justify-content: flex-end;

      cursor: pointer;
    }
  }
`;

export const IconShare = styled.div`
  mask-image: url(${Share});
  -webkit-mask-image: url(${Share});

  width: 1.5rem;
  height: 1.55rem;

  margin-top: 30px;
  margin: 0 auto;

  background-size: contain;
  background-repeat: no-repeat;
  background-color: ${(props) => (props.theme === 'Dark' ? '#fff' : '#484646')};
`;

export const IconCopy = styled.div`
  mask-image: url(${Copy});
  -webkit-mask-image: url(${Copy});

  width: 1.5rem;
  height: 1.55rem;

  margin-top: 30px;
  margin: 0 auto;

  background-size: contain;
  background-repeat: no-repeat;
  background-color: ${(props) => (props.theme === 'Dark' ? '#fff' : '#484646')};
`;

export const Input = styled.input`
  width: 90%;
  padding: 0 15px;

  border: none;

  color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
  background: transparent;
`;

export const InputLabel = styled.div`
  color: ${(props) => (props.theme === 'Dark' ? '#fff' : '#484646')};
`;

export const TitleLink = styled.h4`
  text-align: center;
  font-size: 14px;
  font-weight: bold;
  color: ${(props) => (props.theme === 'Dark' ? '#fff' : '#484646')};
`;
