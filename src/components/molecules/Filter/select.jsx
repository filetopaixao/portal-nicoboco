import React from 'react';
import PropTypes from 'prop-types';

import * as S from './styled';

const Select = ({ onChange, options }) => {
  return (
    <S.FilterSelect name="gender" id="inputGender" options={options} onChange={onChange} defaultValue="Filtrar por" />
  );
};

Select.propTypes = {
  onChange: PropTypes.func,
  options: PropTypes.arrayOf(PropTypes.object),
};
Select.defaultProps = {
  onChange: () => false,
  options: [],
};

export default Select;
