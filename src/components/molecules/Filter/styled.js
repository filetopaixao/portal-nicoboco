import styled from 'styled-components';
import { shade } from 'polished';

import Input from '@components/atoms/Input';
import Select from '@components/atoms/Select';

import Search from '@assets/images/search.svg';

export const ContentInput = styled.div`
  display: flex;
  justify-items: center;

  width: ${(props) => props.width};
  height: 40px;
  padding: 5px 10px;

  border-radius: 10px;
  border: ${(props) => (props.theme === 'Dark' ? '1px solid #2F3743' : '1px solid #EAEAEA')};

  background: ${(props) => (props.theme === 'Dark' ? '#202731' : '#FFFFFF')};

  transition: border-color 0.3s;

  &::after {
    content: '';
    clear: both;
    display: block;
    visibility: visible;
    align-self: center;

    height: 22px;
    width: 22px;

    position: relative;
    right: 0px;
    bottom: 0;
    opacity: 1;
    z-index: 1;

    -webkit-mask-image: url(${Search});
    mask-image: url(${Search});
    mask-repeat: no-repeat;
    mask-size: contain;
    background-color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
  }

  > div {
    width: 100%;
  }

  &:hover {
    border-color: ${shade(0.1, '#cdcdcd')};
  }

  @media (max-width: 991px) {
    width: 100%;
  }
`;

export const FilterInput = styled(Input)`
  font-size: 13px;
  text-decoration: none;
  text-transform: uppercase;

  width: 100%;
  height: 100%;

  padding: 0;
  padding-left: 5px;

  border: none;
  cursor: pointer;

  background: ${(props) => (props.theme === 'Dark' ? '#202731' : '#FFFFFF')};
  color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};

  &:focus {
    outline: thin dotted;
    outline: 0px auto -webkit-focus-ring-color;
    outline-offset: 0px;

    border: 0;
    outline: none;
    text-shadow: 0 0 0 #000;
  }
  ::placeholder {
    color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
  }
`;

export const FilterSelect = styled(Select)`
  text-decoration: none;

  width: 400px;
  height: 100%;

  padding: 0;
  padding-left: 5px;
  margin-right: 30px;

  border: none;
  cursor: pointer;

  select {
    border: 1px solid #cdcdcd;

    background: #f1eeee 0% 0% no-repeat padding-box;

    color: #cecccc;
  }

  &:focus {
    outline: thin dotted;
    outline: 0px auto -webkit-focus-ring-color;
    outline-offset: 0px;
  }
`;
