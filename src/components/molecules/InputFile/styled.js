import styled from 'styled-components';

export const Container = styled.div`
  text-align: center;
  font-size: 16px;

  padding: 14px 30px 14px 30px;

  width: 100%;
  min-height: 40px;

  border-radius: 15px;
  border: solid 1px #bdbdbd4d;
  cursor: pointer;

  border: ${(props) => (props.theme === 'Dark' ? '1px solid #47505E' : '1px solid #EAEAEA')};

  background: ${(props) => (props.theme === 'Dark' ? '#29303A' : '#FBFBFB')};

  color: ${(props) => props.primaryColor};

  input[type='file'] {
    display: none;
  }
`;

export const Label = styled.label`
  font-weight: bold;

  width: 100%;

  padding: 0 50px;

  cursor: pointer;

  background-color: transparent;
  color: ${(props) => props.primaryColor};
`;

export const Input = styled.input`
  width: 100%;
`;

export const ErrorMessage = styled.div`
  font-size: 10px;
  text-align: left;
  text-transform: uppercase;

  margin-top: 5px;

  color: #f14479;
`;
