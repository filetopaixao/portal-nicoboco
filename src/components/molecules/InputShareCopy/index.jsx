import React, { useState } from 'react';
import PropTypes from 'prop-types';

import Toast from '@components/atoms/Toast';

import WahtsApp from '@assets/images/whatsapp.svg';
import Facebook from '@assets/images/facebook.svg';
import Email from '@assets/images/email.svg';

import * as S from './styled';

const InputShareCopy = ({ value, id, urlWhatsApp, urlFacebook, urlEmail }) => {
  const [isToShareVisible, setIsToShareVisible] = useState(false);
  const [isToastVisible, setIsToastVisible] = useState(false);

  const handleToShare = () => {
    setIsToShareVisible(!isToShareVisible);

    setTimeout(() => {
      setIsToShareVisible(false);
    }, 50000);
  };

  const handleActiveToast = () => {
    setIsToastVisible(true);

    setTimeout(() => {
      setIsToastVisible(false);
    }, 3000);
  };

  const handleCopy = () => {
    const btnCopy = document.getElementById(id);
    btnCopy.select();
    document.execCommand('copy');

    handleActiveToast();
  };

  return (
    <>
      <S.ContentInput>
        <S.SCInput id={id} name="search" type="text" placeholder="copiar" value={value} onChange={() => false} />
        <S.CopyButton onClick={() => handleCopy()} />
        <S.ShareButton onClick={() => handleToShare()} />
      </S.ContentInput>
      <Toast
        variant="success"
        icon="success"
        showIcon
        isVisible={isToastVisible}
        onClose={() => setIsToastVisible(!isToastVisible)}
      >
        Link copiado com sucesso!!
      </Toast>
      {isToShareVisible ? (
        <S.ContentToShare>
          <li>
            <a href={urlWhatsApp} rel="noopener noreferrer" target="_blank">
              <img src={WahtsApp} alt="WhatsApp" />
              <p>WhatsApp</p>
            </a>
          </li>
          <li>
            <a href={urlFacebook} rel="noopener noreferrer" target="_blank">
              <img src={Facebook} alt="Facebook" />
              <p>Facebook</p>
            </a>
          </li>
          <li>
            <a href={urlEmail} rel="noopener noreferrer" target="_blank">
              <img src={Email} alt="E-mail" />
              <p>E-mail</p>
            </a>
          </li>
        </S.ContentToShare>
      ) : null}
    </>
  );
};

InputShareCopy.propTypes = {
  value: PropTypes.string,
  id: PropTypes.string,
  urlWhatsApp: PropTypes.oneOfType([PropTypes.string]),
  urlFacebook: PropTypes.oneOfType([PropTypes.string]),
  urlEmail: PropTypes.oneOfType([PropTypes.string]),
};
InputShareCopy.defaultProps = {
  value: '',
  id: 'copy-share',
  urlWhatsApp: '!#',
  urlFacebook: '!#',
  urlEmail: '!#',
};

export default InputShareCopy;
