import React, { useState } from 'react';
import PropTypes from 'prop-types';

import ModalVideo from '@components/molecules/Modals/ModalVideo';
import ButtonGroupShareCopy from '@components/molecules/ButtonGroupShareCopy';

import Video from '@assets/images/video.svg';

import * as S from './styled';

const Banner = ({ link, image, title, imageMobile, copyShare }) => {
  const [isModalVideoVisible, setIsModalVideoVisible] = useState(false);
  const linkVideo = 'https://www.youtube.com/embed/LiiYMEWKVnY';
  const handleOpenModalVideo = () => {
    setIsModalVideoVisible(!isModalVideoVisible);
  };
  return (
    <>
      <S.BannerContainer image={image} imageMobile={imageMobile}>
        <S.BannerContent>
          <S.BannerTitle>{title}</S.BannerTitle>
          <S.ButtonGroup className="content_banner">
            {link ? <S.BannerLinkText type="text" id="copy" onChange={link} value={link} /> : null}
            <div>{copyShare ? <ButtonGroupShareCopy /> : null}</div>
          </S.ButtonGroup>
        </S.BannerContent>
        <S.BannerContentVideo>
          <a href="#!" onClick={() => handleOpenModalVideo()}>
            <img src={Video} alt="Assitir" />
          </a>
        </S.BannerContentVideo>
      </S.BannerContainer>
      <ModalVideo
        isModalVideoVisible={isModalVideoVisible}
        setIsModalVideoVisible={setIsModalVideoVisible}
        linkVideo={linkVideo}
      />
    </>
  );
};

Banner.propTypes = {
  link: PropTypes.string,
  image: PropTypes.string,
  title: PropTypes.string,
  imageMobile: PropTypes.bool,
  copyShare: PropTypes.bool,
};
Banner.defaultProps = {
  link: '',
  image: '',
  title: '',
  imageMobile: true,
  copyShare: true,
};

export default Banner;
