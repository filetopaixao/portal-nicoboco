import React from 'react';
import { withKnobs, text } from '@storybook/addon-knobs';

import { ContainerStorybook } from '@assets/styles/components';
import MenuOpsFilterPrint from './index';

export default {
  title: 'molecules/MenuOpsFilterPrint',
  component: MenuOpsFilterPrint,
  decorators: [withKnobs],
};

export const element = () => (
  <ContainerStorybook>
    <MenuOpsFilterPrint setFilterText={text('FilterText', 'Text')} filterText={text('FilterText', 'Text')} />
  </ContainerStorybook>
);

element.story = {
  name: 'Default',
};
