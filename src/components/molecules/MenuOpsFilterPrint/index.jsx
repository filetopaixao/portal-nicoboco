import React, { useCallback } from 'react';
import PropTypes from 'prop-types';
import html2canvas from 'html2canvas';

import Button from '@components/atoms/Button';
import Filter from '@components/molecules/Filter';

import Print from '@assets/images/print.svg';

import * as S from './styled';

const MenuOpsFilterPrint = ({ setFilterText, filterText }) => {
  const handlePrint = useCallback(() => {
    const table = document.querySelector('#print');
    html2canvas(table).then((canvas) => {
      const img = canvas.toDataURL('image/jpeg', 1.0).replace('image/jpeg', 'image/octet-stream');
      const link = document.createElement('a');
      const d = new Date();
      const date = d.toLocaleDateString();
      const hours = `${d.getHours()}:${d.getMinutes()}`;
      link.download = `screenshot-${date} ${hours}.jpeg`;
      link.href = img;
      link.click();
    });
  }, []);

  return (
    <S.MenuOpsTable>
      <Filter onFilter={(e) => setFilterText(e.target.value)} filterText={filterText} />
      <S.ContentButton>
        <img src={Print} alt="Imprimir" />
        <Button onClick={() => handlePrint()} type="button">
          Imprimir
        </Button>
      </S.ContentButton>
    </S.MenuOpsTable>
  );
};

MenuOpsFilterPrint.propTypes = {
  filterText: PropTypes.string,
  setFilterText: PropTypes.func,
};
MenuOpsFilterPrint.defaultProps = {
  filterText: '',
  setFilterText: () => {},
};

export default MenuOpsFilterPrint;
