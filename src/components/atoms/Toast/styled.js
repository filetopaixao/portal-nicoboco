import styled, { css } from 'styled-components';

import ToastSuccessIcon from '@assets/images/toast_success_icon.svg';
import ToastDangerIcon from '@assets/images/toast_danger_icon.png';
import ToastWarningIcon from '@assets/images/toast_warning_icon.png';

const icons = {
  success: () => css`
    background: url(${ToastSuccessIcon}) no-repeat;
  `,
  danger: () => css`
    background: url(${ToastDangerIcon}) no-repeat;
  `,
  warning: () => css`
    background: url(${ToastWarningIcon}) no-repeat;
  `,
};

const active = css`
  display: block;

  z-index: 100;

  transition: all 6s linear 0.1s;
`;

const showIcon = () => css`
  padding-right: 40px;

  &::after {
    content: '';
    clear: both;
    display: block;
    visibility: visible;

    position: absolute;
    left: 8px;
    top: 6px;
    height: 24px;
    width: 24px;
  }
`;

export const ToastContainer = styled.div`
  display: none;

  min-height: 28px;

  padding: 8px 24px;

  position: fixed;
  right: 8px;
  top: 10px;

  border: none;
  border-radius: 8px;

  box-shadow: 0px 3px 10px #00000033;
  cursor: pointer;
  opacity: 1;
  z-index: 2;

  background: #fff 0% 0% no-repeat padding-box;

  ${(props) => (props.isVisible ? active : null)};

  ${(props) => (props.showIcon ? showIcon : '')}

  &::after {
    ${(props) => icons[props.icon]}
    background-size: contain;
  }

  @media (min-width: 992px) {
    padding: 8px 32px;
  }
`;

export const ToastContent = styled.p`
  font-size: 14px;
  text-transform: uppercase;

  margin-left: 20px;

  color: #5e5e5e;
`;
