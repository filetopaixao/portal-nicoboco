import React from 'react';
import PropTypes from 'prop-types';

import * as S from './styled';

const Toast = ({ children, variant, showIcon, icon, onClose, id, isVisible }) => {
  const handleOutSideClick = (e) => {
    if (e.target.id === id) onClose();
  };

  return (
    <>
      {isVisible ? (
        <S.ToastContainer
          isVisible={isVisible}
          showIcon={showIcon}
          icon={icon}
          variant={variant}
          id={id}
          onClick={handleOutSideClick}
        >
          <S.ToastContent>{children}</S.ToastContent>
        </S.ToastContainer>
      ) : null}
    </>
  );
};

Toast.propTypes = {
  children: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  variant: PropTypes.string,
  showIcon: PropTypes.bool,
  icon: PropTypes.string,
  onClose: PropTypes.func,
  id: PropTypes.string,
  isVisible: PropTypes.bool,
};
Toast.defaultProps = {
  children: '',
  variant: 'success',
  showIcon: true,
  icon: 'success',
  onClose: () => false,
  id: 'toast',
  isVisible: true,
};

export default Toast;
