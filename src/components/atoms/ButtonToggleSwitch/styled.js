import styled from 'styled-components';

export const Slider = styled.div`
  position: absolute;
  bottom: 0;
  left: 0;
  width: 45px;
  height: 15px;
  background-color: #f2f2f2;
  border-radius: 14px;
  transition-duration: 2s;
  transition: cubic-bezier(0.68, -0.55, 0.265, 1.55);

  &.slider div {
    content: '';
    height: 21px;
    width: 21px;
    border-radius: 50%;
    position: absolute;
    bottom: -3px;
    left: -1px;

    ${(props) => (props.value ? 'background-color: #2fdf46;' : 'background-color: #aaa9a9;;')};

    cursor: pointer;
    transition-duration: 2s;
    transition: cubic-bezier(0.68, -0.55, 0.265, 1.55);
  }
`;

export const ButtonToggleSwitch = styled.label`
  position: relative;

  input[type='checkbox'] {
    visibility: hidden;
  }

  input:checked + .slider {
    background: #99f5a6;
  }

  input:checked + .slider div {
    transform: translateX(26px);
    background-color: #2fdf46;
  }
`;

export default ButtonToggleSwitch;
