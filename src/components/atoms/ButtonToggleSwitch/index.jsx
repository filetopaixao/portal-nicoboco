import React from 'react';
import PropTypes from 'prop-types';

import * as S from './styled';

const ButtonToggleSwitch = ({ checked, onChange }) => (
  <S.ButtonToggleSwitch className="toogle">
    <input type="checkbox" name="toogleOp" id="toogleOp" onChange={onChange} checked={checked} value={checked} />
    <S.Slider className="slider" value={checked}>
      <div />
    </S.Slider>
  </S.ButtonToggleSwitch>
);

ButtonToggleSwitch.propTypes = {
  checked: PropTypes.bool,
  onChange: PropTypes.func,
};
ButtonToggleSwitch.defaultProps = {
  checked: false,
  onChange: () => false,
};

export default ButtonToggleSwitch;
