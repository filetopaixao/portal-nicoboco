import React from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';

import * as S from './styled';

const SelectCategories = ({ className, options, onChange, defaultValue }) => {
  const themeStore = useSelector((state) => state.theme);
  return (
    <S.ContainerSelect className={className}>
      <S.Select onChange={onChange} theme={themeStore.themeName}>
        <S.Option hidden>{defaultValue}</S.Option>
        {options.map(({ name, id, type }) => (
          <S.Option value={type} key={id}>
            {name}
          </S.Option>
        ))}
      </S.Select>
    </S.ContainerSelect>
  );
};

SelectCategories.propTypes = {
  className: PropTypes.string,
  defaultValue: PropTypes.string,
  onChange: PropTypes.func,
  options: PropTypes.arrayOf(PropTypes.object),
};
SelectCategories.defaultProps = {
  className: '',
  defaultValue: 'Selecione uma opção',
  onChange: () => false,
  options: [],
};

export default SelectCategories;
