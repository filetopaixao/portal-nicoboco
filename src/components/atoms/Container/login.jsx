import React from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import Logo from '@components/atoms/Logo';
import { Link } from 'react-router-dom';
import * as S from './styled';

const ContainerForm = ({ children, className, id, title, subtitle, backTo }) => {
  const themeStore = useSelector((state) => state.theme);
  return (
    <div className={className} id={id}>
      <S.ContainerHeader>
        <Logo />
      </S.ContainerHeader>
      <S.ContainerBody theme={themeStore.themeName} primaryColor={themeStore.primaryColor}>
        {backTo ? (
          <Link to={backTo}>
            <S.Close>fechar X</S.Close>
          </Link>
        ) : null}
        <div className="title">
          <S.ContainerTitle theme={themeStore.themeName}>{title}</S.ContainerTitle>
          <S.ContainerSubtitle theme={themeStore.themeName}>{subtitle}</S.ContainerSubtitle>
        </div>
        {children}
      </S.ContainerBody>
    </div>
  );
};

ContainerForm.propTypes = {
  children: PropTypes.element.isRequired,
  className: PropTypes.string,
  id: PropTypes.string,
  title: PropTypes.string,
  subtitle: PropTypes.string,
  backTo: PropTypes.bool,
};

ContainerForm.defaultProps = {
  className: '',
  id: '',
  title: 'title,',
  subtitle: 'subtitle',
  backTo: false,
};

export default ContainerForm;
