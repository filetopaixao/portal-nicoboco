import React from 'react';
import PropTypes from 'prop-types';
import * as S from './styled';

const MainContainer = ({ children, className }) => <S.Container className={className}>{children}</S.Container>;

MainContainer.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.number,
    PropTypes.func,
    PropTypes.string,
    PropTypes.node,
  ]),
  className: PropTypes.string,
};
MainContainer.defaultProps = {
  children: '',
  className: '',
};

export default MainContainer;
