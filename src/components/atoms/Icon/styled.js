import styled, { css } from 'styled-components';

const iconPosition = {
  left: () => css`
    left: 0;
  `,
  right: () => css`
    right: 0;
  `,
};

export const Icon = styled.div`
  top: 0;

  height: 25px;
  width: 25px;

  ${(props) => iconPosition[props.position]}

  margin: 9px 10px 9px 11px;

  @media (min-width: 1400px) {
    margin: 13px 27px 13px 24px;
  }
`;

export const ContainerIcon = styled.div`
  display: block;
  height: 25px;
  width: 25px;

  svg {
    height: 100%;
    width: 100%;
  }
`;
