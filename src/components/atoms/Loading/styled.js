import styled from 'styled-components';

export const LoadingContainer = styled.div`
  width: 100%;

  img {
    height: 200px;
    width: 200px;

    margin: 0 auto;
  }
`;

export default LoadingContainer;
