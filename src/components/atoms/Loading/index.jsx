import React from 'react';

import * as S from './styled';

const Loading = () => {
  return (
    <S.LoadingContainer className="flex-center">
      <img
        className="img200"
        alt="loading"
        src="https://agenciabrasilia.df.gov.br/wp-conteudo/themes/agencia-brasilia/img/carregando.gif"
      />
    </S.LoadingContainer>
  );
};

export default Loading;
