import React from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';

import * as S from './styled';

const Button = ({
  iconPosition,
  margin,
  padding,
  height,
  width,
  children,
  variant,
  showIcon,
  icon,
  onClick,
  fullSize,
  alignText,
  typeButton,
}) => {
  const themeStore = useSelector((state) => state.theme);
  return (
    <S.Button
      margin={margin}
      padding={padding}
      width={width}
      variant={variant}
      showIcon={showIcon}
      icon={icon}
      onClick={onClick}
      fullSize={fullSize}
      height={height}
      iconPosition={iconPosition}
      alignText={alignText}
      type={typeButton}
      primaryColor={themeStore.primaryColor}
      secondaryColor={themeStore.secondaryColor}
      tertiaryColor={themeStore.tertiaryColor}
    >
      {children}
    </S.Button>
  );
};

Button.propTypes = {
  variant: PropTypes.string,
  children: PropTypes.oneOfType([PropTypes.element, PropTypes.number, PropTypes.func, PropTypes.string]),
  showIcon: PropTypes.bool,
  icon: PropTypes.string,
  onClick: PropTypes.func,
  fullSize: PropTypes.bool,
  margin: PropTypes.string,
  padding: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
  iconPosition: PropTypes.string,
  alignText: PropTypes.string,
  typeButton: PropTypes.string,
};
Button.defaultProps = {
  variant: '',
  children: '',
  showIcon: false,
  icon: '',
  onClick: () => false,
  fullSize: false,
  margin: '',
  padding: '8px 24px;',
  width: '',
  height: '',
  iconPosition: '',
  alignText: '',
  typeButton: '',
};

Button.Group = S.ButtonGroup;

export default Button;
