import styled, { css } from 'styled-components';

import IconCopy from '@assets/images/copyWhite.svg';
import IconShare from '@assets/images/shareWhite.svg';
import IconTrash from '@assets/images/trash.svg';
import IconArrowRight from '@assets/images/arrowRight.svg';
import IconWhatsApp from '@assets/images/WhatsApp_White.svg';
import IconEmail from '@assets/images/e-mail-white.svg';
import InconTrash from '@assets/images/trash2.png';
import InconAdd from '@assets/images/icon_add.svg';

const variants = {
  primary: () => css`
    background: #ff7f00;
  `,
  secondary: () => css`
    background: #01a3ff;
  `,
  dark: () => css`
    background: #1045d4;
  `,
  success: () => css`
    background: #2fdf46;
  `,
  disabled: () => css`
    background: #bdbdbd;
    pointer-events: none;
  `,
  danger: () => css`
    background: #ff0000;
  `,
};

const icons = {
  copy: () => css`
    background: url(${IconCopy}) no-repeat;
  `,
  share: () => css`
    background: url(${IconShare}) no-repeat;
  `,
  trash: () => css`
    background: url(${IconTrash}) no-repeat;
  `,
  arrowRight: () => css`
    background: url(${IconArrowRight}) no-repeat;
  `,
  whats: () => css`
    background: url(${IconWhatsApp}) no-repeat;
  `,
  email: () => css`
    background: url(${IconEmail}) no-repeat;
  `,
  trash_grey: () => css`
    background: url(${InconTrash}) no-repeat;
  `,
  inconAdd: () => css`
    background: url(${InconAdd}) no-repeat;
  `,
};

const iconsPosition = {
  arrowRight: () => css`
    right: -1px;
    top: 10px;
    width: 20px;
  `,
  share: () => css`
    top: 6px;
  `,
  center: () => css`
    position: inherit;
    right: inherit;
    top: inherit;
  `,
};
const showIcon = () => css`
  &::after {
    content: '';
    clear: both;
    display: block;
    visibility: visible;

    position: absolute;
    right: 8px;
    top: 8px;

    @media (min-width: 1500px) {
      height: 25px;
      width: 25px;
    }
    height: 17px;
    width: 17px;
  }
`;

export const Button = styled.button`
  font-size: 18px;
  text-transform: uppercase;
  text-align: ${(props) => props.alignText};

  position: relative;
  cursor: pointer;
  border: none;
  border-radius: 15px;

  box-shadow: 0 3px 6px rgba(0, 0, 0, 0.16);
  outline: 0;

  color: #fff;

  height: ${(props) => (props.height ? props.height : '53px')};
  width: ${(props) => props.width};
  margin: ${(props) => props.margin};
  padding: ${(props) => props.padding};

  ${(props) => (props.fullSize ? 'width: 100%;' : '')};
  /*${(props) => (props.variant ? variants[props.variant] : `background: ${props.secondaryColor}`)};*/

  background: ${(props) =>
    props.tertiaryColor
      ? `transparent linear-gradient(270deg, ${props.tertiaryColor} 0%, ${props.primaryColor} 46%, ${props.secondaryColor} 100%) 0% 0% no-repeat padding-box;`
      : `transparent linear-gradient(270deg, ${props.primaryColor} 0%, ${props.secondaryColor} 100%) 0% 0% no-repeat padding-box;`};

  ${(props) => (props.showIcon ? showIcon : '')};

  &::after {
    ${(props) => icons[props.icon]};
    ${(props) => iconsPosition[props.iconPosition]};
    background-size: contain;
  }
  /* @media (min-width: 1500px) {
    min-height: 38px;
  } */
`;

export const ButtonCopy = styled(Button)`
  border-radius: 0;
  box-shadow: none;

  background: none;
`;

export const ButtonGroup = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  align-content: center;
  align-items: center;

  margin: 25px 0;
  width: 100%;

  &.start {
    justify-content: flex-start;
  }
  &.center {
    justify-content: center;
  }
  &.end {
    justify-content: flex-end;
  }

  @media (min-width: 992px) {
    flex-direction: row;
    width: 100%;
  }
`;

export const Label = styled.label`
  position: relative;
  top: -15px;
  float: left;
  width: 100%;
  height: 45px;

  input[type='checkbox'] {
    visibility: hidden;
  }

  .slider {
    position: absolute;
    bottom: 0;
    left: 0;
    width: 45px;
    height: 15px;
    background-color: #f2f2f2;
    border-radius: 14px;
    transition: 0.2s;
  }

  .slider div {
    content: '';
    height: 21px;
    width: 21px;
    border-radius: 50%;
    position: absolute;
    bottom: -3px;
    left: -1px;
    background-color: #2fdf46;
    cursor: pointer;
    transition: 0.2s;
  }

  input:checked + .slider {
    background: #99f5a6;
  }

  input:checked + .slider div {
    transform: translateX(26px);
  }
`;

export default Button;
