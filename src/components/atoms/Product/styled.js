import styled, { css } from 'styled-components';

import Lnk from '@components/atoms/Link';
import Txt from '@components/atoms/Text';

const selectedProduct = {
  yes: () => css`
    content: '';
    clear: both;
    display: block;
    visibility: visible;
    align-self: center;

    height: 100%;
    width: 100%;

    position: absolute;
    right: 0px;
    bottom: 0;

    border-radius: 8px;

    background: #e5e5e5;
    opacity: 0.5;
    background-size: contain;
  `,
};

export const Product = styled.div`
  position: relative;

  max-width: 250px;
  width: 100%;

  background: ${(props) => (props.theme === 'Dark' ? '#29303A' : '#FBFBFB')};

  color: ${(props) => (props.theme === 'Dark' ? '#47505E' : '#CECECE')};
  box-shadow: 0px 3px 10px #00000029;
  border-radius: 10px;

  &::after {
    ${(props) => (props.isSelected ? selectedProduct.yes : '')}
  }

  ${(props) => props.purchase && 'cursor: pointer;'}

  p {
    @media (max-width: 1400px) {
      font-size: 10px;
    }
  }

  @media (min-width: 2000px) {
    width: 80%;
  }

  @media (min-width: 992px) and (max-width: 2000px) {
    margin: 0 auto;

    max-width: 281px;
    width: 100%;

    border-radius: 15px;
  }
  /* ${(props) => props.isSelected && 'filter: grayscale(100%); opacity: 0.7;'} */
`;

export const ProductContainer = styled.div`
  width: 100%;

  padding: 10px;

  border-radius: 10px;

  background: ${(props) => (props.theme === 'Dark' ? '#202731' : '#FBFBFB')};

  color: ${(props) => (props.theme === 'Dark' ? '#47505E' : '#CECECE')};
`;

export const ProductTitle = styled.h3`
  font-size: 12px;
  font-weight: 600;
  text-align: center;

  margin-bottom: 15px;

  width: 100%;

  color: ${(props) => (props.theme === 'Dark' ? '#fff' : '#484646')};

  @media (min-width: 1336px) {
    font-size: 18px;

    margin-bottom: 20px;
  }
`;

export const ProductImage = styled.div`
  width: 100%;

  img {
    margin: 0 auto;
    height: auto;
    height: 100%;
    width: 100%;

    max-height: 170px;
    max-width: 170px;

    @media (min-width: 992px) {
      max-width: 140px;
      height: 140px;
    }
  }
`;

export const ProductValues = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;

  width: 100%;

  p {
    color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
  }

  &.product__value {
    margin-top: 15px;

    @media (min-width: 992px) {
      margin-top: 20px;
    }
  }
`;

export const ProducInfo = styled.div`
  display: none;

  @media (min-width: 992px) {
    display: flex;
    justify-content: space-between;

    margin-top: 15px;
    width: 100%;

    div {
      display: flex;

      width: 100%;

      &:first-child {
        width: 364px;
      }

      &:last-child {
        justify-content: flex-end;
      }
    }

    p {
      height: 25px;
      width: 25px;
    }

    p:not(:last-child) {
      margin-right: 10px;
    }
  }
`;

export const ProductLink = styled(Lnk)`
  font-size: 12px;
  font-weight: 500;
`;

export const ProductTextClick = styled(Txt)`
  cursor: pointer;
`;
