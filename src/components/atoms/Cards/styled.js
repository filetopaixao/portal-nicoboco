import styled, { css } from 'styled-components';

import Btn from '@components/atoms/Button';

import CommisionWhite from '@assets/images/commision_white.svg';
import Discount from '@assets/images/discount.svg';
import Saldo from '@assets/images/saldo.svg';
import Faturado from '@assets/images/faturado.svg';
import Liberacao from '@assets/images/liberacao.svg';
import Liberado from '@assets/images/liberado.svg';
import Bank from '@assets/images/bank.svg';
import Shopping from '@assets/images/shopping.svg';
import Marketing from '@assets/images/marketing.svg';
import University from '@assets/images/university.svg';
import Sales from '@assets/images/sales.svg';
// import LootReleased from '@assets/images/discount.svg';
import TotalBilled from '@assets/images/total_faturado.svg';
import IconCopy from '@assets/images/copyWhite.svg';
import IconShare from '@assets/images/shareWhite.svg';
import IconTrash from '@assets/images/trash.svg';
import IconArrowRight from '@assets/images/arrowRight.svg';
import IconWhatsApp from '@assets/images/WhatsApp_White.svg';
import IconEmail from '@assets/images/e-mail-white.svg';
import InconTrash from '@assets/images/trash2.png';
import InconAdd from '@assets/images/icon_add.svg';

const images = {
  sales: () => css`
    -webkit-mask-image: url(${Sales});
    mask-image: url(${Sales});
    mask-size: 50px;
  `,
  commision: () => css`
    -webkit-mask-image: url(${CommisionWhite});
    mask-image: url(${CommisionWhite});
    mask-size: 50px;
  `,
  lootReleased: () => css`
    -webkit-mask-image: url(${Discount});
    mask-image: url(${Discount});
    mask-size: 50px;
  `,
  totalBilled: () => css`
    -webkit-mask-image: url(${TotalBilled});
    mask-image: url(${TotalBilled});
    mask-size: 50px;
  `,
  discount: () => css`
    -webkit-mask-image: url(${Discount});
    mask-image: url(${Discount});
    mask-size: 50px;
  `,
  saldo: () => css`
    -webkit-mask-image: url(${Saldo});
    mask-image: url(${Saldo});
    mask-size: 50px;
  `,
  faturado: () => css`
    -webkit-mask-image: url(${Faturado});
    mask-image: url(${Faturado});
    mask-size: 50px;
  `,
  liberacao: () => css`
    -webkit-mask-image: url(${Liberacao});
    mask-image: url(${Liberacao});
    mask-size: 50px;
  `,
  liberado: () => css`
    -webkit-mask-image: url(${Liberado});
    mask-image: url(${Liberado});
    mask-size: 50px;
  `,
  bank: () => css`
    -webkit-mask-image: url(${Bank});
    mask-image: url(${Bank});
    mask-size: 50px;
  `,
  shopping: () => css`
    -webkit-mask-image: url(${Shopping});
    mask-image: url(${Shopping});
    mask-size: 50px;
  `,
  marketing: () => css`
    -webkit-mask-image: url(${Marketing});
    mask-image: url(${Marketing});
    mask-size: 50px;
  `,
  university: () => css`
    -webkit-mask-image: url(${University});
    mask-image: url(${University});
    mask-size: 50px;
  `,
};

const variants = {
  sales: () => css`
    background: transparent linear-gradient(270deg, #fc599a 0%, #ffc400 100%);
  `,
  commision: () => css`
    background: transparent linear-gradient(270deg, #1289e3 0%, #3ce9e9 100%);
  `,
  lootReleased: () => css`
    background: transparent linear-gradient(270deg, #28bc8f 0%, #6aff6f 100%);
  `,
  totalBilled: () => css`
    background: transparent linear-gradient(90deg, #e351d0 0%, #9355d9 100%);
  `,
  bank: () => css`
    background: linear-gradient(261deg, #000 0%, #fff 100%);
  `,
  shopping: () => css`
    background: linear-gradient(261deg, #f14479 0%, #ffaf28 100%);
  `,
  marketing: () => css`
    background: linear-gradient(261deg, #28bc8f 0%, #6aff6f 100%);
  `,
  university: () => css`
    background: linear-gradient(261deg, #1289e3 0%, #3ce9e9 100%);
  `,
  discount: () => css`
    background: linear-gradient(261deg, #9355d9 0%, #e351d0 100%);
  `,
};

const colorVariantNotification = {
  success: () => css`
    color: #2fdf46;
  `,
  info: () => css`
    color: #2fdfd3;
  `,
  infoDark: () => css`
    color: #1590e3;
  `,
  warning: () => css`
    color: #f6b546;
  `,
  danger: () => css`
    color: #f14479;
  `,
  ever: () => css`
    color: #ff7f00;
  `,
  partner: () => css`
    color: #9655d9;
  `,
};

const bgVariantNotification = {
  success: () => css`
    background: #2fdf46;
  `,
  info: () => css`
    background: #2fdfd3;
  `,
  infoDark: () => css`
    background: #1590e3;
  `,
  warning: () => css`
    background: #f6b546;
  `,
  danger: () => css`
    background: #f14479;
  `,
  ever: () => css`
    background: #ff7f00;
  `,
  partner: () => css`
    background: #9655d9;
  `,
};

export const CardStatus = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  height: 70px;
  max-width: 152px;
  width: 100%;

  padding: 10px 21px;
  border-radius: 15px;
  border: ${(props) => (props.theme === 'Dark' ? '1px solid #2F3743' : '1px solid #EAEAEA')};

  background: ${(props) => (props.theme === 'Dark' ? '#202731' : '#FFFFFF')};

  @media (min-width: 800px) {
    max-width: 365px;
  }

  @media (min-width: 1300px) {
    height: 100px;
  }

  @media (min-width: 1580px) {
    max-width: 300px;
  }

  @media (min-width: 1750px) {
    max-width: 365px;
  }
`;

export const CardStatusIcon = styled.div`
  height: 40px;
  width: 40px;

  ${(props) => images[props.icon]}
  mask-size: 100%;
  mask-repeat: no-repeat;
  background: ${(props) => props.color};
  background-repeat: no-repeat;
  background-size: contain;

  @media (min-width: 600px) {
    height: 60px;
    width: 60px;
  }

  @media (min-width: 1337px) {
    height: 80px;
    width: 80px;
  }
`;

export const CardStatusInfo = styled.div`
  display: flex;
  flex-direction: column;

  width: 120px;

  p {
    text-align: right;
  }

  .cardStatus__info__title {
    text-transform: uppercase;
    color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
  }
  .cardStatus__info__description {
    color: ${(props) => (props.theme === 'Dark' ? '#fff' : '#484646')};
  }

  @media (max-width: 1336px) {
    .cardStatus__info__title {
      font-size: 10px;
    }
    .cardStatus__info__description {
      font-size: 18px;
    }
  }

  @media (min-width: 1336px) {
    .cardStatus__info__title {
      font-size: 14px;
    }
    .cardStatus__info__description {
      font-size: 25px;
    }
  }
`;

export const CardSpace = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-wrap: wrap;

  height: 100%;
  width: 100%;

  margin: 0 auto;
  padding: 20px 27px 20px 27px;

  position: relative;

  border-radius: 15px;
  margin: 5px;

  background: ${(props) =>
    props.tertiaryColor
      ? `transparent linear-gradient(270deg, ${props.tertiaryColor} 0%, ${props.primaryColor} 46%, ${props.secondaryColor} 100%) 0% 0% no-repeat padding-box;`
      : `transparent linear-gradient(270deg, ${props.primaryColor} 0%, ${props.secondaryColor} 100%) 0% 0% no-repeat padding-box;`};

  // eslint-disable-next-line no-nested-ternary

  ${(props) => {
    if (props.variant) return '';
    if (props.theme === 'Dark') return 'background: #29303A;';
    return 'background: #EAEAEA;';
  }}

  @media (max-width: 600px) {
    padding: 10px;
  }

  @media (min-width: 1750px) {
    max-width: 320px;
  }
`;

export const CardSpaceIcon = styled.div`
  height: 50px;
  width: 50px;

  div {
    background-color: ${(props) => (props.isSupport ? props.primaryColor : '#fff')};
    -webkit-mask-image: ${(props) => `url(${props.src})`};
    mask-image: ${(props) => `url(${props.src})`};
    width: 100%;
    height: 100%;
    mask-size: 50px;
  }

  ${(props) => (props.isComing ? 'opacity: 0.5;' : '')}
`;

export const CardSpaceTitle = styled.div`
  font-size: 18px;
  font-weight: bold;
  text-align: center;
  text-transform: uppercase;

  margin-top: 10px;
  width: 100%;

  ${(props) => (props.isComing ? 'opacity: 0.5;' : '')}

  color: #fff;

  @media (max-width: 1336px) {
    font-size: 12px;
  }

  @media (min-width: 1336px) {
    font-size: 17px;
  }
`;

export const CardNotification = styled.div`
  padding: calc(75px / 5);

  width: 100%;

  background: #fff;
`;

export const CardNotificationTitle = styled.h4`
  font-weight: 600;
  text-transform: uppercase;
  margin-bottom: 10px;

  ${(props) => colorVariantNotification[props.variant]}
`;

export const CardNoticiationDescription = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;

  .cardNotification__description {
    &__title,
    &__value {
      font-size: 15px;
    }
  }

  .cardNotification__info {
    &__data,
    &__value {
      font-size: 12px;
    }
  }
`;

export const CardPurchase = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;

  max-height: 102px;
  min-height: 80px;
  /* max-width: 750px; */
  height: 100%;
  width: 100%;

  padding: 0 25px;

  border-radius: 15px;

  /*${(props) => (props.variant ? variants[props.variant] : 'background: #E1DEDE;')}*/
  background: ${(props) =>
    // eslint-disable-next-line no-nested-ternary
    props.oneColor
      ? props.oneColor
      : props.tertiaryColor
      ? `transparent linear-gradient(270deg, ${props.tertiaryColor} 0%, ${props.primaryColor} 46%, ${props.secondaryColor} 100%) 0% 0% no-repeat padding-box;`
      : `transparent linear-gradient(270deg, ${props.primaryColor} 0%, ${props.secondaryColor} 100%) 0% 0% no-repeat padding-box;`};
`;

export const CardPurchaseIcon = styled.div`
  height: 50px;
  width: 50px;

  ${(props) => images[props.icon]}
  background-repeat: no-repeat;
  background-size: contain;
  background: #fff;
`;

export const CardPurchaseInfo = styled.div`
  display: flex;
  justify-content: flex-end;
  flex-wrap: wrap;

  p {
    text-align: right;
    width: 100%;
    color: #fff;
  }

  .purchase__info__title {
    text-transform: uppercase;
  }
`;

export const CardSale = styled(CardSpace)`
  cursor: pointer;
  /* margin-bottom: 20px; */

  @media (max-width: 500px) {
    max-width: 700px;
  }

  @media (max-width: 1650px) {
    max-width: 190px;
  }

  @media (min-width: 1651px) {
    max-width: 220px;
  }

  @media (min-width: 1750px) {
    max-width: 320px;
  }

  ${(props) => bgVariantNotification[props.variant]}
`;

export const CardSaleTitle = styled.p`
  font-size: 15px;
  font-weight: 500;
  text-transform: uppercase;

  ${(props) => (props.isComing ? 'opacity: 0.5;' : null)}

  color: #fff;

  @media (max-width: 1749px) {
    font-size: 12px;
  }

  @media (min-width: 1750px) {
    font-size: 17px;
  }
`;

export const ProfileContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;

  /* max-width: 330px; */
  width: 100%;

  border-radius: 15px;
  border: ${(props) => (props.theme === 'Dark' ? '1px solid #2F3743' : '1px solid #EAEAEA')};

  background: ${(props) => (props.theme === 'Dark' ? '#202731' : '#FFFFFF')};

  color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};

  @media (min-width: 768px) {
    flex-direction: row;
    justify-content: center;
    padding: 20px 0;
  }

  @media (min-width: 992px) {
    padding: 105px 0;
  }
`;

export const ProfileImage = styled.div`
  max-height: 140px;
  max-width: 140px;
  height: 100%;
  width: 100%;

  margin-bottom: 10px;

  border-radius: 300px;
  overflow: hidden;

  @media (min-width: 768px) {
    max-height: 230px;
    max-width: 230px;
  }
`;

export const ProfileInfo = styled.div`
  display: flex;
  flex-wrap: wrap;
  align-items: center;

  @media (min-width: 768px) {
    align-items: flex-start;
    max-width: 40%;
    margin-left: 60px;
  }
`;

export const ProfileName = styled.h3`
  font-size: 16px;
  font-weight: 600;
  text-align: center;

  width: 100%;

  @media (min-width: 768px) {
    text-align: left;
  }

  @media (min-width: 1024px) {
    font-size: 25px;
  }
`;

export const ProfileWork = styled.h4`
  font-size: 12px;
  font-weight: normal;
  text-align: center;
  text-transform: uppercase;

  margin-bottom: 45px;
  width: 100%;

  @media (min-width: 768px) {
    text-align: left;

    margin-bottom: 10px;
  }
  @media (min-width: 1024px) {
    font-size: 14px;
  }
`;

export const ProfileText = styled.p`
  font-size: 12px;
  font-weight: normal;
  text-align: center;

  width: 100%;

  &:not(:last-child) {
    margin-bottom: 5px;
  }

  @media (min-width: 768px) {
    text-align: left;
  }
  @media (min-width: 1024px) {
    font-size: 14px;
  }
`;

const icons = {
  copy: () => css`
    background: url(${IconCopy}) no-repeat;
  `,
  share: () => css`
    background: url(${IconShare}) no-repeat;
  `,
  trash: () => css`
    background: url(${IconTrash}) no-repeat;
  `,
  arrowRight: () => css`
    background: url(${IconArrowRight}) no-repeat;
  `,
  whats: () => css`
    background: url(${IconWhatsApp}) no-repeat;
  `,
  email: () => css`
    background: url(${IconEmail}) no-repeat;
  `,
  trash_grey: () => css`
    background: url(${InconTrash}) no-repeat;
  `,
  inconAdd: () => css`
    background: url(${InconAdd}) no-repeat;
  `,
};

const iconsPosition = {
  arrowRight: () => css`
    right: -1px;
    top: 10px;
    width: 20px;
  `,
  share: () => css`
    top: 6px;
  `,
  center: () => css`
    position: inherit;
    right: inherit;
    top: inherit;
  `,
};
const showIcon = () => css`
  &::after {
    content: '';
    clear: both;
    display: block;
    visibility: visible;

    position: absolute;
    right: 24px;
    top: 13px;

    /*
    @media (min-width: 1500px) {
      height: 25px;
      width: 25px;
    }
    */

    height: 25px;
    width: 25px;
  }
`;

export const Button = styled.button`
  font-size: 17px;
  text-transform: uppercase;
  text-align: ${(props) => props.alignText};

  position: relative;
  cursor: pointer;
  border: none;
  border-radius: 15px;

  box-shadow: 0 3px 6px rgba(0, 0, 0, 0.16);
  outline: 0;

  color: #fff;

  height: ${(props) => (props.height ? props.height : '43px')};
  width: ${(props) => props.width};
  margin: ${(props) => props.margin};
  padding: 8px 24px;

  ${(props) => (props.fullSize ? 'width: 100%;' : '')};
  /*${(props) => (props.variant ? variants[props.variant] : `background: ${props.secondaryColor}`)};*/

  background: ${(props) => props.color};

  ${(props) => (props.showIcon ? showIcon : '')};

  &::after {
    ${(props) => icons[props.icon]};
    ${(props) => iconsPosition[props.iconPosition]};
    background-size: contain;
  }
  /* @media (min-width: 1500px) {
    min-height: 38px;
  } */
`;

export const ButtonGroup = styled(Btn.Group)`
  margin: 0;
  margin-top: 45px;

  button:not(:last-child) {
    margin-bottom: 10px;
  }

  @media (min-width: 768px) {
    align-items: flex-start;
    flex-direction: column;

    margin-top: 30px;
  }

  @media (min-width: 1024px) {
    flex-direction: row;

    margin-top: 30px;
    width: 70%;
  }

  @media (min-width: 1366px) {
    width: 70%;
  }
`;
