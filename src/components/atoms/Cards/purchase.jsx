import React from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';

import Text from '@components/atoms/Text';

import * as S from './styled';

const Purchase = ({ className, icon, title, variant, value, discount, oneColor }) => {
  const themeStore = useSelector((state) => state.theme);
  return (
    <S.CardPurchase
      className={`purchase ${className}`}
      variant={variant}
      oneColor={oneColor}
      primaryColor={themeStore.primaryColor}
      secondaryColor={themeStore.secondaryColor}
      tertiaryColor={themeStore.tertiaryColor}
    >
      <S.CardPurchaseIcon icon={icon} className="purchase__icon" />

      <S.CardPurchaseInfo className="purchase__info">
        <Text size="xs" variant="light" className="purchase__info__title">
          {title}
        </Text>
        <Text size="md" variant="semiBold" className="purchase__info__value">
          {value || discount}
        </Text>
      </S.CardPurchaseInfo>
    </S.CardPurchase>
  );
};

Purchase.propTypes = {
  className: PropTypes.string,
  icon: PropTypes.string,
  title: PropTypes.string,
  variant: PropTypes.string,
  value: PropTypes.string,
  discount: PropTypes.string,
  oneColor: PropTypes.string,
};
Purchase.defaultProps = {
  className: '',
  icon: '',
  title: '',
  variant: '',
  value: '',
  discount: '',
  oneColor: '',
};

export default Purchase;
