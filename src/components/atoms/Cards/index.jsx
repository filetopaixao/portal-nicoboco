import React from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';

import Text from '@components/atoms/Text';

import * as S from './styled';

const Card = ({ className, icon, title, description, color }) => {
  const themeStore = useSelector((state) => state.theme);
  return (
    <S.CardStatus className={`cardStatus ${className}`} theme={themeStore.themeName}>
      <S.CardStatusIcon color={color} icon={icon} className="cardStatus__icon" />

      <S.CardStatusInfo className="cardStatus__info" theme={themeStore.themeName}>
        <Text size="md" variant="light" className="cardStatus__info__title">
          {title}
        </Text>
        <Text size="md" variant="semiBold" className="cardStatus__info__description">
          {description || 0}
        </Text>
      </S.CardStatusInfo>
    </S.CardStatus>
  );
};

Card.propTypes = {
  className: PropTypes.string,
  icon: PropTypes.string,
  title: PropTypes.string,
  description: PropTypes.string,
  color: PropTypes.string,
};
Card.defaultProps = {
  className: 'saldo',
  icon: 'saldo',
  title: 'title',
  description: 'description',
  color: '#82868D',
};

export default Card;
