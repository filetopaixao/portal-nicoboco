import styled, { css } from 'styled-components';

const variants = {
  login: () => css`
    background: #ccedff;
  `,

  formData: () => css`
    background: #f2f2f2;
  `,

  default: () => css`
    background: #fff;
    &:active {
      background: inherit;
    }
    &:-webkit-autofill {
      -webkit-box-shadow: inherit;
      background: inherit;
      -webkit-text-fill-color: inherit;
    }
  `,
};

export const InputContainer = styled.div`
  /* input[search] {
    background: red;
  } */
`;

export const Input = styled.input`
  font-size: 12px;

  display: block;
  border: none;
  border-radius: 10px;
  height: 40px;

  padding: 14px 0 14px 30px;
  width: 100%;

  ${(props) => (props.variant ? variants[props.variant] : variants.default)};

  color: #2d2d3a;

  &:disabled {
    cursor: not-allowed;
    background: #eaeaea;
  }

  @media (min-width: 1400px) {
    font-size: 16px;
  }
`;

export const ErrorMessage = styled.div`
  font-size: 10px;
  text-align: left;
  text-transform: uppercase;

  margin-top: 5px;

  color: #f14479;
`;

export default Input;
