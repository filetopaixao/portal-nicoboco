import React from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';

import * as S from './styled';

const CollapseOut = ({ children, title, titlePosition, active, onClick, id, backgroundColor }) => {
  const themeStore = useSelector((state) => state.theme);
  return (
    <>
      <S.Container
        out
        active={active}
        backgroundColor={backgroundColor}
        theme={themeStore.themeName}
        secondaryColor={themeStore.secondaryColor}
      >
        <S.ContainerTitle active={active} out>
          <S.Title active={active} position={titlePosition} theme={themeStore.themeName}>
            {title}
          </S.Title>
          <S.Close onClick={(e) => onClick(e)} active={active} id={id} />
        </S.ContainerTitle>
      </S.Container>
      <S.ContainerContent active={active} out theme={themeStore.themeName}>
        {children}
      </S.ContainerContent>
    </>
  );
};

CollapseOut.propTypes = {
  children: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  active: PropTypes.bool,
  title: PropTypes.string,
  titlePosition: PropTypes.string,
  onClick: PropTypes.func,
  id: PropTypes.string,
  backgroundColor: PropTypes.string,
};
CollapseOut.defaultProps = {
  children: '',
  active: false,
  title: 'Title',
  titlePosition: 'center',
  onClick: () => false,
  id: '',
  backgroundColor: 'primary',
};

export default CollapseOut;
