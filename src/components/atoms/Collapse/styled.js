import styled from 'styled-components';

import { Container as Ctn } from '@components/atoms/Container/styled';

import Arrow from '@assets/images/arrowCheckout.svg';

// const backgroundsColor = {
//   default: () => css`
//     background: #fff;
//   `,
//   primary: () => css`
//     background: #01a3ff;
//   `,
//   collapsesWithdraw: () => css`
//     background: #d6d5d5;
//   `,
// };

export const ContainerWithdraw = styled(Ctn)`
  padding: 30px 15px;
  border-radius: 15px;

  ${(props) => (props.out ? 'padding: 10px 15px; margin-bottom: 10px;' : 'padding: 30px 15px;')};
  ${(props) =>
    props.active && props.out ? `background: #01a3ff; color: #fff;` : 'background: #d6d5d5; color: #bdbdbd;'};

  @media (min-width: 768px) {
    ${(props) => (props.out ? 'padding: 10px 15px;' : 'padding: 30px;')};
  }
`;

export const Container = styled(Ctn)`
  padding: 30px 15px;
  border-radius: 15px;

  ${(props) => (props.out ? 'padding: 10px 15px; margin-bottom: 10px;' : 'padding: 30px 15px;')};
  ${(props) =>
    // eslint-disable-next-line no-nested-ternary
    props.active && props.out
      ? `background: ${props.secondaryColor}; color: #fff;`
      : props.theme === 'Dark'
      ? 'background: #29303A; border: 1px solid #2F3743; color: #fff;'
      : 'background: #FBFBFB; border: 1px solid #EAEAEA; color: #484646;'};

  @media (min-width: 768px) {
    ${(props) => (props.out ? 'padding: 10px 15px;' : 'padding: 30px;')};
  }
`;

export const ContainerTitle = styled.div`
  display: grid;
  text-transform: uppercase;
  grid-template-areas:
    'title'
    'close';
  ${(props) =>
    props.out
      ? 'grid-template-columns: calc(100% - 30px) 25px; font-size: 13px;'
      : 'grid-template-columns: calc(100% - 70px) 30px;'};
  ${(props) => (props.active && !props.out ? 'margin-bottom: 30px;' : null)};

  width: 100%;
`;

export const ContainerContent = styled(Ctn)`
  height: 0;
  overflow: hidden;
  display: none;

  ${(props) => (props.active && props.out ? 'height: auto;' : '')};
  ${(props) => (props.active ? 'overflow: auto; display: flex;' : '')};
  ${(props) =>
    // eslint-disable-next-line no-nested-ternary
    props.out
      ? props.theme === 'Dark'
        ? 'background: #29303A; border-radius: 15px; border: 1px solid #2F3743; padding: 15px 15px; margin: 10px 0px;'
        : 'background: #FBFBFB; border: 1px solid #EAEAEA; border-radius: 15px; padding: 15px 15px; margin: 10px 0px;'
      : ''};
  transition-property: height;
  transition-duration: 500ms;
  transition-timing-function: linear;
`;

export const Title = styled.h3`
  text-align: ${(props) => props.position};
  text-transform: uppercase;

  grid-area: 'title';
  align-self: center;

  width: 100%;

  ${(props) =>
    // eslint-disable-next-line no-nested-ternary
    props.active
      ? props.theme === 'Dark'
        ? 'color: #fff;'
        : 'color: #484646'
      : props.theme === 'Dark'
      ? 'color: #fff;'
      : 'color: #484646'};
`;

export const Close = styled.div`
  grid-area: 'close';

  width: 75%;
  height: 100%;
  cursor: pointer;

  -webkit-mask-image: url(${Arrow});
  mask-image: url(${Arrow});
  mask-repeat: no-repeat;
  mask-size: 20px;

  background-color: #fff;
  background-size: cover;
  background-position: center;
  transform: rotate(180deg);
  transition: transform 300ms linear;

  ${(props) => (props.active ? 'transform: rotate(0);' : '')};
`;

export default Container;
