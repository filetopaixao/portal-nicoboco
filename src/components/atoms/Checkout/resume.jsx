import React, { useState } from 'react';
import PropTypes from 'prop-types';

import ModalVideo from '@components/molecules/Modals/ModalVideo';

import Video from '@assets/images/video.svg';

import * as S from './styled';

const CheckoutResume = ({ className, cupom, data, variant, image }) => {
  const [isModalVideoVisible, setIsModalVideoVisible] = useState(false);
  const linkVideo = 'https://www.youtube.com/embed/LiiYMEWKVnY';
  const handleOpenModalVideo = () => {
    setIsModalVideoVisible(!isModalVideoVisible);
  };

  return (
    <>
      <S.ContainerResume className={`resumeCheckout ${className}`} variant={variant} src={image}>
        <S.ContainerImage>
          <S.ContentImage src={image} />
        </S.ContainerImage>
        <S.ContainerResumeInfo>
          <S.ContainerText>
            <S.TitleResume>{cupom ? 'cupons ativos' : 'carrinhos ativos'}</S.TitleResume>
            <S.TextResume>{data.total || '05'}</S.TextResume>
          </S.ContainerText>

          <S.ContainerText>
            <S.TitleResume>{cupom ? 'vendas com cupons' : 'vendas com carrinhos'}</S.TitleResume>
            <S.TextResume>{data.sales || '5.702'}</S.TextResume>
          </S.ContainerText>

          <S.ContainerText>
            <S.TitleResume>{cupom ? 'ganhos com cupons' : 'ganhos com carrinhos'}</S.TitleResume>
            <S.TextResume>{data.gains || 'R$ 17.835,65'}</S.TextResume>
          </S.ContainerText>
          <S.ContainerButtonVideo>
            <a href="#!" onClick={() => handleOpenModalVideo()}>
              <img src={Video} alt="Assitir" />
            </a>
          </S.ContainerButtonVideo>
        </S.ContainerResumeInfo>
      </S.ContainerResume>
      <ModalVideo
        isModalVideoVisible={isModalVideoVisible}
        setIsModalVideoVisible={setIsModalVideoVisible}
        linkVideo={linkVideo}
      />
    </>
  );
};

CheckoutResume.propTypes = {
  image: PropTypes.string,
  className: PropTypes.string,
  cupom: PropTypes.string,
  data: PropTypes.arrayOf(PropTypes.shape),
  variant: PropTypes.string,
};
CheckoutResume.defaultProps = {
  image: '',
  className: '',
  cupom: '',
  data: [],
  variant: '',
};

export default CheckoutResume;
