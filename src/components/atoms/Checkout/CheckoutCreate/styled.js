import styled from 'styled-components';
import { shade } from 'polished';

export const CheckoutContainer = styled.div`
  padding: 15px 20px;

  border-radius: 15px;
  border: ${(props) => (props.theme === 'Dark' ? '1px solid #2F3743' : '1px solid #EAEAEA')};

  background-color: ${(props) => (props.theme === 'Dark' ? '#202731' : '#FFFFFF')};

  margin: 0px;

  @media (min-width: 600px) {
    padding: 48px;
    min-height: 317px;
  }
`;

export const CheckoutContent = styled.div`
  width: 100%;
`;

export const CheckoutTitle = styled.h2`
  font-size: 16px;
  letter-spacing: 0px;
  font-weight: 600;
  text-align: center;

  width: 100%;
  margin-bottom: 30px;

  opacity: 0.9;
  color: ${(props) => (props.theme === 'Dark' ? '#fff' : '#484646')};

  @media (min-width: 1500px) {
    font-size: 18px;
  }
`;

export const CheckoutInput = styled.input`
  font-size: 14px;

  width: 100%;
  height: 40px;
  padding: 3px 20px;

  /*border: none;*/
  border: ${(props) => (props.theme === 'Dark' ? '1px solid #2F3743' : '1px solid #EAEAEA')};
  border-radius: 15px;
  opacity: 1;

  background: ${(props) => (props.theme === 'Dark' ? '#202731' : '#FFFFFF')};
  color: #47505e;

  @media (min-width: 1500px) {
    font-size: 18px;
  }
`;

export const CheckoutLabel = styled.label`
  font-size: 14px;

  padding-left: 10px;
  margin-bottom: 10px;

  letter-spacing: 0px;
  color: ${(props) => (props.theme === 'Dark' ? '#fff' : '#484646')};
  opacity: 1;

  @media (min-width: 1500px) {
    font-size: 16px;
    margin-bottom: 7px;
  }
`;

export const CheckoutButtonGroup = styled.div`
  margin: 30px 0 0;

  @media (min-width: 768px) {
    margin-top: 49px;
  }
`;

export const CheckoutButton = styled.button`
  width: 100%;

  font-size: 14px;
  text-transform: uppercase;
  text-align: center;

  min-height: 26px;

  cursor: pointer;
  border: none;
  border-radius: 15px;
  padding: 8px 24px;
  position: relative;
  outline: 0;

  color: #fff;
  background: ${(props) => props.primaryColor};
  box-shadow: 0 3px 6px rgba(0, 0, 0, 0.16);

  /*&:hover {
    background: ${shade(0.2, '#ff7f00')};
  }*/

  @media (min-width: 768px) {
    padding: 15px 0;
  }
`;

export const CheckoutText = styled.p`
  font-size: 14px;
  text-transform: uppercase;

  display: block;
  width: 100%;

  color: #fff;

  @media (min-width: 1500px) {
    font-size: 18px;
  }
`;
