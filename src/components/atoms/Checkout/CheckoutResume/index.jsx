import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';

import ModalVideo from '@components/molecules/Modals/ModalVideo';
import Video from '@assets/images/video.svg';

import * as S from './styled';

const CheckoutResume = ({ className, cupom, data }) => {
  const themeStore = useSelector((state) => state.theme);
  const [isModalVideoVisible, setIsModalVideoVisible] = useState(false);
  const linkVideo = 'https://www.youtube.com/embed/LiiYMEWKVnY';
  const handleOpenModalVideo = () => {
    setIsModalVideoVisible(!isModalVideoVisible);
  };

  return (
    <>
      <S.CheckoutResumeContainer
        className={`resumeCheckout ${className}`}
        primaryColor={themeStore.primaryColor}
        secondaryColor={themeStore.secondaryColor}
        tertiaryColor={themeStore.tertiaryColor}
      >
        <S.ResumeContainerImage>{/* <S.ResumeContentImage image={image} /> */}</S.ResumeContainerImage>

        <S.ContainerResumeInfo>
          <S.ContainerText>
            <S.TitleResume>{cupom ? 'cupons ativos' : 'carrinhos ativos'}</S.TitleResume>
            <S.TextResume>{data.total || '05'}</S.TextResume>
          </S.ContainerText>

          <S.ContainerText>
            <S.TitleResume>{cupom ? 'vendas com cupons' : 'vendas com carrinhos'}</S.TitleResume>
            <S.TextResume>{data.sales || '5.702'}</S.TextResume>
          </S.ContainerText>

          <S.ContainerText>
            <S.TitleResume>{cupom ? 'ganhos com cupons' : 'ganhos com carrinhos'}</S.TitleResume>
            <S.TextResume>{data.gains || 'R$ 17.835,65'}</S.TextResume>
          </S.ContainerText>
          <S.ContainerButtonVideo>
            <a href="#!" onClick={() => handleOpenModalVideo()}>
              <img src={Video} alt="Assitir" />
            </a>
          </S.ContainerButtonVideo>
        </S.ContainerResumeInfo>
      </S.CheckoutResumeContainer>

      <ModalVideo
        isModalVideoVisible={isModalVideoVisible}
        setIsModalVideoVisible={setIsModalVideoVisible}
        linkVideo={linkVideo}
      />
    </>
  );
};

CheckoutResume.propTypes = {
  className: PropTypes.string,
  cupom: PropTypes.string,
  data: PropTypes.arrayOf(PropTypes.shape),
};
CheckoutResume.defaultProps = {
  className: '',
  cupom: '',
  data: [],
};

export default CheckoutResume;
