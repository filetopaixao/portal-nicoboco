import styled, { css } from 'styled-components';

import Coupon from '@assets/images/discount-coupon.svg';
import Ecommerce from '@assets/images/ecommerce.svg';

const variants = {
  normal: () => css`
    background-color: #fff;
  `,
  resume: () => css`
    background: transparent linear-gradient(247deg, #7adbba 0%, #0077fe 100%) 0% 0% no-repeat padding-box;
  `,
  cupom: () => css`
    background: transparent linear-gradient(116deg, #83c2f5 0%, #01a3ff 100%) 0% 0% no-repeat padding-box;
  `,
};

const images = {
  coupon: () => css`
    content: url(${Coupon});
  `,
  ecommerce: () => css`
    content: url(${Ecommerce});
  `,
};

export const ResumeContainerImage = styled.div`
  display: flex;
  justify-content: center;
`;

export const ResumeContentImage = styled.div`
  ${(props) => images[props.image]}
  width: 13rem;
  display: flex;

  background-size: contain;
  background-repeat: no-repeat;

  @media (min-width: 1500px) {
    width: 16rem;
  }
`;

export const CheckoutResumeContainer = styled.div`
  display: grid;
  grid-template-columns: 1fr;

  min-height: 317px;

  padding: 15px 20px;
  border-radius: 15px;

  position: relative;
  overflow: hidden;

  /*${(props) => variants[props.variant]};*/
  background: ${(props) =>
    props.tertiaryColor
      ? `transparent linear-gradient(270deg, ${props.tertiaryColor} 0%, ${props.primaryColor} 46%, ${props.secondaryColor} 100%) 0% 0% no-repeat padding-box;`
      : `transparent linear-gradient(270deg, ${props.primaryColor} 0%, ${props.secondaryColor} 100%) 0% 0% no-repeat padding-box;`};

  &.resumeCheckout {
    p {
      text-align: right;
    }
  }

  @media (min-width: 800px) {
    display: grid;
    grid-template-columns: 1fr 1fr;
  }

  @media (min-width: 600px) {
    padding: 48px;
  }

  @media (max-width: 600px) {
    padding: 20px 20px;
  }
`;

export const ContainerResumeInfo = styled.div`
  justify-content: flex-end;
`;

export const ContainerText = styled.div`
  line-height: 25px;

  width: 100%;

  &:not(:last-child) {
    margin-bottom: 15px;
  }
`;

export const TitleResume = styled.h2`
  text-align: right;
  font-size: 14px;
  font-weight: normal;
  letter-spacing: 0px;

  text-transform: uppercase;
  opacity: 1;

  color: #fdfefe;
`;

export const TextResume = styled.p`
  font-size: 25px;
  font-weight: 600;

  text-transform: uppercase;

  color: #fff;
`;

export const ContainerButtonVideo = styled.div`
  position: absolute;
  right: 20px;
  bottom: 20px;
  z-index: 2;

  @media (max-width: 600px) {
    bottom: 5px;
  }

  img {
    width: 35px;
    height: 24px;
  }
`;
