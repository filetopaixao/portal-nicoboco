import styled from 'styled-components';

import BgLayout from '@assets/images/bg_layout.svg';

export const Container = styled.div`
  display: grid;
  grid-template-areas: 'content content';

  height: 100vh;
  overflow: hidden;

  /*background: url(${BgLayout}) no-repeat;*/
  background: ${(props) => (props.theme === 'Dark' ? '#2F3743' : '#FFFFFF')};
  background-position-y: 74px;
  background-size: cover;

  @media (max-width: 1400px) and (min-width: 900px) {
    grid-template-areas: 'sidebar content';
    grid-template-columns: ${(props) => (props.retractable ? '95px 1fr' : '18% 1fr')};

    background-size: 100%;
  }

  @media (min-width: 1400px) {
    grid-template-areas: 'sidebar content';
    grid-template-columns: ${(props) => (props.retractable ? '95px 1fr' : '18% 1fr')};

    background-size: 100%;
  }
`;

export const MainContainer = styled.main`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  grid-area: content;

  background: ${(props) => (props.theme === 'Dark' ? '#111A21' : '#F5F6FA')};

  height: 100vh;
  width: 100%;
  overflow-y: scroll;

  padding: 0 20px;
  padding-bottom: 20px;

  ::-webkit-scrollbar {
    -webkit-appearance: none;
  }
  ::-webkit-scrollbar:vertical {
    width: 14px;
  }
  ::-webkit-scrollbar:horizontal {
    height: 14px;
  }
  ::-webkit-scrollbar-thumb {
    border-radius: 3px;

    background-color: ${(props) => (props.theme === 'Dark' ? '#202731' : '#CECECE')};
  }
  ::-webkit-scrollbar-track {
    border-radius: 10px;
  }

  @media (min-width: 768px) {
    padding: 15px 22px 41px 22px;
  }

  @media (min-width: 1024px) {
    padding-bottom: 0;
  }
`;

export const Sidebar = styled.aside`
  display: none;

  height: 100vh;

  overflow-y: scroll;

  box-shadow: 0 4px 19px rgba(0, 0, 0, 0.5);

  background: ${(props) => (props.theme === 'Dark' ? '#111A21' : '#F5F6FA')};
  color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};

  ::-webkit-scrollbar {
    -webkit-appearance: none;
  }
  ::-webkit-scrollbar:vertical {
    width: 14px;
  }
  ::-webkit-scrollbar:horizontal {
    height: 14px;
  }
  ::-webkit-scrollbar-thumb {
    border-radius: 3px;

    background-color: ${(props) => (props.theme === 'Dark' ? '#202731' : '#CECECE')};
  }
  ::-webkit-scrollbar-track {
    border-radius: 10px;
  }

  @media (min-width: 992px) {
    display: block;
  }
`;

export const HeaderRetractable = styled.div`
  cursor: pointer;
  img {
    // transform: rotate(-90deg);
    height: 18px;
    width: 23px;

    position: absolute;
    top: 10px;
    right: 6px;
  }
  div {
    margin-left: 12px;
  }

  @media (min-width: 992px) {
    margin-top: 20px;
    display: block;
    div {
      margin-left: 0px;
    }
  }
`;

export const SidebarMobile = styled.aside`
  display: ${(props) => (props.retractableMobile ? 'none' : 'block')};
  position: absolute;
  z-index: 99999;
  height: 1000px;

  margin-right: 36px;

  box-shadow: 0 4px 19px rgba(0, 0, 0, 0.5);

  background: ${(props) => (props.theme === 'Dark' ? '#111A21' : '#F5F6FA')};
  color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};

  .logo-buton {
    display: grid;
    grid-template-columns: 1fr min-content;
    align-items: center;
  }

  @media (min-width: 992px) {
    display: none;
  }
`;

export const Header = styled.header`
  display: flex;
  justify-content: space-between;

  margin-bottom: 10px;
  width: 100%;

  @media (min-width: 992px) {
    margin-bottom: 20px;
  }
`;

export const Title = styled.div`
  font-size: 20px;
  font-weight: 500;
  text-transform: uppercase;

  margin-bottom: 20px;

  color: ${(props) => (props.theme === 'Dark' ? '#fff' : '#484646')};

  h3 {
    font-size: 20px;
    text-align: center;

    color: ${(props) => (props.theme === 'Dark' ? '#fff' : '#484646')};

    @media (min-width: 992px) {
      text-align: left;
    }
  }
`;

export const Children = styled.div``;
