export const dataSelect = [
  {
    label: 'Saldo Liberado',
    value: 'saldo',
  },
  {
    label: 'Cartão de Crédito',
    value: 'cartao',
  },
  {
    label: 'Boleto Bancário',
    value: 'boleto',
  },
];

export const numberParcelas = [
  {
    label: '1x Sem Juros',
    value: '1',
  },
  {
    label: '2x Sem Juros',
    value: '2',
  },
  {
    label: '3x Sem Juros',
    value: '3',
  },
  {
    label: '4x Sem Juros',
    value: '4',
  },
  {
    label: '5x Sem Juros',
    value: '5',
  },
  {
    label: '6x Sem Juros',
    value: '6',
  },
  {
    label: '7x',
    value: '7',
  },
  {
    label: '8x',
    value: '8',
  },
  {
    label: '9x',
    value: '9',
  },
  {
    label: '10x',
    value: '10',
  },
  {
    label: '11x',
    value: '11',
  },
  {
    label: '12x',
    value: '12',
  },
];

export const mesCartao = [
  {
    label: '01',
    value: '01',
  },
  {
    label: '02',
    value: '02',
  },
  {
    label: '03',
    value: '03',
  },
  {
    label: '04',
    value: '04',
  },
  {
    label: '05',
    value: '05',
  },
  {
    label: '06',
    value: '06',
  },
  {
    label: '07',
    value: '07',
  },
  {
    label: '08',
    value: '08',
  },
  {
    label: '09',
    value: '09',
  },
  {
    label: '10',
    value: '10',
  },
  {
    label: '11',
    value: '11',
  },
  {
    label: '12',
    value: '12',
  },
];

export const anoCartao = [
  {
    label: '19',
    value: '19',
  },
  {
    label: '20',
    value: '20',
  },
  {
    label: '21',
    value: '21',
  },
  {
    label: '22',
    value: '22',
  },
  {
    label: '22',
    value: '22',
  },
  {
    label: '23',
    value: '23',
  },
  {
    label: '24',
    value: '24',
  },
  {
    label: '25',
    value: '25',
  },
  {
    label: '26',
    value: '26',
  },
  {
    label: '27',
    value: '27',
  },
  {
    label: '28',
    value: '28',
  },
  {
    label: '29',
    value: '29',
  },
  {
    label: '30',
    value: '30',
  },
  {
    label: '31',
    value: '31',
  },
  {
    label: '32',
    value: '32',
  },
  {
    label: '33',
    value: '33',
  },
  {
    label: '34',
    value: '34',
  },
  {
    label: '35',
    value: '35',
  },
  {
    label: '36',
    value: '36',
  },
  {
    label: '37',
    value: '37',
  },
  {
    label: '38',
    value: '38',
  },
  {
    label: '39',
    value: '39',
  },
  {
    label: '40',
    value: '40',
  },
];
