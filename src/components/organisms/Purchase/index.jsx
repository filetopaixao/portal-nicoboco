import React, { useState, useEffect, useCallback } from 'react';
// import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
import Cards from 'react-credit-cards';
import { useFormik } from 'formik';

import { Actions as ProductsActions } from '@redux/ducks/products';

import 'react-credit-cards/es/styles-compiled.css';

import {
  cpfMask,
  emailMask,
  phoneMask,
  cepMask,
  numberCredit,
  numberCVV,
  formatReal,
  removeMask,
} from '@utils/masksForms';

import CollapseOut from '@components/atoms/Collapse/CollapseOut';
import { Input, Label, Select, Text } from '@components/atoms';
import Modal from '@components/molecules/Modals/';

import { getUserAddress, deleteUserAddress, addUserAddress } from '@services/user';
import { dataSelect, anoCartao, mesCartao, numberParcelas } from './creditCardConfig';

import Schema from './Schema';

import * as S from './styled';

const Purchase = () => {
  const themeStore = useSelector((state) => state.theme);
  const [activeBtn1, setActiveBtn1] = useState(true);
  const [activeBtn2, setActiveBtn2] = useState(false);
  const [activeBtn3, setActiveBtn3] = useState(false);
  const [content, setContent] = useState('content1');

  const [progressActiveBtn2, setProgressActiveBtn2] = useState(false);
  const [progressActiveBtn3, setProgressActiveBtn3] = useState(false);

  const [opPac, setOpPac] = useState(false);
  const [sedex, setSedex] = useState(false);
  const [franquia, setFranquia] = useState(false);
  const [outros, setOutros] = useState(false);
  const [optionSendSelected, setOptionSendSelected] = useState('');

  const [isModalVisible, setIsModalVisible] = useState(false);
  const [addresses, setAddresses] = useState([]);
  const [addressesSelected, setAddressesSelected] = useState([]);

  const [payment, setPayment] = useState(false);

  // const [finalized, setFinalized] = useState(false);

  const handleContent1 = useCallback(() => {
    setContent('content1');
    setActiveBtn1(true);
    setActiveBtn2(false);
    setActiveBtn3(false);
  }, []);

  const handleContent2 = useCallback(() => {
    setContent('content2');
    setActiveBtn1(false);
    setActiveBtn2(true);
    setActiveBtn3(false);
  }, []);

  const handleContent3 = useCallback(() => {
    setContent('content3');
    setActiveBtn1(false);
    setActiveBtn2(false);
    setActiveBtn3(true);
  }, []);

  const loadContent = useCallback(() => {
    switch (content) {
      case 'content1':
        return handleContent1();
      case 'content2':
        return handleContent2();
      case 'content3':
        return handleContent3();
      default:
        return setContent('content1');
    }
  }, [handleContent1, handleContent2, handleContent3, content]);

  useEffect(() => {
    loadContent();
  }, [loadContent]);

  const clickButton = (e) => {
    if (e.target.id === 'btn1') {
      setProgressActiveBtn2(false);
      setProgressActiveBtn3(false);
      if (activeBtn1) {
        setActiveBtn1(false);
      } else {
        handleContent1();
      }
    } else if (e.target.id === 'btn2') {
      setProgressActiveBtn2(true);
      setProgressActiveBtn3(true);
      if (activeBtn2) {
        setActiveBtn2(false);
      } else {
        handleContent2();
      }
    } else if (e.target.id === 'btn3') {
      setProgressActiveBtn2(true);
      setProgressActiveBtn3(false);
      if (activeBtn3) {
        setActiveBtn3(false);
      } else {
        handleContent3();
      }
    }
  };

  const { handleBlur, handleChange, values, setFieldValue, errors } = useFormik({
    initialValues: {
      name: '',
      email: '',
      cpf: '',
      phone: '',
      cep: '',
      city: '',
      complement: '',
      country: '',
      neighborhood: '',
      number: '',
      state: '',
      street: '',
      district: '',
      valueSelectPaymentOne: '',
      valueInstallmentsOne: '',
      valueSelectPaymentTwo: '',
      valueInstallmentsTwo: '',
      valueNumberCard: '',
      valueNameCard: '',
      valuesSelectPaymentCardMonth: '',
      valuesSelectPaymentCardAge: '',
      valuesSelectPaymentCardCvv: '',
      valuesSelectPaymentCardInstallment: '',
    },
    validationSchema: Schema,
    validateOnChange: false,
    validateOnBlur: false,
  });

  const dispatch = useDispatch();

  const handleSubmitForm = (e) => {
    e.preventDefault();
    console.log(values, optionSendSelected, addressesSelected);
    dispatch(ProductsActions.handleSubmitSendForm(values, optionSendSelected, addressesSelected, true));
  };

  useEffect(() => {
    getUserAddress()
      .then((response) => {
        const { data } = response.data;

        console.log(data);
        const serialized = data.map((item) => {
          return {
            id: item.id,
            cep: item.cep,
            city: item.city,
            complement: item.complement,
            country: item.country,
            neighborhood: item.neighborhood,
            number: item.number,
            state: item.state,
            street: item.street,
            address: `${item.street} - ${item.neighborhood}, Número ${item.number}`,
            checked: false,
          };
        });
        setAddresses(serialized);
      })
      .catch(() => {
        setAddresses([
          {
            id: 1,
            cep: '77600000',
            city: 'Palmas',
            complement: 'Apartamento',
            country: 'Brasil',
            neighborhood: 'Resid',
            number: '401',
            state: 'Tocantins',
            street: '806',
            address: 'Rua 806 - Res Pal Real, Número 806',
          },
          {
            id: 2,
            cep: '88600000',
            city: 'Paraiso',
            complement: 'Apartamento',
            country: 'Brasil',
            neighborhood: 'Resid',
            number: '401',
            state: 'Tocantins',
            street: '806',
            address: 'Rua 806 - Res Pal Real, Número 806',
          },
          {
            id: 3,
            cep: '99600000',
            city: 'Goiânia',
            complement: 'Apartamento',
            country: 'Brasil',
            neighborhood: 'Resid',
            number: '401',
            state: 'Tocantins',
            street: '806',
            address: 'Rua 806 - Res Pal Real, Número 806',
          },
        ]);
      });
  }, []);

  const handleRemove = async (e) => {
    await deleteUserAddress(e);
    setAddresses(addresses.filter((address) => address.id !== e));
  };

  const handleAddAddress = async () => {
    const add = {
      cep: values.cep,
      city: values.city,
      complement: values.complement,
      country: values.country,
      neighborhood: values.neighborhood,
      street: values.street,
      number: values.number,
      state: values.state,
    };
    const response = await addUserAddress(add);
    console.log(response);
    setAddresses([...addresses, add]);
    setIsModalVisible(!isModalVisible);
  };

  const handleVerify = (string) => {
    if (string.includes('a')) {
      return true;
    }

    if (string.includes('b')) {
      return true;
    }

    if (string.includes('c')) {
      return true;
    }

    if (string.includes('d')) {
      return true;
    }

    if (string.includes('e')) {
      return true;
    }

    if (string.includes('f')) {
      return true;
    }

    if (string.includes('g')) {
      return true;
    }

    if (string.includes('h')) {
      return true;
    }

    if (string.includes('i')) {
      return true;
    }

    if (string.includes('j')) {
      return true;
    }

    if (string.includes('k')) {
      return true;
    }

    if (string.includes('l')) {
      return true;
    }

    if (string.includes('m')) {
      return true;
    }

    if (string.includes('n')) {
      return true;
    }

    if (string.includes('o')) {
      return true;
    }

    if (string.includes('p')) {
      return true;
    }

    if (string.includes('q')) {
      return true;
    }

    if (string.includes('s')) {
      return true;
    }

    if (string.includes('t')) {
      return true;
    }

    if (string.includes('u')) {
      return true;
    }

    if (string.includes('v')) {
      return true;
    }

    if (string.includes('x')) {
      return true;
    }

    if (string.includes('Z')) {
      return true;
    }

    return false;
  };

  const handleInputChange = (id) => {
    const serialized = addresses.map((item) => {
      if (id === item.id) {
        setAddressesSelected({ ...item, checked: true });
        return { ...item, checked: true };
      }
      return { ...item, checked: false };
    });
    setAddresses(serialized);
  };

  const setOpSend = (op) => {
    switch (op) {
      case 'PAC':
        setOptionSendSelected('Pac');
        setOpPac(true);
        setSedex(false);
        setFranquia(false);
        setOutros(false);
        return;
      case 'Sedex':
        setOptionSendSelected('Sedex');
        setOpPac(false);
        setSedex(true);
        setFranquia(false);
        setOutros(false);
        return;
      case 'Franquia':
        setOpPac(false);
        setSedex(false);
        setFranquia(true);
        setOutros(false);
        return;
      case 'Outros':
        setOpPac(false);
        setSedex(false);
        setFranquia(false);
        setOutros(true);
        return;
      default:
        setOpPac(false);
        setOpPac(false);
        setSedex(false);
        setFranquia(false);
    }
  };

  const handleSelectSendOption = useCallback((e) => {
    setOptionSendSelected(e);
  }, []);

  const optionsFranquia = [
    {
      label: 'Palmas',
      value: 'palmas',
    },
  ];

  const optionsOutros = [
    {
      label: 'Jadlog',
      value: 'Jadlog',
    },
  ];

  return (
    <S.Container>
      <S.ProgressBarCheckout theme={themeStore.themeName}>
        <S.BodyProgressBarCheckout className="line" collapse="collapse1" verify="completo" key={Math.random()}>
          <S.Ball
            theme={themeStore.themeName}
            secondaryColor={themeStore.secondaryColor}
            className="ball collapse1"
            verify="completo"
          >
            1
          </S.Ball>
          <S.Bar
            theme={themeStore.themeName}
            secondaryColor={themeStore.secondaryColor}
            label="collapse1"
            verify={progressActiveBtn2 ? 'completo' : 'incompleto'}
            className="bar"
          />
        </S.BodyProgressBarCheckout>

        <S.BodyProgressBarCheckout
          className="line"
          collapse="collapse2"
          verify={progressActiveBtn2 ? 'completo' : 'incompleto'}
          key={Math.random()}
        >
          <S.Ball
            theme={themeStore.themeName}
            secondaryColor={themeStore.secondaryColor}
            className="ball"
            verify={progressActiveBtn2 ? 'completo' : 'incompleto'}
          >
            2
          </S.Ball>
          <S.Bar
            theme={themeStore.themeName}
            secondaryColor={themeStore.secondaryColor}
            label="collapse2"
            verify={progressActiveBtn3 ? 'completo' : 'incompleto'}
            className="bar"
          />
        </S.BodyProgressBarCheckout>

        <S.BodyProgressBarCheckout
          className="line"
          collapse="collapse3"
          verify={progressActiveBtn3 ? 'completo' : 'incompleto'}
          key={Math.random()}
        >
          <S.Ball
            theme={themeStore.themeName}
            secondaryColor={themeStore.secondaryColor}
            className="ball"
            verify={progressActiveBtn3 ? 'completo' : 'incompleto'}
          >
            3
          </S.Ball>
        </S.BodyProgressBarCheckout>
      </S.ProgressBarCheckout>

      <S.Form>
        <S.GridCollapse className="dataPersonalForm">
          <CollapseOut active={activeBtn1} title="Dados Pessoais" onClick={clickButton} id="btn1">
            <div>
              <div>
                <S.ContainerInput className="dataPersonal" theme={themeStore.themeName}>
                  <Label htmlFor="dataPersonal__name">Nome do destinátário:</Label>
                  <Input
                    id="dataPersonal__name"
                    type="text"
                    name="name"
                    variant="formData"
                    placeholder="Nome do destinatário"
                    value={values.name}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    errorMessage={errors.name}
                  />
                </S.ContainerInput>

                <S.ContainerInput className="dataPersonal" theme={themeStore.themeName}>
                  <Label htmlFor="dataPersonal__email">E-mail:</Label>
                  <Input
                    id="dataPersonal__email"
                    type="email"
                    variant="formData"
                    name="email"
                    placeholder="Email => 'mail@xxx.xx'"
                    value={emailMask(values.email)}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    errorMessage={errors.email}
                  />
                </S.ContainerInput>
              </div>

              <div>
                <S.ContainerInput theme={themeStore.themeName}>
                  <Label htmlFor="dataPersonal__cpf">CPF:</Label>
                  <Input
                    id="dataPersonal__cpf"
                    type="text"
                    variant="formData"
                    name="cpf"
                    placeholder="CPF"
                    value={cpfMask(values.cpf)}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    errorMessage={errors.cpf}
                  />
                </S.ContainerInput>

                <S.ContainerInput theme={themeStore.themeName}>
                  <Label htmlFor="dataPersonal__phone">Telefone de contato:</Label>
                  <Input
                    id="dataPersonal__phone"
                    type="text"
                    variant="formData"
                    name="phone"
                    placeholder="Telefone do Destinatário"
                    value={phoneMask(values.phone)}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    errorMessage={errors.phone}
                  />
                </S.ContainerInput>
              </div>
            </div>
          </CollapseOut>
          <CollapseOut active={activeBtn3} title="Informações de entrega" onClick={clickButton} id="btn3">
            <S.ContainerAccount>
              {addresses.map((address) => (
                <S.CardAddress key={address.id} theme={themeStore.themeName}>
                  <div>
                    <input
                      type="checkbox"
                      name={`check${address.id}`}
                      checked={address.checked}
                      onChange={() => handleInputChange(address.id)}
                    />
                  </div>
                  <div className="div-content-card">
                    <p>{`${address.city}-${address.state} (${address.cep})`}</p>
                    <p>{address.address}</p>
                    <p>{address.number ? `Número: ${address.number}` : ''}</p>
                    <p>{address.complement}</p>
                  </div>
                  <button
                    className="remove_address"
                    type="button"
                    onClick={() => handleRemove(address.id)}
                    id={address.id}
                  >
                    X
                  </button>
                </S.CardAddress>
              ))}
              <Text size="sm" variant="normal">
                Tipo de Entrega:
              </Text>

              <S.FormFreteSelect
                primaryColor={themeStore.primaryColor}
                theme={themeStore.themeName}
                className="form_frete_select"
              >
                <p className="frete_pac mb15">Frete PAC grátis nas compras acima de R$ 300,00</p>

                <div className="form_frete_select_content">
                  <div className="full_width border_right">
                    <div className="flex_baseline_space_between content_input_pac">
                      <div className="full_width flex_baseline">
                        <input type="checkbox" name="opPac" checked={opPac} onChange={() => setOpSend('PAC')} />
                        <div>
                          <p>Correios PAC</p>
                          <p>Até 15 dias úteis</p>
                        </div>
                      </div>
                      <div className="width_40_percent">
                        <p>R$ 0,00</p>
                      </div>
                    </div>

                    <div className="flex_baseline_space_between content_input_sedex">
                      <div className="full_width flex_baseline">
                        <input type="checkbox" name="Sedex" checked={sedex} onChange={() => setOpSend('Sedex')} />
                        <div>
                          <p>Correios Sedex</p>
                          <p>Até 3 dias úteis</p>
                        </div>
                      </div>
                      <div className="width_40_percent">
                        <p>R$ 27,00</p>
                      </div>
                    </div>
                  </div>

                  <div className="full_width">
                    <div className="mb20">
                      <div className="flex_baseline_space_between">
                        <div className="full_width flex_baseline">
                          <input
                            type="checkbox"
                            name="Franquia"
                            checked={franquia}
                            onChange={() => setOpSend('Franquia')}
                          />
                          <div>
                            <p>Retirar na franquia</p>
                            <p>Retirada imediata</p>
                          </div>
                        </div>
                        <div className="width_40_percent">
                          <p>R$ 0,00</p>
                        </div>
                      </div>
                      <Select
                        defaultValue="Selecionar Envio"
                        options={optionsFranquia}
                        onChange={(e) => handleSelectSendOption(e.target.value)}
                      />
                    </div>

                    <div>
                      <div className="flex_baseline_space_between">
                        <div className="full_width flex_baseline">
                          <input type="checkbox" name="Outros" checked={outros} onChange={() => setOpSend('Outros')} />
                          <div>
                            <p>Outros</p>
                          </div>
                        </div>
                        <div className="width_40_percent">
                          <p>R$ 0,00</p>
                        </div>
                      </div>
                      <Select
                        defaultValue="Selecionar Envio"
                        options={optionsOutros}
                        onChange={(e) => handleSelectSendOption(e.target.value)}
                      />
                    </div>
                  </div>
                </div>
              </S.FormFreteSelect>

              {/* <S.FormGroup className="btn_group">
                <S.ButtonAccount variant="primary" type="button" onClick={() => {}}>
                  Confirmar
                </S.ButtonAccount>
              </S.FormGroup> */}

              <Modal
                title="Adicionar endereço"
                variant="md"
                isVisible={isModalVisible}
                onClose={() => setIsModalVisible(!isModalVisible)}
              >
                <S.FormGroup>
                  <S.LabelAccount htmlFor="inputCep">CEP:</S.LabelAccount>
                  <S.InputAccount
                    variant="formData"
                    name="cep"
                    type="text"
                    id="inputCep"
                    onChange={handleChange}
                    value={cepMask(values.cep)}
                  />
                  {errors.cep ? <S.ErrorMessage>{errors.cep}</S.ErrorMessage> : null}
                </S.FormGroup>

                <S.FormGroup>
                  <S.LabelAccount htmlFor="inputCity">Cidade:</S.LabelAccount>
                  <S.InputAccount
                    name="city"
                    variant="formData"
                    type="text"
                    id="inputCity"
                    onChange={handleChange}
                    value={values.city}
                  />
                  {errors.city ? <S.ErrorMessage>{errors.city}</S.ErrorMessage> : null}
                </S.FormGroup>

                <S.FormGroup>
                  <S.LabelAccount htmlFor="inputComplement">Aos cuidados / Complemento:</S.LabelAccount>
                  <S.InputAccount
                    name="complement"
                    variant="formData"
                    type="text"
                    id="inputComplement"
                    onChange={handleChange}
                    value={values.complement}
                  />
                  {errors.complement ? <S.ErrorMessage>{errors.complement}</S.ErrorMessage> : null}
                </S.FormGroup>

                <S.FormGroup>
                  <S.LabelAccount htmlFor="inputCountry">País:</S.LabelAccount>
                  <S.InputAccount
                    name="country"
                    variant="formData"
                    type="text"
                    id="inputCountry"
                    onChange={handleChange}
                    value={values.country}
                  />
                  {errors.country ? <S.ErrorMessage>{errors.country}</S.ErrorMessage> : null}
                </S.FormGroup>

                <S.FormGroup>
                  <S.LabelAccount htmlFor="inputNeighborhood">Tipo de Vizinhança:</S.LabelAccount>
                  <S.InputAccount
                    name="neighborhood"
                    variant="formData"
                    type="text"
                    id="inputNeighborhood"
                    onChange={handleChange}
                    value={values.neighborhood}
                  />
                  {errors.neighborhood ? <S.ErrorMessage>{errors.neighborhood}</S.ErrorMessage> : null}
                </S.FormGroup>

                <S.FormGroup>
                  <S.LabelAccount htmlFor="inputStreet">Rua:</S.LabelAccount>
                  <S.InputAccount
                    name="street"
                    variant="formData"
                    type="text"
                    id="inputStreet"
                    onChange={handleChange}
                    value={values.street}
                  />
                  {errors.street ? <S.ErrorMessage>{errors.street}</S.ErrorMessage> : null}
                </S.FormGroup>

                <S.FormGroup>
                  <S.LabelAccount htmlFor="inputDistrict">Bairro:</S.LabelAccount>
                  <S.InputAccount
                    name="district"
                    variant="formData"
                    type="text"
                    id="inputDistrict"
                    onChange={handleChange}
                    value={values.district}
                  />
                  {errors.district ? <S.ErrorMessage>{errors.district}</S.ErrorMessage> : null}
                </S.FormGroup>

                <S.FormGroup>
                  <S.LabelAccount htmlFor="inputNumber">Número:</S.LabelAccount>
                  <S.InputAccount
                    name="number"
                    variant="formData"
                    type="text"
                    id="inputNumber"
                    onChange={handleChange}
                    value={values.number}
                  />
                  {errors.number ? <S.ErrorMessage>{errors.number}</S.ErrorMessage> : null}
                </S.FormGroup>

                <S.FormGroup>
                  <S.LabelAccount htmlFor="inputState">Estado:</S.LabelAccount>
                  <S.InputAccount
                    name="state"
                    variant="formData"
                    type="text"
                    id="inputState"
                    onChange={handleChange}
                    value={values.state}
                  />
                  {errors.state ? <S.ErrorMessage>{errors.state}</S.ErrorMessage> : null}
                </S.FormGroup>

                <S.FormGroup>
                  <S.ButtonAccount variant="primary" type="button" onClick={() => handleAddAddress()}>
                    Salvar Endereço
                  </S.ButtonAccount>
                </S.FormGroup>
              </Modal>
            </S.ContainerAccount>
          </CollapseOut>
          <CollapseOut active={activeBtn2} title="Formas de Pagamento" onClick={clickButton} id="btn2">
            <>
              <S.ContainerPaymentForm theme={themeStore.themeName} className="paymentForms">
                <div className="container__methods__payment">
                  <S.Container className="paymentForms__methods">
                    <div>
                      <Text size="sm" variant="normal" className="paymentForms__title">
                        Qual sua forma de pagamento?
                      </Text>

                      <Select
                        options={dataSelect}
                        variant="formData"
                        className="paymentForms__options"
                        value={values.valueSelectPaymentOne}
                        onChange={(e) => setFieldValue('valueSelectPaymentOne', e.target.value)}
                      />
                    </div>

                    <div>
                      <Text size="sm" variant="normal" className="paymentForms__title">
                        Valor em reais
                      </Text>

                      <S.Container className="paymentForms__methods__valueI">
                        <Input
                          type="text"
                          name="valueInstallmentsOne"
                          id="valueInstallmentsOne"
                          placeholder="R$ 0,00"
                          variant="formData"
                          value={formatReal(values.valueInstallmentsOne)}
                          onChange={handleChange}
                        />
                      </S.Container>
                      {parseFloat(values.valueInstallmentsOne) < 0 ? (
                        <S.ErrorMessage>Não digite valores negativos</S.ErrorMessage>
                      ) : null}
                      {handleVerify(values.valueInstallmentsOne) ? (
                        <S.ErrorMessage>Não digite letras</S.ErrorMessage>
                      ) : null}
                    </div>
                  </S.Container>

                  {payment && (
                    <S.Container className="paymentForms__methods">
                      <div>
                        <Text size="sm" variant="normal" className="paymentForms__title">
                          Qual sua forma de pagamento?
                        </Text>

                        <Select
                          options={dataSelect}
                          className="paymentForms__options"
                          value={values.valueSelectPaymentTwo}
                          onChange={(e) => setFieldValue('valueSelectPaymentTwo', e.target.value)}
                        />
                      </div>

                      <div>
                        <Text size="sm" variant="normal" className="paymentForms__title">
                          Valor em reais
                        </Text>

                        <S.Container className="paymentForms__methods__valueII">
                          <Input
                            type="text"
                            variant="formData"
                            name="valueInstallmentsTwo"
                            id="valueInstallmentsTwo"
                            placeholder="R$ 0,00"
                            value={formatReal(values.valueInstallmentsTwo)}
                            onChange={handleChange}
                          />
                        </S.Container>
                        {parseFloat(values.valueInstallmentsTwo) < 0 ? (
                          <S.ErrorMessage>Não digite valores negativos</S.ErrorMessage>
                        ) : null}
                        {handleVerify(values.valueInstallmentsTwo) ? (
                          <S.ErrorMessage>Não digite letras</S.ErrorMessage>
                        ) : null}
                      </div>
                    </S.Container>
                  )}
                </div>

                {!payment && (
                  <S.AddPaymentForm
                    secondaryColor={themeStore.secondaryColor}
                    size="xs"
                    variant="semiBold"
                    onClick={() => setPayment(!payment)}
                  >
                    Adicionar mais formas a esse pagamento
                  </S.AddPaymentForm>
                )}
                {!!payment && (
                  <S.AddPaymentForm
                    secondaryColor={themeStore.secondaryColor}
                    size="xs"
                    variant="semiBold"
                    onClick={() => setPayment(!payment)}
                  >
                    Remover esta forma de pagamento adicional?
                  </S.AddPaymentForm>
                )}

                {values.valueSelectPaymentOne === 'cartao' || values.valueSelectPaymentTwo === 'cartao' ? (
                  <S.ContainerCreditCard>
                    <div className="full_width">
                      <div className="full_width">
                        <S.Container className="creditCard__number">
                          <Text size="sm" variant="normal">
                            Número do cartão:
                          </Text>
                          <Input
                            className="full_width"
                            type="text"
                            name="valueNumberCard"
                            id="valueNumberCard"
                            placeholder="0000 0000 0000 0000"
                            value={numberCredit(values.valueNumberCard)}
                            onChange={handleChange}
                          />
                        </S.Container>

                        <S.Container className="creditCard__name">
                          <Text size="sm" variant="normal">
                            Nome do titular:
                          </Text>
                          <Input
                            className="full_width"
                            type="text"
                            name="valueNameCard"
                            id="valueNameCard"
                            placeholder="NOME DO TITULAR"
                            value={values.valueNameCard}
                            onChange={handleChange}
                          />
                        </S.Container>

                        <S.ContainerValidateCard className="creditCard__validate">
                          <div className="group_validate_cvv">
                            <div>
                              <Text size="sm" variant="normal">
                                Validade:
                              </Text>
                              <div className="selects_group flex_align_center">
                                <Select
                                  defaultValue="Mês"
                                  options={mesCartao}
                                  value={values.valuesSelectPaymentCardMonth}
                                  onChange={(e) => setFieldValue('valuesSelectPaymentCardMonth', e.target.value)}
                                />
                                <Select
                                  defaultValue="Ano"
                                  options={anoCartao}
                                  value={values.valuesSelectPaymentCardAge}
                                  onChange={(e) => setFieldValue('valuesSelectPaymentCardAge', e.target.value)}
                                />
                              </div>
                            </div>

                            <div className="cvv">
                              <Text size="sm" variant="normal">
                                Código de Segurança:
                              </Text>
                              <Input
                                type="text"
                                name="valuesSelectPaymentCardCvv"
                                id="valuesSelectPaymentCardCvv"
                                placeholder="Digite o CVV"
                                value={numberCVV(values.valuesSelectPaymentCardCvv)}
                                onChange={handleChange}
                              />
                            </div>
                          </div>
                        </S.ContainerValidateCard>
                      </div>

                      <div>
                        <S.Container className="creditCard__card">
                          <Cards
                            cvc={removeMask(values.valuesSelectPaymentCardCvv)}
                            expiry={`${values.valuesSelectPaymentCardMonth}${values.valuesSelectPaymentCardAge}`}
                            focused=""
                            name={values.valueNameCard}
                            number={removeMask(values.valueNumberCard)}
                          />
                        </S.Container>
                      </div>
                    </div>

                    <S.Container>
                      <Text size="sm" variant="normal">
                        Número de parcelas:
                      </Text>
                      <Select
                        defaultValue="Escolhar o número de parcelas"
                        options={numberParcelas}
                        value={values.valuesSelectPaymentCardInstallment}
                        onChange={(e) => setFieldValue('valuesSelectPaymentCardInstallment', e.target.value)}
                      />
                    </S.Container>
                  </S.ContainerCreditCard>
                ) : null}
              </S.ContainerPaymentForm>
            </>
          </CollapseOut>
        </S.GridCollapse>
        <S.FormGroup
          className={`btn_group_submit ${
            parseFloat(removeMask(values.valueInstallmentsOne)) > 0 &&
            parseFloat(removeMask(values.valuesSelectPaymentCardCvv)) > 0
              ? 'success'
              : 'disabled'
          }`}
        >
          <S.ButtonAccount
            variant={
              (parseFloat(removeMask(values.valuesSelectPaymentCardCvv)) > 0 &&
                parseFloat(removeMask(values.valueInstallmentsOne)) > 0) ||
              (values.valueSelectPaymentOne === 'cartao' && parseFloat(removeMask(values.valueInstallmentsOne)) > 0) ||
              (values.valueSelectPaymentTwo === 'cartao' && parseFloat(removeMask(values.valueInstallmentsTow)) > 0) ||
              (values.valueSelectPaymentOne === 'boleto' && parseFloat(removeMask(values.valueInstallmentsOne)) > 0) ||
              (values.valueSelectPaymentTwo === 'boleto' && parseFloat(removeMask(values.valueInstallmentsTow)) > 0) ||
              (values.valueSelectPaymentOne === 'saldo' && parseFloat(removeMask(values.valueInstallmentsOne)) > 0) ||
              (values.valueSelectPaymentTwo === 'saldo' && parseFloat(removeMask(values.valueInstallmentsTow)) > 0)
                ? 'success'
                : 'disabled'
            }
            type="button"
            onClick={(e) => handleSubmitForm(e)}
            className="finish-order"
          >
            Finalizar Pedido
          </S.ButtonAccount>
        </S.FormGroup>
      </S.Form>
    </S.Container>
  );
};

Purchase.propTypes = {};

Purchase.defaultProps = {};

export default Purchase;
