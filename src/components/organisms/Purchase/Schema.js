import * as Yup from 'yup';

import { validCpf, validationPhone } from '@utils/validators';

const Schema = Yup.object().shape({
  name: Yup.string().required('campo obrigatório'),
  // .test({
  //   name: 'validateName',
  //   test(value) {
  //     const onlyText = /^[a-zA-Z ]*$/.gitest(value);

  //     if (!onlyText) {
  //       return this.createError({
  //         message: 'não pode conter números',
  //         path: 'name',
  //       });
  //     }

  //     if (!validName(value)) {
  //       return this.createError({
  //         message: 'deve ter sobrenome',
  //         path: 'name',
  //       });
  //     }

  //     return true;
  //   },
  // }),
  email: Yup.string().email('email inválido').required('campo obrigatório'),
  // .test({
  //   name: 'validateEmail',
  //   test(value) {
  //     const onlyText = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/gi.gitest(
  //       value
  //     );

  //     if (!onlyText) {
  //       return this.createError({
  //         message: 'digite o email corretamente',
  //         path: 'email',
  //       });
  //     }
  //     return true;
  //   },
  // }),
  cpf: Yup.string()
    .max(14)
    .required('campo obrigatório')
    .test('testCpf', 'campo obrigatório', (value) => validCpf(value)),
  whats: Yup.string()
    .max(19)
    .required('campo obrigatório')
    .test('testPhone', 'campo obrigatório', (value) => validationPhone(value)),
  phone: Yup.string()
    .max(18)
    .required('campo obrigatório')
    .test('testPhone', 'campo obrigatório', (value) => validationPhone(value)),
  cep: Yup.string().max(18).required('campo obrigatório'),
  street: Yup.string().required('campo obrigatório'),
  complement: Yup.string().required('campo obrigatório'),
  country: Yup.string().required('campo obrigatório'),
  neighborhood: Yup.string().required('campo obrigatório'),
  district: Yup.string().required('campo obrigatório'),
  number: Yup.string().required('campo obrigatório'),
  city: Yup.string().required('campo obrigatório'),
  state: Yup.string().required('campo obrigatório'),
  valueSelectPaymentOne: Yup.string().required('campo obrigatório'),
  valueInstallmentsOne: Yup.string().required('campo obrigatório'),
  valueSelectPaymentTwo: Yup.string().required('campo obrigatório'),
  valueInstallmentsTwo: Yup.string().required('campo obrigatório'),
  valueNumberCard: Yup.string().required('campo obrigatório'),
  valueNameCard: Yup.string().required('campo obrigatório'),
  // .test({
  //   name: 'validateName',
  //   test(value) {
  //     const onlyText = /^[a-zA-Z ]*$/.gitest(value);

  //     if (!onlyText) {
  //       return this.createError({
  //         message: 'não pode conter números',
  //         path: 'name',
  //       });
  //     }

  //     if (!validName(value)) {
  //       return this.createError({
  //         message: 'deve ter sobrenome',
  //         path: 'name',
  //       });
  //     }

  //     return true;
  //   },
  // }),
  valuesSelectPaymentCardMonth: Yup.string().required('campo obrigatório'),
  valuesSelectPaymentCardAge: Yup.string().required('campo obrigatório'),
  valuesSelectPaymentCardCvv: Yup.string().required('campo obrigatório').max(4),
  valuesSelectPaymentCardInstallment: Yup.string().required('campo obrigatório'),
});

export default Schema;
