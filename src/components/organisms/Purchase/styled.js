import styled from 'styled-components';
import { Input, Button, Text, Label as lb, Container as Ct } from '@components/atoms';

import IconClose from '@assets/images/close.svg';

export const Container = styled(Ct)`
  width: 100%;
  display: block;

  .flex_align_center {
    display: flex;
    justify-items: center;
    align-items: center;
  }

  .full_width {
    width: 100%;
  }
`;

export const GridCollapse = styled.div`
  display: block;

  width: 100%;

  > div {
    width: 100%;
  }
`;

export const ContainerInput = styled(Container)`
  display: block;

  height: auto;

  &:not(:last-child) {
    margin-bottom: 10px;
  }

  input {
    border: ${(props) => (props.theme === 'Dark' ? '1px solid #2F3743' : '1px solid #EAEAEA')};

    background: ${(props) => (props.theme === 'Dark' ? '#202731' : '#FFFFFF')};

    color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};

    ::placeholder {
      color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
    }
  }
`;

export const Form = styled.form`
  width: 100%;

  > div {
    > div:nth-child(2n + 1) {
      padding: 25px;
    }

    > div:nth-child(2n + 2) {
      @media (min-width: 600px) {
        padding: 25px;
      }
    }
  }

  .dataPersonalForm {
    > div:nth-child(2) {
      > div {
        @media (min-width: 992px) {
          display: grid;
          grid-template-columns: 1fr 1fr;
          column-gap: 30px;
        }
        width: 100%;
      }
    }
  }
`;

export const ContainerAccount = styled(Container)``;

export const CardAddress = styled.div`
  display: grid;
  grid-template-columns: 1fr 1000fr 1fr;
  align-items: center;

  width: 100%;
  height: auto;

  padding-left: 3px;
  margin-bottom: 10px;

  border-radius: 10px;

  border: ${(props) => (props.theme === 'Dark' ? '1px solid #2F3743' : '1px solid #EAEAEA')};

  background: ${(props) => (props.theme === 'Dark' ? '#202731' : '#FFFFFF')};

  color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};

  overflow: hidden;

  overflow: hidden;

  input[type='checkbox'] {
    border: ${(props) => (props.theme === 'Dark' ? '1px solid #2F3743' : '1px solid #EAEAEA')};

    background: ${(props) => (props.theme === 'Dark' ? '#29303A' : '#FBFBFB')};

    color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
  }

  .div-content-card {
    font-size: 13px;

    width: 100%;

    padding: 10px 20px;

    color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
  }

  .remove_address {
    width: 20px;
    height: 100%;

    border: none;

    background: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#CECECE')};
    color: ${(props) => (props.theme === 'Dark' ? '#47505E' : '#82868D')};

    cursor: pointer;
  }
`;

export const RemoveAddress = styled.div`
  width: 20px;

  background: url(${IconClose}) center center no-repeat #bdbdbd;

  cursor: pointer;
`;

export const ContainerButton = styled(Container)`
  display: flex;

  width: 100%;
  @media (min-width: 992px) {
    justify-content: flex-end;
  }
`;

export const ButtonAccount = styled(Button)`
  font-size: 12px;

  width: 100%;

  margin-bottom: 10px;
  padding: 10px 0px;

  border-radius: 8px;
  @media (min-width: 992px) {
    ${(props) => (props.size === '100%' ? 'width: 100%;' : 'width: 200px; position: relative; float: right;')}
  }
`;

export const FormGroup = styled.div`
  margin-bottom: 15px;

  &.btn_group {
    display: flex;
    justify-content: flex-end;

    width: 100%;

    button {
      width: 100%;
      @media (min-width: 500px) {
        width: 230px;
      }
    }
  }

  &.btn_group_submit {
    display: flex;

    width: 100%;
    height: 40px;

    button {
      width: 100%;
      background: #2fdf46 !important;
      @media (min-width: 500px) {
        width: 90%;
        margin: 0 auto;
      }
    }
  }
`;

export const FormAccount = styled.form`
  flex: 100%;

  margin-top: 15px;

  input[type='radio'] {
    display: flex;

    width: 20px;

    margin-right: 10px;
  }
  .form-group {
    margin-bottom: 10px;
  }

  @media (min-width: 992px) {
    flex: 55%;
    ${(props) => (props.type === 'changePass' ? 'padding: 0 30px;' : '')}
  }
`;

export const LabelAccount = styled(lb)`
  font-size: 15px;

  margin-bottom: 5px;
`;

export const InputAccount = styled(Input)``;

export const ErrorMessage = styled.div`
  font-size: 12px;

  color: red;
`;

export const AddPaymentForm = styled(Text)`
  margin-bottom: 10px;
  padding-left: 20px;
  position: relative;
  cursor: pointer;

  color: ${(props) => props.secondaryColor};
  font-size: 13px;

  &::after,
  &::before {
    clear: both;
    display: block;
    visibility: visible;

    position: absolute;
    left: 1px;
    top: 1px;

    width: 12px;
    height: 12px;
  }

  &::before {
    content: '';
    border-radius: 3px;

    background: ${(props) => props.secondaryColor};
  }
  &::after {
    font-size: 12px;
    content: '+';

    left: 2.5px;
    top: -0.6px;

    color: #fff;
  }
`;

export const ContainerCreditCard = styled(Container)`
  padding: 14px 16px;
  border-radius: 15px;

  border-style: solid;
  border-width: 1px;

  background: #fbfbfb;
  border-color: #eaeaea;

  > div:first-child {
    @media (min-width: 1500px) {
      display: grid;
      grid-template-columns: 1fr 1fr;
      grid-column-gap: 10px;
    }
  }

  .creditCard__name {
    input {
      text-transform: uppercase;
    }
  }

  .creditCard__name,
  .creditCard__number {
    > div {
      width: 100%;
    }
  }

  .creditCard__card {
    margin-top: 10px;
    .rccs__cvc__front {
      visibility: visible;
      opacity: 0.5;
    }
  }

  > div:not(:last-child) {
    margin-bottom: 10px;
  }

  .rccs__card--unknown > div {
    @media (max-width: 600px) {
      width: 85%;
    }
  }
`;

export const ContainerPaymentForm = styled(Container)`
  .paymentForms__title,
  .paymentForms__options {
    margin-bottom: 10px;
  }

  select {
    cursor: pointer;
    border: ${(props) => (props.theme === 'Dark' ? '1px solid #2F3743' : '1px solid #EAEAEA')};

    background: ${(props) => (props.theme === 'Dark' ? '#202731' : '#FFFFFF')};

    color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
  }

  input {
    border: ${(props) => (props.theme === 'Dark' ? '1px solid #2F3743' : '1px solid #EAEAEA')};

    background: ${(props) => (props.theme === 'Dark' ? '#202731' : '#FFFFFF')};

    color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};
  }

  > div {
    width: 100%;
    > div.paymentForms__methods {
      display: flex;
      align-items: center;
      justify-content: space-between;
      width: 100%;
      > div {
        display: block;
        @media (max-width: 600px) {
          width: 100%;
        }
      }

      > div:first-child {
        margin-top: 10px;
      }
    }
  }
`;

export const ContainerValidateCard = styled(Container)`
  justify-content: space-between;

  select {
    width: 70px;

    padding: inherit;
    padding-left: 8px;
  }

  .selects_group {
    select:first-child {
      margin-right: 5px;
    }
  }

  .group_validate_cvv {
    display: flex;
    justify-items: center;
    align-items: center;

    width: 100%;

    @media (max-width: 700px) {
      display: block;
    }
  }

  .cvv {
    min-width: 160px;
    width: 100%;
  }

  /* .creditCard__validate {
    &__cvv {
      width: 100%;
      margin-top: 10px;
    }
  } */
`;

export const Label = styled(lb)`
  font-size: 14px;
  font-weight: 400;

  @media (min-width: 768px) {
    font-size: 16px;
  }
`;

export const ProgressBarCheckout = styled.div`
  display: flex;
  justify-self: center;
  justify-items: center;
  align-items: center;

  height: 100px;
  width: 100%;

  margin-bottom: 15px;
  padding: 15px;

  border-radius: 10px;

  border: ${(props) => (props.theme === 'Dark' ? '1px solid #2F3743' : '1px solid #EAEAEA')};

  background: ${(props) => (props.theme === 'Dark' ? '#202731' : '#FFFFFF')};

  color: ${(props) => (props.theme === 'Dark' ? '#fff' : '#484646')};

  > div {
    width: 100%;
  }

  > div:nth-child(3) {
    width: auto;
  }
`;

export const BodyProgressBarCheckout = styled.div`
  display: grid;
  grid-template-columns: 1fr 100%;
  justify-self: center;
  justify-items: center;
  align-items: center;
`;

export const Ball = styled.div`
  text-align: center;

  display: flex;
  justify-content: center;
  justify-items: center;
  align-items: center;

  height: 34px;
  width: 34px;

  padding: 1%;

  border-radius: 50%;

  //background: #f6f6f6 no-repeat;
  font-weight: bold;

  &.collapse1 {
    background: ${(props) => props.secondaryColor};
  }

  ${(props) => {
    if (props.verify === 'completo') return `background: ${props.secondaryColor}; color: #fff`;
    if (props.theme === 'Dark') return 'background: #47505E; color: #202731';
    return 'background: #CECECE; color: #FFFFFF';
  }}
`;

export const Bar = styled.div`
  height: 2px;
  width: 100%;

  ${(props) => {
    if (props.verify === 'completo') return `background: ${props.secondaryColor}`;
    if (props.theme === 'Dark') return 'background: #47505E';
    return 'background: #CECECE';
  }}
`;

export const FormFreteSelect = styled.div`
  font-size: 14px;

  margin: 15px 0;
  padding: 20px;

  border-radius: 8px;

  border: ${(props) => (props.theme === 'Dark' ? '1px solid #2F3743' : '1px solid #EAEAEA')};

  background: ${(props) => (props.theme === 'Dark' ? '#202731' : '#FFFFFF')};

  color: ${(props) => (props.theme === 'Dark' ? '#979EA8' : '#82868D')};

  input {
    margin: 0 5px;
  }

  @media (min-width: 1400px) {
    font-size: 16px;
  }

  @media (min-width: 500px) {
    .border_right {
      border-right-color: #9c9a9a94;
      border-right: groove;
      margin-right: 8px;
      padding-right: 8px;
      border-right-width: 1px;
    }
  }

  .mb50 {
    margin-bottom: 60px;
  }

  .content_input_pac {
    margin-bottom: 15px;
    @media (min-width: 500px) {
      margin-bottom: 60px;
    }
  }

  .content_input_sedex {
    margin-bottom: 20px;
    @media (min-width: 500px) {
      margin-bottom: 0px;
    }
  }

  .frete_pac {
    color: ${(props) => props.primaryColor};
  }

  .mb15 {
    margin-bottom: 15px;
  }

  .mb20 {
    margin-bottom: 20px;
  }

  .flex_baseline_space_between {
    display: flex;
    align-items: baseline;
    justify-content: space-between;
  }

  .flex_baseline {
    display: flex;
    align-items: baseline;
  }

  .width_40_percent {
    width: 40%;
  }

  .flex_center {
    display: flex;
    align-items: center;
  }

  .full_width {
    width: 100%;
  }

  .flex_space_between {
    display: flex;
    justify-content: space-between;
    align-items: center;
  }

  > div.form_frete_select_content {
    display: block;

    @media (min-width: 500px) {
      display: flex;
      align-items: center;
    }
  }
`;
