import React from 'react';
import PropTypes from 'prop-types';

import { Button, Checkbox, Text } from '@components/atoms';

import * as S from './styled';

const Address = ({ datAddress }) => (
  <>
    <S.ContainerAddress>
      {datAddress.map((address) => (
        <S.ContainerInputAddress className="paymentAddress__address">
          <div className="container__checkbox">
            <Checkbox text="" isChecked={false} onChange={() => false} />
          </div>

          <div className="container__address">
            <Text size="sm" variant="normal">
              {`${address.state} ${address.cep}`}
            </Text>

            <Text size="sm" variant="normal">
              {address.address}
            </Text>

            <Text size="sm" variant="normal">
              {address.neighborhood}
            </Text>

            <Text size="sm" variant="normal">
              {address.complement}
            </Text>
          </div>

          <S.ContainerExclude>x</S.ContainerExclude>
        </S.ContainerInputAddress>
      ))}
    </S.ContainerAddress>

    <S.ButtonGroup className="center">
      <Button variant="secondary">Adicionar endereço</Button>
    </S.ButtonGroup>
  </>
);

Address.propTypes = {
  datAddress: PropTypes.arrayOf(
    PropTypes.shape({
      state: PropTypes.string,
      cep: PropTypes.string,
      address: PropTypes.string,
      neighborhood: PropTypes.string,
      complement: PropTypes.string,
    })
  ),
};
Address.defaultProps = {
  datAddress: [],
};

export default Address;
