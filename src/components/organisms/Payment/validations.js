import * as Yup from 'yup';

import { validName, validCpf, validationPhone } from '@utils/validators';

export const DataPersonalValidation = Yup.object({
  name: Yup.string()
    .required('campo obrigatório')
    .test({
      name: 'validateName',
      test(value) {
        const onlyText = /^[a-zA-Z ]*$/.gitest(value);

        if (!onlyText) {
          return this.createError({
            message: 'não pode conter números',
            path: 'name',
          });
        }

        if (!validName(value)) {
          return this.createError({
            message: 'deve ter sobrenome',
            path: 'name',
          });
        }

        return true;
      },
    }),
  cpf: Yup.string()
    .max(14)
    .required('campo obrigatório')
    .test('testCpf', 'campo obrigatório', (value) => validCpf(value)),
  email: Yup.string().email('email inválido').required('campo obrigatório'),
  whats: Yup.string()
    .max(19)
    .required('campo obrigatório')
    .test('testPhone', 'campo obrigatório', (value) => validationPhone(value)),
  phone: Yup.string()
    .max(18)
    .required('campo obrigatório')
    .test('testPhone', 'campo obrigatório', (value) => validationPhone(value)),
  cep: Yup.string().max(18).required('campo obrigatório'),
  birthday: Yup.string().max(10).required('campo obrigatório'),
  genrer: Yup.string().required('campo obrigatório'),
});

export const AddressValidation = {};
