import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Cards from 'react-credit-cards';

import { Container, Input, Select, Text } from '@components/atoms';

import * as S from './styled';
import 'react-credit-cards/es/styles-compiled.css';

const dataSelect = [
  {
    label: 'Saldo Liberado',
    value: 'saldo',
  },
  {
    label: 'Cartão de Crédito',
    value: 'cartao',
  },
  {
    label: 'Boleto Bancário',
    value: 'boleto',
  },
];

const CreditCard = ({
  valueSelectPaymentOne,
  valueInstallmentsOne,
  valueSelectPaymentTwo,
  valueInstallmentsTwo,
  valueNumberCard,
  valuenameCard,
  valuesSelectPaymentCardMonth,
  valuesSelectPaymentCardAge,
  valuesSelectPaymentCardCvv,
  valuesSelectPaymentCardInstallment,
  onChange,
}) => {
  const [payment, setPayment] = useState(false);

  return (
    <>
      <S.ContainerPaymentForm className="paymentForms">
        <Container className="paymentForms__methods">
          <Text size="sm" variant="normal" className="paymentForms__title">
            Qual sua forma de pagamento?
          </Text>

          <Select
            options={dataSelect}
            className="paymentForms__options"
            value={valueSelectPaymentOne}
            onChange={onChange}
          />

          <Container className="paymentForms__methods__valueI">
            <S.Label htmlFor="valueI">Valor em reais</S.Label>
            <Input type="string" name="valueI" placeholder="R$ 0,00" value={valueInstallmentsOne} onChange={onChange} />
          </Container>
        </Container>

        {!payment && (
          <S.AddPaymentForm size="xs" variant="semiBold" onClick={() => setPayment(!payment)}>
            Adicionar mais formas a esse pagamento
          </S.AddPaymentForm>
        )}

        {payment && (
          <Container className="paymentForms__methods">
            <Text size="sm" variant="normal" className="paymentForms__title">
              Qual sua forma de pagamento?
            </Text>

            <Select
              options={dataSelect}
              className="paymentForms__options"
              onChange={onChange}
              value={valueSelectPaymentTwo}
            />

            <Container className="paymentForms__methods__valueII">
              <S.Label htmlFor="valueII">Valor em reais</S.Label>
              <Input
                type="string"
                name="valueII"
                placeholder="R$ 0,00"
                value={valueInstallmentsTwo}
                onChange={onChange}
              />
            </Container>
          </Container>
        )}

        <S.ContainerCreditCard>
          <Container className="creditCard__name">
            <Text size="sm" variant="normal">
              Número do cartão:
            </Text>
            <Input
              type="number"
              name="credtiCard"
              placeholder="0000 0000 0000 0000"
              value={valueNumberCard}
              onChange={onChange}
            />
          </Container>

          <Container className="creditCard__number">
            <Text size="sm" variant="normal">
              Nome do titular:
            </Text>
            <Input
              type="string"
              name="credtiCard"
              placeholder="0000 0000 0000 0000"
              value={valuenameCard}
              onChange={onChange}
            />
          </Container>

          <S.ContainerValidateCard className="creditCard__validate">
            <Container className="creditCard__validate__month">
              <Text size="sm" variant="normal">
                Mês:
              </Text>
              <Select defaultValue="Mês" options={[]} onChange={onChange} value={valuesSelectPaymentCardMonth} />
            </Container>

            <Container className="creditCard__validate__age">
              <Text size="sm" variant="normal">
                Ano:
              </Text>
              <Select defaultValue="Ano" options={[]} onChange={onChange} value={valuesSelectPaymentCardAge} />
            </Container>

            <Container className="creditCard__validate__cvv">
              <Text size="sm" variant="normal">
                CVV:
              </Text>
              <Input
                type="string"
                name="credtiCard"
                placeholder="000"
                value={valuesSelectPaymentCardCvv}
                onChange={onChange}
              />
            </Container>
          </S.ContainerValidateCard>

          <Container>
            <Text size="sm" variant="normal">
              Número de parcelas:
            </Text>
            <Select
              defaultValue="Escolhar o número de parcelas"
              options={[]}
              onChange={onChange}
              value={valuesSelectPaymentCardInstallment}
            />
          </Container>

          <Container className="creditCard__card">
            <Cards cvc="" expiry="" focused="" name="" number="" />
          </Container>
        </S.ContainerCreditCard>
      </S.ContainerPaymentForm>
    </>
  );
};

CreditCard.propTypes = {
  valueSelectPaymentOne: PropTypes.string,
  valueInstallmentsOne: PropTypes.string,
  valueSelectPaymentTwo: PropTypes.string,
  valueInstallmentsTwo: PropTypes.string,
  valueNumberCard: PropTypes.string,
  valuenameCard: PropTypes.string,
  valuesSelectPaymentCardMonth: PropTypes.string,
  valuesSelectPaymentCardAge: PropTypes.string,
  valuesSelectPaymentCardCvv: PropTypes.string,
  valuesSelectPaymentCardInstallment: PropTypes.string,
  onChange: PropTypes.func,
};
CreditCard.defaultProps = {
  valueSelectPaymentOne: '',
  valueInstallmentsOne: '',
  valueSelectPaymentTwo: '',
  valueInstallmentsTwo: '',
  valueNumberCard: '',
  valuenameCard: '',
  valuesSelectPaymentCardMonth: '',
  valuesSelectPaymentCardAge: '',
  valuesSelectPaymentCardCvv: '',
  valuesSelectPaymentCardInstallment: '',
  onChange: () => false,
};

export default CreditCard;
