export const toolsUser = {
  entrepreneurs: () => [
    { description: 'E-COMMERCE', variant: 'infoDark', isComing: false },
    { description: 'LANDING PAGE', variant: '', isComing: true },
    { description: 'CARRINHO DINÂMICO', variant: 'danger', isComing: false },
    { description: 'CUPOM DE DESCONTO', variant: 'warning', isComing: false },
  ],
};

export const spacesUser = {
  entrepreneurs: () => [
    { icon: 'bank', description: 'BANK', variant: '', isComing: true },
    { icon: 'shopping', description: 'SHOPPING', variant: '', isComing: true },
    { icon: 'marketing', description: 'MARKETING', variant: 'marketing', isComing: false },
    { icon: 'university', description: 'UNIVERSITY', variant: 'university', isComing: false },
  ],
};

export const userHomeToolsConfig = (op) => {
  switch (op) {
    case 'Meu Ecommerce':
      return 'info';
    case 'Landing Page':
      return '';
    case 'Carrinho Dinâmico':
      return 'danger';
    case 'Cupom de Desconto':
      return 'warning';
    case 'Links de Apoio':
      return 'warning';
    case 'Adquirir Produtos':
      return 'danger';
    case 'Meus Clientes':
      return '';
    case 'Parceiros':
      return '';
    case 'Empreendedores':
      return 'info';
    default:
      return '';
  }
};

export const verifyIsComing = (op) => {
  switch (op) {
    case 'Meu Ecommerce':
      return false;
    case 'Landing Page':
      return true;
    case 'Carrinho Dinâmico':
      return false;
    case 'Cupom de Desconto':
      return false;
    case 'Links de Apoio':
      return false;
    case 'Adquirir Produtos':
      return false;
    case 'Meus Clientes':
      return true;
    case 'Parceiros':
      return true;
    case 'Empreendedores':
      return false;
    default:
      return true;
  }
};
